This project represents a game "engine" on top of which multiple different game programs
may be built.  A game program is built on top of the engine by providing extensions of the
engine's types, as well as overrides of extension points which are marked with an
@ExtensionPoint annotation.

This project's code is provided under an open-source license.
     
This project's codebase employs a rudimentary form of strategic Domain Driven Design by
isolating its code for various game-related tasks into bounded contexts which have no
external dependencies on one another.  This organization makes it easier to reason about
the code contained in any singular such context.  Most of the code which was leftover
after forming these contexts was itself placed into an unbounded "gameplay" context which
makes direct calls as necessary into the other contexts.

This project's JAR file is built using the Gradle build system.  Run the Gradle "build"
task to build the JAR, which should then be consumed as an external library by the game
program which is leveraging this engine.
