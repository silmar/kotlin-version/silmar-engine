##### Simplifying assumptions made in the game's codebase

- Monsters which can see in the dark can also see in the light or any combination of light
 and darkness, just as well.  Trying to get more realistic than that isn't worth the
  greatly increased code complexity.  
  - I think it's a rather standard presumption 
  in these kinds of games that monsters in their native dungeons can see (or, sense) no 
  matter what, perhaps through extra-visual sensory perceptions of whatever sort.  Some 
  users could even think it was a bug if lit monsters started moving the wrong 
  direction when the player is in the dark.
  - The player's werewolf night-vision power is perhaps a little more nuanced, in that
   it is spoiled when the player's location is lit