package silmarengine

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

/**
 * Use this to schedule future tasks which have no dependency on ordering with any other
 * tasks (other than that implied by their relative scheduling, if the other task is also
 * scheduled).  Meaning, this is meant for tasks which change what is being presented in
 * the UI, but don't affect the state of the running game or whatever other portion of the
 * program is currently running.
 *
 * Note that whenever this scheduler fails to run a scheduled task, it is likely
 * because the code scheduling the task is itself being run as a task by this same
 * scheduler.  This scheduler uses only one thread, so every task has to finish
 * before the next one can start.
 */
val scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
