package silmarengine.events

/**
 * Reports "events" to a set of listeners by calling methods on their interface, L.
 */
interface ReporterMixin<L> {
    var listeners: HashSet<L>

    fun addListener(listener: L) {
        listeners.add(listener)
    }

    fun removeListener(listener: L) {
        listeners.remove(listener)
    }

    fun reportToListeners(report: (listener: L) -> Unit) {
        listeners.forEach(report)
    }
}