package silmarengine

import silmarengine.contexts.gameplay.game.extensions.loadGameFromFile
import silmarengine.gui.dialogs.MessageDialog
import javax.swing.JFrame

fun resumeGame(path: String, fileName: String, parentFrame: JFrame) {
    // show a small loading-game dialog
    val dialog = MessageDialog(parentFrame, "Loading game...", false)
    dialog.isModal = false
    dialog.isVisible = true

    // load the game
    val game = loadGameFromFile(path, fileName)

    // hide the loading-game dialog
    dialog.isVisible = false

    // if the game can't be loaded
    if (game == null) {
        MessageDialog.show(parentFrame, "Error loading saved game.")
        parentFrame.isVisible = true
        return
    }

    val player = game.player
    createPlayUI(player)

    // inform the player that the area it is on is loading
    player.reportToListeners { it.onAreaLoading() }

    // inform the player that it is at its current area
    player.reportToListeners { it.onArrivedInArea() }

    parentFrame.isVisible = false

    game.processTurns()

    parentFrame.isVisible = true
}
