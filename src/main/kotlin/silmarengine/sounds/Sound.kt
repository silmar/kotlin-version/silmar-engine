package silmarengine.sounds

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.io.IOException
import java.util.*
import javax.sound.sampled.*
import kotlin.math.ln
import kotlin.math.max
import kotlin.math.min

/**
 * A sound effect to be played while the game is proceeding.
 */
class Sound(
    /**
     * The name of the file that stores the sound data for this sound.
     */
    private val fileName: String?,
    /**
     * The range (in tiles) this sound will travel.
     */
    range: TileDistance = defaultTileRange,
    /**
     * Whether this sound causes monsters who hear it to automatically activate.
     */
    val activatesMonsters: Boolean = true
) {

    /**
     * How many pixels this sound will travel when issued.
     */
    val range = PixelDistance(range.distance * tileWidth)

    /**
     * A list of identical Clip's (see Clip inner class) of this sound.  Having more than
     * one Clip lets us play them simultaneously.
     */
    private val clips = ArrayList<ClipWrapper>()

    constructor(fileName: String, activatesMonsters: Boolean) : this(fileName,
        defaultTileRange, activatesMonsters)

    override fun toString(): String = fileName ?: "no filename given"

    /**
     * Instructs this sound to play once, with a volume determined according to the
     * given distance.
     *
     * @param distance  The distance (in pixels) that the listener is from the sound's source.
     * A null value means this parameter shouldn't matter.
     */
    @JvmOverloads
    fun play(distance: PixelDistance? = null) {
        // some sounds don't actually play (e.g. those used for alert purposes only)
        if (fileName == null) return

        // if we have a clip of this sound available for playing
        val clipWrapper = clips.firstOrNull { !it.playing }
        var clip: Clip? = null
        if (clipWrapper != null) {
            // use it
            clipWrapper.playing = true
            clip = clipWrapper.clip
        }

        // if no existing clip of this sound was available above
        if (clip == null) {
            // get an input stream on the sound's file
            val url =
                ClassLoader.getSystemClassLoader().getResource("sounds/$fileName.wav")
            val stream: AudioInputStream
            try {
                stream = AudioSystem.getAudioInputStream(url!!)
            } catch (e: Exception) {
                e.printStackTrace()
                return
            }

            // create a new clip of this sound; note that, per the workaround given
            // for the Tiger wave-file bug (java.sun.com ID: 5070730), we get the line
            // from the mixer (if we've found one), rather than from the AudioSystem
            val info: DataLine.Info?
            val format = stream.format
            info = DataLine.Info(Clip::class.java, format)
            try {
                clip = (if (mixer != null) mixer!!.getLine(info)
                else AudioSystem.getLine(info)) as Clip
                clip.open(stream)
            } catch (e: LineUnavailableException) {
                e.printStackTrace()
                return
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }

            // wrap the clip
            val wrapper = ClipWrapper(clip)

            // add this listener to the clip
            clip.addLineListener { event ->
                // when this clip is done playing
                if (event.type == LineEvent.Type.STOP) {
                    // reposition the clip's playing position at its start
                    wrapper.clip.stop()
                    wrapper.clip.framePosition = 0

                    // mark that it is done, so it may be reused later
                    wrapper.playing = false
                }
            }

            // add the wrapped clip to this sound's list of clips
            clips.add(wrapper)
        }

        // if a distance was given
        if (distance != null) {
            // if the clip has a gain control
            if (clip.isControlSupported(FloatControl.Type.MASTER_GAIN)) {
                // get the clip's gain control
                val control =
                    clip.getControl(FloatControl.Type.MASTER_GAIN) as FloatControl

                // determine the gain fraction according to the given distance from the source,
                // trying to make things that are close-by sound at their full volume
                var gain = max(1.5f - 1.5f * distance.distance / range.distance, 0.1f)
                gain = min(gain, 1f)

                // if there should be some gain loss due to distance
                if (gain < 1f) {
                    // set the value into the gain control; note that we have to convert
                    // the gain fraction to decibels, as that is the unit employed by the
                    // control
                    val dB = ln(gain.toDouble()) / ln(10.0) * 20.0
                    control.value = dB.toFloat()
                }
            }
        }

        // play the sound
        clip.start()
    }

    /**
     * Associates a clip with the time it was last played.
     */
    private inner class ClipWrapper(
        /**
         * The clip being wrapped.
         */
        var clip: Clip
    ) {

        /**
         * Whether or not the wrapped clipped is currently playing.
         */
        var playing = false
    }

    companion object {
        /**
         * The default value (in tiles) of this sound's range field.
         */
        private val defaultTileRange = TileDistance(15)

        /**
         * The mixer used by the Java audio system.  We use this per the workaround
         * given for the Tiger wave-file bug (java.sun.com ID: 5070730).
         */
        private var mixer: Mixer? = null

        /**
         * This code is given by the the workaround provided for the Tiger
         * wave-file bug (java.sun.com ID: 5070730).
         */
        init {
            // retrieve the mixer used by the Java audio system; we will obtain
            // lines from it below, rather than directly from the audio system
            val mixerInfos = AudioSystem.getMixerInfo()
            val mixerName = "Java Sound Audio Engine"
            val mixerInfo = mixerInfos.firstOrNull { it.name == mixerName }
            if (mixerInfo != null) mixer = AudioSystem.getMixer(mixerInfo)
        }
    }
}
