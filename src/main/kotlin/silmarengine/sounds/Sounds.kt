package silmarengine.sounds

import silmarengine.contexts.gameplay.areas.geometry.TileDistance

/**
 * The flyweight sounds.
 */
object Sounds {
    val damage = Sound("damage")
    val miss = Sound("miss")
    val playerDead = Sound("playerDead")
    val arrivedAtArea = Sound("arrivedAtArea", false)
    val loadingLevel = Sound("loadingLevel", false)
    val attributeLost = Sound("attributeLost")
    val attributeGained = Sound("attributeGained")
    val levelLost = Sound("levelLost")
    val levelGained = Sound("levelGained")
    val levelChoice = Sound("levelChoice")
    val itemAdded = Sound("itemAdded", false) // i.e. added to the area
    val itemRemoved = Sound("itemRemoved", false) // i.e. removed from the area
    val itemDamaged = Sound("itemDamaged")
    val itemRepaired = Sound("itemRepaired")
    val itemEquipped = Sound("itemEquipped")
    val itemUnequipped = Sound("itemUnequipped")
    val itemIdentified = Sound("itemIdentified")
    val handsConfigSet = Sound("handsConfigSet")
    val movement = Sound("movement", TileDistance(10))
    val trapSprung = Sound("trapSprung", TileDistance(20))
    val trapDetected = Sound("trapDetected", false)
    val torchLit = Sound("torchLit")
    val torchGoesOut = Sound("torchGoesOut", false)
    val itemLost = Sound("itemLost", false)
    val gameFinished = Sound("gameFinished")
    val doorOpens = Sound("doorOpens")
    val doorCloses = Sound("doorCloses")
    val chestOpens = Sound("chestOpens")
    val itemBought = itemRemoved
    val itemSold = Sound("itemSold")
    val itemDonated = Sound("itemDonated")
    val potionDrunk = Sound("potionDrunk")
    val monsterActivation = Sound("monsterActivation")
    val curePoison = Sound("curePoison")
    val cureDisease = Sound("cureDisease")
    val powerCircle = Sound("powerCircle")
    val confused = Sound("confused", false)
    val monsterGenerated = Sound("monsterGenerated", false)
    val livingDeadRepel = Sound("livingDeadRepelled")
    val teleportIn = Sound("teleportIn")
    val teleportOut = Sound("teleportOut")
    val heal = Sound("heal")

    /**
     * Attack sounds
     */
    val meleeAttack = Sound("meleeAttack")
    val oldProjectile = Sound("oldProjectile")
}