package silmarengine

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.frames.TitleFrame
import silmarengine.gui.playui.PlayUI
import silmarengine.sounds.Sound
import silmarengine.util.file.redirectSystemOutToFile

fun startup(
    args: Array<String>, titleImageFilename: String, creditsText: String,
    copyrightText: String
) {
    // according to this post from 2012:
    // https://stackoverflow.com/questions/4627320/java-hardware-acceleration,
    // graphics acceleration is not enabled unless you set this system
    // property, which does seem to make a big difference, especially with
    // the SIL-102 bug where player movement slows way down after any tooltip
    // is displayed
    System.setProperty("sun.java2d.opengl", "true")

    // if we were given at least one command-line parameter
    if (args.isNotEmpty()) {
        // if the parameter says to log
        if (args[0] == "log") {
            redirectSystemOutToFile("log.txt")
        }
    }

    // create the title frame
    val titleFrame =
        TitleFrame(titleImageFilename, creditsText, copyrightText, ::createGame,
            ::resumeGame)

    // determine whether the should-quick-start configuration property has been set
    loadConfigurationFile()
    val shouldQuickStart = getBooleanConfigurationValue("shouldQuickStart")

    // if a quick start is prescribed
    if (shouldQuickStart) {
        // resume the default-named game file
        Thread { resumeGame("./saved games", "game", titleFrame) }.start()
    }
    else {
        // otherwise, show the title frame
        titleFrame.isVisible = true
        titleSound.play()
    }
}

const val programName = "Silmar"

/**
 * The version number of this game program.
 */
@ExtensionPoint
lateinit var programVersion: String

@ExtensionPoint
lateinit var createPlayUI: (player: Player) -> PlayUI

@ExtensionPoint
lateinit var titleSound: Sound
