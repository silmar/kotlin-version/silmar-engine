package silmarengine

import java.util.concurrent.LinkedBlockingQueue

typealias Task = () -> Unit

/**
 * A thread which maintains a queue of tasks supplied by callers, and executes those
 * tasks in their queued sequence.
 */
object WorkThread : Thread("WorkThread") {
    /**
     * A queue of tasks.
     */
    private val queue = LinkedBlockingQueue<Task>()

    /**
     * This lets us see when debugging what task this thread
     * is currently executing (if any).
     */
    private var currentTask: Task? = null

    init {
        start()
    }

    fun queueTask(task: Task) {
        queue.add(task)
    }

    override fun run() {
        // keep doing this
        while (true) {
            // work the next task
            try {
                val task = queue.take()
                currentTask = task
                task()
                currentTask = null
            } catch (e: Exception) {
                // if something goes wrong while executing the task,
                // just print a stack trace; we want the game loop to continue no
                // matter what, so the user doesn't lose their progress
                e.printStackTrace()
            }
        }
    }
}
