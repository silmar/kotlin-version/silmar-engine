package silmarengine.gui.tooltips

import silmarengine.Task
import silmarengine.scheduler
import java.awt.GraphicsEnvironment
import java.awt.event.MouseEvent
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import javax.swing.JComponent
import javax.swing.SwingUtilities
import javax.swing.event.MouseInputAdapter

/**
 * Controls and performs the display of tooltips of UI components within this program.
 */
object TooltipManager {
    /**
     * The single tooltip window which is reused by this manager to display every
     * tooltip shown in the program.
     */
    private var tooltip = Tooltip()

    /**
     * See inner class.
     */
    private val mouseListener = MouseListener()

    /**
     * The component for which a tooltip will be shown.
     */
    private var hasTooltip: HasTooltip? = null

    /**
     * The future task (if any) to display an imminent tooltip, stored here in case
     * we need to cancel it.
     */
    private var showFuture: Future<Task>? = null

    /**
     * Informs this manager that it should manage the tooltip display of the given UI component.
     */
    fun registerComponent(comp: JComponent) {
        comp.addMouseListener(mouseListener)
        comp.addMouseMotionListener(mouseListener)
    }

    /**
     * This listens for the mouse cursor entering and exiting a component which is
     * to display a tooltip.
     */
    private class MouseListener : MouseInputAdapter() {
        override fun mouseEntered(e: MouseEvent) {
            // if the source component has tooltip text
            val source = e.source as HasTooltip
            if (source.hasTooltipImpl.tooltipText != null) {
                // schedule the imminent display of the tooltip to occur if the mouse
                // is still over the source component at that time
                hasTooltip = source
                scheduler.schedule({ if (hasTooltip == source) showTooltip() }, 500,
                    TimeUnit.MILLISECONDS)
            }
        }

        override fun mouseExited(e: MouseEvent) {
            // if the source component is the one for which this manager is currently
            // displaying (or, is about to display) a tooltip
            val source = e.source as HasTooltip
            if (source == hasTooltip) {
                // hide the tooltip (if any)
                tooltip.isVisible = false

                // abort any imminent showing of the tooltip
                showFuture?.cancel(true)
                showFuture = null

                hasTooltip = null
            }
        }
    }

    private fun showTooltip() {
        // run this code on the swing events thread, since it modifies the
        // state of the tooltip (which has been realized if the tooltip has
        // been shown at least once)
        SwingUtilities.invokeLater(Runnable {
            // if the component isn't showing on the screen, don't display the tooltip, as trying
            // to do so under this circumstance will produce an IllegalComponentStateException;
            // we'll just try again during the next iteration of the outer loop
            val comp = hasTooltip as JComponent?
            if (comp == null || !comp.isShowing) return@Runnable

            // hide the previously shown tooltip (if any)
            tooltip.isVisible = false

            // set the text to be displayed by the tooltip
            val text = hasTooltip!!.hasTooltipImpl.tooltipText ?: return@Runnable
            tooltip.setText(text)
            tooltip.pack()

            // determine the location at which to display the tooltip,
            // such that it fully fits on the screen
            val location = comp.locationOnScreen
            val compSize = comp.size
            val env = GraphicsEnvironment.getLocalGraphicsEnvironment()
            val screenBounds = env.maximumWindowBounds
            val x =
                location.x + if (location.x + compSize.width + tooltip.width < screenBounds.width) compSize.width
                else -tooltip.width
            val y =
                location.y + if (location.y + compSize.height + tooltip.height < screenBounds.height) compSize.height
                else -tooltip.height
            tooltip.setLocation(x, y)

            // display the tooltip
            tooltip.isVisible = true
            tooltip.toFront()
        })
    }

    fun hideTooltip() {
        tooltip.isVisible = false
    }
}
