package silmarengine.gui.tooltips

import silmarengine.gui.Fonts
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Window
import javax.swing.*
import javax.swing.border.EmptyBorder
import javax.swing.border.LineBorder

/**
 * A tooltip window.
 */
class Tooltip : JWindow(null as Window?) {

    /**
     * The label containing the text displayed within this tooltip.
     */
    private val label: JLabel

    init {
        isAlwaysOnTop = true

        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color(255, 255, 208)

        val panel = JPanel()
        panel.border =
            BorderFactory.createCompoundBorder(LineBorder(Color.black, 1, true),
                EmptyBorder(1, 3, 1, 3))
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add the tooltip-text label
        label = JLabel()
        label.alignmentX = 0.5f
        label.font = Fonts.tooltip
        panel.add(label)
    }

    /**
     * Sets the text to be displayed by this tooltip.
     */
    fun setText(text: String) {
        label.text = text
    }
}
