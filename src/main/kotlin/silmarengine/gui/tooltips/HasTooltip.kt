package silmarengine.gui.tooltips

/**
 * Marks a UI-component class as one for which a tooltip should be displayed.
 * It is assumed that any such class is derived from JComponent.
 */
interface HasTooltip {

    /**
     * Returns the marked instance's tooltip implementation.
     */
    val hasTooltipImpl: HasTooltipImpl

    /**
     * Instructs the marked instance to update its tooltip text.
     */
    fun updateTooltip()
}
