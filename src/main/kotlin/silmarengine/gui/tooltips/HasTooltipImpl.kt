package silmarengine.gui.tooltips

import silmarengine.util.html.breakTextAtSpaces
import javax.swing.JComponent

/**
 * Tooltip functionality designed to be composed into various UI components.
 *
 * @param hasTooltip    The UI component composing this tooltip functionality into itself.
 */
class HasTooltipImpl(val hasTooltip: HasTooltip) {

    /**
     * The text (if any) displayed within this button's tooltip.
     */
    var tooltipText: String? = null
        private set

    init {
        TooltipManager.registerComponent(hasTooltip as JComponent)
    }

    /**
     * @param breakLength   How many characters per line the tooltip should display.
     * Values <= 0 instruct to not break the text into lines.
     */
    fun setTooltipText(text: String, breakLength: Int) {
        tooltipText =
            "<html>${if (breakLength > 0) breakTextAtSpaces(text, breakLength) else text}</html>"
    }
}
