package silmarengine.gui.displays

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import java.awt.Color
import java.util.*
import javax.swing.border.MatteBorder
import kotlin.collections.ArrayList

class StatusMessagesDisplay {
    /**
     * The UI component which displays the messages.
     */
    val label = Label("", Fonts.mediumText)

    /**
     * The status messages currently being shown by this display.
     */
    private val messages = ArrayList<String>()

    /**
     * Schedules messages for removal from this display.
     */
    private val timer = Timer()

    init {
        label.border = MatteBorder(2, 0, 0, 0, Color.lightGray)
    }

    fun addMessage(message: String, duration: Int = 0) {
        messages.add(message)
        onMessagesChange()

        if (duration > 0) {
            timer.schedule(object : TimerTask() {
                override fun run() {
                    messages.remove(message)
                    onMessagesChange()
                }
            }, duration.toLong())
        }
    }

    fun removeMessage(message: String) {
        val index = messages.indexOf(message)
        if (index >= 0) {
            messages.removeAt(index)
            onMessagesChange()
        }
    }

    fun clear() {
        messages.clear()
        onMessagesChange()
    }

    private fun onMessagesChange() {
        val text =
            if (messages.isNotEmpty()) messages.reduce { acc, next -> "${acc}<br>${next}" } else "---"
        label.text = "<html>$text</html>"
    }
}