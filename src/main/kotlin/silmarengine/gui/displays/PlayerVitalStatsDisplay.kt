package silmarengine.gui.displays

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.util.html.breakTextAtSpaces
import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JLabel
import javax.swing.JPanel

/**
 * A display of the usually-most-vital statistics about a player.
 */
open class PlayerVitalStatsDisplay(val player: Player) : JPanel() {
    private val hitPointsLabel: JLabel

    protected val statusLabel: JLabel

    private val defenseLabel: Label

    private val goldLabel: JLabel

    private val levelLabel: JLabel

    private val encumbranceLabel: JLabel

    private val movementRateLabel: JLabel

    /**
     * The encumbrance label heading.  We need this broken out like this to identify when we
     * are dealing with the encumbrance label-pair, which is the widest and therefore defines this
     * display's width.
     */
    private val encumbranceLabelText = "Encumbrance:"

    init {
        layout = BoxLayout(this, BoxLayout.Y_AXIS)
        isOpaque = false

        // add the attribute-value label-pairs
        levelLabel = addLabel("Level:",
            "Your experience level. Affects how well you hit in combat, and your maximum hit points.").second
        hitPointsLabel =
            addLabel("Hit Points:", "How much damage you can take before dying.",
                true).second
        statusLabel =
            addLabel("Status:", "Your general welfare, ignoring your hit points.").second
        statusLabel.font = Font("dialog", Font.BOLD, 12)
        defenseLabel = addLabel("Defense:", defenseLabelTooltip).second
        goldLabel = addLabel("Gold:", "The amount of gold coins you possess.").second
        encumbranceLabel = addLabel(encumbranceLabelText,
            "How much your currently held possessions are encumbering you.  Affects your movement rate.",
            true).second
        movementRateLabel = addLabel("Movement Rate:",
            "How many squares you can move per game turn.").second
    }

    /**
     * Adds an attribute-label with the given text and tooltip-text, along with
     * an associated value-label, to this display.
     *
     * @param addCurrentMaximum     Whether text should be added explaining what the two
     * numbers (if present) in the label mean.
     * @return      The value-label added.
     */
    protected fun addLabel(
        text: String, tooltip: String, addCurrentMaximum: Boolean = false
    ): Pair<Box, Label> {
        // format the given tooltip text for use as a tooltip
        val currentMaximum = "<br><I>(Current / Maximum)</I>"
        val formattedTooltip =
            breakTextAtSpaces(tooltip, 30) + if (addCurrentMaximum) currentMaximum else ""

        // add a box to hold both labels side-by-side
        val box = Box.createHorizontalBox()
        add(box)

        // add the attribute label
        val label = AttributeLabel(text)
        box.add(label)
        label.hasTooltipImpl.setTooltipText(formattedTooltip, 0)

        // add a padding area, so that changes to the widest label-pair's width
        // don't cause this display to have to resize itself
        box.add(Box.createRigidArea(
            Dimension(if (text != encumbranceLabelText) 20 else 5, 0)))

        box.add(Box.createHorizontalGlue())

        // add the value label
        val label2 = ValueLabel()
        box.add(label2)
        label2.hasTooltipImpl.setTooltipText(formattedTooltip, 0)

        return Pair(box, label2)
    }

    /**
     * A label describing what the associated value-label represents.
     */
    private class AttributeLabel(text: String) : Label(text, Fonts.mediumText) {

        init {
            horizontalAlignment = LEFT
        }

        override fun getAlignmentX(): Float {
            return 0f
        }
    }

    /**
     * A label displaying the value of one of the attributes presented by this display.
     */
    private class ValueLabel : Label("0", Fonts.mediumNumber) {

        init {
            horizontalAlignment = RIGHT
        }

        override fun getAlignmentX(): Float {
            return 1f
        }
    }

    /**
     * Informs this display that one of the attribute values it shows has changed.
     */
    open fun onAttributeValueChanged() {
        hitPointsLabel.text = "${player.hitPoints} / ${player.maxHitPoints}"
        levelLabel.text = "${player.levelValues.level}"
        goldLabel.text = "${player.stats.gold}"
        encumbranceLabel.text =
            "${player.items.encumbrance} / ${player.items.maxEncumbrance}"
        movementRateLabel.text = "${player.maxMovementPoints}"

        updateStatusLabel()

        updateDefenseLabel()

        repaint()
    }

    /**
     * Updates this display's status label to reflect the player's current status.
     */
    protected open fun updateStatusLabel() {
        var color = Color.white
        var text = "normal"
        when {
            player.isParalyzed -> {
                color = Color.cyan
                text = "paralyzed"
            }
            player.isConfused -> {
                color = Color.gray
                text = "confused"
            }
            player.statuses.isPoisoned -> {
                color = Color.green
                text = "poisoned"
            }
            player.statuses.isDiseased -> {
                color = Color(152, 185, 53)
                text = "diseased"
            }
        }
        statusLabel.foreground = color
        statusLabel.text = text
    }

    /**
     * Updates this display's defense label to reflect the player's current defense value(s).
     */
    private fun updateDefenseLabel() {
        // start with the defense value for normal penetration
        val buffer = StringBuffer()
        buffer.append(player.getDefense(AttackTypes.meleeAttack, null))

        appendToDefenseLabel(buffer, player)

        defenseLabel.text = buffer.toString()

        defenseLabel.hasTooltipImpl.setTooltipText(getDefenseTooltipText(), 30)
    }
}

@ExtensionPoint
var appendToDefenseLabel: (buffer: StringBuffer, player: Player) -> Unit = { _, _ -> }

const val defenseLabelTooltip = "How hard it is to hit you in combat."

@ExtensionPoint
var getDefenseTooltipText: () -> String = { defenseLabelTooltip }