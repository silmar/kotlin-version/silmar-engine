package silmarengine.gui.frames

import silmarengine.annotations.ExtensionPoint
import silmarengine.gui.Colors
import silmarengine.gui.Fonts
import silmarengine.gui.dialogs.CheckForNewerVersionDialog
import silmarengine.gui.dialogs.MessageDialog
import silmarengine.gui.save
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.ImagePanel
import silmarengine.gui.widgets.Label
import silmarengine.gui.windowLayout
import silmarengine.programName
import silmarengine.programVersion
import silmarengine.util.image.loadImage
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.FileDialog
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.io.File
import javax.swing.*
import kotlin.system.exitProcess

/**
 * The frame that allows the user to either create a new game, or resume an
 * existing game, or exit the program.
 */
class TitleFrame(
    titleImageFilename: String, creditsText: String, copyrightText: String,
    val onCreateGame: (parent: JFrame) -> Unit,
    val onResumeGame: (path: String, fileName: String, parent: JFrame) -> Unit?
) : JFrame() {
    init {
        // configure this frame
        val pane = contentPane
        pane.background = Colors.lighterGray
        isResizable = false
        title = "$programName version $programVersion"
        setSize(100, 100)
        iconImage = loadImage("icon")
        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(event: WindowEvent?) {
                // store the final layout of the game's main UI windows
                windowLayout.save()

                isVisible = false

                onProgramClosing(this@TitleFrame)

                exitProcess(0)
            }
        })

        pane.layout = BorderLayout()

        val panel = JPanel()
        panel.isOpaque = false
        pane.add(panel, BorderLayout.CENTER)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        // add the game logo image-panel
        val panel2 = ImagePanel(loadImage(titleImageFilename))
        panel.add(panel2)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        panel.add(Box.createVerticalGlue())

        // add the credits label
        var label: JLabel = Label(creditsText, Fonts.smallText)
        label.foreground = Color.black
        label.isOpaque = false
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 20)))

        // add the buttons panel
        var buttonsPanel = JPanel()
        buttonsPanel.isOpaque = false
        buttonsPanel.layout = BoxLayout(buttonsPanel, BoxLayout.X_AXIS)
        panel.add(buttonsPanel)

        // add the create-game button
        var button = Button(" Create a game ")
        button.font = Fonts.mediumText
        buttonsPanel.add(button)
        button.addActionListener {
            // in a new thread, since we're on the UI thread;
            // this thread will run the game's playing
            Thread task@{ onCreateGame(this@TitleFrame) }.start()
        }

        buttonsPanel.add(Box.createRigidArea(Dimension(5, 0)))

        // add the resume-game button
        button = Button(" Resume a game ")
        button.font = Fonts.mediumText
        buttonsPanel.add(button)
        button.addActionListener {
            // in a new thread, since we're on the UI thread;
            // this thread will run the game's playing
            Thread {
                val result = selectResumeGameFile(this)
                if (result.existingFileChosen) {
                    onResumeGame(result.path!!, result.fileName!!, this)
                }
            }.start()
        }

        panel.add(Box.createRigidArea(Dimension(0, 5)))

        // add a second buttons panel
        buttonsPanel = JPanel()
        buttonsPanel.isOpaque = false
        buttonsPanel.layout = BoxLayout(buttonsPanel, BoxLayout.X_AXIS)
        panel.add(buttonsPanel)

        // add the check-for-newer-version button
        button = Button(" " + CheckForNewerVersionDialog.title + " ")
        button.font = Fonts.mediumText
        buttonsPanel.add(button)
        button.addActionListener {
            // display a check-for-newer-version dialog
            CheckForNewerVersionDialog(this@TitleFrame).isVisible = true
        }

        panel.add(Box.createRigidArea(Dimension(0, 20)))

        // add the copyright label
        label = Label(copyrightText, Fonts.smallText)
        label.setForeground(Color.black)
        label.setOpaque(false)
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        pack()
        setLocationRelativeTo(null)
    }
}

private class SelectResumeGameFileResult(
    val existingFileChosen: Boolean, val path: String? = null,
    val fileName: String? = null
)

private fun selectResumeGameFile(parent: JFrame): SelectResumeGameFileResult {
    // display a file-open dialog to get the game-file path and name
    val fileDialog = FileDialog(parent, "Select game file")
    fileDialog.mode = FileDialog.LOAD
    fileDialog.isVisible = true
    val path = fileDialog.directory
    val fileName = fileDialog.file

    // if no game-file path or name were entered, no resume can occur
    if (path == null || fileName == null) return SelectResumeGameFileResult(false)

    // if the game file doesn't exist
    val file = File("$path/$fileName")
    if (!file.exists()) {
        MessageDialog.show(parent, "File not found.")
        return SelectResumeGameFileResult(false)
    }

    // the game will (hopefully) be resumed, so hide the parent frame to make way
    // for the play-frame
    parent.isVisible = false

    return SelectResumeGameFileResult(true, path, fileName)
}

@ExtensionPoint
var onProgramClosing: (titleFrame: TitleFrame) -> Unit = {}