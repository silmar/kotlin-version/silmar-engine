package silmarengine.gui.frames.playframe.extensions

import silmarengine.gui.dialogs.MessageDialog
import silmarengine.gui.frames.playframe.PlayFrame

fun PlayFrame.onPlayerDead() {
    MessageDialog.show(frame, "You are dead!")

    close()
}

