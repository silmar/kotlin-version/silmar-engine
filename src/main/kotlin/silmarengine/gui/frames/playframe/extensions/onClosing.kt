package silmarengine.gui.frames.playframe.extensions

import silmarengine.gui.dialogs.OkCancelDialog
import silmarengine.gui.frames.playframe.PlayFrame

/**
 * Is called when this play-frame is being closed by the user.
 */
fun PlayFrame.onClosing() {
    // if the player isn't dead, ask if the user is sure about quitting
    if (!player.isDead && !OkCancelDialog.show(frame,
            "Quit, and lose all changes since the last save point?")) return

    close()
}

