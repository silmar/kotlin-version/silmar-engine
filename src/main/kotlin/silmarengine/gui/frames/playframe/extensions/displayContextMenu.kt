package silmarengine.gui.frames.playframe.extensions

import silmarengine.Task
import silmarengine.WorkThread
import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.shutDoor
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextToLocation
import silmarengine.gui.*
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.widgets.PopupMenu
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.extensions.isOpenDoor
import java.awt.Point
import javax.swing.JMenuItem

fun PlayFrame.displayContextMenu(areaLocation: PixelPoint, screenLocation: Point) {
    val menu = PopupMenu()
    menu.location = screenLocation
    menu.invoker = frame
    addContextMenuItems(menu, areaLocation)
    menu.isVisible = true
}

private fun PlayFrame.addContextMenuItems(menu: PopupMenu, areaLocation: PixelPoint) {
    val area = player.area

    fun addMenuItem(text: String, task: Task?) {
        val item = JMenuItem(text)
        item.font = Fonts.mediumText
        if (task != null) item.addActionListener {
            WorkThread.queueTask(task)
        }
        menu.add(item)
    }

    // if the tile that was clicked on is an open door which is next to the player,
    // and it's not the same type of tile that's underneath the player (since the
    // player shouldn't be able to close a door he's standing on)
    val tile = area.getTile(areaLocation)
    if (tile.isOpenDoor && player.isNextToLocation(areaLocation) && tile != area.getTile(
            player.location)) {
        // add a menu item to close the door
        addMenuItem("Close door") { player.shutDoor(areaLocation) }
    }

    // if the player is using a ranged weapon
    val weapon = player.weapon
    val weaponType = weapon.weaponType
    if (weaponType.isRanged) {
        // add an option to make a ranged attack in the direction of the given location
        addMenuItem("Fire ${weapon.name} in this direction") {
            LocationSelections.rangedAttack.onSelection(areaLocation, player)
        }
    }

    addGameSpecificContextActions(area, areaLocation, player, ::addMenuItem)

    // if no items were added to the menu, above
    if (menu.components.isEmpty()) {
        // add a disabled item indicating no actions are available
        addMenuItem("-- No context actions --", null)
    }
}

@ExtensionPoint
var addGameSpecificContextActions: (
    area: Area, areaLocation: PixelPoint, player: Player, addMenuItem: (text: String, task: Task?) -> Unit
) -> Unit = { _, _, _, _ -> }