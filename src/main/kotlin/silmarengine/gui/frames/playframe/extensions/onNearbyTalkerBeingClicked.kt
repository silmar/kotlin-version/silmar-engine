package silmarengine.gui.frames.playframe.extensions

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.Buyer
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.ItemDonationReceiver
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.ItemIdentifier
import silmarengine.contexts.gameplay.entities.talkers.ItemFixer
import silmarengine.contexts.gameplay.entities.talkers.ItemSeller
import silmarengine.contexts.gameplay.entities.talkers.ServiceSeller
import silmarengine.gui.dialogs.MessageDialog
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.playui.extensions.*

fun PlayFrame.onNearbyTalkerBeingClicked(being: TalkerBeing) {
    when (being) {
        is ItemSeller -> playUI.showBuyDialog(being)
        is ServiceSeller -> playUI.showServicesDialog(being)
        is ItemDonationReceiver -> playUI.showDonateDialog(being)
        is ItemIdentifier -> playUI.showIdentifyDialog(being)
        is Buyer -> playUI.showSellDialog(being)
        is ItemFixer -> playUI.showRepairDialog(being)
        else -> {
            // display the being's greeting in a message dialog
            MessageDialog.show(frame, being.greeting)

            // have the player proceed with the talker beyond its greeting
            being.onGreetingHeardByPlayer(player)
        }
    }

    being.onTalkedTo()
}

