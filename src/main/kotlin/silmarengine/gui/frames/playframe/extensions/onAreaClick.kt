package silmarengine.gui.frames.playframe.extensions

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.move
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.pickupItem
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextTo
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextToLocation
import silmarengine.contexts.gameplay.entities.talkers.Talker
import silmarengine.gui.LocationSelections
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.awt.Point

/**
 * Informs this play-frame that the user has clicked in this area at the
 * given area pixel-location.
 */
fun PlayFrame.onAreaClick(
    location: PixelPoint, screenLocation: Point, isRightClick: Boolean
) {
    // the area-view must be enabled for the click to be valid
    if (!areaView.panel.isEnabled) return

    // if the player is paralyzed, ignore this click
    if (player.isParalyzed) return

    val area = player.area

    // determine whether the given location is visible from the player's location
    val locationVisible =
        area.canSeeExt(player.location, location, tileWidth, true, false).canSee

    // if the user is currently selecting a ranged location
    val selection = locationSelection
    if (selection != null && !selection.isNearbyOnly) {
        // the location must be visible
        if (!locationVisible) return

        // the selection has been made
        areaView.useNormalCursor()
        val prompt = locationSelectionPrompt
        if (prompt != null) statusMessagesDisplay.removeMessage(prompt)
        locationSelection = null
        selection.onSelection(location, player)
        return
    }

    // if the user is currently selecting a nearby location
    if (selection != null && selection.isNearbyOnly) {
        // if the location selected is too far away, and either there is no monster there,
        // or the monster isn't next to the player
        val monster = getFirstEntityAt(location, area.monsters)
        if (!player.isNextToLocation(location) && (monster == null || !player.isNextTo(
                monster))) {
            statusMessagesDisplay.addMessage("Location is too far away.", 2000)
            return
        }

        // the selection has been made
        areaView.useNormalCursor()
        val prompt = locationSelectionPrompt
        if (prompt != null) statusMessagesDisplay.removeMessage(prompt)
        locationSelection = null
        selection.onSelection(location, player)
        return
    }

    if (isRightClick) {
        displayContextMenu(location, screenLocation)
        return
    }

    // if a monster was clicked on
    val being = getFirstEntityAt(location, area.beings)
    val weapon = player.weapon
    if (being is Monster) {
        // the player hasn't yet attacked this turn, so if its weapon is ranged
        if (weapon.weaponType.isRanged) {
            // the location must be visible
            if (!locationVisible) return

            // treat the click as a ranged-attack location selection
            LocationSelections.rangedAttack.onSelection(location, player)
            return
        }
        // otherwise, if the player is next to the monster
        else if (player.isNextTo(being)) {
            // treat the click as a melee-attack location selection
            LocationSelections.meleeAttack.onSelection(location, player)
            return
        }
    }

    // if a being was clicked on that isn't the player, but is adjacent to the player
    if (being != null && being !== player && player.isNextTo(being)) {
        // if the being is a talker
        if (being is Talker) {
            onNearbyTalkerBeingClicked(being as TalkerBeing)
            return
        }
    }

    // if an item was clicked that is next to the player
    val itemEntity = getFirstEntityAt(location, area.items)
    if (itemEntity != null && player.isNextTo(itemEntity)) {
        player.pickupItem(itemEntity, frame)
        return
    }

    // if a terrain feature was clicked that is next to the player
    val terrain = getFirstEntityAt(location, area.terrains)
    if (terrain != null && player.isNextTo(terrain)) {
        if (onNearbyTerrainClicked(terrain, player, frame)) return
    }

    // otherwise, just have the player move to the location
    player.move(location)
}

