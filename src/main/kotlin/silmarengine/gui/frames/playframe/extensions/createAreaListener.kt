package silmarengine.gui.frames.playframe.extensions

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.views.areaview.extensions.reactions.onEntityPresenceChange
import silmarengine.gui.views.areaview.extensions.visibility.recomputeView
import silmarengine.contexts.gameplay.areas.AreaListener
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.tiles.Tile

fun PlayFrame.createAreaListener(): AreaListener = object : AreaListener {
    override fun onTileChanged(location: TilePoint, to: Tile) {
        if (!areaView.isGroupOfChangesOccurring) areaView.recomputeView(delayInMs = 50)
    }

    override fun onEntityPresenceChange(entity: Entity) {
        areaView.onEntityPresenceChange(entity)
    }

    override fun onEntityChangedAppearance() {
        // get the entity redrawn
        areaView.panel.repaint()
    }

    override fun onGroupOfChangesToOccur() {
        areaView.isGroupOfChangesOccurring = true
    }

    override fun onGroupOfChangesOccurred() {
        areaView.isGroupOfChangesOccurring = false
        areaView.panel.repaint()
    }
}
