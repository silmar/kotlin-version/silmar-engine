package silmarengine.gui.frames.playframe.extensions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.LocationalMessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.gui.dialogs.MessageDialog
import silmarengine.gui.dialogs.StoryDialog
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.tooltips.TooltipManager
import silmarengine.gui.views.areaview.extensions.displayMessage
import silmarengine.util.sleepThread

fun PlayFrame.onPlayerReceivedMessage(
    message: String, messageType: MessageType, duration: Int
) {
    // hide whatever tooltip might currently be displayed, to help ensure the user
    // can see the message
    TooltipManager.hideTooltip()

    when {
        messageType === MessageType.normal ->
            // display the message at the player's location in the area-view
            areaView.displayMessage(message, null, duration)
        messageType is LocationalMessageType ->
            // display the message in the area-view at the area location embedded in
            // the message type
            areaView.displayMessage(message, messageType.location, duration)
        messageType === MessageType.important || messageType === MessageType.error ->
            // show the message in a modal dialog
            MessageDialog.show(frame, message)
        messageType === MessageType.status ->
            // show the message in a play-frame's status line display
            statusMessagesDisplay.addMessage(message, 5000)
        messageType === MessageType.gameFinished -> {
            // close the play-frame
            frame.isVisible = false

            // show the game ending message in its own dialog
            val dialog = StoryDialog(frame, gameFinishedText, "End game")
            dialog.isVisible = true

            // while the game-ending-message dialog is still visible
            while (dialog.isVisible) {
                // sleep a bit
                sleepThread(100)
            }
        }
    }
}

@ExtensionPoint
lateinit var gameFinishedText: String