package silmarengine.gui.frames.playframe.extensions

import silmarengine.sounds.Sounds
import silmarengine.gui.frames.playframe.PlayFrame

fun PlayFrame.onAreaLoading() {
    // turn the area-view's view off
    areaView.shouldDisplay = false

    // ensure the play-frame is visible, since it might not yet be
    frame.isVisible = true

    // if this isn't the first area loaded for this play-UI
    if (areaView.area != null) {
        // play the loading-area sound
        Sounds.loadingLevel.play()
    }

    statusMessagesDisplay.addMessage("Loading area...")
}

