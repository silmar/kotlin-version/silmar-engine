package silmarengine.gui.frames.playframe.extensions

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.sounds.Sounds

/**
 * Is called after the player has just arrived at an area.
 */
fun PlayFrame.onPlayerArrivedAtArea(player: Player) {
    // remove this play-frame's listener from the old area, if there was one
    val oldArea = areaView.area
    if (oldArea != null && areaListener != null) oldArea.removeListener(areaListener!!)

    // set the player's new area as the one the area-view should be displaying
    val area = player.area
    areaView.area = area
    areaView.viewpointEntity = player

    // add a listener to the new area for this play-frame
    areaListener = createAreaListener()
    area.addListener(areaListener!!)

    statusMessagesDisplay.clear()

    areaView.shouldDisplay = true

    Sounds.arrivedAtArea.play()
}