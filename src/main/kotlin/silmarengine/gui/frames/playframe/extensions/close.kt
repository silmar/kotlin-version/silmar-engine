package silmarengine.gui.frames.playframe.extensions

import silmarengine.contexts.gameplay.game.Game
import silmarengine.gui.frames.playframe.PlayFrame

fun PlayFrame.close() {
    frame.isVisible = false

    // stop the current game
    Game.currentGame!!.stopProcessingTurns()

    reset()
}