package silmarengine.gui.frames.playframe.extensions

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.gui.LocationSelections
import silmarengine.gui.frames.playframe.PlayFrame

fun PlayFrame.onPlayerCanMakeAttack(player: Player) {
    // display a status message to let the user know it's about to attack
    val offhand = player.isCurrentAttackOffhand
    val weapon = if (!offhand) player.weapon else player.offhandWeapon
    val prompt = "Attacking with ${weapon!!.name}${if (offhand) " (offhand)" else ""}"
    locationSelectionPrompt = prompt
    statusMessagesDisplay.addMessage(prompt)

    // have the user select where to attack
    locationSelection = if (weapon.weaponType.isRanged) LocationSelections.rangedAttack
    else LocationSelections.meleeAttack
}

