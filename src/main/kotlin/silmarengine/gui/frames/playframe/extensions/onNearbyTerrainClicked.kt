package silmarengine.gui.frames.playframe.extensions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains.interactWithTerrain
import silmarengine.contexts.gameplay.entities.talkers.ItemFixer
import silmarengine.contexts.gameplay.entities.talkers.ItemSeller
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.playui.extensions.showBuyDialog
import silmarengine.gui.playui.extensions.showRepairDialog
import javax.swing.JFrame

/**
 * Is called when the user has clicked on the given terrain, which is close to the player.
 *
 * @return  Whether any meaningful result occurred from this click (otherwise, the click should
 *          be checked by the caller for further meanings).
 */
fun PlayFrame.onNearbyTerrainClicked(
    terrain: Terrain, player: Player, parentFrame: JFrame
): Boolean {
    // if the click was on a game-specific terrain, handle it that way
    if (checkForNearbyGameSpecificTerrainClicked(terrain, player,
            parentFrame)) return true

    var resultOccurred = false
    when {
        terrain.type == TerrainTypes.chest -> {
            // open the chest
            val chest = terrain as Chest
            chest.open(player)
            resultOccurred = true
        }
        terrain is ItemSeller -> playUI.showBuyDialog(terrain)
        terrain is ItemFixer -> playUI.showRepairDialog(terrain)
        else -> resultOccurred = player.interactWithTerrain(terrain)
    }
    return resultOccurred
}

@ExtensionPoint
var checkForNearbyGameSpecificTerrainClicked: (terrain: Terrain, player: Player, parentFrame: JFrame) -> Boolean =
    { _, _, _ -> false }