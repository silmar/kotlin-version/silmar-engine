package silmarengine.gui.frames.playframe

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.LocationSelection
import silmarengine.contexts.playeritemsmanagement.ui.HandsItemsDisplay
import silmarengine.gui.displays.PlayerVitalStatsDisplay
import silmarengine.gui.displays.StatusMessagesDisplay
import silmarengine.gui.playui.PlayUI
import silmarengine.gui.views.areaview.AreaView
import silmarengine.contexts.gameplay.areas.AreaListener
import javax.swing.JFrame

/**
 * The main frame that displays as the user is actually playing the game.
 */
interface PlayFrame {
    /**
     * This play-frame's physical UI component.
     */
    val frame: JFrame

    val player: Player

    /**
     * The UI which manages this play-frame.
     */
    val playUI: PlayUI

    /**
     * Shows what items are currently in the player's hands.
     */
    val handsItemsDisplay: HandsItemsDisplay

    /**
     * Shows the player's current surroundings in its area.
     */
    val areaView: AreaView

    /**
     * A quick-summary view of the player's current stats.
     */
    val playerVitalStatsDisplay: PlayerVitalStatsDisplay

    /**
     * The display that stretches across below the area-view and displays informational
     * messages to the user.
     */
    val statusMessagesDisplay: StatusMessagesDisplay

    /**
     * The current location-selection (if any) being undertaken by the user.
     */
    var locationSelection: LocationSelection?

    /**
     * The status message (if any) displayed while the user is supposed to select a
     * location.  We remember it so we can remove it once the selection is made.
     */
    var locationSelectionPrompt: String?

    var areaListener: AreaListener?

    /**
     * Informs all relevant components on this play-frame that they should
     * either (according to the given value) start or stop accepting user input.
     */
    fun setUIEnabled(enable: Boolean)

    /**
     * Resets this play-frame to a state as if no game was being played.
     */
    fun reset()
}