package silmarengine.gui.frames.playframe

import silmarengine.*
import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.Fonts
import silmarengine.gui.LocationSelection
import silmarengine.gui.displays.PlayerVitalStatsDisplay
import silmarengine.gui.displays.StatusMessagesDisplay
import silmarengine.gui.frames.playframe.extensions.onClosing
import silmarengine.gui.frames.playframe.extensions.onAreaClick
import silmarengine.gui.playui.PlayUI
import silmarengine.gui.views.areaview.AreaView
import silmarengine.gui.views.areaview.AreaView1
import silmarengine.gui.views.areaview.AreaViewListener
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import silmarengine.gui.windowLayout
import silmarengine.contexts.gameplay.areas.AreaListener
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.playeritemsmanagement.ui.HandsItemsDisplay
import silmarengine.util.image.loadImage
import java.awt.*
import java.awt.event.ActionListener
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.*
import javax.swing.border.MatteBorder

@Suppress("LeakingThis")
open class PlayFrame1(override val playUI: PlayUI, override val player: Player) :
    PlayFrame, JFrame(programName) {

    override val frame: JFrame get() = this

    private val playerNameLabel: Label

    /**
     * The button that displays the items dialog.
     */
    private var itemsButton: Button? = null

    /**
     * The button that displays the attributes dialog.
     */
    private var attributesButton: Button? = null

    /**
     * The button that displays the game's commands list.
     */
    private var commandsButton: Button? = null

    /**
     * The button that causes the player to pass its current turn.
     */
    private var passButton: Button? = null

    /**
     * The button that re-centers the area-view on the player's location.
     */
    private var homeViewButton: Button? = null

    /**
     * The button that selects for the player its previously-used hands config.
     */
    private var previousHandsConfigButton: Button? = null

    /**
     * The button that displays the use-hands-config dialog.
     */
    private var useHandsConfigButton: Button? = null

    /**
     * The button that displays the set-hands-config dialog.
     */
    private var setHandsConfigButton: Button? = null

    override var locationSelection: LocationSelection? = null
        set(value) {
            field = value

            // set the area-view's mouse-cursor appropriately
            if (locationSelection != null) areaView.useCursor(crosshairsCursor)
            else areaView.useNormalCursor()
        }

    override var locationSelectionPrompt: String? = null

    override val handsItemsDisplay: HandsItemsDisplay

    override val areaView: AreaView

    override val playerVitalStatsDisplay: PlayerVitalStatsDisplay

    override val statusMessagesDisplay = StatusMessagesDisplay()

    /**
     * The mouse cursor used when selecting a location in the area-view.
     */
    private val crosshairsCursor: Cursor

    override var areaListener: AreaListener? = null

    init {
        // configure this frame
        val pane = contentPane
        pane.layout = BorderLayout()
        val backgroundColor = PlayUI.backgroundColor
        pane.background = backgroundColor
        iconImage = loadImage("icon")
        setSize(700, 600)
        setLocationRelativeTo(parent)

        addWindowListener(object : WindowAdapter() {
            override fun windowActivated(event: WindowEvent?) {
                // this is here because there is a Swing (or AWT) bug where the parent frame
                // of a dialog doesn't have it's paint() method called when the dialog is
                // dismissed; rather, the frame gets painted with its last previous image;
                // this code forces paint() to be called when the dialog goes away
                pane.repaint()
            }

            override fun windowClosing(event: WindowEvent?) {
                onClosing()
            }
        })

        // onClosing will handle hiding this window when its close button is clicked
        defaultCloseOperation = WindowConstants.DO_NOTHING_ON_CLOSE

        val leftPanel = JPanel()
        leftPanel.isOpaque = false
        leftPanel.layout = BorderLayout()
        pane.add(leftPanel, BorderLayout.CENTER)

        // add the area-view
        areaView = AreaView1()
        leftPanel.add(areaView.panel, BorderLayout.CENTER)

        // add this mouse-listener to the area-view
        areaView.addListener(object : AreaViewListener {
            override fun onClicked(
                areaLocation: PixelPoint, screenLocation: Point, isRightClick: Boolean
            ) {
                WorkThread.queueTask {
                    onAreaClick(areaLocation, screenLocation, isRightClick)
                }
            }
        })

        // add the status-messages display
        leftPanel.add(statusMessagesDisplay.label, BorderLayout.SOUTH)

        // add the right panel
        val rightPanel = JPanel()
        rightPanel.background = backgroundColor
        val outer = MatteBorder(0, 2, 0, 0, Color.lightGray)
        val inner = MatteBorder(2, 2, 2, 2, Color.black)
        rightPanel.border = BorderFactory.createCompoundBorder(outer, inner)

        rightPanel.layout = BoxLayout(rightPanel, BoxLayout.Y_AXIS)
        pane.add(rightPanel, BorderLayout.EAST)

        // add the name label
        var label = Label("", Fonts.mediumText)
        playerNameLabel = label
        rightPanel.add(label)

        // add the vital-stats display
        playerVitalStatsDisplay = createPlayerVitalStatsDisplay(player)
        rightPanel.add(playerVitalStatsDisplay)
        playerVitalStatsDisplay.onAttributeValueChanged()

        rightPanel.add(Box.createRigidArea(Dimension(0, 20)))

        // add the items button
        var button = Button(" Items ")
        itemsButton = button
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener(ActionListener {
            if (!isEnabled) return@ActionListener
            if (locationSelection == null) playUI.showItems()
        })
        setTooltip(button, "Displays your equipped items and inventory.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))

        addButtonHook1(rightPanel)

        // add the attributes button
        button = Button(" Attributes ")
        attributesButton = button
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener(ActionListener {
            if (!isEnabled) return@ActionListener
            if (locationSelection == null) playUI.showAttributes()
        })
        setTooltip(button, "Displays your attribute scores.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))

        // add the pass button
        button = Button(" Pass ")
        passButton = button
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener(ActionListener {
            if (!isEnabled) return@ActionListener
            if (locationSelection == null) playUI.player.endTurn()
        })
        setTooltip(button, "Manually ends your current game turn.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))

        // add the home-view button
        button = Button(" Home view ")
        homeViewButton = button
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener { areaView.homeViewpoint() }
        setTooltip(button, "Homes the view on you.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 20)))

        // add the commands button
        button = Button(" Commands ")
        commandsButton = button
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener {
            if (locationSelection == null) playUI.displayCommands()
        }
        setTooltip(button, "Lists the more obscure game commands.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))

        // add the hints button
        button = Button(" Hints ")
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener {
            if (locationSelection == null) playUI.displayHints()
        }
        setTooltip(button, "Provides helpful hints about gameplay.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))

        @Suppress("ConstantConditionIf") if (isDebugOn) {
            // add debug buttons, through which we can execute any code we need to help us debug
            fun addButton(i: Int) {
                val button1 = Button(" Debug $i")
                button1.alignmentX = 0.5f
                rightPanel.add(button1)
                button1.addActionListener {
                    if (i == 1) performDebugActionOne(playUI.player)
                    else performDebugActionTwo(playUI.player)
                }
            }
            addButton(1)
            rightPanel.add(Box.createRigidArea(Dimension(0, 3)))
            addButton(2)
        }

        rightPanel.add(Box.createRigidArea(Dimension(0, 20)))

        // add the hands label
        label = Label("Hands Items:", Fonts.mediumText)
        label.alignmentX = 0.5f
        rightPanel.add(label)

        rightPanel.add(Box.createRigidArea(Dimension(0, 1)))

        // add the hands-items-display
        handsItemsDisplay =
            HandsItemsDisplay { player.items.equippedItems.getHandsItems() }
        handsItemsDisplay.alignmentX = 0.5f
        rightPanel.add(handsItemsDisplay)

        rightPanel.add(Box.createRigidArea(Dimension(0, 10)))

        // add the hands-configs label
        label = Label("Configurations:", Fonts.mediumText)
        label.alignmentX = 0.5f
        rightPanel.add(label)

        rightPanel.add(Box.createRigidArea(Dimension(0, 1)))

        val panel2 = JPanel()
        panel2.background = backgroundColor
        panel2.layout = BoxLayout(panel2, BoxLayout.X_AXIS)
        rightPanel.add(panel2)

        // add the use-previous-hands-config button
        button = Button(" Prev ")
        previousHandsConfigButton = button
        button.alignmentX = 0.5f
        panel2.add(button)
        button.addActionListener { player.items.playerItems.usePreviousHandsConfig() }
        setTooltip(button, "Employs the previously-selected hands configuration.")

        panel2.add(Box.createRigidArea(Dimension(3, 0)))

        // add the use-hands-config button
        val useLabel = "Use"
        val setLabel = "Set"
        button = Button(" $useLabel ")
        useHandsConfigButton = button
        button.alignmentX = 0.5f
        panel2.add(button)
        button.addActionListener { playUI.selectHandsConfig() }
        setTooltip(button,
            "Lets you select what items you have in your hands from the configurations you have specified using <I>$setLabel</I>.")

        panel2.add(Box.createRigidArea(Dimension(3, 0)))

        // add the set-hands-config button
        button = Button(" $setLabel ")
        setHandsConfigButton = button
        button.alignmentX = 0.5f
        panel2.add(button)
        button.addActionListener { playUI.selectHandsConfigToSet() }
        setTooltip(button,
            "Lets you specify the items currently in your hands as a configuration which you may quickly reassume later by clicking <I>$useLabel</I>.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))

        rightPanel.add(Box.createVerticalGlue())

        // create the crosshairs mouse cursor
        val kit = Toolkit.getDefaultToolkit()
        crosshairsCursor =
            kit.createCustomCursor(loadImage("crosshairsCursor"), Point(15, 15), "")

        // if a bounds value for this frame is available, use it
        val bounds = windowLayout.playFrameBounds
        if (bounds != null) setBounds(bounds)

        playerNameLabel.text = player.name
    }

    protected open fun addButtonHook1(rightPanel: JPanel) {}

    /**
     * Sets the given button's tooltip to the given text, stylized using html.
     */
    protected fun setTooltip(button: Button, text: String) {
        button.hasTooltipImpl.setTooltipText(text, 25)
    }

    override fun setVisible(visible: Boolean) {
        // if this play-frame is no longer to be visible, and is not maximized
        val maximized = (extendedState and MAXIMIZED_BOTH) > 0
        if (!visible && !maximized) {
            // store this frame's bounds
            windowLayout.playFrameBounds = bounds
        }

        super.setVisible(visible)
    }

    override fun setUIEnabled(enable: Boolean) {
        SwingUtilities.invokeLater {
            areaView.panel.isEnabled = enable
            itemsButton!!.isEnabled = enable
            attributesButton!!.isEnabled = enable
            commandsButton!!.isEnabled = enable
            passButton!!.isEnabled = enable
            homeViewButton!!.isEnabled = enable
            previousHandsConfigButton!!.isEnabled = enable
            useHandsConfigButton!!.isEnabled = enable
            setHandsConfigButton!!.isEnabled = enable

            enableGameSpecificButtons(enable)
        }
    }

    protected open fun enableGameSpecificButtons(enable: Boolean) {}

    override fun reset() {
        locationSelection = null
        locationSelectionPrompt = null
        statusMessagesDisplay.clear()
    }
}

@ExtensionPoint
var createPlayerVitalStatsDisplay: (player: Player) -> PlayerVitalStatsDisplay =
    ::PlayerVitalStatsDisplay