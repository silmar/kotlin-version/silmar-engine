package silmarengine.gui

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * An instance of the user clicking on the area-view for the purpose of selecting
 * a location as part of completing some task.
 */
class LocationSelection(
    /**
     * Informs this selection that the given location has been selected for the given player.
     */
    val onSelection: (location: PixelPoint, player: Player) -> Unit,
    /**
     * Returns whether the location selected must be nearby to the player.
     */
    val isNearbyOnly: Boolean = false
)