package silmarengine.gui.dialogs

import silmarengine.gui.widgets.InputStringPanel
import silmarengine.programName
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.WindowConstants

/**
 * A dialog whose only contents is an input-string-panel.
 */
class InputStringDialog(parent: JFrame, message: String) : JDialog(parent, programName) {
    /**
     * The input-string-panel which constitutes the UI of this dialog.
     */
    var inputStringPanel: InputStringPanel
        private set

    init {
        isModal = true

        setSize(300, 170)
        isResizable = false
        setLocationRelativeTo(owner)

        // add the input-string-panel
        inputStringPanel = InputStringPanel(message) { isVisible = false }
        contentPane.add(inputStringPanel)

        // prevent this dialog's close button from working
        defaultCloseOperation = WindowConstants.DO_NOTHING_ON_CLOSE
    }

    companion object {
        /**
         * Creates and displays an input-string-dialog with the given message and the given
         * parent frame.
         *
         * @return      The string entered into the dialog.
         */
        fun show(parent: JFrame, message: String): String? {
            val dialog = InputStringDialog(parent, message)
            dialog.isVisible = true
            return dialog.inputStringPanel.inputString
        }
    }
}

