package silmarengine.gui.dialogs

import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dialog
import java.awt.Frame
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*
import javax.swing.border.EmptyBorder
import javax.swing.border.LineBorder

/**
 * A tooltip-like dialog that is dismissed when the user clicks on it.
 */
class InfoDialog : JDialog {

    constructor(owner: Dialog, info: String) : super(owner, true) {
        init(info)
    }

    constructor(owner: Frame, info: String) : super(owner, true) {
        init(info)
    }

    /**
     * Initializes this dialog.
     *
     * @param info      The info-string to display in this dialog.
     */
    private fun init(info: String) {
        isUndecorated = true

        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color(255, 255, 208)

        val panel = JPanel()
        panel.isOpaque = false
        panel.border =
            BorderFactory.createCompoundBorder(LineBorder(Color.black, 1, true),
                EmptyBorder(5, 5, 5, 5))
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add the info-label
        val label =
            JLabel("<html><p style='color:black;font:12pt sansserif;text-align:left'>$info<br><font style='font:10pt'>(click to dismiss)")
        label.alignmentX = 0.5f
        panel.add(label)

        // when the info-label is clicked
        label.addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                this@InfoDialog.isVisible = false
            }
        })

        pack()

        setLocationRelativeTo(owner)
    }
}
