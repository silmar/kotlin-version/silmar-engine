package silmarengine.gui.dialogs

import silmarengine.WorkThread
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.buyService
import silmarengine.contexts.gameplay.entities.talkers.ServiceSeller
import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import java.awt.BorderLayout
import java.awt.Component
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * The dialog displayed when the player is offered the opportunity to purchase services
 * from a services-seller.
 *
 * Note: This is currently coded such that each instance should be used only once,
 * and then discarded.
 */
class ServicesDialog(
    owner: JFrame, player: Player, seller: ServiceSeller, greeting: String?
) : JDialog(owner) {

    init {
        // configure this dialog
        val pane = contentPane
        pane.background = owner.contentPane.background
        title = "Buy Services"
        isModal = true
        setSize(300, 300)
        pane.layout = BorderLayout()
        setLocationRelativeTo(owner)

        // add the main panel
        val mainPanel = JPanel()
        mainPanel.border = EmptyBorder(5, 5, 5, 5)
        mainPanel.isOpaque = false
        mainPanel.layout = BoxLayout(mainPanel, BoxLayout.Y_AXIS)
        pane.add(mainPanel, BorderLayout.CENTER)

        // if a greeting was supplied
        if (greeting != null) {
            // add a text-area containing the greeting
            val label = Label("<html>$greeting</html>", Fonts.mediumText)
            label.font = Fonts.mediumText
            label.border = EmptyBorder(5, 5, 5, 5)
            label.preferredSize = Dimension(300, 100)
            label.maximumSize = Dimension(300, 300)
            mainPanel.add(label)
        }

        mainPanel.add(Box.createRigidArea(Dimension(0, 5)))

        // add the buttons panel
        val buttonsPanel = JPanel()
        buttonsPanel.isOpaque = false
        buttonsPanel.layout = GridLayout(0, 1, 0, 1)
        mainPanel.add(buttonsPanel)

        // for each service for sale
        buttonsPanel.removeAll()
        seller.servicesForSale.forEach { service ->
            // add a row panel for this service
            val rowPanel = JPanel()
            rowPanel.alignmentX = Component.CENTER_ALIGNMENT
            rowPanel.isOpaque = false
            buttonsPanel.add(rowPanel)

            // add to the row panel a button to buy this service; disable it
            // if the service can't currently be bought
            val price = seller.getServicePrice(service, player)
            val button = Button(" ${service.name} ")
            button.isEnabled = price > 0
            rowPanel.add(button)

            // when the button is clicked
            button.addActionListener {
                // on the work-thread, as we are currently on the UI thread
                WorkThread.queueTask {
                    // buy the associated service
                    player.buyService(service, seller)
                }

                // hide this dialog
                isVisible = false
            }

            // add to the row panel a label for the service price, if the
            // service can currently be bought
            val priceLabel = Label("for $price gold", Fonts.mediumText)
            if (price > 0) rowPanel.add(priceLabel)
        }

        pack()
    }
}
