package silmarengine.gui.dialogs

import silmarengine.WorkThread
import silmarengine.annotations.ExtensionPoint
import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.programVersion
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.URL
import java.util.*
import javax.swing.*
import javax.swing.border.EmptyBorder

@ExtensionPoint
lateinit var websiteUrl: String

/**
 * The dialog that's displayed when the user wishes to check the program's website online to
 * see if there's a newer version of the game available.
 */
class CheckForNewerVersionDialog(owner: JFrame) : JDialog(owner) {
    /**
     * Displays the status of the version-checking process.
     */
    private var statusLabel: JLabel? = null

    init {
        // configure this dialog
        val pane = contentPane
        pane.background = owner.contentPane.background
        title = title
        isModal = true
        setSize(100, 100)
        isResizable = false
        pane.layout = BorderLayout()

        // have this dialog close when its close button is pressed
        defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE

        // add the main panel
        val panel = JPanel()
        panel.border = EmptyBorder(5, 5, 5, 5)
        panel.isOpaque = false
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // this makes the dialog at least a certain width, so we know that the full width
        // of its title will be displayed, even when the dialog's contents aren't that wide
        panel.add(Box.createRigidArea(Dimension(300, 0)))

        // add the label that describes what's going on
        val label = Label("Placeholder", Fonts.mediumText)
        statusLabel = label
        label.foreground = Color.black
        panel.add(label)

        // size and center this dialog
        pack()
        setLocationRelativeTo(owner)

        // add this listener
        addWindowListener(object : WindowAdapter() {
            // when this dialog is first shown
            override fun windowOpened(e: WindowEvent?) {
                // check for a newer version in a new task, since we're on the UI thread
                WorkThread.queueTask { checkForNewerVersion() }
            }
        })
    }

    /**
     * Accesses the number of the latest version of the game program from its website, then
     * compares it to that of this running program.  If there's a newer version, this
     * prompts to user to download it from the website.
     */
    private fun checkForNewerVersion() {
        updateStatusText("Connecting to this game's website...")

        // open a reader on the website file containing the latest version number
        val fileUrl = URL("$websiteUrl/version.txt")
        val `in`: BufferedReader
        try {
            `in` = BufferedReader(InputStreamReader(fileUrl.openStream()))
        } catch (e: IOException) {
            updateStatusText("Website version-number file could not be accessed.")
            e.printStackTrace()
            return
        }

        try {
            // get the version number from the file
            val latest: String
            try {
                latest = `in`.readLine()
                `in`.close()
            } catch (e: IOException) {
                updateStatusText(
                    "Website version-number file found, but version number could not be read.")
                e.printStackTrace()
                return
            }

            // create tokenizers on both the latest- and current-version strings
            var newer = false
            val current = programVersion
            val latestTokenizer = StringTokenizer(latest, ". ")
            val currentTokenizer = StringTokenizer(current, ". ")

            // for each of the three parts of both version numbers
            for (i in 0..2) {
                // if the latest version number part is greater than the current version number part
                val latestNum = Integer.parseInt(latestTokenizer.nextToken())
                val currentNum = Integer.parseInt(currentTokenizer.nextToken())
                if (latestNum > currentNum) {
                    // there is a newer version
                    newer = true
                    break
                }

                // the only other possibility is that the two number-parts are equal, so we
                // should continue with the next pair
            }

            // if a new version is available
            if (newer) {
                // inform the user to visit the website to get the newest version
                updateStatusText(
                    "<html><p style='color:black;font-size:12pt;text-align:center'>A newer version is available!<br><br>Please visit the game's website at<br><b style='font-size:14pt'>$websiteUrl</b><br>to view the version history and/or<br>download the newest version.<br><br>(The above link has been copied to your system's<br>clipboard, for quick pasting into your browser.)")

                // copy the website link to the system clipboard
                val clipboard = Toolkit.getDefaultToolkit().systemClipboard
                val selection = StringSelection(websiteUrl)
                clipboard.setContents(selection, selection)
            }
            else {
                // tell the user their version is up-to-date
                updateStatusText("Your current version is the latest available!")
            } // otherwise
        }
        // if anything went wrong in the above block
        catch (e: Exception) {
            // inform the user
            updateStatusText("The version numbers could not be compared.")
            e.printStackTrace()

            // we don't want an exception here to stop the game from working
        }
    }

    /**
     * Updates the status label with the given text, then resizes and re-centers this dialog.
     */
    private fun updateStatusText(text: String) {
        statusLabel!!.text = text
        statusLabel!!.parent.invalidate()
        pack()
        setLocationRelativeTo(owner)
    }

    companion object {
        /**
         * The title of this dialog.
         */
        const val title = "Check website for newer version"
    }
}
