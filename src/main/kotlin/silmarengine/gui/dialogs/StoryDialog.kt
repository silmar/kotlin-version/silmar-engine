package silmarengine.gui.dialogs

import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.ScrollingTextPanel
import silmarengine.programName
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * A dialog that displays the given story text in a slowly scrolling manner.
 */
class StoryDialog(parent: JFrame, storyText: String, buttonText: String) :
    JDialog(parent, programName) {
    init {
        isModal = true
        setSize(400, 240)
        isResizable = false

        // don't allow this dialog's close button to work
        defaultCloseOperation = WindowConstants.DO_NOTHING_ON_CLOSE

        val pane = contentPane
        pane.background = Color.black
        pane.layout = BorderLayout()

        // add the main panel of the dialog
        val panel = JPanel()
        panel.border = EmptyBorder(5, 5, 5, 5)
        panel.isOpaque = false
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add the story scrolling-text-panel
        val panel2 = ScrollingTextPanel(storyText)
        panel.add(panel2)

        panel.add(Box.createRigidArea(Dimension(0, 15)))

        // add the begin-game button
        val button = Button(" $buttonText ")
        panel.add(button)
        button.addActionListener { isVisible = false }

        panel.add(Box.createRigidArea(Dimension(0, 5)))

        pack()
        setLocationRelativeTo(owner)
    }
}

