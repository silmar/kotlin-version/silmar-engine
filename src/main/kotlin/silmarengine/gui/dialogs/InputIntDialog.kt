package silmarengine.gui.dialogs

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import java.awt.Color
import java.awt.Dimension
import java.awt.Window
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.text.ParseException
import java.util.*
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * A modal dialog that displays a text field with a descriptive label and an 'OK'
 * and a "Cancel" button. A number within a given range is expected to be entered into
 * the text field.
 */
class InputIntDialog : JDialog {

    /**
     * See Result inner class.
     */
    val result = Result()

    /**
     * The label that displays this dialog's prompting message to the user.
     */
    private var messageLabel: JLabel? = null

    /**
     * The model object for this dialog's spinner.
     */
    private lateinit var spinnerModel: SpinnerNumberModel

    constructor(parent: JFrame) : super(parent) {
        build()
    }

    constructor(parent: JDialog) : super(parent) {
        build()
    }

    /**
     * Builds this dialog.
     */
    private fun build() {
        isModal = true
        isResizable = false

        val pane = contentPane
        val panel = JPanel()
        pane.add(panel)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        panel.border = EmptyBorder(10, 10, 10, 10)
        panel.background = Color.black

        // add the message
        messageLabel = Label("message", Fonts.mediumText)
        val label = messageLabel
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        val panel1 = JPanel()
        panel1.isOpaque = false
        panel1.layout = BoxLayout(panel1, BoxLayout.X_AXIS)
        panel.add(panel1)

        // create the spinner model
        spinnerModel = SpinnerNumberModel()
        val model = spinnerModel

        // add the min button
        var button = Button(" Min ")
        panel1.add(button)
        button.addActionListener { model.value = model.minimum }

        panel1.add(Box.createRigidArea(Dimension(5, 0)))

        // add the spinner
        val spinner = Spinner(model)
        spinner.font = Fonts.mediumText
        panel1.add(spinner)

        panel1.add(Box.createRigidArea(Dimension(5, 0)))

        // add the max button
        button = Button(" Max ")
        panel1.add(button)
        button.addActionListener { model.value = model.maximum }

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        val panel2 = JPanel()
        panel2.isOpaque = false
        panel.add(panel2)

        // add the ok button
        button = Button(" Ok ")
        panel2.add(button)
        button.addActionListener {
            // commit the value shown in the spinner to the spinner's model
            // (otherwise, if the player has typed in a number, but has not
            // hit "enter", the number that was previously in the spinner
            // will be the one that gets reported)
            try {
                spinner.commitEdit()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            // remember the value input as part of this dialog's result
            result.valueEntered = (spinner.value as Int).toInt()

            isVisible = false
        }

        // add the cancel button
        button = Button(" Cancel ")
        panel2.add(button)
        button.addActionListener {
            result.cancelled = true

            isVisible = false
        }

        // make this dialog's close button do the same thing as its cancel button
        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(e: WindowEvent?) {
                result.cancelled = true
            }
        })
    }

    /**
     * Sets this dialog's attributes to facilitate an imminent number selection by the user.
     *
     * See doShow().
     */
    private fun init(message: String, min: Int, max: Int, defaultValue: Int) {
        result.init()

        messageLabel!!.text = message

        spinnerModel.maximum = max
        spinnerModel.minimum = min
        spinnerModel.value = defaultValue
        spinnerModel.stepSize = if (max - min <= 30) 1 else (max - min) / 10

        pack()

        setLocationRelativeTo(owner)
    }

    /**
     * The result of the user interacting with this dialog.
     */
    class Result {

        /**
         * Whether the user hit the cancel button to dismiss this dialog.
         */
        var cancelled: Boolean = false

        /**
         * The integer the user entered into this dialog.
         */
        var valueEntered: Int = 0

        /**
         * Initializes this object's values.
         */
        fun init() {
            cancelled = false
            valueEntered = 0
        }
    }

    /**
     * This extends JSpinner just to override it's getMaximumSize method.
     */
    private inner class Spinner(model: SpinnerModel) : JSpinner(model) {

        override fun getMaximumSize(): Dimension {
            // without this, the spinner will expand to fit the available width of its container.
            val size = preferredSize
            return Dimension(size.width + 20, size.height)
        }

        override fun getMinimumSize(): Dimension {
            return maximumSize
        }
    }

    companion object {
        /**
         * A mapping to instances of this class from the parent frame or dialog with which they
         * are being reused.
         */
        private val showInstances = HashMap<Window, InputIntDialog>()

        /**
         * Creates and displays an input-int-dialog with the given message, the given
         * parent frame, the given min and max of the input range, and the given default
         * input value.  Only one such dialog may be displayed at any one time.
         */
        fun show(
            parent: Window, message: String, min: Int, max: Int, defaultValue: Int
        ): Result {
            val dialog = getInstance(parent)
            dialog.init(message, min, max, defaultValue)
            dialog.isVisible = true
            return dialog.result
        }

        /**
         * Returns the instance of this class being reused for the given parent.
         */
        private fun getInstance(parent: Window): InputIntDialog {
            // if we don't yet have an instance of this class to reuse for the given parent
            var dialog: InputIntDialog? = showInstances[parent]
            if (dialog == null) {
                // create an instance to reuse, and associate it with its parent
                dialog = if (parent is JFrame) InputIntDialog(parent)
                else InputIntDialog(parent as JDialog)
                showInstances[parent] = dialog
            }

            return dialog
        }
    }
}
