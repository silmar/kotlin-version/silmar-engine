package silmarengine.gui.dialogs

import java.awt.Frame
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent

//@formatter:off
private const val commandsText =
    "[Click] view to accomplish most actions<br/>" +
    "[Click] on outer view borders to shift view in a fixed direction.<br/>" +
    "[Shift + drag] view to shift view freely.<br/>" +
    "[Right click] view for context actions for that location."
//@formatter:on

/**
 * A modal dialog that lists various commands the user can employ while playing the game.
 */
class CommandsDialog(parent: Frame) : MessageDialog(parent, commandsText,
    lineLength = null) {

    init {
        title = "Commands"
        isResizable = false

        addWindowListener(object : WindowAdapter() {
            override fun windowOpened(e: WindowEvent?) {
                setLocationRelativeTo(parent)
            }
        })
    }
}

