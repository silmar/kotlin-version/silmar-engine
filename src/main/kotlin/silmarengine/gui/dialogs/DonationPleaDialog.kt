package silmarengine.gui.dialogs

import silmarengine.gui.widgets.Button
import silmarengine.storeBooleanConfigurationValue
import java.awt.Color
import java.awt.Desktop
import java.awt.Dimension
import java.awt.Frame
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.net.URI
import javax.swing.*
import javax.swing.border.EmptyBorder

class DonationPleaDialog(parent: Frame, message: String, websiteURI: String,
    doesKeyEventSignalDonation: (e: KeyEvent) -> Boolean) : JDialog(parent) {
    init {
        isModal = true

        val pane = contentPane
        val panel = JPanel()
        pane.add(panel)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        panel.border = EmptyBorder(10, 10, 10, 10)
        panel.background = Color.black

        defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE

        // add the message
        val textPane = JTextPane()
        textPane.isEditable = false
        textPane.background = Color.BLACK
        textPane.contentType = "text/html"
        val header = "<html><div style='color:white;text-align:left'>"
        val closingTags = "</div></html>"
        textPane.text = header + message + closingTags
        textPane.preferredSize = Dimension(400, 110)
        panel.add(textPane)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        panel.add(Box.createVerticalGlue())

        // add the buttons panel
        val buttonsPanel = JPanel()
        buttonsPanel.isOpaque = false
        buttonsPanel.layout = BoxLayout(buttonsPanel, BoxLayout.X_AXIS)
        panel.add(buttonsPanel)

        // add the exit button
        val exitButton = Button(" Exit ")
        buttonsPanel.add(exitButton)
        exitButton.addActionListener { isVisible = false }

        buttonsPanel.add(Box.createRigidArea(Dimension(5, 0)))

        // add the visit-website button
        val visitButton = Button(" Visit website ")
        buttonsPanel.add(visitButton)
        visitButton.addActionListener {
            if (Desktop.isDesktopSupported()) {
                val desktop = Desktop.getDesktop()
                if (desktop.isSupported(Desktop.Action.BROWSE)) desktop.browse(
                    URI(websiteURI))
            }
        }

        pack()

        setLocationRelativeTo(owner)

        // if the user types the keystroke we were given which indicates they have donated
        textPane.addKeyListener(object : KeyAdapter() {
            override fun keyTyped(e: KeyEvent?) {
                super.keyTyped(e)
                if (e != null && doesKeyEventSignalDonation(e)) {
                    // store that this dialog should not be displayed again
                    storeBooleanConfigurationValue(hasDonatedPropertyKey, true)

                    isVisible = false
                }
            }
        })
    }
}

const val hasDonatedPropertyKey = "hasDonated"