package silmarengine.gui.dialogs

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import silmarengine.util.html.breakTextAtSpaces
import silmarengine.util.html.labelHeader
import java.awt.Color
import java.awt.Dialog
import java.awt.Dimension
import java.awt.Frame
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * A modal dialog that displays a text message with an 'OK' button.
 */
open class MessageDialog : JDialog {

    /**
     * The label that displays the message presented by this dialog.
     */
    protected lateinit var label: Label

    constructor(parent: Dialog, message: String) : super(parent) {
        build(message)
    }

    constructor(parent: Frame, message: String, lineLength: Int?) : super(parent) {
        build(message, lineLength = lineLength)
    }

    constructor(parent: Frame, message: String) : super(parent) {
        build(message)
    }

    constructor(parent: Frame, message: String, mayClose: Boolean) : super(parent) {
        build(message, mayClose)
    }

    constructor(message: String) {
        build(message)
    }

    constructor(message: String, lineLength: Int?) {
        build(message, lineLength = lineLength)
    }

    /**
     * Builds the structure of this dialog, incorporating the given message.
     *
     * @param mayClose      Whether this dialog is allowed to be closed by the user.
     */
    private fun build(message: String, mayClose: Boolean = true, lineLength: Int? = 50) {
        isModal = true
        isResizable = false

        val pane = contentPane
        val panel = JPanel()
        pane.add(panel)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        panel.border = EmptyBorder(10, 10, 10, 10)
        panel.background = Color.black

        defaultCloseOperation = if (mayClose) WindowConstants.DISPOSE_ON_CLOSE
        else WindowConstants.DO_NOTHING_ON_CLOSE

        // add a label to display the message
        val formattedMessage =
            if (lineLength != null) breakTextAtSpaces(message, lineLength) else message
        label = Label(labelHeader + formattedMessage, Fonts.mediumPlus)
        panel.add(label)

        // this makes a message with no 'ok' button more centered within the dialog
        panel.add(Box.createRigidArea(Dimension(0, 5)))

        // if this dialog may be closed
        if (mayClose) {
            panel.add(Box.createRigidArea(Dimension(0, 15)))

            // add the ok button
            val button = Button(" Ok ")
            panel.add(button)
            button.addActionListener { isVisible = false }
        }

        pack()

        setLocationRelativeTo(owner)
    }

    companion object {

        fun show(message: String) {
            show(null, null, message)
        }

        fun show(parent: Frame, message: String) {
            show(parent, null, message)
        }

        fun show(parent: Dialog, message: String) {
            show(null, parent, message)
        }

        /**
         * Creates and displays a message dialog with the given message and the given
         * parent dialog or frame.
         */
        fun show(frameParent: Frame?, dialogParent: Dialog?, message: String) {
            val dialog: Dialog = when {
                frameParent != null -> MessageDialog(frameParent, message)
                dialogParent != null -> MessageDialog(dialogParent, message)
                else -> MessageDialog(message)
            }

            dialog.isVisible = true
        }
    }
}
