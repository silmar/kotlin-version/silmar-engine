package silmarengine.gui.dialogs

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import silmarengine.util.html.labelHeader
import java.awt.Color
import java.awt.Dialog
import java.awt.Dimension
import java.awt.Frame
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * A modal dialog that displays a text message with an "OK" and a "Cancel" button.
 */
class OkCancelDialog : JDialog {
    /**
     * Whether the user clicked the cancel button to dismiss this dialog.
     */
    var isCancelled = false
        private set

    constructor(parent: Dialog, message: String) : super(parent) {
        build(message)
    }

    constructor(parent: Frame, message: String) : super(parent) {
        build(message)
    }

    /**
     * Builds the structure of this dialog, incorporating the given message.
     */
    private fun build(message: String) {
        isModal = true

        val pane = contentPane
        val panel = JPanel()
        pane.add(panel)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        panel.border = EmptyBorder(10, 10, 10, 10)
        panel.background = Color.black

        defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE

        // add the message
        val label = Label(message, Fonts.mediumPlus)
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        panel.add(Box.createVerticalGlue())

        // add the buttons panel
        val buttonsPanel = JPanel()
        buttonsPanel.isOpaque = false
        buttonsPanel.layout = BoxLayout(buttonsPanel, BoxLayout.X_AXIS)
        panel.add(buttonsPanel)

        // add the ok button
        var button = Button(" Ok ")
        buttonsPanel.add(button)
        button.addActionListener { isVisible = false }

        // when this dialog is closed
        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(e: WindowEvent?) {
                // consider this as a choice to cancel
                isCancelled = true
            }
        })

        buttonsPanel.add(Box.createRigidArea(Dimension(5, 0)))

        // add the cancel button
        button = Button(" Cancel ")
        buttonsPanel.add(button)
        button.addActionListener {
            isCancelled = true
            isVisible = false
        }

        pack()

        setLocationRelativeTo(owner)
    }

    companion object {
        fun show(parent: Dialog, message: String): Boolean {
            return show(null, parent, message)
        }

        fun show(parent: Frame, message: String): Boolean {
            return show(parent, null, message)
        }

        /**
         * Creates and displays an ok-cancel dialog with the given message.
         *
         * @return  Whether the user clicked the ok button (vs. the cancel button).
         */
        fun show(frameParent: Frame?, dialogParent: Dialog?, message: String): Boolean {
            val headerWithMessage = labelHeader + message

            val dialog: OkCancelDialog
            dialog = when {
                frameParent != null -> OkCancelDialog(frameParent, headerWithMessage)
                dialogParent != null -> OkCancelDialog(dialogParent, headerWithMessage)
                else -> return false
            }

            dialog.isVisible = true
            return !dialog.isCancelled
        }
    }
}
