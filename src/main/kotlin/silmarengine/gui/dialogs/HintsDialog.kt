package silmarengine.gui.dialogs

import silmarengine.annotations.ExtensionPoint
import java.awt.Color
import java.awt.Frame
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JScrollPane
import javax.swing.border.LineBorder

@ExtensionPoint
lateinit var hintsText: String

/**
 * A modal dialog that displays hints and advice about the game's play, and an "OK" button.
 */
class HintsDialog(parent: Frame) : MessageDialog(parent, hintsText) {

    init {
        setSize(400, 300)
        title = "Hints"
        isResizable = false

        addWindowListener(object : WindowAdapter() {
            override fun windowOpened(e: WindowEvent?) {
                setLocationRelativeTo(parent)
            }
        })

        // remove the message-label from its containing panel
        val container = label.parent
        container.remove(label)

        // put the message-label into a scroll-panel since the hints text it will contain
        // will be too big for this dialog to display at once
        val scrollPane = JScrollPane(label)
        scrollPane.border = LineBorder(Color.lightGray, 2)
        val bar = scrollPane.verticalScrollBar
        bar.unitIncrement = 16
        bar.blockIncrement = 160
        label.isOpaque = true
        label.background = Color.black

        // add the scroll-pane where the message-label was in its container
        scrollPane.horizontalScrollBarPolicy = JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        container.add(scrollPane, 0)
    }
}