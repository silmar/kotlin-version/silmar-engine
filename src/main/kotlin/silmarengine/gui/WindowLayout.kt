package silmarengine.gui

import java.awt.Point
import java.awt.Rectangle
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*

/**
 * Stores the positions and/or bounds of the main windows shown
 * in the game's UI, so that the next time the game is loaded, the windows may
 * be repositioned automatically to the previous layout.
 */
class WindowLayout(
    var playFrameBounds: Rectangle? = null,
    var useOrDropItemsDialogLocation: Point? = null,
    var playerAttributesDialogLocation: Point? = null,
    var powersDialogLocation: Point? = null
)

val windowLayout = loadWindowLayout() ?: WindowLayout()

fun WindowLayout.save() {
    val props = Properties()
    props.storeRectangle("playFrameBounds", playFrameBounds)
    props.storePoint("useOrDropItemsDialogLocation", useOrDropItemsDialogLocation)
    props.storePoint("playerAttributesDialogLocation", playerAttributesDialogLocation)
    props.storePoint("powersDialogLocation", powersDialogLocation)
    val outputStream = FileOutputStream(fileName)
    props.store(outputStream, "Silmar window layout")
    outputStream.close()
}

private const val fileName = "layout"

private fun loadWindowLayout(): WindowLayout? {
    val props = Properties()
    return try {
        val inputStream = FileInputStream(fileName)
        props.load(inputStream)
        inputStream.close()
        WindowLayout(props.getRectangle("playFrameBounds"),
            props.getPoint("useOrDropItemsDialogLocation"),
            props.getPoint("playerAttributesDialogLocation"),
            props.getPoint("powersDialogLocation"))
    } catch (e: Exception) {
        null
    }
}

/**
 * Point-related functions.
 */
private fun Point.serialize(): String = "$x,$y"

private fun Properties.storePoint(name: String, p: Point?) =
    if (p != null) setProperty(name, p.serialize()) else Unit

private fun deserializePoint(s: String): Point {
    val parts = s.split(",")
    return Point(parts[0].toInt(), parts[1].toInt())
}

private fun Properties.getPoint(name: String): Point? {
    val prop = getProperty(name, null) ?: return null
    return deserializePoint(prop)
}

/**
 * Rectangle-related functions.
 */
private fun Rectangle.serialize(): String = "$x,$y,$width,$height"

private fun Properties.storeRectangle(name: String, r: Rectangle?) =
    if (r != null) setProperty(name, r.serialize()) else Unit

private fun deserializeRectangle(s: String): Rectangle {
    val parts = s.split(",")
    return Rectangle(parts[0].toInt(), parts[1].toInt(), parts[2].toInt(),
        parts[3].toInt())
}

private fun Properties.getRectangle(name: String): Rectangle? {
    val prop = getProperty(name, null) ?: return null
    return deserializeRectangle(prop)
}

