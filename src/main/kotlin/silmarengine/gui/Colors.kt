package silmarengine.gui

import java.awt.Color

/**
 * Colors shared between different entities in the game.
 */
object Colors {

    var lighterGray = Color(224, 224, 224)
}