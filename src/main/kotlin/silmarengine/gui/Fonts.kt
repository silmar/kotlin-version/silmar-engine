package silmarengine.gui

import java.awt.Font

/**
 * Various fonts used throughout the game's user interface.
 */
object Fonts {

    const val textFont = "Serif"
    const val numberFont = "SansSerif"

    val mediumText = Font(textFont, Font.PLAIN, 14)
    val mediumTextBold = Font(textFont, Font.BOLD, 14)
    val mediumPlus = Font(textFont, Font.PLAIN, 16)
    val smallText = Font(textFont, Font.PLAIN, 12)

    val mediumNumber = Font(numberFont, Font.PLAIN, 14)

    val viewMessage = Font("Dialog", Font.BOLD, 12)
    val tooltip = Font("Dialog", Font.PLAIN, 12)
}
