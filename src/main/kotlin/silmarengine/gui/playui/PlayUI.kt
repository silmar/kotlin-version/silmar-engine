package silmarengine.gui.playui

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.dialogs.ServicesDialog
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.contexts.playeritemsmanagement.ui.*
import java.awt.Color

/**
 * Administers the user interface presented to the user while they are playing a game.
 * This UI consists of the play-frame and many dialogs which serve different purposes. The
 * administration mostly consists of listening for events reported by various elements in
 * the game (especially the player entity and the current area), and distributing those
 * events to interested elements within the UI.
 */
interface PlayUI {
    /**
     * The player entity in the game currently being presented by this UI.
     */
    val player: Player

    val playFrame: PlayFrame

    /**
     * Whether this play-UI's user is currently allowed to specify actions for its
     * player.
     */
    val playerCanAct: Boolean

    /**
     * Has this play-UI show its display of the player's items.
     */
    fun showItems()

    /**
     * Has this play-UI show its display of the player's attributes.
     */
    fun showAttributes()

    /**
     * Has this play-UI display a list of the game-play commands to the user.
     */
    fun displayCommands()

    /**
     * Has this play-UI display a list of game-play hints to the user.
     */
    fun displayHints()

    /**
     * Has this play-UI prompt its user to select a hands-config for its player to use.
     */
    fun selectHandsConfig()

    /**
     * Has this play-UI prompt its user to select a hands-config-slot to be filled
     * with its player's current hands-config in use.
     */
    fun selectHandsConfigToSet()

    fun onRepairDialogCreated(dialog: RepairDialog)

    fun onBuyDialogCreated(dialog: BuyDialog)

    fun onSellDialogCreated(dialog: SellDialog)

    fun onDonateDialogCreated(dialog: DonateDialog)

    fun onIdentifyDialogCreated(dialog: IdentifyDialog)

    fun onServicesDialogCreated(dialog: ServicesDialog)

    companion object {
        /**
         * The background color to use in all play-UI frames and dialogs.
         */
        val backgroundColor = Color(0, 0, 0)
    }
}