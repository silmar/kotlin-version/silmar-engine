package silmarengine.gui.playui.extensions

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.buyItem
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.entities.talkers.ItemSeller
import silmarengine.gui.playui.PlayUI
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import silmarengine.contexts.playeritemsmanagement.ui.BuyDialog

fun PlayUI.showBuyDialog(seller: ItemSeller) {
    fun getPrice(pimItem: PIMItem): Int {
        val item = pimItem as Item
        return seller.getAdjustedItemPrice(item, item.getValue(ItemValueContext.BUY))
    }

    val dialog =
        BuyDialog(playFrame.frame, seller.greeting, seller.itemsForSale,
            { player.items.equippedItems }, { player.items.inventory.items },
            ::getPrice) { item, price ->
            player.buyItem(item as Item, price)
        }
    onBuyDialogCreated(dialog)
    dialog.isVisible = true
}