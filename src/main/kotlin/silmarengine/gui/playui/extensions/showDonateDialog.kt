package silmarengine.gui.playui.extensions

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.donateItem
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.ItemDonationReceiver
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.gui.playui.PlayUI
import silmarengine.contexts.playeritemsmanagement.ui.DonateDialog

fun PlayUI.showDonateDialog(receiver: ItemDonationReceiver) {
    val dialog =
        DonateDialog(playFrame.frame, receiver.greeting, { player.items.equippedItems },
            { player.items.inventory.items }, {
                (it as Item).getValue(ItemValueContext.EXPERIENCE)
            }) { item, quantity -> player.donateItem(item as Item, quantity) }
    onDonateDialogCreated(dialog)
    dialog.isVisible = true
}