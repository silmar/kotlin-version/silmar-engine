package silmarengine.gui.playui.extensions

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.repairItem
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.entities.talkers.ItemFixer
import silmarengine.gui.playui.PlayUI
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import silmarengine.contexts.playeritemsmanagement.ui.RepairDialog

fun PlayUI.showRepairDialog(fixer: ItemFixer) {
    fun getPrice(pimItem: PIMItem): Int {
        val item = pimItem as Item
        return fixer.getAdjustedItemPrice(item, item.getValue(ItemValueContext.REPAIR))
    }

    val dialog =
        RepairDialog(playFrame.frame, fixer.greeting, { player.items.equippedItems },
            { player.items.inventory.items }, { it.condition == ItemCondition.EXCELLENT },
            { fixer.getCanFixItem(it as Item) }, ::getPrice) { item, price ->
            player.repairItem(item as Item, price)
        }
    onRepairDialogCreated(dialog)
    dialog.isVisible = true
}