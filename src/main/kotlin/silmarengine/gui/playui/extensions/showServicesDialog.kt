package silmarengine.gui.playui.extensions

import silmarengine.contexts.gameplay.entities.talkers.ServiceSeller
import silmarengine.gui.dialogs.ServicesDialog
import silmarengine.gui.playui.PlayUI

fun PlayUI.showServicesDialog(seller: ServiceSeller) {
    val dialog = ServicesDialog(playFrame.frame, player, seller, seller.greeting)
    onServicesDialogCreated(dialog)
    dialog.isVisible = true
}