package silmarengine.gui.playui.extensions

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.identifyItem
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.ItemIdentifier
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.gui.playui.PlayUI
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import silmarengine.contexts.playeritemsmanagement.ui.IdentifyDialog

fun PlayUI.showIdentifyDialog(identifier: ItemIdentifier) {
    fun getPrice(pimItem: PIMItem): Int {
        val item = pimItem as Item
        return identifier.getAdjustedItemPrice(item,
            item.getValue(ItemValueContext.IDENTIFY))
    }

    val dialog = IdentifyDialog(playFrame.frame, identifier.greeting,
        { player.items.equippedItems }, { player.items.inventory.items },
        ::getPrice) { item, price ->
        player.identifyItem(item as Item, price)
    }
    onIdentifyDialogCreated(dialog)
    dialog.isVisible = true
}