package silmarengine.gui.playui.extensions

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.sellItem
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.Buyer
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.gui.playui.PlayUI
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import silmarengine.contexts.playeritemsmanagement.ui.SellDialog

fun PlayUI.showSellDialog(buyer: Buyer) {
    fun getPrice(pimItem: PIMItem): Int {
        val item = pimItem as Item
        return buyer.getAdjustedItemOffering(item, item.getValue(ItemValueContext.SELL))
    }

    val dialog =
        SellDialog(playFrame.frame, buyer.greeting, { player.items.equippedItems },
            { player.items.inventory.items }, ::getPrice) { item, quantity ->
            player.sellItem(item as Item, buyer, quantity)
        }
    onSellDialogCreated(dialog)
    dialog.isVisible = true
}