package silmarengine.gui.playui

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.PlayerListener
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.Attribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.appendAttributeInfo
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.onInventoryItemClicked
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onEquippedItemClicked
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.gui.dialogs.CommandsDialog
import silmarengine.gui.dialogs.HintsDialog
import silmarengine.gui.dialogs.ServicesDialog
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.frames.playframe.PlayFrame1
import silmarengine.gui.frames.playframe.extensions.*
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.playerattributedisplay.domain.model.AttributeValues
import silmarengine.contexts.playerattributedisplay.domain.model.PADAttribute
import silmarengine.contexts.playerattributedisplay.ui.AttributesDialog
import silmarengine.contexts.playeritemsmanagement.ui.*
import silmarengine.sounds.Sound
import silmarengine.sounds.Sounds

@Suppress("LeakingThis")
open class PlayUI1(override val player: Player) : PlayUI {
    override var playerCanAct: Boolean = false
        protected set(canAct) {
            field = canAct

            playFrame.setUIEnabled(canAct)
        }

    /**
     * A dialog that displays the allows the user to view, use, and/or drop the player's
     * currently equipped items and inventory items.
     */
    private val useOrDropItemsDialog: UseOrDropItemsDialog

    /**
     * A dialog that displays the current values of the player's attributes.
     */
    private val playerAttributesDialog: AttributesDialog

    /**
     * The dialog that is displayed when the player has an opportunity to buy services.
     */
    private var servicesDialog: ServicesDialog? = null

    override fun onServicesDialogCreated(dialog: ServicesDialog) {
        servicesDialog = dialog
    }

    /**
     * The dialog that is displayed when the player has an opportunity to donate some of its
     * items.
     */
    private var donateDialog: DonateDialog? = null

    override fun onDonateDialogCreated(dialog: DonateDialog) {
        donateDialog = dialog
    }

    /**
     * The dialog that is displayed when the player has an opportunity to identify some of its
     * items.
     */
    private var identifyDialog: IdentifyDialog? = null

    override fun onIdentifyDialogCreated(dialog: IdentifyDialog) {
        identifyDialog = dialog
    }

    /**
     * The dialog that is displayed when the player has an opportunity to repair some of its
     * items.
     */
    private var repairDialog: RepairDialog? = null

    override fun onRepairDialogCreated(dialog: RepairDialog) {
        repairDialog = dialog
    }

    /**
     * The dialog that is displayed when the player has an opportunity to buy items.
     */
    private var buyDialog: BuyDialog? = null

    override fun onBuyDialogCreated(dialog: BuyDialog) {
        run { buyDialog = dialog }
    }

    /**
     * The dialog that is displayed when the player has an opportunity to sell some of its
     * items.
     */
    private var sellDialog: SellDialog? = null

    override fun onSellDialogCreated(dialog: SellDialog) {
        sellDialog = dialog
    }

    /**
     * The dialog that is shown when this play-UI is to select a hands-config for use
     * by its player, or is to select a hands-config-slot to be filled by its player's
     * current hands-config in use.
     */
    private var handsConfigDialog: HandsConfigDialog? = null

    override val playFrame: PlayFrame = createPlayFrame()

    private val commandsDialog = CommandsDialog(playFrame.frame)

    private val hintsDialog = HintsDialog(playFrame.frame)

    init {
        player.addListener(createGameSpecificPlayerListener(this))

        // create the hands-config dialog
        handsConfigDialog =
            HandsConfigDialog(playFrame.frame, player.items.playerItems.handsConfigs,
                { player.items.playerItems.setHandsConfig(it) },
                { player.items.playerItems.tryToUseHandsConfig(it) })

        // create the player-items dialog
        useOrDropItemsDialog = UseOrDropItemsDialog(playFrame.frame,
            { player.items.playerItems.equippedItems },
            { player.items.playerItems.inventory.items },
            { item, rightClick -> player.onEquippedItemClicked(item, rightClick) },
            { event, rightClick, dialog ->
                player.onInventoryItemClicked(event, rightClick, dialog)
            })
        useOrDropItemsDialog.equippedItemsDisplay.onEquippedItemsChanged()
        useOrDropItemsDialog.inventoryDisplay.onItemsChanged()

        // create the player-attributes dialog
        val append = { attribute: PADAttribute, value: Int, to: StringBuffer ->
            appendAttributeInfo(Attribute.values()[attribute.ordinal], value, to)
        }
        playerAttributesDialog =
            AttributesDialog({ AttributeValues(player.attributeValues.immutableValues) },
                playFrame.frame, append)
    }

    protected open fun createPlayFrame() = PlayFrame1(this, player)

    override fun displayCommands() {
        commandsDialog.isVisible = true
    }

    override fun displayHints() {
        hintsDialog.isVisible = true
    }

    /**
     * Informs this play-UI that the composition of its player's equipped items has changed.
     */
    private fun onPlayerEquippedItemsChange() {
        useOrDropItemsDialog.equippedItemsDisplay.onEquippedItemsChanged()

        playFrame.handsItemsDisplay.onItemsChanged()

        sellDialog?.equippedItemsDisplay?.onEquippedItemsChanged()
        donateDialog?.equippedItemsDisplay?.onEquippedItemsChanged()
        repairDialog?.equippedItemsDisplay?.onEquippedItemsChanged()
        identifyDialog?.equippedItemsDisplay?.onEquippedItemsChanged()
    }

    /**
     * Has this play-UI tell all its player-equipped-items displays to update themselves.
     */
    private fun updateEquippedItemDisplays() {
        useOrDropItemsDialog.equippedItemsDisplay.updateItems()

        sellDialog?.equippedItemsDisplay?.updateItems()
        donateDialog?.equippedItemsDisplay?.updateItems()
        repairDialog?.equippedItemsDisplay?.updateItems()
        identifyDialog?.equippedItemsDisplay?.updateItems()
    }

    /**
     * Informs this play-UI that the composition of its player's inventory has changed.
     */
    private fun onPlayerInventoryChange() {
        useOrDropItemsDialog.inventoryDisplay.onItemsChanged()

        sellDialog?.inventoryDisplay?.onItemsChanged()
        donateDialog?.inventoryDisplay?.onItemsChanged()
        repairDialog?.inventoryDisplay?.onItemsChanged()
        buyDialog?.inventoryDisplay?.onItemsChanged()
        identifyDialog?.inventoryDisplay?.onItemsChanged()
    }

    /**
     * Has this play-UI tell all its player-inventory displays to update themselves.
     */
    private fun updateInventoryDisplays() {
        useOrDropItemsDialog.inventoryDisplay.updateItems()

        sellDialog?.inventoryDisplay?.updateItems()
        donateDialog?.inventoryDisplay?.updateItems()
        repairDialog?.inventoryDisplay?.updateItems()
        buyDialog?.inventoryDisplay?.updateItems()
        identifyDialog?.inventoryDisplay?.updateItems()
    }

    open inner class UIPlayerListener : PlayerListener {
        override fun onAreaLoading() {
            playFrame.onAreaLoading()
            playerCanAct = false
        }

        override fun onArrivedInArea() {
            playFrame.onPlayerArrivedAtArea(player)
            playerCanAct = true
        }

        override fun onCanActAgain() {
            playerCanAct = true
        }

        override fun onMustWaitToAct() {
            playerCanAct = false
        }

        override fun onItemEquipped(item: Item?) {
            // play an appropriate sound
            val sound = if (item != null) Sounds.itemEquipped else Sounds.itemUnequipped
            sound.play()

            onPlayerEquippedItemsChange()
        }

        override fun onVitalStatChanged() {
            playFrame.playerVitalStatsDisplay.onAttributeValueChanged()
        }

        override fun onItemAddedToInventory(item: Item) {
            onPlayerInventoryChange()
        }

        override fun onItemRemovedFromInventory(item: Item) {
            onPlayerInventoryChange()
        }

        override fun onMessageReceived(
            message: String, messageType: MessageType, duration: Int
        ) {
            playFrame.onPlayerReceivedMessage(message, messageType, duration)
        }

        override fun onHandsConfigChanged() {
            handsConfigDialog!!.handsConfigsDisplay.onConfigChanged()
        }

        override fun onCanMakeAttack() {
            playFrame.onPlayerCanMakeAttack(player)
        }

        override fun onUsedImmediateEffectItem() {
            useOrDropItemsDialog.isVisible = false
        }

        override fun onHandsConfigSet(index: Int) {
            handsConfigDialog!!.handsConfigsDisplay.onConfigChanged()
            Sounds.handsConfigSet.play()
        }

        override fun onBuyerLeft() {
            sellDialog!!.isVisible = false
        }

        override fun onLevelChanged() {}
        override fun onAttributeChanged(attribute: PlayerAttribute) {
            playFrame.playerVitalStatsDisplay.onAttributeValueChanged()
            playerAttributesDialog.attributesDisplay.onAttributeChanged(
                PADAttribute.values()[attribute.index])
        }

        override fun onSoundHeard(
            sound: Sound, location: PixelPoint, distance: PixelDistance
        ) {
            sound.play(distance)
        }

        override fun onHeldItemChanged() {
            onPlayerHeldItemChanged()
        }

        override fun onBecameBones(bones: Terrain) {
            playFrame.areaView.viewpointEntity = bones
        }

        override fun onDead() {
            playFrame.onPlayerDead()
        }
    }

    /**
     * Informs this play-UI that one of the items currently held by its player has changed
     * in some visible way.
     */
    private fun onPlayerHeldItemChanged() {
        // update all displays that might be showing the changed item
        updateInventoryDisplays()
        updateEquippedItemDisplays()
        handsConfigDialog!!.handsConfigsDisplay.updateItems()
        playFrame.handsItemsDisplay.updateItems()

        // since the item's quantity might have changed, update the player-vital-stats display,
        // which shows the player's encumbrance, gold, and movement rate
        playFrame.playerVitalStatsDisplay.onAttributeValueChanged()
    }

    override fun showAttributes() {
        playerAttributesDialog.isVisible = true
    }

    override fun showItems() {
        useOrDropItemsDialog.isVisible = true
    }

    private fun placeHandsConfigDialog() {
        val dialog = handsConfigDialog!!
        val frame = playFrame.frame
        dialog.setLocation(frame.x + frame.width - dialog.width,
            frame.y + frame.height - dialog.height)
    }

    override fun selectHandsConfig() {
        placeHandsConfigDialog()
        handsConfigDialog!!.display(false)
    }

    override fun selectHandsConfigToSet() {
        placeHandsConfigDialog()
        handsConfigDialog!!.display(true)
    }
}

@ExtensionPoint
var createGameSpecificPlayerListener: (playUI: PlayUI1) -> PlayerListener =
    { it.UIPlayerListener() }