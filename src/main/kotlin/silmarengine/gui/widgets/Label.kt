package silmarengine.gui.widgets

import silmarengine.gui.tooltips.HasTooltip
import silmarengine.gui.tooltips.HasTooltipImpl
import java.awt.Color
import java.awt.Font
import javax.swing.JLabel

/**
 * A centered label meant to be a subheading over some other panel.
 */
open class Label(text: String, textFont: Font) : JLabel(text, CENTER), HasTooltip {

    /**
     * Provides this component's tooltip implementation.
     */
    override var hasTooltipImpl: HasTooltipImpl = HasTooltipImpl(this)

    init {
        foreground = Color.white
        font = textFont
    }

    override fun getAlignmentX(): Float {
        return 0.5f
    }

    /**
     * Override this if the label's tooltip text is dynamic.
     */
    override fun updateTooltip() {}
}
    
