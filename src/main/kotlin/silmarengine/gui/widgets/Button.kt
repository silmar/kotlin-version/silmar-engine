package silmarengine.gui.widgets

import silmarengine.gui.Colors
import silmarengine.gui.Fonts
import silmarengine.gui.tooltips.HasTooltip
import silmarengine.gui.tooltips.HasTooltipImpl
import javax.swing.JButton
import javax.swing.border.BevelBorder

/**
 * A button stylized for this program.
 */
class Button(text: String) : JButton(text), HasTooltip {

    /**
     * Provides this component's tooltip implementation.
     */
    override val hasTooltipImpl = HasTooltipImpl(this)

    init {
        font = Fonts.mediumText
        border = BevelBorder(BevelBorder.RAISED)
        background = Colors.lighterGray
        alignmentX = 0.5f
    }

    override fun isRequestFocusEnabled(): Boolean {
        return false
    }

    /**
     * Override this if the button's tooltip text is dynamic.
     */
    override fun updateTooltip() {}
}

