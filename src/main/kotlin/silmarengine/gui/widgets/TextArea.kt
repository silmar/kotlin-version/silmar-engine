package silmarengine.gui.widgets

import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import javax.swing.JTextArea

/**
 * A text-area stylized for this game.
 */
class TextArea(text: String) : JTextArea(text, 4, 25) {

    init {
        foreground = Color.white
        background = Color.black
        font = Font("Serif", Font.PLAIN, 15)
        wrapStyleWord = true
        lineWrap = true
    }

    override fun getMaximumSize(): Dimension {
        return Dimension(super.getMaximumSize().width, preferredSize.height)
    }
}