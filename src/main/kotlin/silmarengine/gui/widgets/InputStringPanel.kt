package silmarengine.gui.widgets

import silmarengine.gui.Fonts
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JPanel
import javax.swing.JTextField
import javax.swing.border.EmptyBorder

/**
 * A panel that displays a text field with a descriptive label and an 'OK' button.
 */
class InputStringPanel(message: String, val onStringInput: () -> Unit) : JPanel() {

    /**
     * The text-field into which the user will enter the input-string.
     */
    private var textField: JTextField? = null

    /**
     * The string the user entered into this panel.
     */
    var inputString: String? = null
        private set

    init {
        layout = BorderLayout()
        background = Color.black

        val panel = JPanel()
        panel.border = EmptyBorder(10, 10, 10, 10)
        panel.isOpaque = false
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        add(panel, BorderLayout.CENTER)

        // add the message
        val label = Label(message, Fonts.mediumText)
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        // add a panel with a flow layout to put the text-field (below) into;
        // otherwise, the field will take up way too much space
        val panel1 = JPanel()
        panel1.isOpaque = false
        panel.add(panel1)

        // add the text-field
        textField = JTextField(10)
        val field = textField!!
        field.horizontalAlignment = JTextField.CENTER
        field.font = Fonts.mediumText
        panel1.add(field)
        field.addKeyListener(object : KeyAdapter() {
            override fun keyTyped(e: KeyEvent?) {
                if (e!!.keyChar == '\n') {
                    onTextEntered()
                }
            }
        })

        panel.add(Box.createRigidArea(Dimension(0, 20)))

        // add the ok button
        val button = Button(" Ok ")
        panel.add(button)
        button.addActionListener { onTextEntered() }

        // start the focus at this panel's text-field
        addComponentListener(object : ComponentAdapter() {
            override fun componentShown(e: ComponentEvent?) {
                field.requestFocus()
            }
        })
    }

    /**
     * Informs this panel that the user has finished entering the input string.
     */
    private fun onTextEntered() {
        // remember the contents of this dialog's field as our input string
        inputString = textField!!.text

        onStringInput()
    }
}
