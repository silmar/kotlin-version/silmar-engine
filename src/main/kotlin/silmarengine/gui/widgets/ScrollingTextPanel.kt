package silmarengine.gui.widgets

import silmarengine.scheduler
import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.util.concurrent.TimeUnit
import javax.swing.JPanel
import kotlin.math.max

/**
 * Displays text which scrolls vertically from the bottom to the top of the panel.
 */
class ScrollingTextPanel(text: String) : JPanel() {
    /**
     * How many pixels the text has been scrolled upward, so far.
     */
    internal var pixelsScrolled = 0

    /**
     * Is used to break up the text into lines, and to produce an
     * image of those lines that will scroll in this panel.
     */
    private val textArea: TextArea

    /**
     * An image of the text after it has been broken into lines.  This image is
     * scrolled vertically within this panel.
     */
    private var image: BufferedImage? = null

    /**
     * The default dimensions of this panel.
     */
    private val panelWidth = 300
    private val panelHeight = 200

    /**
     * The height of the last two lines of the text, which is important since,
     * by convention, they are the only ones not scrolled off the top of this
     * panel.
     */
    private var lastTwoLinesHeight: Int = 0

    init {
        background = Color.black

        // create a text-area containing the text to be scrolled
        textArea = TextArea(text)

        // make the text-area's width the same as this panel's; this will
        // cause the text-area to break the text into lines of that width
        textArea.setSize(panelWidth, textArea.height)

        // now, make the text-area's height its preferred height, which is
        // the sum of the heights of the lines determined above
        textArea.setSize(panelWidth, textArea.preferredSize.height)

        // set the size of this panel, the height of which is smaller than
        // that of the text-area
        preferredSize = Dimension(panelWidth, panelHeight)

        scrollOneIncrement()
    }

    private fun scrollOneIncrement() {
        // if we haven't yet scrolled the complete text (minus
        // the last two lines, which we want to remain
        // at the top of this area when the scrolling is done)
        // from the bottom to the top of this panel
        val topYMax = textArea.preferredSize.height + height
        val delayBetweenScrolls = 90L
        if (pixelsScrolled <= topYMax - lastTwoLinesHeight) {
            // if this panel is visible
            if (isVisible) {
                // scroll the text upwards one pixel
                pixelsScrolled++
                repaint()
            }

            // schedule the next scroll increment
            scheduler.schedule({ scrollOneIncrement() }, delayBetweenScrolls,
                TimeUnit.MILLISECONDS)
        }
    }

    override fun paint(g: Graphics?) {
        // if this is the first call to this method for this panel
        val width = width
        if (image == null) {
            // create an image of this panel's width, and the text-area's height
            image = BufferedImage(width, textArea.preferredSize.height,
                BufferedImage.TYPE_INT_RGB)

            // have the text-area paint its contents into the image
            textArea.paint(image!!.graphics)

            // determine the height of the last two lines of the text, since we want
            // to scroll the text only up until the point that they are the only
            // lines left visible
            val graphics = graphics as Graphics2D
            val context = graphics.fontRenderContext
            val metrics = font.getLineMetrics(textArea.text.substring(0, 20), context)
            lastTwoLinesHeight = metrics.height.toInt() * 2
        }

        // determine the y-value of this panel at which (and below) the text
        // should currently appear
        val panelHeight = height
        val panelTextY = max(0, panelHeight - pixelsScrolled)

        // determine how much of this panel's height is currently being covered by the displayed text
        val storyShowingHeight = panelHeight - panelTextY

        // determine the y-value of the text's image which is at the top of the horizontal strip
        // that will be displayed this time
        val imageY = max(0, pixelsScrolled - panelHeight)

        // if the image of the text has been scrolled enough that the bottom of it no
        // longer fills this panel
        if (panelTextY > 0) {
            // blacken-out the lower portion of this panel that is no longer covered
            // by the image of the text
            g!!.color = background
            g.fillRect(0, 0, width, panelTextY - 1)
        }

        // draw the horizontal strip of the text's image that currently lies
        // within this panel
        g!!.drawImage(image, 0, panelTextY, width, panelHeight, 0, imageY, width,
            imageY + storyShowingHeight, null)
    }
}
