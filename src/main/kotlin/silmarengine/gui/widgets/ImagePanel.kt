package silmarengine.gui.widgets

import java.awt.Dimension
import java.awt.Graphics
import java.awt.Image
import javax.swing.JPanel

/**
 * A panel that displays an image.
 */
class ImagePanel(private val image: Image) : JPanel() {

    /**
     * The dimensions (in pixels) of the image to display.
     */
    private val imageSize = Dimension(image.getWidth(null), image.getHeight(null))

    override fun paint(g: Graphics) {
        g.drawImage(image,
            (size.width - imageSize.width) / 2,
            (size.height - imageSize.height) / 2,
            null)
    }

    override fun getPreferredSize(): Dimension {
        return imageSize
    }
}
