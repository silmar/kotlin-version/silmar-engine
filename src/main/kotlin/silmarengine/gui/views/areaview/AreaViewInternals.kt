package silmarengine.gui.views.areaview

import silmarengine.contexts.arealighting.domain.model.LitDeterminer
import silmarengine.contexts.lineofsightblocking.domain.model.LOSDeterminer
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.Cursor
import java.awt.Dimension

/**
 * Publishes AreaView members which are needed by its collaborators in the area-view's
 * package, but (mostly) not by clients of the area-view.
 */
interface AreaViewInternals : AreaView {
    /**
     * Whatever special mouse cursor (if any) is currently being employed by this area-view.
     */
    var specialCursor: Cursor?

    val viewTileSize: Dimension

    /**
     * The current viewpoint to use for this area-view when no shift is being applied.
     */
    val viewpoint: PixelPoint
    fun moveViewpoint(to: PixelPoint)

    fun shiftViewpoint(dx: Int, dy: Int)

    fun updateShiftedViewpoint()

    val losDeterminer: LOSDeterminer

    val litDeterminer: LitDeterminer

    val messageDrawer: MessageDrawer?

    /**
     * Whether this area-view is currently waiting for the painting thread to do a
     * repaint of its contents.
     */
    var isWaitingForPaint: Boolean
}