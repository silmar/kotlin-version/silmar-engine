package silmarengine.gui.views.areaview

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.arealighting.domain.model.LitDeterminer
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.gui.views.areaview.extensions.drawing.drawView
import silmarengine.gui.views.areaview.extensions.geometry.computeViewTileSize
import silmarengine.gui.views.areaview.extensions.geometry.getAreaLocationAtPoint
import silmarengine.gui.views.areaview.extensions.isPointInShiftArea
import silmarengine.gui.views.areaview.extensions.painting.createOffscreenImage
import silmarengine.gui.views.areaview.extensions.painting.waitForPaint
import silmarengine.gui.views.areaview.extensions.reactions.onViewpointChange
import silmarengine.contexts.lineofsightblocking.domain.model.LOSDeterminer
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.*
import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.objectpools.*
import java.awt.Color
import java.awt.Cursor
import java.awt.Dimension
import java.awt.Graphics
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.image.VolatileImage
import javax.swing.JPanel

class AreaView1 : AreaViewInternals {
    override val panel: JPanel = Panel()

    override var messageDrawer: MessageDrawer? = null

    override val losDeterminer =
        LOSDeterminer(losbPixelPointPool, ::MutableTilePoint, floatPointPool,
            { location, use ->
                adjustToTileCenter(location as PixelPoint, use as MutablePixelPoint)
            }, { from, to, use ->
                getUnitVector(from as PixelPoint, to as PixelPoint, use)
            }, { from, to, use ->
                getDistance(from as PixelPoint, to as PixelPoint, use as PixelDistance)
            }, { from, to, use ->
                (to as PixelPoint).getNearestTileCorner(from as PixelPoint,
                    use as MutablePixelPoint)
            })

    override val litDeterminer =
        LitDeterminer(alPixelPointPool, alTilePointPool, alPixelDistancePool,
            { isNightVisionInUse(this) }, { from, to, most ->
                isDistanceAtMost(from as PixelPoint, to as PixelPoint,
                    most as PixelDistance)
            }, { from, to, use ->
                (to as PixelPoint).getNearestTileCorner(from as PixelPoint,
                    use as MutablePixelPoint)
            })

    override val viewpoint = MutablePixelPoint()

    override fun moveViewpoint(to: PixelPoint): Unit = run { viewpoint.set(to) }

    private val viewpointShift = MutablePixelPoint()
    override fun shiftViewpoint(dx: Int, dy: Int) {
        viewpointShift.translate(dx, dy)
    }

    override val shiftedViewpoint = MutablePixelPoint()

    override var isEnabled: Boolean
        get() = panel.isEnabled
        set(value) = run { panel.isEnabled = value }

    override var specialCursor: Cursor? = null

    override var viewpointEntity: Entity? = null
        set(value) {
            field = value
            value?.run { viewpoint.set(value.location) }
        }

    override var shouldDisplay = false
        set(value) {
            field = value
            panel.repaint()
        }

    override var isGroupOfChangesOccurring = false
        set(value) {
            // if this field is turning false, a group of changes has now been fully reported
            // to this area-view's client, and thus it should now be displayed before any other
            // changes come in
            if (!value && field) waitForPaint()
            field = value
        }

    override var area: Area? = null
        set(value) {
            field = value
            if (value != null) {
                litDeterminer.setArea(value)
                losDeterminer.setArea(value)
            }
        }

    /**
     * The offscreen image buffer used in rendering the next image of the
     * player's current surroundings to be shown by this area-view.
     */
    private var offscreen: VolatileImage? = null

    override var shouldUseLOSBlocking = true
        set(value) {
            field = value
            panel.repaint()
        }

    override var shouldConsiderLighting = true
        set(value) {
            field = value
            litDeterminer.shouldConsiderLighting = value
            panel.repaint()
        }

    override var isWaitingForPaint = false

    override lateinit var viewTileSize: Dimension

    /**
     * The current locations of the light sources in the area being displayed.
     */
    private val lightSourceLocations = ArrayList<TilePoint?>()

    override var listeners = HashSet<AreaViewListener>()

    init {
        AreaView.instance = this

        EdgeShifter(this)
        DragShifter(this)

        setupListeners()
    }

    private fun setupListeners() {
        viewpoint.addListener { oldX, oldY ->
            updateShiftedViewpoint()
            litDeterminer.setViewpoint(viewpoint)
            losDeterminer.setViewpoint(viewpoint)
            onViewpointChange(oldX, oldY)
            panel.repaint()
        }
        viewpointShift.addListener { _, _ ->
            updateShiftedViewpoint()
            panel.repaint()
        }
    }

    override fun updateShiftedViewpoint(): Unit = run {
        shiftedViewpoint.set(viewpoint.x + viewpointShift.x,
            viewpoint.y + viewpointShift.y)
    }

    private inner class Panel : JPanel() {
        init {
            background = Color.black

            addComponentListener(object : ComponentAdapter() {
                override fun componentResized(event: ComponentEvent?) {
                    onResized()
                }
            })

            addMouseListener(object : MouseAdapter() {
                // when this area-view is clicked (note that we use the release event for this
                // purpose, since clicks are too stringent about the mouse staying in place,
                // meaning that an annoying amount of clicks don't register)
                override fun mouseReleased(e: MouseEvent?) {
                    if (e!!.isConsumed || !isEnabled || e.isShiftDown || isPointInShiftArea(
                            e.point)) return

                    val areaLocation = getAreaLocationAtPoint(e.point)
                    reportToListeners {
                        it.onClicked(areaLocation, e.locationOnScreen, e.button > 1)
                    }
                }
            })
        }

        override fun addNotify() {
            super.addNotify()

            messageDrawer = MessageDrawer(this@AreaView1)
        }

        override fun update(g: Graphics) {
            paint(g)
        }

        override fun paint(g_: Graphics?) {
            // if we have an offscreen surface to draw on, and the view is currently on
            val g = g_ ?: return
            if (offscreen != null && shouldDisplay) {
                // do this until, after an iteration, the offscreen surface hasn't been
                // externally, adversely affected
                do {
                    // if the offscreen surface has been lost, recreate it
                    val gc = this.graphicsConfiguration
                    val valCode = offscreen?.validate(gc)
                    if (valCode == VolatileImage.IMAGE_INCOMPATIBLE) offscreen =
                        createOffscreenImage()

                    // update this view's display of the surroundings of the current
                    // (shifted) viewpoint
                    drawView(offscreen!!.graphics, lightSourceLocations)
                    g.drawImage(offscreen, 0, 0, null)
                    messageDrawer!!.drawMessages(g)
                } while (offscreen?.contentsLost() == true)
            }
            // otherwise, get this view blacked-out
            else super.paint(g)

            isWaitingForPaint = false
        }
    }

    /**
     * Informs this area-view that it has been resized.
     */
    private fun onResized() {
        viewTileSize = computeViewTileSize()

        offscreen = createOffscreenImage()

        losDeterminer.onResized(viewTileSize.width, viewTileSize.height)
        litDeterminer.onResized(viewTileSize.width, viewTileSize.height)

        panel.repaint()
    }

    override fun homeViewpoint() {
        viewpointShift.set(0, 0)

        panel.repaint()
    }

    override fun useCursor(cursor: Cursor) {
        this.specialCursor = cursor

        panel.cursor = cursor
    }

    override fun useNormalCursor() {
        specialCursor = null

        panel.cursor = Cursor.getDefaultCursor()
    }
}

@ExtensionPoint
var isNightVisionInUse: (view: AreaViewInternals) -> Boolean = { false }
