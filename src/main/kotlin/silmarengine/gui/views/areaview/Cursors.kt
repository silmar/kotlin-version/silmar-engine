package silmarengine.gui.views.areaview

import silmarengine.util.image.loadImage
import java.awt.Cursor
import java.awt.Point
import java.awt.Toolkit

/**
 * Contains the various mouse-cursors employed by this area-view.
 */
object Cursors {

    /**
     * The cursors for shifting the viewpoint in different directions.
     */
    val shiftNorthCursor: Cursor
    val shiftSouthCursor: Cursor
    val shiftWestCursor: Cursor
    val shiftEastCursor: Cursor
    val shiftNorthwestCursor: Cursor
    val shiftNortheastCursor: Cursor
    val shiftSouthwestCursor: Cursor
    val shiftSoutheastCursor: Cursor
    val shiftAnyDirectionCursor: Cursor

    init {
        // create the different cursors using their images loaded from disk
        val kit = Toolkit.getDefaultToolkit()
        fun create(imageName: String, hotSpotX: Int, hotSpotY: Int): Cursor =
            kit.createCustomCursor(loadImage(imageName), Point(hotSpotX, hotSpotY), "")
        shiftNorthCursor = create("shiftNorthCursor", 15, 1)
        shiftSouthCursor = create("shiftSouthCursor", 15, 30)
        shiftWestCursor = create("shiftWestCursor", 1, 15)
        shiftEastCursor = create("shiftEastCursor", 30, 15)
        shiftNorthwestCursor = create("shiftNorthwestCursor", 1, 1)
        shiftNortheastCursor = create("shiftNortheastCursor", 30, 1)
        shiftSouthwestCursor = create("shiftSouthwestCursor", 1, 30)
        shiftSoutheastCursor = create("shiftSoutheastCursor", 30, 30)
        shiftAnyDirectionCursor = create("shiftAnyDirectionCursor", 15, 15)
    }
}
