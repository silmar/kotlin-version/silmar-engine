package silmarengine.gui.views.areaview

import silmarengine.gui.views.areaview.extensions.isPointInShiftArea
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.scheduler
import java.awt.Cursor
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionAdapter
import java.awt.geom.Point2D
import java.util.concurrent.TimeUnit
import javax.swing.SwingUtilities

/**
 * Shifts this area-view when the user does a mouse-press along its outer edge.
 */
class EdgeShifter(private val areaView: AreaViewInternals) {

    /**
     * Whether this shifter is currently supposed to be shifting the area-view.
     */
    private var shifting = false

    /**
     * The current vector by which this shifter is to shift the area-view.
     */
    private val shiftDelta = Point2D.Float()

    init {
        // add this shifter's mouse-listeners to the area-view
        areaView.panel.addMouseListener(MouseListener())
        areaView.panel.addMouseMotionListener(MouseMotionListener())
    }

    /**
     * Shifts the view one increment, then schedules the next shift if further shifting
     * is being requested.
     */
    private fun shift() {
        // shift the viewpoint
        val pixelsPerShift = 8
        areaView.shiftViewpoint((shiftDelta.x * pixelsPerShift).toInt(),
            (shiftDelta.y * pixelsPerShift).toInt())

        // if more shifted is desired, schedule the next shift increment
        val shiftInterval = 20L
        if (shifting) scheduler.schedule({ shift() }, shiftInterval,
            TimeUnit.MILLISECONDS)
    }

    /**
     * Instructs this shifter to start shifting the view in the given direction.
     */
    private fun startShifting(direction: Point2D.Float) {
        shiftDelta.setLocation(direction)
        shifting = true

        shift()
    }

    /**
     * Listens for area-view mouse events in which this shifter is interested.
     */
    private inner class MouseListener : MouseAdapter() {
        /**
         * Whether this listener should consume the next mouse-release event it hears,
         * to prevent it from being processed elsewhere.
         */
        private var consumeRelease = false

        override fun mousePressed(e: MouseEvent?) {
            // if the event is for a right-click, ignore it
            if (SwingUtilities.isRightMouseButton(e!!)) return

            // if the event didn't occur in the shift area
            val point = e.point
            if (!areaView.isPointInShiftArea(point)) return

            // start the shifting
            val size = areaView.panel.size
            val viewCenter = PixelPoint(size.width / 2, size.height / 2)
            val direction = getUnitVector(viewCenter, PixelPoint(point))
            startShifting(direction)

            // don't let this event get processed elsewhere, nor the ensuing release
            e.consume()
            consumeRelease = true
        }

        override fun mouseReleased(e: MouseEvent?) {
            shifting = false

            if (consumeRelease) {
                // don't let this event get processed elsewhere
                e!!.consume()
                consumeRelease = false
            }
        }
    }

    /**
     * Listens for area-view mouse events in which this shifter is interested.
     */
    private inner class MouseMotionListener : MouseMotionAdapter() {
        override fun mouseMoved(e: MouseEvent) {
            // if the event didn't occur in the shift area
            val point = e.point
            if (!areaView.isPointInShiftArea(point)) {
                // use whatever non-shift cursor is currently in use
                areaView.panel.cursor = areaView.specialCursor ?: Cursor.getDefaultCursor()
            }
            else {
                // otherwise, use the appropriate shift-view cursor
                val cursor: Cursor
                val width = areaView.panel.width
                val height = areaView.panel.height
                cursor = if (point.x < width / 3) {
                    when {
                        point.y < height / 3 -> Cursors.shiftNorthwestCursor
                        point.y > 2 * height / 3 -> Cursors.shiftSouthwestCursor
                        else -> Cursors.shiftWestCursor
                    }
                }
                else if (point.x > 2 * width / 3) {
                    when {
                        point.y < height / 3 -> Cursors.shiftNortheastCursor
                        point.y > 2 * height / 3 -> Cursors.shiftSoutheastCursor
                        else -> Cursors.shiftEastCursor
                    }
                }
                else {
                    if (point.y < height / 2) Cursors.shiftNorthCursor
                    else Cursors.shiftSouthCursor
                }
                areaView.panel.cursor = cursor

                // don't let this event cause any other action
                e.consume()
            }
        }
    }
}