package silmarengine.gui.views.areaview

import silmarengine.contexts.lineofsightblocking.domain.model.LOSBLOSStatus
import silmarengine.util.image.loadImage

/**
 * Describes the state of the line-of-sight between the current (non-shifted) viewpoint
 * and another point in the area.
 */
enum class LOSStatus(imageName: String?) {
    UNBLOCKED(null),
    BLOCKED("darkness"),
    BLOCKED_UPPER_LEFT("darknessUpperLeft"),
    BLOCKED_UPPER_RIGHT("darknessUpperRight"),
    BLOCKED_LOWER_LEFT("darknessLowerLeft"),
    BLOCKED_LOWER_RIGHT("darknessLowerRight");

    /**
     * The image used in the area-view to depict this status.
     */
    val image = if (imageName != null) loadImage(imageName) else null
}

fun LOSBLOSStatus.toLOSStatus() = LOSStatus.values()[ordinal]