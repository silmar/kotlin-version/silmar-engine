package silmarengine.gui.views.areaview

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.events.ReporterMixin
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.Cursor
import javax.swing.JPanel

/**
 * A panel that shows the player's current surroundings.
 */
interface AreaView: ReporterMixin<AreaViewListener> {
    /**
     * This view's physical UI component.
     */
    val panel: JPanel

    /**
     * The area being viewed.
     */
    var area: Area?

    /**
     * The center location of the area this view is currently displaying.
     */
    val shiftedViewpoint: PixelPoint

    /**
     * Instructs this area-view to zero the shift of its viewpoint.
     */
    fun homeViewpoint()

    /**
     * If set, the entity that is considered to be at the non-shifted viewpoint.
     */
    var viewpointEntity: Entity?

    /**
     * Whether line-of-sight blocking should be in effect for this view.
     */
    var shouldUseLOSBlocking: Boolean

    /**
     * Whether lighting considerations should be in effect for this view.
     */
    var shouldConsiderLighting: Boolean

    /**
     * Whether or not the view is to show the surroundings of the current viewpoint.
     * This is turned off while the area is loading.
     */
    var shouldDisplay: Boolean

    /**
     * Has this area-view employ the given mouse-cursor.
     */
    fun useCursor(cursor: Cursor)

    /**
     * Has this area-view go back to employing its usual mouse-cursor.
     */
    fun useNormalCursor()

    /**
     * Whether this area-view should currently be accepting user input.
     */
    var isEnabled: Boolean

    /**
     * Indicates to this view whether the area is undergoing a group of changes, such that
     * only the end result need be displayed.
     */
    var isGroupOfChangesOccurring: Boolean

    companion object {
        var instance: AreaView? = null
    }
}