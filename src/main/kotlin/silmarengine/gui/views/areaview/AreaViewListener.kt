package silmarengine.gui.views.areaview

import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.Point

interface AreaViewListener {
    fun onClicked(
        areaLocation: PixelPoint, screenLocation: Point, isRightClick: Boolean
    )
}