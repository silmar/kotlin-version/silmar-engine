package silmarengine.gui.views.areaview.extensions.reactions

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.gui.views.areaview.AreaView
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.gui.views.areaview.extensions.geometry.doesRectangleIntersectViewArea
import silmarengine.gui.views.areaview.extensions.painting.waitForPaint
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeAnyPartOfRectangle

/**
 * Informs this area-view that the given entity has changed where it is present
 * within its area.
 */
fun AreaView.onEntityPresenceChange(entity: Entity) {
    // if the given entity is this area-view's viewpoint entity
    this as AreaViewInternals
    if (entity === viewpointEntity) {
        // update this area-view's viewpoint
        moveViewpoint(entity.location)
        return
    }

    // if this change isn't part of a group, and the entity's bounds intersect
    // this area-view's view-area
    if (!isGroupOfChangesOccurring && doesRectangleIntersectViewArea(entity.bounds)) {
        // if any of the entity's pixel-rectangle can also be seen from the viewpoint
        if (area!!.canSeeAnyPartOfRectangle(viewpoint, entity.location, entity.size)) {
            // wait for this change to get drawn
            waitForPaint()
        }
        else {
            // otherwise, ask for a repaint in the future, that will likely get grouped with other
            // repaints, just in case any part of the entity is visible from the viewpoint
            panel.repaint(10)
        }
    }
}

