package silmarengine.gui.views.areaview.extensions.painting

import silmarengine.gui.views.areaview.AreaView
import java.awt.image.VolatileImage
import kotlin.math.max

/**
 * Creates a new offscreen image buffer of this view's current size.
 */
fun AreaView.createOffscreenImage(): VolatileImage =
    panel.graphicsConfiguration.createCompatibleVolatileImage(max(panel.width, 1),
        max(panel.height, 1))

