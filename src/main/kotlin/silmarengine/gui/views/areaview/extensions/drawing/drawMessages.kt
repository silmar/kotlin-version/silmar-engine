package silmarengine.gui.views.areaview.extensions.drawing

import silmarengine.gui.Fonts
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.gui.views.areaview.Message
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.scheduler
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.text.computeWidth
import java.awt.Color
import java.awt.FontMetrics
import java.awt.Graphics
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Draws the given messages to be displayed within the given area-view onto the given
 * graphics context.
 */
fun drawMessages(
    g: Graphics, messages: List<Message>, messageYs: HashMap<Int, Message>,
    messageFontMetrics: FontMetrics, areaView: AreaViewInternals
) {
    // for each message in this area-view's message list
    g.font = Fonts.viewMessage
    messageYs.clear()
    val currentTime = System.currentTimeMillis()
    messages.map { it }.forEach forEach@{ message ->
        // if we haven't yet logged this message's start-time
        if (message.startTime == 0L) {
            // log the start-time
            message.startTime = System.currentTimeMillis()

            // determine the pixel width of the message's text
            message.width = computeWidth(message.text, messageFontMetrics)

            // ask for a repaint just after this message should stop being displayed
            // (the extra elapsed time is to help avoid the repaint issued here from
            // being ignored because the UI thread is already in the midst of a
            // repaint when the message's duration ends)
            scheduler.schedule({ areaView.panel.repaint() }, message.duration + 20L,
                TimeUnit.MILLISECONDS)
        }

        // if this message has been displayed for its full duration, skip it;
        // it will be removed from the list, below
        if (currentTime - message.startTime >= message.duration) return@forEach

        // if the message's location can't be seen from the viewpoint
        if (!areaView.area!!.canSeeExt(areaView.viewpoint, message.location, tileWidth,
                true).canSee) return@forEach

        // determine where x-wise to draw the text
        val x = message.location.x - message.width / 2

        // keep trying y's until we find one we haven't yet used this time for a message;
        // note that when hashing we snap the message's y-location-value to the
        // nearest 8th pixel to prevent y-close messages from overlapping
        var y = message.location.y + messageFontMetrics.ascent / 2 - 2
        var yInt = y
        while ({ yInt = (y / 8 * 8); messageYs[yInt] != null }()) {
            y += tileHeight / 2
        }
        messageYs[yInt] = message

        // draw the text with a slight shadow
        val viewpoint = areaView.shiftedViewpoint
        val gx = x - (viewpoint.x - areaView.panel.width / 2)
        val gy = y - (viewpoint.y - areaView.panel.height / 2)
        g.color = Color.black
        g.drawString(message.text, gx + 1, gy + 2)
        g.color = Color.white
        g.drawString(message.text, gx, gy)
    }
}

