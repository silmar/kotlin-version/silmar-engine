package silmarengine.gui.views.areaview.extensions.geometry

import silmarengine.gui.views.areaview.AreaView
import silmarengine.contexts.gameplay.areas.geometry.PixelRectangle

/**
 * Returns whether the given rectangle (assumed to be expressed in area-pixel-coordinates)
 * intersects the region of the area currently displayed by this area-view.
 */
fun AreaView.doesRectangleIntersectViewArea(rectangle: PixelRectangle): Boolean {
    val viewpoint = shiftedViewpoint
    val halfViewWidth = panel.width / 2
    val halfViewHeight = panel.height / 2
    return (rectangle.x + rectangle.width >= viewpoint.x - halfViewWidth && rectangle.y + rectangle.height >= viewpoint.y - halfViewHeight && rectangle.x < viewpoint.x + halfViewWidth && rectangle.y < viewpoint.y + halfViewHeight)
}

