package silmarengine.gui.views.areaview.extensions.drawing

import silmarengine.annotations.ExtensionPoint
import silmarengine.annotations.UpdatesUI
import silmarengine.contexts.arealighting.domain.model.ALLitStatus
import silmarengine.gui.views.areaview.LOSStatus
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.gui.views.areaview.extensions.lighting.checkForLightSourcesChange
import silmarengine.gui.views.areaview.extensions.lighting.isViewpointEntityLit
import silmarengine.gui.views.areaview.toLOSStatus
import silmarengine.gui.views.areaview.toLitStatus
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.objectpools.pixelPointPool
import silmarengine.objectpools.tilePointPool
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.awt.Graphics
import java.awt.Image
import java.util.*

/**
 * Has this area-view draw the current view of the surroundings around the current
 * shifted viewpoint, into the given graphics object.
 */
@UpdatesUI
fun AreaViewInternals.drawView(
    g: Graphics, lightSourceLocations: ArrayList<TilePoint?>
) {
    val area = area ?: return

    // calculate values necessary for the loops below
    val viewTileLocation = tilePointPool.supply()
    viewTileLocation.set(shiftedViewpoint.x / tileWidth, shiftedViewpoint.y / tileHeight)
    val size = panel.size
    val startTileLocation = tilePointPool.supply()
    startTileLocation.set(viewTileLocation.x - viewTileSize.width / 2,
        viewTileLocation.y - viewTileSize.height / 2)
    val endTileLocation = tilePointPool.supply()
    endTileLocation.set(viewTileLocation.x + viewTileSize.width / 2,
        viewTileLocation.y + viewTileSize.height / 2)
    val startLocation = pixelPointPool.supply()
    startLocation.set(
        size.width / 2 - (shiftedViewpoint.x - startTileLocation.x * tileWidth),
        size.height / 2 - (shiftedViewpoint.y - startTileLocation.y * tileHeight))

    // tell the los-determiner where the upper-left-hand corner is of the region of the area
    // for which it must determine LOS
    losDeterminer.setUpperLeftTileLocation(startTileLocation)

    // tell the lit-determiner where the upper-left-hand corner is of the region of the area
    // for which it must determine lit-statuses
    litDeterminer.setUpperLeftTileLocation(startTileLocation)

    checkForLightSourcesChange(lightSourceLocations)

    // for each tile location that could be within this view's bounds
    val tileLocation = tilePointPool.supply()
    var i = startTileLocation.x
    var x = startLocation.x
    while (i <= endTileLocation.x) {
        var j = startTileLocation.y
        var y = startLocation.y
        while (j <= endTileLocation.y) {
            // if the los to this location is fully blocked, or is unlit, then skip this location
            tileLocation.set(i, j)
            if (losDeterminer.getLOSStatus(tileLocation)
                    .toLOSStatus() === LOSStatus.BLOCKED || litDeterminer.getLitStatus(
                    tileLocation) == ALLitStatus.UNLIT) {
                j++
                y += tileHeight
                continue
            }

            // draw this tile's image
            val tile = area.getTile(tileLocation)
            val tileImage = area.motif.tileImages[tile.index]
            g.drawImage(tileImage, x, y, null)
            j++
            y += tileHeight
        }
        i++
        x += tileWidth
    }

    // draw the different kinds of entities, in order of which types should be
    // displayed on top of others
    val notOnTopTerrains = area.terrains.filterNot { t -> t.shouldDrawOnTop }
    drawEntities(notOnTopTerrains, g)
    drawEntities(area.items, g)
    drawEntities(area.beings, g)

    // draw those entities which should drawn on top of all others
    val onTopTerrains = area.terrains.filter { t -> t.shouldDrawOnTop }
    drawEntities(onTopTerrains, g)

    drawEntities(area.visualEffects, g)

    // for each tile location that could be within this view's bounds
    i = startTileLocation.x
    x = startLocation.x
    while (i <= endTileLocation.x) {
        var j = startTileLocation.y
        var y = startLocation.y
        while (j <= endTileLocation.y) {
            // if there is currently an los-blockage image assigned to this tile location
            tileLocation.set(i, j)
            val losStatus = losDeterminer.getLOSStatus(tileLocation).toLOSStatus()
            var image: Image? = losStatus.image
            if (image != null) {
                // draw the los-blockage image over this tile
                g.drawImage(image, x, y, null)
            }

            // if the LOS to this location isn't completely blocked
            if (losStatus !== LOSStatus.BLOCKED) {
                // if there is an image associated with the lit-status of this location
                val litStatus = litDeterminer.getLitStatus(tileLocation)
                image = litStatus.toLitStatus().image
                g.drawImage(image, x, y, null)
            }
            j++
            y += tileHeight
        }
        i++
        x += tileWidth
    }

    // draw any special markers on the view
    val viewpoint = shiftedViewpoint
    val viewStartX = viewpoint.x - size.width / 2
    val viewStartY = viewpoint.y - size.height / 2
    if (!isViewpointEntityLit && !isGameSpecificNightVisionInUse(
            this)) drawViewpointEntityStandInImage(g, viewStartX, viewStartY)

    tilePointPool.reclaim(viewTileLocation)
    tilePointPool.reclaim(startTileLocation)
    tilePointPool.reclaim(endTileLocation)
    tilePointPool.reclaim(tileLocation)
}

@ExtensionPoint
var isGameSpecificNightVisionInUse: (view: AreaViewInternals) -> Boolean = { false }