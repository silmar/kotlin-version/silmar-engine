package silmarengine.gui.views.areaview.extensions.lighting

import silmarengine.annotations.UpdatesUI
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.objectpools.tilePointPool
import java.util.*

/**
 * Determines whether the lit statuses calculated by this area-view's lit-determiner
 * need to be recalculated due to lightsource addition/removal/movement.
 */
@UpdatesUI
fun AreaViewInternals.checkForLightSourcesChange(
    lightSourceLocations: ArrayList<TilePoint?>
) {
    // use a copy of the area's light-sources list, since the original may get modified
    // by a game thread while this function executes
    val area = area ?: return
    val sources = area.lightSources.toTypedArray()

    // if the size of our light-sources list doesn't match that of the area's list
    var changed = false
    if (lightSourceLocations.size != sources.size) {
        // we know our light-sources info is out of date
        changed = true
    }

    // if the above test didn't already tell us our light-sources info is out of date
    if (!changed) {
        // for each light-source in the area
        val tileLocation = tilePointPool.supply()
        var i = 0
        sources.firstOrNull first@{ source ->
            // if either:
            // 1) we recorded this source as not shining, but now it is
            // 2) we recorded this source as shining, but now it isn't
            // 3) this source's location has changed since we last recorded it
            (source.location as PixelPoint).asTileLocation(tileLocation)
            val oldLocation = lightSourceLocations[i]
            if (oldLocation == null && source.isEmittingLight || oldLocation != null && !source.isEmittingLight || tileLocation != lightSourceLocations[i]) {
                // our lightsource info is out of date
                changed = true
                return@first true
            }
            i++
            return@first false
        }
        tilePointPool.reclaim(tileLocation)
    }

    // if the above determined our lightsource info is out of date
    if (changed) {
        // we must recalculate all lit statuses
        litDeterminer.determineStatuses()

        // for each lightsource in the area
        lightSourceLocations.clear()
        sources.forEach { source ->
            // record whether this source is on, and if so, it's current location,
            // so we'll have this info for next time
            lightSourceLocations.add(
                if (source.isEmittingLight) (source.location as PixelPoint).asTileLocation()
                else null)
        }
    }
}
