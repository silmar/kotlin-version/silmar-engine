package silmarengine.gui.views.areaview.extensions.geometry

import silmarengine.gui.views.areaview.AreaView
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.Point

/**
 * Returns the area pixel-location of the given point within this area-view.
 */
fun AreaView.getAreaLocationAtPoint(point: Point): PixelPoint {
    this as AreaViewInternals
    return PixelPoint(shiftedViewpoint.x + (point.x - panel.width / 2),
        shiftedViewpoint.y + (point.y - panel.height / 2))
}

