package silmarengine.gui.views.areaview.extensions.visibility

import silmarengine.gui.views.areaview.AreaView
import silmarengine.gui.views.areaview.AreaViewInternals

/**
 * Has this area-view recompute its depiction of the area it is displaying, and
 * redraw itself after the given delay.
 */
fun AreaView.recomputeView(delayInMs: Long = 0L) {
    this as AreaViewInternals

    // los-of-sight might have been affected
    losDeterminer.determineStatuses()

    // lit-statuses might have been affected
    litDeterminer.determineStatuses()

    // wait the given delay amount to actually redraw the view, in case
    // there are other changes coming
    panel.repaint(delayInMs)
}

