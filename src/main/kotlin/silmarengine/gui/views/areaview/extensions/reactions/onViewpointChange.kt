package silmarengine.gui.views.areaview.extensions.reactions

import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.objectpools.pixelPointPool
import silmarengine.objectpools.tilePointPool

fun AreaViewInternals.onViewpointChange(oldX: Int, oldY: Int) {
    // determine whether the new viewpoint lies within a different tile than the
    // last viewpoint
    val pixelLocation = pixelPointPool.supply()
    pixelLocation.set(oldX, oldY)
    val tileLocation = tilePointPool.supply()
    pixelLocation.asTileLocation(tileLocation)
    val tileChanged = tileLocation == viewpoint.asTileLocation()
    pixelPointPool.reclaim(pixelLocation)
    tilePointPool.reclaim(tileLocation)

    // if this viewpoint has crossed over a tile boundary, all los-statuses must
    // be re-determined; note that we can't rely upon the los-determiner's tracking of its
    // own upper-left-corner to perform this invalidation, since its value and this
    // value don't have to change in lockstep, due to viewpoint-shifting
    if (tileChanged) losDeterminer.determineStatuses()
}

