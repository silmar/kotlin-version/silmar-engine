package silmarengine.gui.views.areaview.extensions.lighting

import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit

val AreaViewInternals.isViewpointEntityLit: Boolean
    get() {
        val area = area ?: return false
        val entity = viewpointEntity ?: return false
        return area.isLit(entity.location).isHalfOrMoreLit
    }
