package silmarengine.gui.views.areaview.extensions.painting

import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.util.sleepThread
import javax.swing.SwingUtilities

/**
 * Has this area-view issue a total-repaint request, and then waits for the ensuing painting
 * to complete.
 */
fun AreaViewInternals.waitForPaint() {
    // if this is being called on the Swing event thread
    if (SwingUtilities.isEventDispatchThread()) {
        // we cannot wait for a repaint, since it is this thread that will do the painting
        // (and thus there will be a deadlock), so instead simply order a repaint
        panel.repaint()

        // print a stack trace so the author is alerted about this problem
        println("doWaitForPaint called on event thread")
        Exception().printStackTrace()

        return
    }

    // if this area-view's window isn't visible, then this area-view isn't being shown,
    // so there is no paint operation to wait for
    if (!SwingUtilities.getWindowAncestor(panel).isVisible) return

    // order the painting
    isWaitingForPaint = true
    panel.repaint()

    // while the paint hasn't occurred, up to a certain time limit (to prevent a deadlock)
    var timeSlept = 0L
    val sleepInterval = 1L
    while (isWaitingForPaint && timeSlept < 1000) {
        sleepThread(sleepInterval)
        timeSlept += sleepInterval
    }
}

