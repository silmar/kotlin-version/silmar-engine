package silmarengine.gui.views.areaview.extensions.drawing

import silmarengine.getImage
import silmarengine.gui.views.areaview.AreaViewInternals
import java.awt.Graphics

fun AreaViewInternals.drawViewpointEntityStandInImage(
    g: Graphics, viewStartX: Int, viewStartY: Int
) {
    val bounds = viewpointEntity!!.bounds
    g.drawImage(getImage("noDamage"), bounds.x - viewStartX, bounds.y - viewStartY, null)
}
