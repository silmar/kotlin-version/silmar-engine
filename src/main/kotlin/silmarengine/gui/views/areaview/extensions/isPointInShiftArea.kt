package silmarengine.gui.views.areaview.extensions

import silmarengine.gui.views.areaview.AreaView
import java.awt.Point

/**
 * Returns whether the given point is within the border of this area-view
 * where a mouse click means to shift the view.
 */
fun AreaView.isPointInShiftArea(point: Point): Boolean {
    //@formatter:off
    return (point.x < shiftViewBorderWidth ||
        point.x >= panel.width - shiftViewBorderWidth ||
        point.y < shiftViewBorderWidth ||
        point.y >= panel.height - shiftViewBorderWidth)
    //@formatter:on
}

/**
 * The width of the border area around the outside of this area-view inside of which a
 * mouse-click means to shift the view.
 */
private const val shiftViewBorderWidth = 32