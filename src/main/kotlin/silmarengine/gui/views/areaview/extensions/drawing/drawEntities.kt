package silmarengine.gui.views.areaview.extensions.drawing

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.damageStatus
import silmarengine.contexts.gameplay.entities.damage.DamageStatus
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.gui.views.areaview.extensions.geometry.doesRectangleIntersectViewArea
import java.awt.Graphics

/**
 * Draws the entities within the given list onto the given graphics context.
 */
fun AreaViewInternals.drawEntities(
    entities: List<Entity>, g: Graphics
) {
    // draw each visible entity in the given list; note that we first copy the list
    // to an array, and iterate over that, instead, since the list might be concurrently
    // modified by other threads
    entities.toTypedArray().forEach { entity ->
        if (entity.isVisible) drawEntity(entity, g)
    }
}

/**
 * Draws the given entity onto the given graphics context.
 */
fun AreaViewInternals.drawEntity(entity: Entity, g: Graphics) {
    // if no part of the entity is in the view area
    val location = entity.location
    if (!doesRectangleIntersectViewArea(entity.bounds)) return

    // draw the entity
    var image = entity.image ?: return
    var halfImageWidth = image.getWidth(null) / 2
    var halfImageHeight = image.getHeight(null) / 2
    val size = panel.size
    val entityX = size.width / 2 - (shiftedViewpoint.x - location.x)
    val entityY = size.height / 2 - (shiftedViewpoint.y - location.y)
    g.drawImage(image, entityX - halfImageWidth, entityY - halfImageHeight, null)

    // if the entity is a being
    if (entity is Being) {
        // if the being is damaged and visible
        val status = entity.damageStatus
        if (status != DamageStatus.undamaged && entity.isVisible) {
            // draw the marker image at the center of this being that is specified by
            // this being's damage status
            image = status.markerImage ?: return
            halfImageWidth = image.getWidth(null) / 2
            halfImageHeight = image.getHeight(null) / 2
            g.drawImage(image, entityX - halfImageWidth, entityY - halfImageHeight,
                null)
        }
    }
}
