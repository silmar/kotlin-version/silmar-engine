package silmarengine.gui.views.areaview.extensions.geometry

import silmarengine.gui.views.areaview.AreaView
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.awt.Dimension
import kotlin.math.ceil

/**
 * Returns the dimensions (in tiles) of the region of the area covered by this area-view.
 * Tiles (potentially) partially covered are included in the result.
 */
fun AreaView.computeViewTileSize(): Dimension =
    Dimension(ceil((panel.size.width / tileWidth.toFloat())).toInt() + 2,
        ceil((panel.size.height / tileHeight.toFloat())).toInt() + 2)

