package silmarengine.gui.views.areaview.extensions

import silmarengine.gui.views.areaview.AreaView
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.gui.views.areaview.Message
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this area-view display the given message text at the given location within its
 * bounds.
 *
 * @param duration  For how long (in ms) the message should be displayed.
 * A zero value causes a default to be used.
 */
fun AreaView.displayMessage(
    messageText: String, location: PixelPoint?, duration: Int = 0
) {
    this as AreaViewInternals

    // create and populate a message object for this message
    val actualDuration = if (duration != 0) duration
    else 1000 + messageText.length * 50
    val message = Message(location ?: viewpointEntity?.location ?: return, messageText,
        actualDuration)

    // add the message object to this view's list of messages
    messageDrawer!!.addMessage(message)
    panel.repaint()
}

