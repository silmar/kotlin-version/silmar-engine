package silmarengine.gui.views.areaview

import silmarengine.contexts.arealighting.domain.model.ALLitStatus
import silmarengine.util.image.loadImage

/**
 * Describes how well a tile in an area is lit.
 */
enum class LitStatus(imageName: String?) {
    UNLIT("darkness"),
    UNLIT_UPPER_LEFT("unlitUpperLeft"),
    UNLIT_UPPER_RIGHT("unlitUpperRight"),
    UNLIT_LOWER_LEFT("unlitLowerLeft"),
    UNLIT_LOWER_RIGHT("unlitLowerRight"),
    HALF_LIT("halfLit"),
    HALF_LIT_UPPER_LEFT("halfLitUpperLeft"),
    HALF_LIT_UPPER_RIGHT("halfLitUpperRight"),
    HALF_LIT_LOWER_LEFT("halfLitLowerLeft"),
    HALF_LIT_LOWER_RIGHT("halfLitLowerRight"),
    FULLY_LIT(null);

    val isHalfOrMoreLit: Boolean get() = ordinal >= HALF_LIT.ordinal

    /**
     * The image used in the area-view to depict this status.
     */
    val image = if (imageName != null) loadImage(imageName) else null
}

fun ALLitStatus.toLitStatus() = LitStatus.values()[ordinal]