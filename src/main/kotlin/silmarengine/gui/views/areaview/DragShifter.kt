package silmarengine.gui.views.areaview

import java.awt.Point
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionAdapter

/**
 * Shifts this area-view when the user drags the mouse using
 * the right button, or the left button with the shift key down.
 */
class DragShifter(private val areaView: AreaViewInternals) {

    /**
     * Where in the area-view the current drag started.
     */
    private var lastDragLocation: Point? = null

    init {
        // add this shifter's mouse-listeners to the area-view
        areaView.panel.addMouseListener(MouseListener())
        areaView.panel.addMouseMotionListener(MouseMotionListener())
    }

    /**
     * Listens for area-view mouse events in which this shifter is interested.
     */
    private inner class MouseListener : MouseAdapter() {

        /**
         * Whether this listener should consume the next mouse-release event it hears,
         * to prevent it from being processed elsewhere.
         */
        private var consumeRelease = false

        override fun mousePressed(e: MouseEvent) {
            // if the event is not for a shift-click, ignore it
            if (!e.isShiftDown) return

            // record where the drag will start, presuming this
            // press is followed by a drag
            lastDragLocation = e.point

            // don't let this event get processed elsewhere, nor the ensuing release
            e.consume()
            consumeRelease = true

            areaView.specialCursor = Cursors.shiftAnyDirectionCursor
            areaView.panel.cursor = areaView.specialCursor!!
        }

        override fun mouseReleased(e: MouseEvent) {
            // if the event is not for a shift-click, ignore it
            if (!e.isShiftDown) return

            areaView.useNormalCursor()

            lastDragLocation = null

            if (consumeRelease) {
                // don't let this event get processed elsewhere
                e.consume()
                consumeRelease = false
            }
        }
    }

    /**
     * Listens for area-view mouse motion events in which this shifter is interested.
     */
    private inner class MouseMotionListener : MouseMotionAdapter() {

        override fun mouseDragged(e: MouseEvent) {
            // if the event is not for a shift-click, ignore it
            if (!e.isShiftDown) return

            // add the drag amount (since the last drag-event) to the current viewpoint shift
            val last = lastDragLocation
            if (last != null) areaView.shiftViewpoint(last.x - e.x, last.y - e.y)
            lastDragLocation = e.point
            areaView.panel.repaint()

            // don't let this event cause any other action
            e.consume()
        }
    }
}