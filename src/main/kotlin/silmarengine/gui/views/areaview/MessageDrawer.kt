package silmarengine.gui.views.areaview

import silmarengine.gui.Fonts
import silmarengine.gui.views.areaview.extensions.drawing.drawMessages
import java.awt.FontMetrics
import java.awt.Graphics
import java.util.*

/**
 * Draws the textual messages currently displayed within this area-view.
 */
class MessageDrawer(private val areaView: AreaViewInternals) {

    /**
     * The y-locations within this area-view's bounds of the messages currently
     * being displayed.
     */
    private val messageYs = HashMap<Int, Message>()

    /**
     * The metrics of the font used to draw the messages.
     */
    private val messageFontMetrics: FontMetrics =
        areaView.panel.getFontMetrics(Fonts.viewMessage)

    /**
     * The list of textual messages currently being displayed within this area-view.
     */
    private val messages = ArrayList<Message>()

    /**
     * Has this message-drawer include the given message in the list of those it is
     * currently supposed to draw.
     */
    fun addMessage(message: Message) {
        messages.add(message)
    }

    /**
     * Draws the current messages being displayed within this area-view onto the given
     * graphics context.
     */
    fun drawMessages(g: Graphics) {
        drawMessages(g, messages, messageYs, messageFontMetrics, areaView)

        // remove those messages which have been displayed for their full duration
        messages.removeIf { System.currentTimeMillis() - it.startTime >= it.duration }
    }
}