package silmarengine.gui.views.areaview

import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * A text message to be displayed within this area-view.
 */
class Message(
   /**
     * The location at which this message should be displayed.
     */
    val location: PixelPoint,
    /**
     * The text of this message.
     */
    val text: String,
    /**
     * For how many ms this message is to be shown.
     */
    val duration: Int = 0
) {
    /**
     * The time (in ms) at which this message was first displayed in this area-view.
     */
    var startTime: Long = 0

    /**
     * How many pixels wide this message is within this area-view.
     */
    var width: Int = 0
}
