package silmarengine.gui

import silmarengine.contexts.gameplay.entities.beings.player.extensions.attacks.attack

/**
 * The flyweight location selections.
 */
object LocationSelections {
    val meleeAttack =
        LocationSelection({ location, player -> player.attack(location) }, true)
    val rangedAttack = LocationSelection({ location, player -> player.attack(location) })
}