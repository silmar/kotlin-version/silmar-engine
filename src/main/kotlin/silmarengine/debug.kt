package silmarengine

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player

/**
 * Whether or not this game is being run in debug mode.
 */
const val isDebugOn = false

@ExtensionPoint
lateinit var performDebugActionOne: (player: Player) -> Unit
@ExtensionPoint
lateinit var performDebugActionTwo: (player: Player) -> Unit
