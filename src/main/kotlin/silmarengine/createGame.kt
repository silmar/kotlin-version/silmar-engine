package silmarengine

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.Player1
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.Attribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.appendAttributeInfo
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.game.Game1
import silmarengine.contexts.gameplay.game.extensions.save
import silmarengine.contexts.gamecreation.domain.model.*
import silmarengine.contexts.gamecreation.domain.model.functions.createGame
import silmarengine.contexts.gamecreation.domain.model.functions.InputGameNameResult
import silmarengine.gui.dialogs.InputStringDialog
import silmarengine.contexts.playerattributemodification.ui.AttributesDialog
import silmarengine.gui.dialogs.StoryDialog
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.playerattributemodification.domain.model.PAMAttribute
import silmarengine.contexts.playerattributemodification.domain.model.PAMAttributeValues
import java.awt.FileDialog
import javax.swing.JFrame

fun createGame(parentFrame: JFrame) {
    fun inputGameName(): InputGameNameResult {
        // display a file-save dialog to get the game's name and path
        val fileDialog = FileDialog(parentFrame, "Enter game filename")
        fileDialog.mode = FileDialog.SAVE
        fileDialog.isVisible = true
        return InputGameNameResult(fileDialog.file, fileDialog.directory)
    }

    fun inputPlayerName(): String {
        // while a valid name has yet to be entered
        var playerName: String? = null
        while (playerName == null || playerName == "") {
            // have the user input the name
            playerName =
                InputStringDialog.show(parentFrame, "Enter your character's name:")
        }
        return playerName
    }

    fun selectPlayerAttributeValues(
        startingValues: PlayerAttributeValues
    ): PlayerAttributeValues {
        // show the modify-player-attributes dialog
        val values = PAMAttributeValues(startingValues.values)
        val append = { attribute: PAMAttribute, value: Int, to: StringBuffer ->
            appendAttributeInfo(Attribute.values()[attribute.ordinal], value, to)
        }
        val dialog = AttributesDialog(values, parentFrame, append)
        dialog.isVisible = true
        return PlayerAttributeValues(values.immutableValues)
    }

    fun showStory(text: String) {
        // show the story dialog
        StoryDialog(parentFrame, text, "Begin game").isVisible = true
    }

    // create the game object from the game created by the game creation domain
    val startingArea: Area = createGameSpecificStartingArea()
    val createdGame =
        createGame(::inputGameName, ::inputPlayerName, ::selectPlayerAttributeValues,
            ::showStory) {createGameSpecificStartingGCArea(startingArea)} ?: return

    // create the player object from the player created by the game creation domain
    val fromPlayer = createdGame.player
    val player = createPlayer(fromPlayer)
    val location = fromPlayer.location
    player.moveTo(PixelPoint(location.x, location.y))

    createPlayUI(player)

    // inform the player's listeners that the area it is on is loading
    player.reportToListeners { it.onAreaLoading() }

    // create the game object
    val game = createGame(createdGame, player, startingArea)

    onNewGameReadyToStart(game)

    game.save()

    parentFrame.isVisible = false

    game.processTurns()

    parentFrame.isVisible = true
}

@ExtensionPoint
var createGame: (fromGame: GCGame, player: Player, startingArea: Area) -> Game =
    { fromGame, player, _ -> Game1(fromGame, player) }

@ExtensionPoint
var createPlayer: (fromPlayer: GCPlayer) -> Player = { Player1(it) }

@ExtensionPoint
lateinit var createGameSpecificStartingArea: () -> Area

@ExtensionPoint
lateinit var createGameSpecificStartingGCArea: (area: Area) -> GCArea

@ExtensionPoint
var onNewGameReadyToStart: (game: Game) -> Unit = {}
