package silmarengine

import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

fun loadConfigurationFile() {
    try {
        config.load(FileInputStream(configFileName))
    } catch (ex: IOException) {
        println("No configuration file found.")
    }
}

fun getBooleanConfigurationValue(key: String): Boolean {
    return (config.getProperty(key) ?: return false).toBoolean()
}

fun storeBooleanConfigurationValue(key: String, value: Boolean) {
    config.setProperty(key, value.toString())
    config.store(FileOutputStream(configFileName), "")
}

private val config = Properties()

const val configFileName = "config.properties"

