package silmarengine.geometry.points

class Difference(val x: Int, val y: Int)

/**
 * Returns the difference between the given two points
 */
fun getDifference(x1: Int, y1: Int, x2: Int, y2: Int): Difference =
     Difference(x2 - x1, y2 - y1)

