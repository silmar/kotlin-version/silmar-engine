package silmarengine.geometry.points

import silmarengine.geometry.vectors.getUnitVector
import kotlin.math.abs

/**
 * Returns the textual name of the compass direction between the given two points.
 */
fun getDirectionString(x1: Int, y1: Int, x2: Int, y2: Int): String {
    // determine the vector between the two points
    val vector = getUnitVector(x1, y1, x2, y2)

    // if the vector is as much or less in the x-direction than the y-
    var direction = ""
    if (abs(vector.x) <= abs(vector.y)) {
        if (vector.y < 0) direction += "north"
        if (vector.y > 0) direction += "south"
    }

    // if the vector is as much or more in the x-direction than the y-
    if (abs(vector.x) >= abs(vector.y)) {
        if (vector.x < 0) direction += "west"
        if (vector.x > 0) direction += "east"
    }

    return direction
}
