package silmarengine.geometry.points

import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Returns the index of the point in the given list that is nearest to the given location.
 */
fun getIndexOfNearestPoint(location: PixelPoint, points: List<PixelPoint>): Int {
    var nearestIndex = 0
    var minDistance = Float.MAX_VALUE
    points.reduceIndexed { i, acc, point ->
        // if this point is the nearest to the given location encountered so far
        val distance = getDistance(location.x, location.y, point.x, point.y)
        if (distance < minDistance) {
            // remember the index of this point
            nearestIndex = i
            minDistance = distance
            point
        }
        else acc
    }
    return nearestIndex
}