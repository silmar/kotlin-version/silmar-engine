package silmarengine.geometry.points

import kotlin.math.sqrt

/**
 * Returns the distance between the given two points.
 */
fun getDistance(x1: Int, y1: Int, x2: Int, y2: Int): Float {
    val a = (y2 - y1).toFloat()
    val b = (x2 - x1).toFloat()
    return sqrt((a * a + b * b).toDouble()).toFloat()
}

