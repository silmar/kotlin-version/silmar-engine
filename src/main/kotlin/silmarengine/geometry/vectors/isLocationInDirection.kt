package silmarengine.geometry.vectors

import java.awt.geom.Point2D

/**
 * Returns whether the given 'to' point is in the general given direction from the
 * given 'from' point.
 */
fun isLocationInDirection(
    x1: Int, y1: Int, x2: Int, y2: Int, direction: Point2D.Float
): Boolean {
    return getAngleBetween(getUnitVector(x1, y1, x2, y2), direction) <= Math.PI / 3
}

