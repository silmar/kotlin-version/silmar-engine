package silmarengine.geometry.vectors

import java.awt.geom.Point2D
import kotlin.math.sqrt

/**
 * Returns the magnitude of the given vector.
 */
fun getMagnitude(vector: Point2D.Float): Float {
    return sqrt((vector.x * vector.x + vector.y * vector.y).toDouble()).toFloat()
}