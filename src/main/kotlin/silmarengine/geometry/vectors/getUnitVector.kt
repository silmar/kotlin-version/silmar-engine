package silmarengine.geometry.vectors

import silmarengine.geometry.points.getDifference
import silmarengine.geometry.points.getDistance
import java.awt.geom.Point2D

/**
 * Returns the unit vector between the given two points
 */
fun getUnitVector(
    x1: Int, y1: Int, x2: Int, y2: Int, use: Point2D.Float = Point2D.Float()
): Point2D.Float {
    val vector = getDifference(x1, y1, x2, y2)
    val distance = getDistance(x1, y1, x2, y2)
    use.setLocation(vector.x / distance, vector.y / distance)
    return use
}

