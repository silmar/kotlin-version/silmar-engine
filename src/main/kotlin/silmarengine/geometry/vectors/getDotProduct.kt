package silmarengine.geometry.vectors

import java.awt.geom.Point2D

/**
 * Returns the dot product of the given two vectors.
 */
fun getDotProduct(vector1: Point2D.Float, vector2: Point2D.Float): Float {
    return vector1.x * vector2.x + vector1.y * vector2.y
}