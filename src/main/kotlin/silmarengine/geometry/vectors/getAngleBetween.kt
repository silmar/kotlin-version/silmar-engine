package silmarengine.geometry.vectors

import java.awt.geom.Point2D
import kotlin.math.acos

/**
 * Returns the angle (in radians) formed between the two given vectors
 */
fun getAngleBetween(vector1: Point2D.Float, vector2: Point2D.Float): Float {
    // if either given vector has a zero magnitude
    val magnitude1 = getMagnitude(vector1)
    val magnitude2 = getMagnitude(vector2)
    if (magnitude1 == 0f || magnitude2 == 0f) {
        return 0f
    }

    // use the formula for the dot product between the two vectors
    // to determine the angle between them
    var cosine = getDotProduct(vector1, vector2) / (magnitude1 * magnitude2)
    if (cosine > 1) cosine = 1f
    if (cosine < -1) cosine = -1f
    return acos(cosine.toDouble()).toFloat()
}

