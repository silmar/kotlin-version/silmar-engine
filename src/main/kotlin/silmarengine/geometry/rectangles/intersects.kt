package silmarengine.geometry.rectangles

fun intersects(r1x: Int, r1y: Int, r1w: Int, r1h: Int, r2x: Int, r2y: Int, r2w: Int, r2h: Int): Boolean {
    var tw = r1w
    var th = r1h
    var rw = r2w
    var rh = r2h
    if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) {
        return false
    }
    val tx = r1x
    val ty = r1y
    val rx = r2x
    val ry = r2y
    rw += rx
    rh += ry
    tw += tx
    th += ty
    //      overflow || intersect
    return (rw < rx || rw > tx) && (rh < ry || rh > ty) && (tw < tx || tw > rx) && (th < ty || th > ry)
}