package silmarengine.geometry.rectangles

fun containsPoint(rx: Int, ry: Int, rw: Int, rh: Int, x: Int, y: Int): Boolean {
    var w = rw
    var h = rh
    if (w or h < 0) {
        // At least one of the dimensions is negative...
        return false
    }
    // Note: if either dimension is zero, tests below must return false...
    if (x < rx || y < ry) {
        return false
    }
    w += rx
    h += ry
    //    overflow || intersect
    return (w < rx || w > x) && (h < ry || h > y)
}