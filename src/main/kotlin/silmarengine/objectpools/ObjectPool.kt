package silmarengine.objectpools

import java.util.*
import java.util.function.Supplier

open class ObjectPool<T>(
    private val supplier: Supplier<T>, private val onSupply: ((t: T) -> Unit)? = null
) {
    private val pool = ArrayDeque<T>()

    /**
     * Supplies an instance from this pool..
     */
    open fun supply(): T {
        synchronized(pool) {
            val instance = if (pool.isNotEmpty()) pool.pop() else supplier.get()
            onSupply?.invoke(instance)
            return instance
        }
    }

    /**
     * Returns the given object to this pool.  Be sure it is no longer in use!
     */
    open fun reclaim(t: T) {
        synchronized(pool) {
            pool.push(t)
        }
    }
}

