package silmarengine.objectpools

import silmarengine.contexts.arealighting.domain.model.ALMutablePixelPoint
import silmarengine.contexts.arealighting.domain.model.ALMutableTilePoint
import silmarengine.contexts.arealighting.domain.model.ALPixelDistance
import silmarengine.contexts.beingmovement.domain.model.BMMutablePixelPoint
import silmarengine.contexts.beingmovement.domain.model.BMPixelDistance
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBMutablePixelPoint
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBPixelDistance
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelRectangle
import silmarengine.contexts.gameplay.areas.geometry.MutableTilePoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.pathfinding.domain.model.*
import java.awt.Dimension
import java.awt.geom.Point2D
import java.util.*
import java.util.function.Supplier

/**
 * Core
 */
val dimensionPool = ObjectPool(Supplier { Dimension() })
val pixelDistancePool = ObjectPool(Supplier { PixelDistance() })
val pixelPointPool = ObjectPool(Supplier { MutablePixelPoint() })
val pixelRectanglePool = ObjectPool(Supplier { MutablePixelRectangle() })
val tilePointPool = ObjectPool(Supplier { MutableTilePoint() })
val floatPointPool = ObjectPool(Supplier { Point2D.Float() })

/**
 * For domains
 */
val alPixelPointPool = ObjectPool(Supplier { MutablePixelPoint() as ALMutablePixelPoint })
val alTilePointPool = ObjectPool(Supplier { MutableTilePoint() as ALMutableTilePoint })
val alPixelDistancePool = ObjectPool(Supplier { PixelDistance() as ALPixelDistance })
val losbPixelPointPool =
    ObjectPool(Supplier { MutablePixelPoint() as LOSBMutablePixelPoint })
val losbPixelDistancePool = ObjectPool(Supplier { PixelDistance() as LOSBPixelDistance })
val pfPixelPointPool = ObjectPool(Supplier { MutablePixelPoint() as PFMutablePixelPoint })
val pfPixelDistancePool = ObjectPool(Supplier { PixelDistance() as PFPixelDistance })
val pfPixelRectanglePool =
    ObjectPool(Supplier { MutablePixelRectangle() as PFMutablePixelRectangle })
val pfPathDequePool = ObjectPool(Supplier { ArrayDeque<PFPixelPoint>() })
val stepMapsPool = ObjectPool(Supplier { StepsMap() }) { it.clear() }
val bmPixelPointPool = ObjectPool(Supplier { MutablePixelPoint() as BMMutablePixelPoint })
val bmPixelDistancePool = ObjectPool(Supplier { PixelDistance() as BMPixelDistance })
