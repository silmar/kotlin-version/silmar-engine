package silmarengine.util

/**
 * Has the current thread sleep for the given amount of milli- and nano-seonds.
 */
fun sleepThread(millis: Long, nanos: Int = 0) {
    try {
        Thread.sleep(millis, nanos)
    } catch (e: InterruptedException) {}
}