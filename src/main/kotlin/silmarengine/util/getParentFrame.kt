package silmarengine.util

import java.awt.Component
import java.awt.Frame

/**
 * Returns the parent (i.e. containing) frame of the given component.
 */
fun Component.getParentFrame(): Frame {
    // search up the parent chain from the given component, until a frame is found
    var result = this
    while (result !is Frame) result = result.parent
    return result
}
