package silmarengine.util.gui

import silmarengine.Task
import javax.swing.SwingUtilities

fun runOnSwingThread(task: Task) {
    if (SwingUtilities.isEventDispatchThread()) task()
    else SwingUtilities.invokeAndWait { task() }
}