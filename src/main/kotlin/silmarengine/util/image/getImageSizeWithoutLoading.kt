package silmarengine.util.image

import java.awt.Dimension
import java.io.IOException

/**
 * Returns the size of the image in the file of the given name without
 * having to read in the whole image.
 */
fun getImageSizeWithoutLoading(fileName: String): Dimension {
    var header: ByteArray? = null
    try {
        // open a stream on the given file
        val stream =
            ClassLoader.getSystemClassLoader().getResourceAsStream("pics/$fileName.gif")

        // read the gif header
        header = ByteArray(13)
        stream!!.read(header)
        stream.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    // extract the image size from the header
    val size = Dimension()
    size.width = (header!![7].toInt() shl 8) or header[6].toInt()
    size.height = (header[9].toInt() shl 8) or header[8].toInt()

    return size
}