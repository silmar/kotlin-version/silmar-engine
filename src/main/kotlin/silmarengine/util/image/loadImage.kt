package silmarengine.util.image

import java.awt.GraphicsEnvironment
import java.awt.Transparency
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

/**
 * Loads the gif image of the given filename from the disk.
 *
 * @param   fileName            The name of the file holding the image,
 * minus the ".gif" extension.
 * @param   useDefaultPath      Whether the image is to be found in the "pics/" path.
 */
fun loadImage(fileName: String, useDefaultPath: Boolean = true): BufferedImage {
    // load the image from the URL determined by the class-loader
    val path = (if (useDefaultPath) "pics/" else "") + fileName + ".gif"
    val url = ClassLoader.getSystemClassLoader().getResource(path)
    val image = ImageIO.read(url!!) ?: throw Exception("Could not load image $fileName")

    // create an image of the same size as the one loaded above that has the same color
    // depth as the screen, for optimal blitting
    val compatibleImage = GraphicsEnvironment.getLocalGraphicsEnvironment()
        .defaultScreenDevice.defaultConfiguration.createCompatibleImage(image.width,
        image.height, Transparency.BITMASK)

    // draw the loaded image into the screen-compatible image
    val g = compatibleImage.graphics
    g.drawImage(image, 0, 0, null)

    // the screen-compatible image is the one that will get used
    return compatibleImage
}