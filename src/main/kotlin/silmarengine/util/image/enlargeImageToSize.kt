package silmarengine.util.image

import java.awt.Color
import java.awt.Component
import java.awt.Dimension
import java.awt.Image
import java.awt.image.MemoryImageSource
import java.awt.image.PixelGrabber

/**
 * Returns a version of the given image scaled to the given size.
 *
 * @param imageCreator      AWT demands the presence of a component to allow
 * creation of an image, so this is it.
 */
fun Image.enlargeImageToSize(size: Dimension, imageCreator: Component): Image {
    // create an array to hold the pixels of the enlarged image
    val oldWidth = getWidth(null)
    val oldHeight = getHeight(null)
    val pixels = IntArray(size.width * size.height)

    // grab the pixels of the current image into the above array, such that
    // the old image is center within the new one
    val pg = PixelGrabber(this, 0, 0, oldWidth, oldHeight, pixels,
        (size.width - oldWidth) / 2 + (size.height - oldHeight) / 2 * size.width,
        size.width)
    try {
        pg.grabPixels()
    } catch (e: InterruptedException) {
    }

    // draw a center mark on the image
    val centerX = size.width / 2
    val centerY = size.height / 2
    for (i in -3..3) {
        makePixelWhite(centerX + i, centerY, pixels, size.width)
        makePixelWhite(centerX, centerY + i, pixels, size.width)
    }

    // create and return an image made from the array of pixels
    return imageCreator.createImage(
        MemoryImageSource(size.width, size.height, pixels, 0, size.width))
}

/**
 * Sets the color at the given position in the given image data array to white.
 */
private fun makePixelWhite(x: Int, y: Int, pixels: IntArray, imageWidth: Int) {
    pixels[x + y * imageWidth] = Color.white.rgb
}
