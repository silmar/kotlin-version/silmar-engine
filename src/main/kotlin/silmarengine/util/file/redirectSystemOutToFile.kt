package silmarengine.util.file

import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.PrintStream

/**
 * Redirects System.out to the file of the given name.
 */
fun redirectSystemOutToFile(filename: String) {
    println("Redirecting out to: $filename")
    val stream = PrintStream(BufferedOutputStream(FileOutputStream(File(filename))), true)
    System.setOut(stream)
    System.setErr(stream)
}