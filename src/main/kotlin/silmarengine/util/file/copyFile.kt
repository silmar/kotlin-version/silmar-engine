package silmarengine.util.file

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

/**
 * Copies the contents of the input file of the given name into an output file of the
 * given name.
 */
fun copyFile(inputFileName: String, outputFileName: String) {
    // open streams on the input and output files
    val inputFile = File(inputFileName)
    val outputFile = File(outputFileName)
    val `in` = FileInputStream(inputFile)
    val out = FileOutputStream(outputFile)

    // copy the bytes of the one file into the other
    val bytes = ByteArray(`in`.available())
    `in`.read(bytes)
    out.write(bytes)

    // close the streams on the files
    `in`.close()
    out.close()
}
