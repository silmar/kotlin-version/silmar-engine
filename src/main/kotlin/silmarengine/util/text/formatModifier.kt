package silmarengine.util.text

/**
 * Returns the given modifier formatted with a plus (if it's positive) or nothing
 * (if it's negative).
 */
fun formatModifier(modifier: Int): String =
    "" + (if (modifier >= 0) "+" else "") + modifier
