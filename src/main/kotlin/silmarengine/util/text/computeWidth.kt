package silmarengine.util.text

import java.awt.FontMetrics

/**
 * Returns the pixel-width of the given string when displayed with the font of the
 * given metrics.
 */
fun computeWidth(s: String, fm: FontMetrics): Int {
    // for each character in the string
    var width = 0
    for (char in s) {
        // add this character's width to our total
        width += fm.charWidth(char)
    }

    return width
}
