package silmarengine.util

/**
 * Return the log-10 value of the given number.
 */
fun log10(d: Float): Float = (Math.log(d.toDouble()) / Math.log(10.0)).toFloat()
