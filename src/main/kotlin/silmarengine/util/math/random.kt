package silmarengine.util.math

import kotlin.math.abs

/**
 * The random number generator used by the utility functions in this file.
 */
private var random = java.util.Random()

/**
 * Returns a random value between 0 and 1 (inclusive).
 */
val randomFloat: Float
    get() = random.nextFloat()

/**
 * Returns a random true or false result.
 */
val randomBoolean: Boolean
    get() = random.nextInt() % 2 == 0

/**
 * Returns a random integer within the given inclusive range.
 *
 * @param   min    The low end of the range within which the result must fall.
 * @param   max    The high end of the range within which the result must fall.
 */
fun randomInt(min: Int, max: Int): Int {
    return if (min >= max) {
        max
    }
    else min + abs(random.nextInt()) % (max - min + 1)

}

/**
 * Returns a random integer within the given max-delta of the given base.
 */
fun randomIntNear(base: Int, maxDelta: Int): Int {
    return randomInt(base - maxDelta, base + maxDelta)
}

/**
 * Returns the result of totalling the values of the given number
 * of doDie rolls.
 *
 * @param minPerRoll        The min value that can occur per roll.
 * @param maxPerRoll        The max value that can occur per roll.
 */

fun randomBellCurveInt(minPerRoll: Int, maxPerRoll: Int, numRolls: Int): Int {
    // for each roll to make
    var total = 0
    for (i in 0 until numRolls) {
        // add this roll's result to our running total
        total += randomInt(minPerRoll, maxPerRoll)
    }

    return total
}
