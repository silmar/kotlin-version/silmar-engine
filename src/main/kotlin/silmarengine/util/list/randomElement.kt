package silmarengine.util.list

/**
 * Returns an element selected randomly from the given list.
 * Unlike the List.random() extension method, this function gracefully handles
 * the list being empty.
 */
fun <E> List<E>.randomElement(): E? = if (isEmpty()) null else random()