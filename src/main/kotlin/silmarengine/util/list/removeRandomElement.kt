package silmarengine.util.list

import silmarengine.util.math.randomInt

/**
 * Returns an element selected randomly and then removed from the given list.
 */
fun <E> MutableList<E>.removeRandomElement(): E = removeAt(
    randomInt(0, size - 1))