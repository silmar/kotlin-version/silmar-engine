package silmarengine.util.list

/**
 * Returns the sum of the elements in the given list, up to the given index.  The
 * list elements are all assumed to be integers.
 */
fun List<Int>.sum(upToIndex: Int = size): Int {
    // for each integer in the list, up to the given index
    var sum = 0
    for (i in 0 until upToIndex) {
        val entry = this[i]

        // add this value to our running sum
        sum += entry
    }
    return sum
}