package silmarengine.util.list

import java.util.stream.Collectors
import java.util.stream.Stream

fun <T> concat(vararg lists: List<T>): List<T> =
    Stream.of(*lists).flatMap { list -> list.stream() }.collect(
        Collectors.toList())