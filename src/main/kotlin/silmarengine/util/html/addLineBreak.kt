package silmarengine.util.html

/**
 * Inserts a line break into the given text-buffer.
 */
fun addLineBreak(text: StringBuffer) {
    text.append("<br>")
}

/**
 * Inserts a line break followed by an indent into the given text-buffer.
 */
fun addLineBreakPlusIndent(text: StringBuffer) {
    addLineBreak(text)
    indent(text)
}