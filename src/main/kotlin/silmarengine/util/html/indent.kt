package silmarengine.util.html

/**
 * Inserts an indention into the given text-buffer.
 */
fun indent(text: StringBuffer) {
    text.append("&nbsp;&nbsp;&nbsp;")
}