package silmarengine.util.html

/**
 * Returns a new string that equivalent to the text given, but has html line-breaks
 * inserted in place of spaces such that each line is at most the given max-length.
 */
fun breakTextAtSpaces(
    text: String, maxLineLength: Int, linePrefix: String? = null
): String {
    // while we have yet to process the entire given text
    val buffer = StringBuffer()
    var start = 0
    val length = text.length
    val breakLength = 4
    while (start < length) {
        // if we aren't at the last line of the text
        if (start + maxLineLength < length) {
            // break off the next max-line-length's worth of the text
            var line = text.substring(start,
                Integer.min(length, start + maxLineLength))

            // if there is an explicit html-break in this line of text
            var addBreak = true
            var index = line.indexOf("<br>")
            if (index >= 0) {
                // we'll break the line at this explicit break
                index += breakLength
                addBreak = false
            }
            else {
                // find the last occurrence of a space character within the line;
                // if no such character is found, use the whole line
                index = line.lastIndexOf(' ')
                if (index < 0) index =
                    Integer.min(line.length, maxLineLength)
            } // otherwise

            // chop off the text after the space character found above
            line = line.substring(0, index)

            // append the line of text to our result, and add a line-break
            buffer.append(line)
            if (addBreak) buffer.append("<br>")

            // if a line-prefix was given, append it
            if (linePrefix != null) buffer.append(linePrefix)

            // advance our start index just beyond the line of text just determined
            start += index
        }
        else {
            // merely append the last line to our result, and we are done
            buffer.append(text.substring(start, length))
            break
        } // otherwise
    }

    return buffer.toString()
}