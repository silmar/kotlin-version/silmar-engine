package silmarengine.util.html

import java.awt.Color

/**
 * Returns the hex-string representation of the given color.
 */
fun computeColorHexString(color: Color): String {
    return Integer.toHexString(color.rgb and 0x00ffffff)
}