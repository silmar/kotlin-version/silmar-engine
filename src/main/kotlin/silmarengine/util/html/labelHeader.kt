package silmarengine.util.html

/**
 * The standard html-header to use for a label.
 */
const val labelHeader = "<html><p style='color:white;text-align:center'>"