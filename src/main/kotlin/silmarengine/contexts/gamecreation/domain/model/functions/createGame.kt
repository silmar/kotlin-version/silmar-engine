package silmarengine.contexts.gamecreation.domain.model.functions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gamecreation.domain.model.Area
import silmarengine.contexts.gamecreation.domain.model.Game
import silmarengine.contexts.gamecreation.domain.model.Player
import silmarengine.contexts.gamecreation.domain.model.PlayerAttributeValues

data class InputGameNameResult(val name: String?, val path: String?)

fun createGame(
    inputGameName: () -> InputGameNameResult, inputPlayerName: () -> String,
    selectPlayerAttributeValues: (startingValues: PlayerAttributeValues) -> PlayerAttributeValues,
    showStory: (text: String) -> Unit, createStartingArea: () -> Area
): Game? {
    // have the user input the game's name and path
    val result = inputGameName()
    if (result.name == null || result.path == null) return null

    // create the game object
    val game = Game(result.name, result.path)

    // create the player object
    val playerName = inputPlayerName()
    val player = Player(playerName)
    game.player = player

    // have the user select the player's attribute values
    player.attributeValues =
        selectPlayerAttributeValues(PlayerAttributeValues.randomize())

    showStory(storyText)

    // create the starting area
    val area = createStartingArea()
    game.startingArea = area

    // place the player at the starting point in the starting area
    player.location = area.findStart()

    return game
}

@ExtensionPoint
lateinit var storyText: String
