package silmarengine.contexts.gamecreation.domain.model

typealias GCPlayer = Player

class Player(val name: String) {
    lateinit var attributeValues: PlayerAttributeValues
    lateinit var location: PixelPoint
}