package silmarengine.contexts.gamecreation.domain.model

import silmarengine.util.list.removeRandomElement
import kotlin.random.Random

class PlayerAttributeValues(
    val values: List<Int>
) {
    companion object {
        fun randomize(): PlayerAttributeValues {
            // for each player attribute, in a random order
            val values = IntArray(5) { 11 }
            val attributeIndices = mutableListOf(0, 1, 2, 3, 4)
            var totalAdjustment = 0
            while (attributeIndices.isNotEmpty()) {
                val index = attributeIndices.removeRandomElement()

                // determine the adjustment for this attribute randomly, in the opposite
                // direction of the total adjustment so far; but if this is tne final
                // attribute left, its adjustment is the opposite of the total adjustment,
                // so that the final total adjustment is zero
                val maxAdjustment = 6
                val adjustment = when {
                    attributeIndices.isNotEmpty() -> Random.nextInt(0,
                        (maxAdjustment + 1)) * (if (totalAdjustment > 0) -1 else 1)
                    else -> -totalAdjustment
                }
                values[index] += adjustment
                totalAdjustment += adjustment
            }

            return PlayerAttributeValues(values.toList())
        }
    }
}