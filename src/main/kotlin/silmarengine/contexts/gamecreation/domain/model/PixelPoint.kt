package silmarengine.contexts.gamecreation.domain.model

typealias GCPixelPoint = PixelPoint

data class PixelPoint(val x: Int, val y: Int) {
    constructor(other: PixelPoint) : this(other.x, other.y)
}