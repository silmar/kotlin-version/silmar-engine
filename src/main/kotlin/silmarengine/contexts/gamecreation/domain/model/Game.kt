package silmarengine.contexts.gamecreation.domain.model

typealias GCGame = Game

class Game(val name: String, val path: String) {
    lateinit var player: Player
    lateinit var startingArea: Area
}