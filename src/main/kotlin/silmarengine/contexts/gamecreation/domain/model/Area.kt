package silmarengine.contexts.gamecreation.domain.model

typealias GCArea = Area

interface Area {
    fun findStart(): PixelPoint
}