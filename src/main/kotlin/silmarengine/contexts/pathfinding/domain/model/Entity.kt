package silmarengine.contexts.pathfinding.domain.model

typealias PFEntity = Entity

interface Entity {
    val bounds: PixelRectangle
    val location: PixelPoint
    val size: PixelDimensions
}