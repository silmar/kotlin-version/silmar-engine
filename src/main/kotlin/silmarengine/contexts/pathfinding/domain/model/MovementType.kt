package silmarengine.contexts.pathfinding.domain.model

typealias PFMovementType = MovementType

enum class MovementType {
    WALKING,
    WALKING_NO_HANDS,
    FLYING,
    FLYING_NO_HANDS,
    EARTHING,
    ETHEREAL
}