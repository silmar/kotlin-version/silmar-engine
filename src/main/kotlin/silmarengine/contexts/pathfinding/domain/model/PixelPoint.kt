package silmarengine.contexts.pathfinding.domain.model

typealias PFPixelPoint = PixelPoint

interface PixelPoint {
    val x: Int
    val y: Int
}

typealias PFMutablePixelPoint = MutablePixelPoint

interface MutablePixelPoint : PixelPoint {
    fun set(x: Int, y: Int): MutablePixelPoint
    fun set(p: PixelPoint): MutablePixelPoint = set(p.x, p.y)
}