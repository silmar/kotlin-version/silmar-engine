package silmarengine.contexts.pathfinding.domain.model.functions

import silmarengine.objectpools.ObjectPool
import silmarengine.objectpools.pfPixelRectanglePool as pixelRectanglePool
import silmarengine.contexts.pathfinding.domain.model.*
import silmarengine.objectpools.pfPathDequePool
import silmarengine.objectpools.pixelPointPool
import java.util.*
import java.util.function.Supplier

class FindPassiblePathResult(
    /**
     * The path of passible steps that was found (if one was).
     */
    val path: ArrayDeque<PixelPoint>?,
    /**
     * Whether an entity considered harmful to the entity for which the path is needed
     * was encountered during the search for a path (if such entities were specified
     * as needing to be considered).
     */
    val foundHarm: Boolean = false,
    /**
     * Whether only the best path (and not a path that fully reaches the goal) could
     * be returned.
     */
    val bestPathReturned: Boolean = false
)

/**
 * Returns a path of steps (i.e. pixel-locations that form implicit line segments)
 * between the given pixel-locations, where each step is passible per the
 * for the given moving-entity.  The steps returned are pixel-locations that are each one tile
 * apart from the next, and each such location is at the intersection of 4 tiles
 * (in a 2x2 square), rather than at the tile centers, so that we may find movement
 * paths for entities that are up to 2x2 tiles in size.
 *
 * See findPath for info about the path returned.
 *
 * @param goalEntity        The entity (if any) that is the goal of taking the path found
 *                          to the to-location.  We need to not count this entity when
 *                          detm'ing passibility of locations.
 * @param maxLength         See findPath.
 * @param shouldFindBestPath      See findPath.
 * @param checkForHarmfulTerrains   Whether to exclude paths that intersect terrains
 *                                  which are harmful to the given moving-entity
 *                                  (which must then be a non-null monster).
 */
fun Area.findPassiblePath(
    from_: PixelPoint?, to: PixelPoint, movingEntity: Entity, goalEntity: Entity?,
    maxLength: TileDistance, movementType: MovementType, shouldFindBestPath: Boolean,
    checkForHarmfulTerrains: Boolean,
    isDistanceAtMost: (from: PixelPoint, to: PixelPoint, atMost: PixelDistance) -> Boolean,
    getSquaredDistance: (from: PixelPoint, to: PixelPoint, use: PixelDistance) -> PixelDistance
): FindPassiblePathResult {
    // move the given from-location to a nearby passible half-tile location,
    // since this method operates only over such locations; as far as I can think,
    // this call should always succeed, as at least one of those half-tile locations should
    // be passible, but if none are, no path is possible
    val from =
        getNearbyPassibleQuarterTileLocation(from_!!, movingEntity.size, movementType,
            movingEntity, goalEntity) ?: return FindPassiblePathResult(null)

    // move the given to-location to a nearby passible half-tile location, if possible,
    // since this method operates only over such locations; if we can't, it's ok,
    // because the goal criterion will check for intersection between the moving and goal
    // entities to indicate if the goal has been reached
    var penultimateTo =
        getNearbyPassibleQuarterTileLocation(to, movingEntity.size, movementType,
            movingEntity, goalEntity)
    if (penultimateTo == null) penultimateTo = to

    // if the modified from- and to- locations are coincident
    if (from == penultimateTo) {
        // return copies (since they will be reclaimed) of the modified from-location
        // and the original to-location as the only steps needed to be taken
        val path = pfPathDequePool.supply()
        path.add(pixelPointPool.supply().set(from))
        path.add(pixelPointPool.supply().set(to))
        return FindPassiblePathResult(path)
    }

    // express our criteria for finding a suitable path
    val pathCriterion = pathCriterionPool.supply()
    pathCriterion.area = this
    pathCriterion.movingEntity = movingEntity
    pathCriterion.movementType = movementType
    pathCriterion.goalEntity = goalEntity
    pathCriterion.checkForHarmfulTerrains = checkForHarmfulTerrains
    pathCriterion.harmfulTerrainFound = false
    val goalCriterion = goalCriterionPool.supply()
    goalCriterion.movingEntity = movingEntity
    goalCriterion.goalEntity = goalEntity
    goalCriterion.penultimateTo = penultimateTo

    // try to find a path
    val result1 =
        findPath(from, penultimateTo, maxLength, pathCriterion::canUseRectangleLocation,
            goalCriterion::isAGoal, true, true, shouldFindBestPath, isDistanceAtMost,
            getSquaredDistance)
    pathCriterionPool.reclaim(pathCriterion)
    goalCriterionPool.reclaim(goalCriterion)

    // if harmful terrains were a concern
    var foundHarm = false
    if (checkForHarmfulTerrains) {
        // store in the result whether a harmful terrain was encountered during
        // the search for a path
        foundHarm = pathCriterion.harmfulTerrainFound
    }

    // if we found a path above, return that; otherwise, return the best partial path found
    return if (result1.path != null) FindPassiblePathResult(result1.path, foundHarm,
        false)
    else FindPassiblePathResult(result1.bestPath, foundHarm, true)
}

/**
 * Is used by findPassiblePath() to evaluate the passibility of potential next path-step
 * locations, perhaps taking into account the presence of terrains which would be harmful
 * for the mover to pass through or over.
 */
private class PathStepEvaluator {
    /**
     * The area on which the search for a path is taking place.
     */
    var area: Area? = null

    /**
     * The entity that's doing the moving.
     */
    var movingEntity: Entity? = null

    /**
     * The movement type to consider when checking for passibility.
     */
    lateinit var movementType: MovementType

    /**
     * The entity (if any) which represents the goal of doing the moving.
     */
    var goalEntity: Entity? = null

    /**
     * Whether the presence of harmful terrains prevents passibility.
     */
    var checkForHarmfulTerrains: Boolean = false

    /**
     * Stores whether a harmful terrain was found in the rectangle
     * judged by this criteria.
     */
    var harmfulTerrainFound: Boolean = false

    fun canUseRectangleLocation(location: PixelPoint): Boolean {
        val spotSize = movingEntity!!.size
        val result = area!!.isRectanglePassible(location, spotSize, movementType, true,
            movingEntity, goalEntity, checkForHarmfulTerrains)
        harmfulTerrainFound = harmfulTerrainFound || result.harmful
        return result.passible && (!checkForHarmfulTerrains || !result.harmful)
    }
}

/**
 * Is used by findPassiblePath() to determine whether a next path step may be
 * considered a "goal" location.  Such a goal may be a particular location, or it
 * might be any location contained by a given entity which represents the goal
 * of the movement.
 */
private class GoalDeterminer {
    /**
     * The entity that's doing the moving.
     */
    lateinit var movingEntity: Entity

    /**
     * The entity (if any) which represents the goal of doing the moving.
     */
    var goalEntity: Entity? = null

    /**
     * The goal location, to be used if no goal-entity was provided.
     */
    var penultimateTo: PixelPoint? = null

    fun isAGoal(location: PixelPoint): Boolean {
        // detm the rectangle that would be taken up by the moving entity when centered
        // at the given location
        val rectangle = pixelRectanglePool.supply()
        rectangle.setLocation(location)
        val size = movingEntity.size
        rectangle.translate(-size.width / 2, -size.height / 2)
        rectangle.setSize(size)

        // if we have a goal-entity, use its bounds as our goal rectangle
        val goal = pixelRectanglePool.supply()
        if (goalEntity != null) goal.set(goalEntity!!.bounds)
        else {
            // use the rectangle centered at the penultimate-to location, of a default size
            goal.setLocation(penultimateTo!!)
            goal.setSize(tileWidth, tileHeight)
            goal.translate(-tileWidth / 2, -tileHeight / 2)
        }

        // return whether the moving-entity's rectangle would intersect with the
        // goal rectangle
        val result = rectangle.intersects(goal)
        pixelRectanglePool.reclaim(rectangle)
        pixelRectanglePool.reclaim(goal)
        return result
    }
}

private val pathCriterionPool = ObjectPool(Supplier { PathStepEvaluator() })
private val goalCriterionPool = ObjectPool(Supplier { GoalDeterminer() })
