package silmarengine.contexts.pathfinding.domain.model.functions

import silmarengine.objectpools.dimensionPool
import silmarengine.objectpools.stepMapsPool
import silmarengine.contexts.pathfinding.domain.model.*
import silmarengine.objectpools.pfPixelDistancePool as pixelDistancePool
import silmarengine.objectpools.pfPixelPointPool as pixelPointPool
import java.awt.Point
import java.util.*
import kotlin.math.max
import kotlin.math.min

class FindPathResult(
    /**
     * The path (if any) found to the goal.
     */
    val path: ArrayDeque<PixelPoint>?,
    /**
     * If no path was found to the goal, a path that gets the closest to the goal.
     */
    val bestPath: ArrayDeque<PixelPoint>?
)

/**
 * Performs a flood-fill to find a path of steps between the given
 * pixel-locations that satisfies the given criteria, and is at most of the given
 * max-tile-length.  The steps returned are pixel-locations that are each a certain
 * orthogonal distance apart from the next.  Accordingly, the given to-location must
 * be an integer number of such steps away from the from-location.
 *
 * @param canUseRectangleLocation   Answers whether a given location is suitable as
 *                                  the center of a spot to be added to the current
 *                                  path being sought.
 * @param isAGoal   Answers whether a given pixel-location corresponds to a "goal"
 *                  location which satisfies that the given to-location has
 *                  effectively (if not exactly) been reached.
 * @param useDoubleResolution   Whether to use a half-tile granularity when trying to find
 *                              a path, which allows for more precise path determination,
 *                              as is necessary when trying to find passible paths.
 * @param pathNeeded            Whether the path should actually be returned
 *                              (vs. the caller only caring about whether a path exists).
 * @param shouldFindBestPath    Whether information about which path gets closest to the
 *                              to-location should be computed and stored.
 */
inline fun Area.findPath(
    from: PixelPoint, to: PixelPoint, maxLength: TileDistance,
    canUseRectangleLocation: (location: PixelPoint) -> Boolean,
    isAGoal: (location: PixelPoint) -> Boolean = { false }, useDoubleResolution: Boolean,
    pathNeeded: Boolean, shouldFindBestPath: Boolean,
    isDistanceAtMost: (from: PixelPoint, to: PixelPoint, atMost: PixelDistance) -> Boolean,
    getSquaredDistance: (from: PixelPoint, to: PixelPoint, use: PixelDistance) -> PixelDistance
): FindPathResult {
    // if we don't care about the best path, and the distance between the two locations
    // is greater than the max-path-length we are allowed
    if (!shouldFindBestPath) {
        val maxDistance = pixelDistancePool.supply()
        maxLength.distance * tileWidth
        val tooFar = !isDistanceAtMost(from, to, maxDistance)
        pixelDistancePool.reclaim(maxDistance)
        if (tooFar) {
            // no path is possible
            return FindPathResult(null, null)
        }
    }

    // detm the maximum amount of orthogonal steps that may be taken to reach the goal
    val stepsPerTile = if (useDoubleResolution) 2 else 1
    val maxSteps = maxLength.distance * stepsPerTile

    // label the starting step as the first step
    val stepsMap = stepMapsPool.supply()
    stepsMap[from as MutablePixelPoint] = 0

    // detm how many pixels are travelled per step
    val widthPerStep = tileWidth / stepsPerTile
    val heightPerStep = tileHeight / stepsPerTile

    // these directions are needed in one of the inner loops below
    val directions = Array(4) { Point() }
    directions[0].move(0, -heightPerStep)
    directions[1].move(0, heightPerStep)
    directions[2].move(-widthPerStep, 0)
    directions[3].move(widthPerStep, 0)

    // for each step up to the max path length
    val current = pixelPointPool.supply()
    val areaPixelSize = dimensionPool.supply()
    areaPixelSize.size = size
    areaPixelSize.width *= tileWidth
    areaPixelSize.height *= tileHeight
    val bestSquaredDistance = pixelDistancePool.supply()
    bestSquaredDistance.distance = Integer.MAX_VALUE
    val bestPathEnd = pixelPointPool.supply()
    var bestPathEndSet = false
    val squaredDistance = pixelDistancePool.supply()
    for (step in 1..maxSteps) {
        // for each tile-sized (or quarter-tile-sized) square in the square region centered on
        // the given from-location, which extends out from that location a number of
        // squares equal to the step value of the parent loop
        var i = max(0, from.x - step * widthPerStep)
        while (i <= min(from.x + step * widthPerStep, areaPixelSize.width - 1)) {
            var j = max(0, from.y - step * heightPerStep)
            while (j <= min(from.y + step * heightPerStep, areaPixelSize.height - 1)) {
                current.set(i, j)

                // if this square is numbered with the previous step-symbol
                val number = stepsMap[current]
                if (number == step - 1) {
                    // for each square to the four compass directions around this square
                    for (k in directions.indices) {
                        val direction = directions[k]
                        current.set(i + direction.x, j + direction.y)

                        // if this square hasn't yet been numbered
                        if (stepsMap[current] == null) {
                            // if this is the goal square
                            val stepLocation = pixelPointPool.supply()
                            stepLocation.set(current)
                            if (current == to || isAGoal(current)) {
                                // this square is the final step, regardless of
                                // what our path-criterion might say about it
                                stepsMap[stepLocation] = step

                                // assemble the path of steps to the goal if that path
                                // is needed by the caller, otherwise, just return an
                                // empty list to at least signal that a path exists

                                // we are done
                                val path =
                                    if (pathNeeded) stepsMap.getPath(current, directions)
                                    else emptyPath
                                pixelPointPool.reclaim(current)
                                pixelPointPool.reclaim(bestPathEnd)
                                stepMapsPool.reclaim(stepsMap)
                                dimensionPool.reclaim(areaPixelSize)
                                pixelDistancePool.reclaim(squaredDistance)
                                pixelDistancePool.reclaim(bestSquaredDistance)
                                return FindPathResult(path, null)
                            }

                            // number this square with this step if it
                            // meets our path-criterion, or with a can't-use
                            // value otherwise
                            val canUse = canUseRectangleLocation(current)
                            stepsMap[stepLocation] = if (canUse) step else cannotUseAsStep

                            // if this square represents a usable step, and we are to detm
                            // the best path to the goal
                            if (canUse && shouldFindBestPath) {
                                // if the distance from this square to the goal is the
                                // least found so far
                                getSquaredDistance(current, to, squaredDistance)
                                if (squaredDistance.distance < bestSquaredDistance.distance) {
                                    // remember this square as the end of the new current
                                    // best path
                                    bestSquaredDistance.distance =
                                        squaredDistance.distance
                                    bestPathEnd.set(current)
                                    bestPathEndSet = true
                                }
                            }
                        }
                    }
                }
                j += heightPerStep
            }
            i += widthPerStep
        }
    }

    // if we are to detm the best-path towards the goal, and a closest path end location
    // was determined above
    var bestPath: ArrayDeque<PixelPoint>? = null
    if (shouldFindBestPath && bestPathEndSet) {
        // compute and store the path back from the closest path end location
        bestPath = stepsMap.getPath(bestPathEnd, directions)
    }

    // if desired for debug purposes, print the portion of the steps array used
    // during this call, to help depict why a path wasn't found
    val enableDebug = false
    if (pathNeeded && enableDebug) stepsMap.print(from, to, maxSteps, widthPerStep,
        heightPerStep)

    pixelPointPool.reclaim(current)
    pixelPointPool.reclaim(bestPathEnd)
    stepMapsPool.reclaim(stepsMap)
    dimensionPool.reclaim(areaPixelSize)
    pixelDistancePool.reclaim(squaredDistance)
    pixelDistancePool.reclaim(bestSquaredDistance)

    // we didn't find a full path
    return FindPathResult(null, bestPath)
}

/**
 * A list that represents an empty path.  This is returned by findPath when a path is
 * found but the client doesn't care about its contents (which we therefore
 * do not compute).
 */
val emptyPath = ArrayDeque<PixelPoint>()

/**
 * A special step-marker that indicates a location cannot used as a step in the path
 * being sought.
 */
const val cannotUseAsStep = -2

