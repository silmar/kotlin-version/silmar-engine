package silmarengine.contexts.pathfinding.domain.model

typealias PFPixelDimensions = PixelDimensions

interface PixelDimensions {
    val width: Int
    val height: Int
}