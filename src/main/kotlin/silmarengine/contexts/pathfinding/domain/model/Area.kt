package silmarengine.contexts.pathfinding.domain.model

import java.awt.Dimension

typealias PFArea = Area

interface Area {
    val size: Dimension
    fun getNearbyPassibleQuarterTileLocation(
        from: PixelPoint, forEntitySize: PFPixelDimensions, movementType: PFMovementType,
        forEntity: Entity?, toEntity: Entity?
    ): PixelPoint?

    fun isRectanglePassible(
        location: PixelPoint, size: PixelDimensions, movementType: MovementType,
        checkForBeing: Boolean, forEntity: Entity?, forEntity2: Entity?,
        checkForHarmfulTerrains: Boolean
    ): IsRectanglePassibleResult
}

typealias PFIsRectanglePassibleResult = IsRectanglePassibleResult

interface IsRectanglePassibleResult {
    val passible: Boolean
    val harmful: Boolean
}