package silmarengine.contexts.pathfinding.domain.model

import silmarengine.objectpools.pfPathDequePool
import silmarengine.objectpools.pfPixelPointPool
import java.awt.Point
import java.util.*

/**
 * Is a mapping of locations that make up a path to the step numbers they represent
 * for that path.
 */
class StepsMap {
    private val steps = HashMap<MutablePixelPoint, Int?>()

    operator fun get(key: MutablePixelPoint): Int? = steps[key]
    operator fun set(key: MutablePixelPoint, step: Int?) {
        steps[key] = step
    }

    fun clear() {
        steps.clear()
    }

    /**
     * Returns a path of pixel-locations retrieved from searching backwards through the
     * given steps-array from the given goal step-location, to eventually find
     * the originating from-location of the path.
     */
    fun getPath(goal: PixelPoint, directions: Array<Point>): ArrayDeque<PixelPoint> {
        // the end of the path is the goal square
        val path = pfPathDequePool.supply()
        path.add(pfPixelPointPool.supply().set(goal))

        // determine the length of the path to be retrieved
        var step = steps[goal]!!

        // keep doing this
        val current = pfPixelPointPool.supply()
        current.set(goal)
        val test = pfPixelPointPool.supply()
        nextStep@ while (true) {
            // for each square to the four compass directions around this square
            for (k in directions.indices) {
                val direction = directions[k]
                test.set(current.x + direction.x, current.y + direction.y)

                // if this square's step # is one less than the current square's step #
                val testStepNum = steps[test]
                if (testStepNum == step - 1) {
                    // add this step at the front of our path
                    val stepLocation = pfPixelPointPool.supply()
                    stepLocation.set(test)
                    path.push(stepLocation)

                    // if we were trying to find the last step
                    if (step == 1) {
                        // we now have our path, so return all step-locations that were
                        // generated during the path determination to their pool,
                        // and clear the steps map
                        steps.keys.forEach { pfPixelPointPool.reclaim(it) }
                        steps.clear()

                        // we now have traced the path back to its beginning; we are done
                        pfPixelPointPool.reclaim(current)
                        pfPixelPointPool.reclaim(test)
                        return path
                    }

                    // this step is now the current step in our path
                    current.set(test)
                    step--

                    continue@nextStep
                }
            }
        }
    }

    /**
     * For debug purposes: prints out the specified portion of this steps-map,
     * to help depict why a path wasn't found
     */
    fun print(
        from: PixelPoint, to: PixelPoint,
        maxSteps: Int, widthPerStep: Int, heightPerStep: Int
    ) {
        println("Failed to find path:")
        println("from:$from")
        println("to:$to")
        val location = pfPixelPointPool.supply()
        for (j in -maxSteps..maxSteps) {
            for (i in -maxSteps..maxSteps) {
                location.set(from.x + i * widthPerStep, from.y + j * heightPerStep)
                val stepNumber = steps[location]
                if (stepNumber == null) print('-')
                else {
                    when {
                        location == to -> print('*')
                        stepNumber < 0 -> print('-')
                        stepNumber < 10 -> print(stepNumber)
                        else -> print(('a'.toInt() + stepNumber - 10).toChar())
                    }
                }
            }

            println()
        }
    }
}