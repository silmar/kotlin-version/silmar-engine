package silmarengine.contexts.pathfinding.domain.model

typealias PFTileDistance = TileDistance

interface TileDistance {
    var distance: Int
}