package silmarengine.contexts.pathfinding.domain.model

typealias PFPixelRectangle = PixelRectangle

interface PixelRectangle {
    val x: Int
    val y: Int
    val width: Int
    val height: Int

    fun intersects(other: PixelRectangle): Boolean
}

typealias PFMutablePixelRectangle = MutablePixelRectangle

interface MutablePixelRectangle : PixelRectangle {
    fun setLocation(to: PixelPoint): MutablePixelRectangle
    fun setSize(width: Int, height: Int): MutablePixelRectangle
    fun setSize(size: PixelDimensions): MutablePixelRectangle
    fun set(other: PixelRectangle): MutablePixelRectangle
    fun translate(dx: Int, dy: Int): MutablePixelRectangle
}