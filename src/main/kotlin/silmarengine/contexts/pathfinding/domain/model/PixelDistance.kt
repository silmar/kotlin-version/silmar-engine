package silmarengine.contexts.pathfinding.domain.model

typealias PFPixelDistance = PixelDistance

interface PixelDistance {
    var distance: Int
}