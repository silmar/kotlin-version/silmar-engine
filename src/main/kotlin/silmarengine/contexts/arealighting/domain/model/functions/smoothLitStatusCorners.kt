package silmarengine.contexts.arealighting.domain.model.functions

import silmarengine.contexts.arealighting.domain.model.LitStatus

/**
 * Changes most of the fully-blocked los-statuses (within this determiner's array of such statuses)
 * that are on the boundaries of larger los-blocked areas, such that they form part of a jagged edge
 * in the display, to diagonal half-blocked los-statuses, which helps to smooth out the appearance of
 * such areas.
 */
fun smoothLitStatusCorners(statuses: Array<Array<LitStatus>>) {
    // for each element of this lit-determiner's lit-statuses array
    for (i in statuses.indices) {
        for (j in statuses[i].indices) {
            if (statuses[i][j] === LitStatus.HALF_LIT) processStatus(
                i, j, statuses, true)
            else if (statuses[i][j] === LitStatus.UNLIT) processStatus(
                i, j, statuses, false)
        }
    }
}

private fun processStatus(
    i: Int, j: Int, statuses: Array<Array<LitStatus>>, isForHalfVsFull: Boolean
) {
    val lessLitStatus = if (isForHalfVsFull) LitStatus.HALF_LIT else LitStatus.UNLIT
    val moreLitStatus = if (isForHalfVsFull) LitStatus.FULLY_LIT else LitStatus.HALF_LIT

    // determine in which of the four cardinal directions the los-status is also less-lit (fully)
    val upLessLit = j == 0 || statuses[i][j - 1] === lessLitStatus
    val downLessLit = j == statuses[i].size - 1 || statuses[i][j + 1] === lessLitStatus
    val leftLessLit = i == 0 || statuses[i - 1][j] === lessLitStatus
    val rightLessLit = i == statuses.size - 1 || statuses[i + 1][j] === lessLitStatus

    // determine in which of the four cardinal directions the lit-status is more lit
    val upMoreLit = j != 0 && statuses[i][j - 1] === moreLitStatus
    val downMoreLit = j != statuses[i].size - 1 && statuses[i][j + 1] === moreLitStatus
    val leftMoreLit = i != 0 && statuses[i - 1][j] === moreLitStatus
    val rightMoreLit = i != statuses.size - 1 && statuses[i + 1][j] === moreLitStatus

    // if the number of statuses in the four cardinal directions above that were less-lit
    // is two, likely implying a sharp corner
    var numLessLit = 0
    if (upLessLit) numLessLit++
    if (downLessLit) numLessLit++
    if (leftLessLit) numLessLit++
    if (rightLessLit) numLessLit++
    if (numLessLit == 2) {
        // if the two less-lit directions are cardinally adjacent, and the other two
        // cardinal directions are more lit, then change the status at this element to be
        // diagonally less-lit/more-lit, only for the purposes of display
        var status: LitStatus? = null
        if (upLessLit && leftLessLit && downMoreLit && rightMoreLit) status =
            if (isForHalfVsFull) LitStatus.HALF_LIT_UPPER_LEFT else LitStatus.UNLIT_UPPER_LEFT
        else if (upLessLit && rightLessLit && downMoreLit && leftMoreLit) status =
            if (isForHalfVsFull) LitStatus.HALF_LIT_UPPER_RIGHT else LitStatus.UNLIT_UPPER_RIGHT
        else if (downLessLit && leftLessLit && upMoreLit && rightMoreLit) status =
            if (isForHalfVsFull) LitStatus.HALF_LIT_LOWER_LEFT else LitStatus.UNLIT_LOWER_LEFT
        else if (downLessLit && rightLessLit && upMoreLit && leftMoreLit) status =
            if (isForHalfVsFull) LitStatus.HALF_LIT_LOWER_RIGHT else LitStatus.UNLIT_LOWER_RIGHT
        if (status != null) statuses[i][j] = status
    }
}