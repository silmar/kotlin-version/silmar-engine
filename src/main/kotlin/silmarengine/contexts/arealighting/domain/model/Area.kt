package silmarengine.contexts.arealighting.domain.model

typealias ALArea = Area

interface Area {
    val lightSources: List<LightSource>
    val isSelfLit: Boolean

    fun getTile(at: PixelPoint): Tile
    fun considerLightSourceFirst(lightSource: LightSource)
    fun canSee(from: PixelPoint, to: PixelPoint): Boolean
}