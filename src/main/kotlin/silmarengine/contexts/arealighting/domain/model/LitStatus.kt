package silmarengine.contexts.arealighting.domain.model

typealias ALLitStatus = LitStatus

enum class LitStatus {
    UNLIT,
    UNLIT_UPPER_LEFT,
    UNLIT_UPPER_RIGHT,
    UNLIT_LOWER_LEFT,
    UNLIT_LOWER_RIGHT,
    HALF_LIT,
    HALF_LIT_UPPER_LEFT,
    HALF_LIT_UPPER_RIGHT,
    HALF_LIT_LOWER_LEFT,
    HALF_LIT_LOWER_RIGHT,
    FULLY_LIT;

    val isHalfOrMoreLit: Boolean get() = ordinal >= HALF_LIT.ordinal
}
