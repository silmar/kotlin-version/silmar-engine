package silmarengine.contexts.arealighting.domain.model

typealias ALTile = Tile

interface Tile {
    val blocksLOS: Boolean
}

const val tileWidth = 32
const val tileHeight = 32