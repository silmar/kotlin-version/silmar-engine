package silmarengine.contexts.arealighting.domain.model.functions

import silmarengine.contexts.arealighting.domain.model.*
import silmarengine.objectpools.ObjectPool

/**
 * Returns whether the given location is being lit (from the perspective of the given
 * viewpoint, if any) by at least one of the light-sources in this area, or is somehow lit to the
 * given entity.
 *
 * @param inLosLightSource      A light source (if any) which has already been determined to
 *                              be in LOS of the given location, such that this
 *                              determination need not be made again.
 * @return                      See IsLitResult.
 */
inline fun Area.isLit(
    location: PixelPoint, viewpoint: PixelPoint? = null,
    inLosLightSource: LightSource? = null, pixelPointPool: ObjectPool<MutablePixelPoint>,
    tilePointPool: ObjectPool<MutableTilePoint>, pixelDistancePool: ObjectPool<PixelDistance>,
    isDistanceAtMost: (from: PixelPoint, to: PixelPoint, most: PixelDistance) -> Boolean,
    getNearestTileCorner: (from: PixelPoint, to: PixelPoint, use: PixelPoint) -> PixelPoint
): LitStatus {
    // if this area is self-lit, the given location is lit
    if (isSelfLit) return LitStatus.FULLY_LIT

    // if the first lightsource in this area's list of such sources is not the given
    // in-los-source
    if (inLosLightSource != null && lightSources.isNotEmpty() && lightSources[0] !== inLosLightSource) {
        // move the given in-los-source to the head of the list, so it gets processed
        // first, since it's the quickest to process, and most likely to bring a positive
        // result
        considerLightSourceFirst(inLosLightSource)
    }

    // for each lightsource in this area
    val nearestCorner = pixelPointPool.supply()
    val sourceLocation = pixelPointPool.supply()
    val tileLocation = tilePointPool.supply()
    val halfLightRadius = pixelDistancePool.supply()
    var result = LitStatus.UNLIT
    lightSources.firstOrNull first@{ source ->
        // if this lightsource isn't on, skip it
        if (!source.isEmittingLight) return@first false

        // consider this lightsource to be emitting from the center of the tile on which
        // it's located; this keeps any movement within that tile by the lightsource from
        // affecting the consistency of the results returned by this method
        source.location.toTilePoint(tileLocation)
        tileLocation.toPixelPoint(sourceLocation)

        // if this lightsource is close enough to be shining on the given location, and
        // the given location is in LOS of the lightsource
        val lightRadius = source.lightRadius
        if (isDistanceAtMost(sourceLocation, location,
                lightRadius) && (source === inLosLightSource || canSee(sourceLocation,
                getNearestTileCorner(sourceLocation, location, nearestCorner)))) {
            // if this source isn't the given in-LOS lightsource, and the tile at the
            // location in question blocks LOS, and if the lightSource can't be seen from
            // the viewpoint
            if (source !== inLosLightSource && getTile(
                    location).blocksLOS && viewpoint != null && !canSee(viewpoint,
                    sourceLocation)) {
                // this lightsource should not contribute to the lighting of the location
                return@first false
            }

            // determine whether the location is fully lit or only half-lit by this lightsource
            // according to whether it's more than half the source's light-radius away
            // from that source; the location may also be fully lit if this source half-lights
            // it, and it is already half-lit by another source
            halfLightRadius.distance = source.lightRadius.distance * 2 / 3
            val fullyLit = isDistanceAtMost(sourceLocation, location, halfLightRadius)
            result = if (fullyLit || result == LitStatus.HALF_LIT) LitStatus.FULLY_LIT
            else LitStatus.HALF_LIT

            // if the location is determined to be fully lit, stop there, since
            // it cannot be lit any more greatly
            if (result == LitStatus.FULLY_LIT) return@first true
        }

        return@first false
    }

    pixelPointPool.reclaim(nearestCorner)
    pixelPointPool.reclaim(sourceLocation)
    tilePointPool.reclaim(tileLocation)
    pixelDistancePool.reclaim(halfLightRadius)
    return result
}
