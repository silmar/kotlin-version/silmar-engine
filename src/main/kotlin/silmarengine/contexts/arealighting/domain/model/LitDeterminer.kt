package silmarengine.contexts.arealighting.domain.model

import silmarengine.annotations.UpdatesUI
import silmarengine.contexts.arealighting.domain.model.functions.isLit
import silmarengine.contexts.arealighting.domain.model.functions.smoothLitStatusCorners
import silmarengine.objectpools.ObjectPool
import silmarengine.util.gui.runOnSwingThread

/**
 * Determines which squares within the current area-view are lit around a current,
 * shifted viewpoint.
 *
 * This is done for view purposes only.  To determine the lighting of a location for
 * reasons relevant to game-play mechanics, call Area.isLit() directly.
 */
class LitDeterminer(
    private val pixelPointPool: ObjectPool<MutablePixelPoint>,
    private val tilePointPool: ObjectPool<MutableTilePoint>,
    private val pixelDistancePool: ObjectPool<PixelDistance>,
    private val isNightVisionInUse: () -> Boolean,
    private val isDistanceAtMost: (from: PixelPoint, to: PixelPoint, most: PixelDistance) -> Boolean,
    private val getNearestTileCorner: (from: PixelPoint, to: PixelPoint, use: PixelPoint) -> PixelPoint
) {
    /**
     * Holds the lit-status for the tiles within the area-view which are around the
     * current (shifted) viewpoint.
     */
    private lateinit var litStatuses: Array<Array<LitStatus>>

    /**
     * The area for which the lighting is being determined.
     */
    private var area: Area? = null

    fun setArea(to: Area) {
        area = to
    }

    /**
     * The area tile-location corresponding to the upper-leftmost element of the
     * lit-statuses array.
     */
    private val upperLeftTileLocation = tilePointPool.supply()

    fun setUpperLeftTileLocation(to: TilePoint) {
        if (!to.matches(upperLeftTileLocation)) {
            upperLeftTileLocation.set(to)
            determineStatuses()
        }
    }

    /**
     * Whether lighting determination should be currently applied (vs. just considering
     * all tiles to be fully lit).
     */
    var shouldConsiderLighting: Boolean = true
        set(value) {
            field = value
            determineStatuses()
        }

    /**
     * The shifted viewpoint within the area which should be considered as the
     * observer for the lighting being determined.
     */
    private val viewpoint = pixelPointPool.supply()

    fun setViewpoint(to: PixelPoint) {
        if (!to.matches(viewpoint)) {
            viewpoint.set(to)
            determineStatuses()
        }
    }

    /**
     * A light source (if any) which has already been determined to be in LOS of the
     * viewpoint, such that this determination need not be made again.
     */
    var inLosLightSource: LightSource? = null

    /**
     * Determines the lit-status for every element in this determiner's array of such statuses.
     */
    fun determineStatuses() = runOnSwingThread { determineStatuses_() }

    @UpdatesUI
    private fun determineStatuses_() {
        val area = area ?: return

        // for each element of this lit-determiner's lit-statuses array
        val pixelAt = pixelPointPool.supply()
        val isNightVisionInUse = isNightVisionInUse()
        for (i in litStatuses.indices) {
            for (j in litStatuses[i].indices) {
                // determine the area pixel location corresponding to the center of this array element
                upperLeftTileLocation.toPixelPoint(pixelAt)
                pixelAt.translate(i * tileWidth, j * tileHeight)

                // detm the lit status at the location determined above
                var status = if (shouldConsiderLighting) area.isLit(pixelAt, viewpoint,
                    inLosLightSource, pixelPointPool, tilePointPool,
                    pixelDistancePool, isDistanceAtMost, getNearestTileCorner)
                else LitStatus.FULLY_LIT
                if (status == LitStatus.UNLIT && isNightVisionInUse) status =
                    LitStatus.HALF_LIT
                litStatuses[i][j] = status
            }
        }
        pixelPointPool.reclaim(pixelAt)

        smoothLitStatusCorners(litStatuses)
    }

    /**
     * Informs this determiner that the area-view has been resized.
     */
    fun onResized(viewTileWidth: Int, viewTileHeight: Int) {
        litStatuses = Array(viewTileWidth) {
            Array(viewTileHeight) { LitStatus.UNLIT }
        }
        determineStatuses()
    }

    /**
     * Returns this determiner's description of how well the given location is lit.
     */
    internal fun getLitStatus(location: TilePoint): LitStatus {
        // if the given location is outside the bounds being overseen by this
        // determiner, return an 'unlit' result
        val upperLeft = upperLeftTileLocation
        val xIndex = location.x - upperLeft.x
        val yIndex = location.y - upperLeft.y
        return if (xIndex < 0 || yIndex < 0 || xIndex >= litStatuses.size || yIndex >= litStatuses[0].size) LitStatus.UNLIT
        else litStatuses[xIndex][yIndex]
    }
}
