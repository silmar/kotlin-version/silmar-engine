package silmarengine.contexts.arealighting.domain.model

typealias ALPixelDistance = PixelDistance

interface PixelDistance {
    var distance: Int
}