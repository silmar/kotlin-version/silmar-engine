package silmarengine.contexts.arealighting.domain.model

/**
 * An entity which is capable of illuminating the area around it.
 */
interface LightSource {
    /**
     * Whether this lightsource is currently emitting light.
     */
    val isEmittingLight: Boolean

    /**
     * The distance this lightsource shines all around itself.
     */
    val lightRadius: PixelDistance

    /**
     * The area location from which this light-source is emitting light.
     */
    val location: PixelPoint
}
