package silmarengine.contexts.lineofsightblocking.domain.model

typealias LOSBLOSStatus = LOSStatus

enum class LOSStatus {
    UNBLOCKED,
    BLOCKED,
    BLOCKED_UPPER_LEFT,
    BLOCKED_UPPER_RIGHT,
    BLOCKED_LOWER_LEFT,
    BLOCKED_LOWER_RIGHT
}