package silmarengine.contexts.lineofsightblocking.domain.model.functions

import silmarengine.annotations.UpdatesUI
import silmarengine.contexts.lineofsightblocking.domain.model.*

/**
 * Changes most of the fully-blocked los-statuses (within this determiner's array of such statuses)
 * that are on the boundaries of larger los-blocked areas, such that they form part of a jagged edge
 * in the display, to diagonal half-blocked los-statuses, which helps to smooth out the appearance of
 * such areas.
 */
@UpdatesUI
fun smoothCorners(
    statuses: Array<Array<LOSStatus>>, viewpoint: PixelPoint,
    upperLeftTileLocation: TilePoint
) {
    // determine where within this los-determiner's los-statuses array the viewpoint is located
    val viewpointX = viewpoint.x / tileWidth - upperLeftTileLocation.x
    val viewpointY = viewpoint.y / tileHeight - upperLeftTileLocation.y

    // for each element of this los-determiner's los-statuses array
    val blocked = LOSStatus.BLOCKED
    val clear = LOSStatus.UNBLOCKED
    for (i in statuses.indices) {
        for (j in statuses[i].indices) {
            // if the los-status of this element is fully blocked
            if (statuses[i][j] === blocked) {
                // determine in which of the four cardinal directions the los-status is also fully blocked
                val upBlocked = j == 0 || statuses[i][j - 1] === blocked
                val downBlocked =
                    j == statuses[i].size - 1 || statuses[i][j + 1] === blocked
                val leftBlocked = i == 0 || statuses[i - 1][j] === blocked
                val rightBlocked =
                    i == statuses.size - 1 || statuses[i + 1][j] === blocked

                // determine in which of the four cardinal directions the los-status is fully clear
                val upClear = j != 0 && statuses[i][j - 1] === clear
                val downClear = j != statuses[i].size - 1 && statuses[i][j + 1] === clear
                val leftClear = i != 0 && statuses[i - 1][j] === clear
                val rightClear = i != statuses.size - 1 && statuses[i + 1][j] === clear

                // if the number of statuses in the four cardinal directions above
                // that were los-blocked is two, likely implying a sharp corner
                var numBlocked = 0
                if (upBlocked) numBlocked++
                if (downBlocked) numBlocked++
                if (leftBlocked) numBlocked++
                if (rightBlocked) numBlocked++
                if (numBlocked == 2) {
                    // if the two los-blockages are cardinally adjacent, and the other
                    // two cardinal direction ares fully clear, and the viewpoint is
                    // in a direction that is (mostly) opposite to the line between
                    // the two blockages, then change the status at this element to be
                    // diagonally half-blocked/half-clear, only for the purposes of
                    // display
                    var status: LOSStatus? = null
                    if (upBlocked && leftBlocked && downClear && rightClear && !(i <= viewpointX && j <= viewpointY)) status =
                        LOSStatus.BLOCKED_UPPER_LEFT
                    else if (upBlocked && rightBlocked && downClear && leftClear && !(i >= viewpointX && j <= viewpointY)) status =
                        LOSStatus.BLOCKED_UPPER_RIGHT
                    else if (downBlocked && leftBlocked && upClear && rightClear && !(i <= viewpointX && j >= viewpointY)) status =
                        LOSStatus.BLOCKED_LOWER_LEFT
                    else if (downBlocked && rightBlocked && upClear && leftClear && !(i >= viewpointX && j >= viewpointY)) status =
                        LOSStatus.BLOCKED_LOWER_RIGHT
                    if (status != null) statuses[i][j] = status
                }
                // else, if the number of statuses in the four cardinal directions
                // above that were los-blocked is one, likely implying two sharp
                // corners
                else if (numBlocked == 1) {
                    // if the other three cardinal directions are fully clear, and the
                    // viewpoint is in one of the two directions opposite to the
                    // adjacent blockages, then change the status at this element to
                    // be diagonally half-blocked/half-clear, only for the purposes of
                    // display
                    var status: LOSStatus? = null
                    if (upBlocked && leftClear && downClear && rightClear) {
                        if (j <= viewpointY) status =
                            if (i <= viewpointX) LOSStatus.BLOCKED_UPPER_RIGHT
                            else LOSStatus.BLOCKED_UPPER_LEFT
                    }
                    else if (downBlocked && leftClear && upClear && rightClear) {
                        if (j >= viewpointY) status =
                            if (i <= viewpointX) LOSStatus.BLOCKED_LOWER_RIGHT
                            else LOSStatus.BLOCKED_LOWER_LEFT
                    }
                    else if (leftBlocked && upClear && downClear && rightClear) {
                        if (i <= viewpointX) status =
                            if (j <= viewpointY) LOSStatus.BLOCKED_LOWER_LEFT
                            else LOSStatus.BLOCKED_UPPER_LEFT
                    }
                    else if (rightBlocked && upClear && downClear && leftClear) {
                        if (i >= viewpointX) status =
                            if (j <= viewpointY) LOSStatus.BLOCKED_LOWER_RIGHT
                            else LOSStatus.BLOCKED_UPPER_RIGHT
                    }
                    if (status != null) statuses[i][j] = status
                }
            }
        }
    }
}
