package silmarengine.contexts.lineofsightblocking.domain.model

typealias LOSBTile = Tile

interface Tile {
    val blocksLOS: Boolean
}

const val tileWidth = 32
const val tileHeight = 32