package silmarengine.contexts.lineofsightblocking.domain.model

typealias LOSBArea = Area

interface Area {
    fun getTile(at: PixelPoint): Tile
    fun getBeingAt(at: PixelPoint): Being?
}