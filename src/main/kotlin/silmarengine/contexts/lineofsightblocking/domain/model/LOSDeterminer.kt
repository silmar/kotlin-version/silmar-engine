package silmarengine.contexts.lineofsightblocking.domain.model

import silmarengine.annotations.UpdatesUI
import silmarengine.contexts.lineofsightblocking.domain.model.functions.CanSeeResult
import silmarengine.contexts.lineofsightblocking.domain.model.functions.canSee
import silmarengine.contexts.lineofsightblocking.domain.model.functions.smoothCorners
import silmarengine.objectpools.ObjectPool
import silmarengine.util.gui.runOnSwingThread
import java.awt.geom.Point2D

/**
 * Reports which squares are visible from the current (non-shifted) viewpoint.
 */
class LOSDeterminer(
    private val pixelPointPool: ObjectPool<MutablePixelPoint>,
    createTilePoint: () -> MutableTilePoint,
    private val floatPointPool: ObjectPool<Point2D.Float>,
    private val adjustToTileCenter: (p: PixelPoint, use: MutablePixelPoint) -> PixelPoint,
    private val getUnitVector: (from: PixelPoint, to: PixelPoint, use: Point2D.Float) -> Point2D.Float,
    private val getDistance: (from: PixelPoint, to: PixelPoint, use: PixelDistance) -> PixelDistance,
    private val getNearestTileCorner: (from: PixelPoint, to: PixelPoint, use: PixelPoint) -> PixelPoint
) {
    /**
     * An array whose elements corresponds to squares around the current (shifted)
     * viewpoints and whose element values describe the LOS-status between the non-shifted
     * viewpoint and the particular squares.
     */
    private lateinit var losStatuses: Array<Array<LOSStatus>>

    /**
     * The area for which the lighting is being determined.
     */
    private var area: Area? = null

    fun setArea(to: Area) {
        area = to
    }

    /**
     * The location corresponding to the upper-leftmost element of the
     * los-statuses array.
     */
    private val upperLeftTileLocation = createTilePoint()

    fun setUpperLeftTileLocation(to: TilePoint) {
        if (!to.matches(upperLeftTileLocation)) {
            upperLeftTileLocation.set(to)

            // we must re-determine all los-statuses, since this change might be due to a
            // viewpoint shift, rather than a viewpoint-entity movement
            determineStatuses()
        }
    }

    var shouldUseLOSBlocking: Boolean = true
        set(value) {
            field = value
            determineStatuses()
        }

    /**
     * The shifted viewpoint within the area which should be considered as the
     * observer for the lighting being determined.
     */
    private val viewpoint = pixelPointPool.supply()

    fun setViewpoint(to: PixelPoint) {
        if (!to.matches(viewpoint)) {
            viewpoint.set(to)
            determineStatuses()
        }
    }

    fun canSee(from: PixelPoint, to: PixelPoint, pixelsPerStep: Int): CanSeeResult =
        area?.canSee(from, to, pixelPointPool, floatPointPool, adjustToTileCenter,
            getUnitVector, getDistance, pixelsPerStep, true) ?: CanSeeResult(false, false)

    /**
     * Determines the los-status for every element in this los-determiner's array of
     * such statuses.
     */
    fun determineStatuses() = runOnSwingThread { determineStatuses_() }

    @UpdatesUI
    private fun determineStatuses_() {
        // for each element of this los-determiner's los-statuses array
        val pixelTo = pixelPointPool.supply()
        val nearestCorner = pixelPointPool.supply()
        for (i in losStatuses.indices) {
            for (j in losStatuses[i].indices) {
                // calculate whether there is LOS between the (unshifted) viewpoint
                // and the pixel location in the area that is at the center of this
                // array element
                upperLeftTileLocation.toPixelPoint(pixelTo)
                pixelTo.translate(i * tileWidth, j * tileHeight)
                getNearestTileCorner(viewpoint, pixelTo, nearestCorner)
                var result = canSee(viewpoint, nearestCorner, tileWidth)

                // if the LOS check failed on the last step along the line checked
                if (!result.canSee && result.failedOnLastStep) {
                    // check the LOS again, but with a slightly smaller step-size, to
                    // perhaps get a more accurate result
                    result = canSee(viewpoint, nearestCorner, tileWidth - 1)

                    // if the LOS check again failed on the last step along the line
                    // checked
                    if (!result.canSee && result.failedOnLastStep) {
                        // check the LOS yet again, but with an even slightly smaller
                        // step-size, to perhaps get a more accurate result
                        result = canSee(viewpoint, nearestCorner, tileWidth - 3)
                    }
                }

                // store the LOS result in our array of LOS-statuses
                losStatuses[i][j] =
                    if (result.canSee) LOSStatus.UNBLOCKED else LOSStatus.BLOCKED
            }
        }
        pixelPointPool.reclaim(pixelTo)
        pixelPointPool.reclaim(nearestCorner)

        smoothCorners(losStatuses, viewpoint, upperLeftTileLocation)
    }

    /**
     * Informs this determiner that the area-view has been resized.
     */
    fun onResized(viewTileWidth: Int, viewTileHeight: Int) {
        losStatuses = Array(viewTileWidth) {
            Array(viewTileHeight) { LOSStatus.UNBLOCKED }
        }
        determineStatuses()
    }

    /**
     * Returns the LOS-status between the current (non-shifted) viewpoint and the
     * given area tile-location.
     */
    internal fun getLOSStatus(at: TilePoint): LOSStatus {
        if (!shouldUseLOSBlocking) return LOSStatus.UNBLOCKED

        // if the given location is outside the bounds being overseen by this determiner,
        // return a 'blocked' result
        val upperLeft = upperLeftTileLocation
        val xIndex = at.x - upperLeft.x
        val yIndex = at.y - upperLeft.y
        return if (xIndex < 0 || yIndex < 0 || xIndex >= losStatuses.size || yIndex >= losStatuses[0].size) LOSStatus.BLOCKED else losStatuses[xIndex][yIndex]
    }
}