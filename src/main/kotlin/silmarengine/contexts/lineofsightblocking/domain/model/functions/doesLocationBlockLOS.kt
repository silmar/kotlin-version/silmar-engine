package silmarengine.contexts.lineofsightblocking.domain.model.functions

import silmarengine.contexts.lineofsightblocking.domain.model.Area
import silmarengine.contexts.lineofsightblocking.domain.model.Being
import silmarengine.contexts.lineofsightblocking.domain.model.PixelPoint

/**
 * Returns whether the given location blocks LOS.
 *
 * @param beingsBlockLOS    Whether beings are considered to block LOS for this call.
 * @param ignore1           If beingsBlockLOS == true, a being to ignore for LOS purposes.
 * @param ignore2           Another being to ignore, as in ignore1.
 */
fun Area.doesLocationBlockLOS(
    location: PixelPoint, beingsBlockLOS: Boolean, ignore1: Being?, ignore2: Being?
): Boolean {
    if (getTile(location).blocksLOS) return true

    if (beingsBlockLOS) {
        val being = getBeingAt(location)
        if (being != null && being !== ignore1 && being !== ignore2) return true
    }

    return false
}
