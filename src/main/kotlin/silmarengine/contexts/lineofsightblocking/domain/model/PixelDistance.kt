package silmarengine.contexts.lineofsightblocking.domain.model

typealias LOSBPixelDistance = PixelDistance

interface PixelDistance {
    var distance: Int
}