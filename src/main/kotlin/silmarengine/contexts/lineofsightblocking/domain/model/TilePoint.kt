package silmarengine.contexts.lineofsightblocking.domain.model

typealias LOSBTilePoint = TilePoint

interface TilePoint {
    val x: Int
    val y: Int

    fun toPixelPoint(use: MutablePixelPoint) {
        use.set(x * tileWidth + tileWidth / 2, y * tileHeight + tileHeight / 2)
    }

    fun matches(other: TilePoint): Boolean = other.x == x && other.y == y
}

typealias LOSBMutableTilePoint = MutableTilePoint

interface MutableTilePoint : TilePoint {
    fun set(x: Int, y: Int): MutableTilePoint
    fun set(p: TilePoint): MutableTilePoint = set(p.x, p.y)
}