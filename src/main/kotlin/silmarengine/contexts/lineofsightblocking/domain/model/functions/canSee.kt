package silmarengine.contexts.lineofsightblocking.domain.model.functions

import silmarengine.contexts.lineofsightblocking.domain.model.*
import silmarengine.objectpools.ObjectPool
import silmarengine.objectpools.losbPixelDistancePool
import java.awt.geom.Point2D
import kotlin.math.roundToInt

data class CanSeeResult(
    val canSee: Boolean,
    /**
     * If the canSee value is false, this indicates whether visibility was found to be
     * blocked at the last step of the line being checked.  In such cases, re-calling the
     * method with a smaller step size can sometimes produce a more accurate result.
     */
    val failedOnLastStep: Boolean
)

/**
 * Returns whether the given from- pixel-location can see the given to- pixel-location.
 *
 * @param pixelsPerStep     How many pixels to jump between steps on the line being tested.
 * @param beingsBlockLOS    Whether intervening beings are considered
 * to block LOS.
 * @param adjustFromToTileCenter    Whether to adjust the from-location to the center
 * of its tile.
 * @param fromBeing    The entity (if any) at the front end of the line, to be ignored
 * when determine'ing LOS (if beingsBlockLOS == true).
 * @param toBeing      The entity (if any) at the to end of the line, to be ignored
 * when determine'ing LOS (if beingsBlockLOS == true).
 */
inline fun Area.canSee(
    from_: PixelPoint, to: PixelPoint, pixelPointPool: ObjectPool<MutablePixelPoint>,
    floatPointPool: ObjectPool<Point2D.Float>,
    adjustToTileCenter: (p: PixelPoint, use: MutablePixelPoint) -> PixelPoint,
    getUnitVector: (from: PixelPoint, to: PixelPoint, use: Point2D.Float) -> Point2D.Float,
    getDistance: (from: PixelPoint, to: PixelPoint, use: PixelDistance) -> PixelDistance,
    pixelsPerStep: Int = tileWidth, adjustFromToTileCenter: Boolean = false,
    beingsBlockLOS: Boolean = false, fromBeing: Being? = null, toBeing: Being? = null
): CanSeeResult {
    // if we are told to adjust the from-location to the center of its tile
    val from =
        if (adjustFromToTileCenter) adjustToTileCenter(from_, pixelPointPool.supply())
        else from_

    // determine the delta vector to be travelled during each step along the line
    val direction = getUnitVector(from, to, floatPointPool.supply())
    direction.x *= pixelsPerStep.toFloat()
    direction.y *= pixelsPerStep.toFloat()

    // determine how many steps it will take to get to the end, but skip the last, fractional step
    // (which takes us to the to-location, about which we don't care if it blocks LOS);
    // the case where there is no such step is handled below
    val distance = getDistance(from, to, losbPixelDistancePool.supply())
    val numSteps = distance.distance / pixelsPerStep
    losbPixelDistancePool.reclaim(distance)

    // for each step along the line
    val exact = floatPointPool.supply()
    exact.setLocation(from.x.toFloat(), from.y.toFloat())
    val current = pixelPointPool.supply()
    var canSee = true
    var failedOnLastStep = false
    for (i in 0 until numSteps) {
        // determine the location of the next step on the line being tested
        exact.x += direction.x
        exact.y += direction.y
        current.set(exact.x.roundToInt(), exact.y.roundToInt())

        // this handles the case mentioned above, where there is no last, fractional step,
        // by explicitly testing if this is the last step and whether the to-location
        // has been reached
        if (i == numSteps - 1 && current == to) break

        // if this step's location blocks LOS
        if (doesLocationBlockLOS(current, beingsBlockLOS, fromBeing, toBeing)) {
            // determine whether this blockage is occurring on the last step of this iteration
            failedOnLastStep = i >= numSteps - 1

            canSee = false
            break
        }
    }

    if (adjustFromToTileCenter) pixelPointPool.reclaim(from as MutablePixelPoint)
    floatPointPool.reclaim(direction)
    pixelPointPool.reclaim(current)
    floatPointPool.reclaim(exact)
    return CanSeeResult(canSee, failedOnLastStep)
}

