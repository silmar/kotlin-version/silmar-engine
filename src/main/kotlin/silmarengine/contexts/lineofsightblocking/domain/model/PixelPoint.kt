package silmarengine.contexts.lineofsightblocking.domain.model

typealias LOSBPixelPoint = PixelPoint

interface PixelPoint {
    val x: Int
    val y: Int

    fun toTilePoint(use: MutableTilePoint) {
        use.set(x / tileWidth, y / tileHeight)
    }

    fun matches(other: PixelPoint): Boolean = other.x == x && other.y == y
}

typealias LOSBMutablePixelPoint = MutablePixelPoint

interface MutablePixelPoint : PixelPoint {
    fun set(x: Int, y: Int): MutablePixelPoint
    fun set(p: PixelPoint): MutablePixelPoint = set(p.x, p.y)

    fun translate(dx: Int, dy: Int): MutablePixelPoint
}