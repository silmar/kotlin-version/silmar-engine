package silmarengine.contexts.playerattributedisplay.domain.model

typealias PADAttribute = Attribute

enum class Attribute(val displayName: String) {
    STRENGTH("Strength"),
    INTELLIGENCE("Intelligence"),
    JUDGEMENT("Judgement"),
    AGILITY("Agility"),
    ENDURANCE("Endurance")
}

class AttributeValues(val values: List<Int>)