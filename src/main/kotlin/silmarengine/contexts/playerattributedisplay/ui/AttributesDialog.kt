package silmarengine.contexts.playerattributedisplay.ui

import silmarengine.gui.Fonts
import silmarengine.contexts.playerattributedisplay.domain.model.AttributeValues
import silmarengine.gui.widgets.Label
import silmarengine.gui.windowLayout
import silmarengine.contexts.playerattributedisplay.domain.model.Attribute
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Frame
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JDialog
import javax.swing.JPanel
import javax.swing.border.CompoundBorder
import javax.swing.border.EmptyBorder
import javax.swing.border.LineBorder

/**
 * A dialog that displays the player's current attribute values.
 */
class AttributesDialog(
    getCurrentValues: () -> AttributeValues, parent: Frame,
    appendAttributeInfo: (attribute: Attribute, value: Int, to: StringBuffer) -> Unit
) : JDialog(parent, "Attributes") {
    val attributesDisplay: AttributesDisplay

    init {
        // configure this dialog; the size has to be greater than it normally would so that
        // tooltips may appear where they are supposed to in relation to the mouse cursor
        setSize(375, 200)
        isResizable = false

        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color.black

        // add the main panel
        val panel = JPanel()
        panel.isOpaque = false
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add the attributes display
        val display = AttributesDisplay(getCurrentValues, appendAttributeInfo)
        attributesDisplay = display
        display.border = CompoundBorder(
            CompoundBorder(EmptyBorder(5, 5, 5, 5), LineBorder(Color.lightGray, 1)),
            EmptyBorder(5, 5, 5, 5))
        panel.add(display)

        panel.add(Box.createRigidArea(Dimension(0, 5)))

        val label = Label("mouseover for info", Fonts.smallText)
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        pack()

        addWindowListener(object : WindowAdapter() {
            override fun windowOpened(e: WindowEvent?) {
                // if a location value for this dialog is available, use it
                val location = windowLayout.playerAttributesDialogLocation
                if (location != null) setLocation(location)

                // otherwise, center this dialog on its parent
                else setLocationRelativeTo(parent)
            }

            override fun windowClosing(event: WindowEvent?) {
                // store this dialog's location
                windowLayout.playerAttributesDialogLocation = location
            }

            override fun windowDeactivated(e: WindowEvent?) = windowClosing(e)
        })
    }
}
