package silmarengine.contexts.playerattributedisplay.ui

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playerattributedisplay.domain.model.Attribute
import silmarengine.contexts.playerattributedisplay.domain.model.AttributeValues
import java.awt.Color
import java.awt.Dimension
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.border.EmptyBorder

/**
 * A panel that displays a player's current attribute values
 */
class AttributesDisplay(
    private val getCurrentValues: () -> AttributeValues,
    private val appendAttributeInfo: (attribute: Attribute, value: Int, to: StringBuffer) -> Unit
) : JPanel() {
    private var attributeValues: AttributeValues = getCurrentValues()

    /**
     * The panels which make up the rows of this display, with one row per attribute.
     */
    private val attributePanels = Array(attributeValues.values.size) { i ->
        AttributePanel(Attribute.values()[i])
    }

    init {
        background = Color.black
        border = EmptyBorder(5, 5, 5, 5)
        layout = BoxLayout(this, BoxLayout.Y_AXIS)

        // for each player attribute
        Attribute.values().forEachIndexed { i, _ ->
            // add a panel for this attribute
            val panel = attributePanels[i]
            add(panel)
            panel.onValueChanged()
            add(Box.createRigidArea(Dimension(0, 1)))
        }
    }

    /**
     * Displays the value of one attribute.
     */
    private inner class AttributePanel(private val attribute: Attribute) : JPanel() {
        private var nameLabel: Label
        private var valueLabel: Label
        private var attributeInfo: String = assembleAttributeInfo()

        init {
            background = Color.black
            border = EmptyBorder(0, 1, 0, 1)
            layout = BoxLayout(this, BoxLayout.X_AXIS)

            // add the name label
            var label = Label(attribute.displayName, Fonts.mediumText)
            nameLabel = label
            label.horizontalAlignment = JLabel.LEFT
            add(label)

            add(Box.createHorizontalGlue())
            add(Box.createRigidArea(Dimension(20, 0)))

            // add the value label
            label = Label("--", Fonts.mediumNumber)
            valueLabel = label
            label.horizontalAlignment = JLabel.RIGHT
            add(label)
        }

        private fun assembleAttributeInfo(): String {
            // add the name
            val text = StringBuffer()
            text.append("<i>")
            text.append(attribute.displayName)

            // add the value
            text.append(":</i> ")
            text.append("<b>")
            val value = attributeValues.values[this.attribute.ordinal]
            text.append(value)

            // add the info
            text.append("</b>")
            appendAttributeInfo(this.attribute, value, text)

            return text.toString()
        }

        /**
         * Informs this panel that its associated attribute value has changed.
         */
        fun onValueChanged() {
            valueLabel.text = attributeValues.values[attribute.ordinal].toString()

            attributeInfo = assembleAttributeInfo()
            nameLabel.hasTooltipImpl.setTooltipText(attributeInfo, 0)
            valueLabel.hasTooltipImpl.setTooltipText(attributeInfo, 0)
        }
    }

    /**
     * Informs this display that the value of the given attribute has changed.
     */
    fun onAttributeChanged(attribute: Attribute) {
        attributeValues = getCurrentValues()

        attributePanels[attribute.ordinal].onValueChanged()
    }
}
