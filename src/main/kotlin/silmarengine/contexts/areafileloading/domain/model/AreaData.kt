package silmarengine.contexts.areafileloading.domain.model

data class AreaData(
    val width: Int,
    val height: Int,
    @Suppress("ArrayInDataClass") val tiles: IntArray,
    val entityRecords: List<EntityRecord>
)

data class EntityRecord(
    val name: String, val type: String, val id: Int, val x: Int, val y: Int
)