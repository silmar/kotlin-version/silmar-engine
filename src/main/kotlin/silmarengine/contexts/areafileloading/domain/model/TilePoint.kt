package silmarengine.contexts.areafileloading.domain.model

data class TilePoint(val x: Int, val y: Int) {
    constructor(other: TilePoint) : this(other.x, other.y)
}