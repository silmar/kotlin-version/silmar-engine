package silmarengine.contexts.areafileloading.domain.model

class Tile(val id: Int) {
    override fun toString() = "Tile($id)"
}

const val tileWidth = 32
const val tileHeight = 32