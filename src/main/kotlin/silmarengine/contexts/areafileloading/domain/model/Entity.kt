package silmarengine.contexts.areafileloading.domain.model

enum class EntityKind {TERRAIN, TALKER_BEING, MONSTER}

open class Entity(val kind: EntityKind, val typeName: String) {
    lateinit var location: PixelPoint
}
