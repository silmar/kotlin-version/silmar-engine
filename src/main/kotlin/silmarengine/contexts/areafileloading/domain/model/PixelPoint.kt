package silmarengine.contexts.areafileloading.domain.model

typealias AFLPixelPoint = PixelPoint

data class PixelPoint(val x: Int, val y: Int) {
    constructor(other: PixelPoint) : this(other.x, other.y)
}