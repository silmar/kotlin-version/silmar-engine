package silmarengine.contexts.areafileloading.domain.model.functions

import silmarengine.contexts.areafileloading.domain.model.*

typealias Loader = (fileName: String) -> AreaData

fun loadArea(fileName: String, loader: Loader): Area {
    val data = loader(fileName)

    // set the area's tiles using the tiles data
    val (width, height) = data
    val area = Area(AreaSize(width, height))
    for (x in 0 until width) for (y in 0 until height) {
        val tile = Tile(data.tiles[x + y * width] - 1)
        area.setTile(x, y, tile)
    }

    // add the area's entities from the entities data
    data.entityRecords.forEach { record ->
        val kind = when (record.type) {
            "terrain" -> EntityKind.TERRAIN
            "talkerBeing" -> EntityKind.TALKER_BEING
            "monster" -> EntityKind.MONSTER
            else -> throw Exception(
                "Unknown entity kind [${record.type}] when loading area data")
        }
        val entity = Entity(kind, record.name)
        entity.location = PixelPoint(record.x + tileWidth / 2, record.y + tileHeight / 2)
        area.addEntity(entity)
    }

    return area
}
