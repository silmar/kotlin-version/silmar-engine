package silmarengine.contexts.areafileloading.domain.model

typealias AFLArea = Area

class Area(val size: AreaSize) {
    private val tiles: Array<Array<Tile>> =
        Array(size.width) { Array(size.height) { Tile(0) } }

    private val entities = ArrayList<Entity>()
    val immutableEntities: List<Entity> get() = entities

    fun getTile(x: Int, y: Int) = tiles[x][y]

    fun setTile(x: Int, y: Int, tile: Tile) {
        tiles[x][y] = tile
    }

    fun addEntity(entity: Entity) = entities.add(entity)

    val summary: String
        get() {
            val terrainCount = entities.count { it.kind == EntityKind.TERRAIN }
            val talkerBeingCount = entities.count { it.kind == EntityKind.TALKER_BEING }
            val monsterCount = entities.count { it.kind == EntityKind.MONSTER }
            return "Area(${size.width}x${size.height} tiles, $terrainCount terrains, $talkerBeingCount talker-beings, $monsterCount monsters)"
        }
}