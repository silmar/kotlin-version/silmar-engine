package silmarengine.contexts.areafileloading.domain.model

data class AreaSize(val width: Int, val height: Int)