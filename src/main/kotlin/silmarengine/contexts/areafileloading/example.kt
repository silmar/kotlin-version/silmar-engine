package silmarengine.contexts.areafileloading

import silmarengine.contexts.areafileloading.domain.model.functions.loadArea
import silmarengine.contexts.areafileloading.services.loadAreaDataFromTiledFile

fun main() {
    val area = loadArea("0", ::loadAreaDataFromTiledFile)
    println(area.summary)
}