package silmarengine.contexts.areafileloading.services

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.modules.SerializersModule
import silmarengine.contexts.areafileloading.domain.model.*

fun loadAreaDataFromTiledFile(fileName: String): AreaData {
    val fullFilePath = "areas/$fileName.json"
    val file = ClassLoader.getSystemResource(fullFilePath)
        ?: throw Exception("Could not load map data from file $fullFilePath")
    val areaData = file.readText()
    val layerModule = SerializersModule {
        polymorphic(Layer::class) {
            TileLayer::class with TileLayer.serializer()
            ObjectGroup::class with ObjectGroup.serializer()
        }
    }
    @Suppress("EXPERIMENTAL_API_USAGE") Json(JsonConfiguration(ignoreUnknownKeys = true),
        context = layerModule).apply {
        val json = parseJson(areaData)
        val tiledMap = fromJson(TiledMap.serializer(), json)
        val (width, height) = tiledMap
        val tileLayer = tiledMap.layers.first { it is TileLayer } as TileLayer
        val tiles = tileLayer.data
        val objectGroup = tiledMap.layers.first { it is ObjectGroup } as ObjectGroup
        val entityRecords =
            objectGroup.objects.map { o -> EntityRecord(o.name, o.type, o.id, o.x, o.y) }
        return AreaData(width, height, tiles, entityRecords)
    }
}

@Suppress("ArrayInDataClass", "unused", "unused")
@Serializable
private data class TiledMap(
    val width: Int, val height: Int, val tilewidth: Int, val tileheight: Int,
    val layers: Array<Layer>
)

@Polymorphic
@Serializable
private sealed class Layer

@Suppress("ArrayInDataClass", "unused")
@SerialName("tilelayer")
@Serializable
private data class TileLayer(
    val name: String, val width: Int, val height: Int, val data: IntArray
) : Layer()

@Suppress("unused")
@Serializable
private data class Object(
    val name: String, val type: String, val id: Int, val x: Int, val y: Int
)

@Suppress("ArrayInDataClass", "unused")
@SerialName("objectgroup")
@Serializable
private data class ObjectGroup(
    val name: String, val objects: List<Object>
) : Layer()

