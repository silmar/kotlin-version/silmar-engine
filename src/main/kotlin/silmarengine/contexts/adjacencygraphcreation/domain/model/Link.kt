package silmarengine.contexts.adjacencygraphcreation.domain.model

import java.io.Serializable

typealias AGCLink = Link

/**
 * Associates two spaces as adjacent.
 */
class Link(
    /**
     * The two spaces associated as adjacent with one another by this link.
     */
    val space1: Space,
    val space2: Space,
    /**
     * Whether the two spaces are connected by doors.
     */
    val usesDoors: Boolean,
    /**
     * The pixel-locations of the center of the two tiles connecting the two spaces
     * (for example, to test whether doors (if present) are open).
     */
    val intersection1: PixelPoint,
    val intersection2: PixelPoint
) : Serializable

