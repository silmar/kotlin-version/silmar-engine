package silmarengine.contexts.adjacencygraphcreation.domain.model

import java.io.Serializable
import java.util.*

typealias AGCSpace = Space

/**
 * A room or a hall in a dungeon.
 */
class Space(
    /**
     * The pixel-rectangle covered by this space within the dungeon.
     */
    val rect: PixelRectangle
) : Serializable {
    val id = idCounter++

    /**
     * Links to the other spaces that are adjacent to this one.
     */
    val links = ArrayList<Link>()

    override fun toString(): String {
        val linksTo = links.fold("",
            { acc, link -> "${if (acc.isNotEmpty()) "$acc, " else ""}${link.space1.id}-${link.space2.id}" })
        return "Space $id  Links: $linksTo"
    }

    companion object {
        var idCounter = 1
    }
}

