package silmarengine.contexts.adjacencygraphcreation.domain.model

import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.util.*

typealias AGCAdjacencyGraph = AdjacencyGraph

/**
 * Stores a graph representation of a dungeon in terms of which of its rooms and hallways are
 * connected to one another, for the purpose of quickly determining if there is a path of such
 * enclosures between any two locations in the dungeon.
 */
class AdjacencyGraph {
    /**
     * The nodes of this graph, sorted by their x-values (with an
     * arbitrary sub-ordering of spaces whose x-values tie).
     */
    private val spaces = ArrayList<Space>()
    val immutableSpaces: List<Space> get() = spaces

    /**
     * A mapping of the spaces within this graph, by their rectangles.
     */
    private val spacesByRect = HashMap<PixelRectangle, Space>()

    override fun toString(): String = spaces.fold("", { acc, space -> "$acc\n$space" })

    /**
     * Creates a node in this graph for the created space, and links the node to
     * the node for the given from-space (if any), which should already
     * be contained by this graph.
     *
     * @param doorway_  The pixel-location of the first of the two tiles that allow
     *                  passage between the above two spaces.
     * @param isDoorwayVertical Whether the two doorway tiles between the above two
     *                          spaces run vertically (vs. horizontally).
     * @param doorwayHasDoors   Whether door tiles (vs. floor tiles) make up the doorway
     *                          to the new space.
     */
    fun onSpaceCreated(space_: PixelRectangle, fromSpace_: PixelRectangle?,
        doorway_: PixelPoint?, isDoorwayVertical: Boolean, doorwayHasDoors: Boolean) {
        // create a new space describing the given rectangle;
        // expand the description of the space by one tile all the way around,
        // so that the doorways between spaces will be covered by the spaces
        val space = Space(space_.addOneTileBorder())

        // hash the new space for later retrieval when spaces are created off of it
        spacesByRect[space.rect] = space

        // if a from-space rectangle was given (as there won't be if the space
        // is the first one created in the area)
        if (fromSpace_ != null) {
            // retrieve the from-space identified by the from-space rectangle given
            val fromRect = fromSpace_.addOneTileBorder()
            val fromSpace = spacesByRect[fromRect] as Space

            // determine the pixel locations of the two tiles that allow passage between the
            // two spaces
            val doorway = doorway_!!
            val intersection2 =
                PixelPoint(doorway.x + (if (!isDoorwayVertical) tileWidth else 0),
                    doorway.y + (if (isDoorwayVertical) tileHeight else 0))

            // create a link between the two spaces
            val link =
                Link(space, fromSpace, doorwayHasDoors, doorway, intersection2)

            // add the link to each of the two spaces' list of links
            space.links.add(link)
            fromSpace.links.add(link)
        }

        addSpace(space)
    }

    /**
     * Adds the given space to those contained within this adjacency graph.
     */
    private fun addSpace(space: Space) {
        // insert the given space where its x-value indicates it should be in the list
        // (which is sorted by the spaces' rectangle's x-value)
        val index = spaces.indexOfFirst { space.rect.x < it.rect.x }
        if (index >= 0) spaces.add(index, space) else spaces.add(space)
    }
}