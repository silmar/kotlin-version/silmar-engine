package silmarengine.contexts.adjacencygraphcreation.domain.model

typealias AGCPixelPoint = PixelPoint

data class PixelPoint(val x: Int, val y: Int) {
    constructor(other: PixelPoint) : this(other.x, other.y)
}