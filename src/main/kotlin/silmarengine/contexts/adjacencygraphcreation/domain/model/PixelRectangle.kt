package silmarengine.contexts.adjacencygraphcreation.domain.model

data class PixelRectangle(val x: Int, val y: Int, val width: Int, val height: Int) {
    constructor(other: PixelRectangle) : this(other.x, other.y, other.width, other.height)

    override fun toString() = "PixelRectangle $x,$y ${width}x$height"

    override fun equals(other: Any?): Boolean =
        other is PixelRectangle && other.x == x && other.y == y && other.width == width && other.height == height

    override fun hashCode(): Int = toString().hashCode()

    /**
     * Returns an enlargement of this rectangle by one tile all around its border.
     */
    fun addOneTileBorder(): PixelRectangle =
        PixelRectangle(x - tileWidth, y - tileHeight, width + 2 * tileWidth,
            height + 2 * tileHeight)
}