package silmarengine.contexts.adjacencygraphcreation

import silmarengine.contexts.adjacencygraphcreation.adapters.AGCDungeonListener
import silmarengine.contexts.adjacencygraphcreation.domain.model.AdjacencyGraph
import silmarengine.contexts.dungeongeneration.domain.model.functions.generateDungeon

fun main() {
    val graph = AdjacencyGraph()
    val dungeon = generateDungeon(AGCDungeonListener(graph))
    dungeon.print()
    println()
    println(graph)
}