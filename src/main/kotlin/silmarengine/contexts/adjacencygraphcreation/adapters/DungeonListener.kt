package silmarengine.contexts.adjacencygraphcreation.adapters

import silmarengine.contexts.adjacencygraphcreation.domain.model.AdjacencyGraph
import silmarengine.contexts.dungeongeneration.domain.model.DGDungeonListener
import silmarengine.contexts.dungeongeneration.domain.model.Dungeon
import silmarengine.contexts.dungeongeneration.domain.model.TileRectangle

typealias AGCDungeonListener = DungeonListener

class DungeonListener(private val graph: AdjacencyGraph) : DGDungeonListener {
    override fun onSpaceCreated(
        dungeon: Dungeon, space: TileRectangle, fromSpace: TileRectangle?,
        doorway: silmarengine.contexts.dungeongeneration.domain.model.TilePoint?,
        isDoorwayVertical: Boolean
    ) {
        val doorwayHasDoors =
            if (doorway != null) dungeon.getTile(doorway)?.isDoor == true
            else false
        graph.onSpaceCreated(space.toPixelRectangle(), fromSpace?.toPixelRectangle(),
            doorway?.toPixelPoint(), isDoorwayVertical, doorwayHasDoors)
    }
}