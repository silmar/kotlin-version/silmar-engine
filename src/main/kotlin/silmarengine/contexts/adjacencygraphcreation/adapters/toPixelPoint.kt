package silmarengine.contexts.adjacencygraphcreation.adapters

import silmarengine.contexts.adjacencygraphcreation.domain.model.PixelPoint
import silmarengine.contexts.adjacencygraphcreation.domain.model.tileHeight
import silmarengine.contexts.adjacencygraphcreation.domain.model.tileWidth
import silmarengine.contexts.dungeongeneration.domain.model.TilePoint

fun TilePoint.toPixelPoint() = PixelPoint(x * tileWidth, y * tileHeight)