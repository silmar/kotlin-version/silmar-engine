package silmarengine.contexts.adjacencygraphcreation.adapters

import silmarengine.contexts.adjacencygraphcreation.domain.model.PixelRectangle
import silmarengine.contexts.adjacencygraphcreation.domain.model.tileHeight
import silmarengine.contexts.adjacencygraphcreation.domain.model.tileWidth
import silmarengine.contexts.dungeongeneration.domain.model.TileRectangle

fun TileRectangle.toPixelRectangle() =
    PixelRectangle(x * tileWidth, y * tileHeight, width * tileWidth, height * tileHeight)