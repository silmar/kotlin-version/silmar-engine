package silmarengine.contexts.beingmovement.domain.functions

import silmarengine.contexts.beingmovement.domain.model.*

/**
 * Tries to move this monster towards the given goal location, using its
 * movement points.  If the movement is blocked, this tries to find a path around the
 * blockage.  Checks for attack opportunities are made during the movement.
 */
fun Monster.moveTowardGoal(
    goal: MutablePixelPoint, player: Being, canSeePlayer_: Boolean
) {
    // while more movement is possible and desirable, this monster's turn isn't over,
    // and this monster isn't dead; note that paths are re-determined with each new turn
    val context = movementContext
    context.reset()
    var movementPoints = movementPoints
    var canSeePlayer = canSeePlayer_
    var done = false
    while (!done && !isTurnOver && !isDead) {
        // determine this monster's immediate goal for this movement
        determineImmediateGoal(goal, player)
        val immediateGoal = context.immediateGoal

        // remember the direction to the immediate goal as the last direction moved
        context.getUnitVector(location, immediateGoal, lastDirectionMoved)

        // move towards the immediate goal
        val result2 = moveTowardsGoal(immediateGoal, movementPoints,
            context.moveTowardsGoalListener, false, context.getUnitVector, context.getDistance)
        movementPoints = result2.movementPointsLeft

        // if the above movement caused this monster to die, we are done
        if (isDead) break

        // if this monster's movement was blocked
        if (result2.impass) {
            // the current path being followed (if any) is invalid
            context.path = null
            context.pathIndex = 0

            // don't try moving anymore again until next turn
            done = true
        }
        // else, if this monster has run out of movement points
        else if (result2.notEnoughMovementPoints || movementPoints <= 0) {
            // this monster's turn is over
            endTurn()
            done = true
        }
        // else, this monster reached the immediate goal
        else {
            // if the immediate goal was also this monster's final goal
            if (immediateGoal == goal) {
                // there is no need for this monster to move anymore this turn
                done = true
            }

            // rid the immediate goal, since it has been reached
            immediateGoal.set(context.noGoalValue)
        }

        // if this monster has reached (or just about reached) some other final goal that has
        // been specified for it
        if (otherGoal != context.noGoalValue && context.isDistanceAtMost(location,
                otherGoal, tileDistance)) {
            // we no longer need to consider this other final goal
            otherGoal.set(context.noGoalValue)
        }

        // if this monster is activated but is not focused on the player
        if (isActivated && !isFocusedOnPlayer) {
            // if this monster's movement above allows it to focus on the player
            if (player.isInLOSOf(location)) focusOnPlayer()
            if (isFocusedOnPlayer) {
                // make this monster's goal the player's location
                goal.set(player.location)

                // re-determine whether the monster can see the player with no beings
                // in the way, for when it goes to check for ranged attacks below
                canSeePlayer =
                    area.canSee(location, player.location, tileWidth / 4,
                        false, true, this, player)
            }
        }

        // if it didn't already do so fully above, have this monster check to see
        // if it should make ranged attacks
        var madeRangedAttack = false
        if (!hadRangedAttackOpportunityThisTurn && canSeePlayer) {
            val result = checkForRangedAttacks()
            hadRangedAttackOpportunityThisTurn = result.couldMakeAttack
            if (result.madeAttack) {
                madeRangedAttack = true
                done = true
            }
        }

        // if no ranged attack was made above, check for the possibility of making melee
        // attacks (and ending this monster's turn) before performing another move
        if (!madeRangedAttack) checkForMeleeAttacks()
    }
}

