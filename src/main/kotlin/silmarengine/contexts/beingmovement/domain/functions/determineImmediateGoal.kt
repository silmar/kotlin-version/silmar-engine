package silmarengine.contexts.beingmovement.domain.functions

import silmarengine.contexts.beingmovement.domain.model.*

/**
 * Computes the next location this monster should try to move to in attempting to reach
 * its given final goal location.  If there is an unblocked line to that final goal, the
 * final goal might also be the immediate goal.  Otherwise, if a path is being followed,
 * the next point along that path might be considered the immediate goal.  If no path
 * is being followed, one may be sought.
 */
fun Monster.determineImmediateGoal(goal: PixelPoint, player: Being): PixelPoint {
    // if this monster doesn't already have a path it's following this turn
    val context = movementContext
    val immediateGoal = context.immediateGoal
    var lineResult: IsLinePassibleResult? = null
    if (context.path == null) {
        // if the line directly to this monster's goal is both passible and not harmful
        lineResult =
            area.isLinePassible(location, goal, bmMovementType, size, this, player, true)
        if (lineResult.passible && !lineResult.harmful) {
            // make the goal this monster's immediate goal
            immediateGoal.set(goal)
        }
    }

    fun useNextPathStepAsImmediateGoal(context: MonsterMovementContext,
        immediateGoal: MutablePixelPoint) {
        // if we have a path to follow
        val path = context.path
        if (path != null) {
            // use the next location in the path as the immediate goal
            val next = path.elementAt(context.pathIndex++)
            immediateGoal.set(next)
            if (context.pathIndex >= path.size) {
                context.reclaimPath(path)
                context.path = null
                context.pathIndex = 0
            }
        }
    }

    // if no immediate goal was determined above
    var pathSearchFoundHarm = false
    if (immediateGoal == context.noGoalValue) {
        // if this monster is activated, and doesn't already have a path to follow
        // to the goal, and we haven't already generated a path to the goal this turn
        // (as such generation is expensive, and will likely produce no better
        // results on successive tries)
        if (isActivated && context.path == null && !context.pathTried) {
            // try to find a non-harmful path to the goal; make sure a player is specified
            // as a goal entity (if this monster isn't fleeing), otherwise if the goal is
            // a player's location, no passible path will be able to be found; note that
            // if only the best path could be returned, we ignore if harmful terrains were
            // encountered during the search, as there may be a better path that crosses
            // those terrains, which we'll hopefully find in the path-search below
            val pathResult = area.findPassiblePath(location, goal, this,
                if (!this.isFleeing) player else null,
                context.maxPathDistanceToGoal, bmMovementType, true, true)
            pathSearchFoundHarm = pathResult.foundHarm
            if (!pathSearchFoundHarm || !pathResult.bestPathReturned) context.path =
                pathResult.path
            context.pathTried = true
        }

        useNextPathStepAsImmediateGoal(context, immediateGoal)
    }

    // if no immediate goal was determined above, and this monster doesn't already
    // have a path it's following this turn, and part of the reason it has no immediate goal is
    // that the direct line to the goal was considered harmful (or, that line hasn't
    // been evaluated yet during this immediate-goal determination, for whatever reason above)
    if (immediateGoal == context.noGoalValue && context.path == null && (lineResult == null || lineResult.harmful)) {
        // if the direct line to the goal is passible (disregarding harmfulness)
        if (area.isLinePassible(location, goal, bmMovementType, size, this, player,
                false).passible) {
            // make the goal this monster's immediate goal
            immediateGoal.set(goal)
        }
    }

    // if no immediate goal was determined above
    if (immediateGoal == context.noGoalValue) {
        // if this monster is activated, and doesn't already have a path to follow
        // to the goal, and the above search for a path indicated that one or more
        // harmful entities were encountered, and we haven't already generated
        // a potentially-harmful path to the goal this turn (as such generation is expensive,
        // and will likely produce no better results on successive tries)
        if (isActivated && context.path == null && pathSearchFoundHarm && !context.harmfulPathTried) {
            // create a potentially harmful path to the goal;
            // make sure a player is specified as a goal entity,
            // otherwise if the goal is a player's location, no passible path
            // will be able to be found
            val pathResult = area.findPassiblePath(location, goal, this, player,
                context.maxPathDistanceToGoal, bmMovementType, true, false)
            context.path = pathResult.path
            context.pathIndex = 0
            context.harmfulPathTried = true
        }

        useNextPathStepAsImmediateGoal(context, immediateGoal)
    }

    // if no immediate goal has yet been found
    if (immediateGoal == context.noGoalValue) {
        // just use the final goal as this monster's immediate goal, so at
        // least some progress might be made
        immediateGoal.set(goal)

        // if there is an other-goal being pursued, there is no way to reach it,
        // so pursue it no longer
        otherGoal.set(context.noGoalValue)
    }

    return immediateGoal
}
