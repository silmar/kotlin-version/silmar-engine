package silmarengine.contexts.beingmovement.domain.model

typealias BMPixelDistance = PixelDistance

interface PixelDistance {
    var distance: Int
}