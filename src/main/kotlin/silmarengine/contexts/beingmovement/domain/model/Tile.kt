package silmarengine.contexts.beingmovement.domain.model

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileSize

typealias BMTile = Tile

interface Tile {
    val movementCost: Int
}

const val tileWidth = 32
const val tileHeight = 32

val tileDistance = PixelDistance(tileSize.width)
