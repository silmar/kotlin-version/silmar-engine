package silmarengine.contexts.beingmovement.domain.model

typealias BMEntity = Entity

interface Entity {
    val location: PixelPoint
    val size: PixelDimensions
    val area: Area
    val bmMovementType: MovementType

    fun checkForMovementSound()
    fun isInLOSOf(location: PixelPoint): Boolean
    fun isVisibleToUser(): Boolean
}