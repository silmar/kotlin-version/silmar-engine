package silmarengine.contexts.beingmovement.domain.model

import java.awt.geom.Point2D

typealias BMMonster = Monster

interface Monster : Being {
    val isTurnOver: Boolean
    val isDead: Boolean
    val isActivated: Boolean
    val isFocusedOnPlayer: Boolean
    val isFleeing: Boolean
    val otherGoal: MutablePixelPoint
    var hadRangedAttackOpportunityThisTurn: Boolean
    val lastDirectionMoved: Point2D.Float
    val movementContext: MonsterMovementContext

    fun focusOnPlayer()
    fun checkForRangedAttacks(): CheckForRangedAttacksResult
    fun checkForMeleeAttacks()
    fun endTurn()
}

typealias BMCheckForRangedAttacksResult = CheckForRangedAttacksResult

interface CheckForRangedAttacksResult {
    val madeAttack: Boolean
    val couldMakeAttack: Boolean
}
