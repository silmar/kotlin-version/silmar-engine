package silmarengine.contexts.beingmovement.domain.model

typealias BMTileDistance = TileDistance

interface TileDistance {
    var distance: Int
}