package silmarengine.contexts.beingmovement.domain.model

typealias BMTerrain = Terrain

interface Terrain {
    /**
     * @return Whether this arrival should cause the given being's movement to stop.
     */
    fun onBeingArrival(being: Being): Boolean
}