package silmarengine.contexts.beingmovement.domain.model

typealias BMBeing = Being

interface Being : Entity {
    val movementPoints: Float
    val shouldMovementBeAccelerated: Boolean
}