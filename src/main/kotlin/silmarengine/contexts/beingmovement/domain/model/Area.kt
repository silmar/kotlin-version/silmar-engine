package silmarengine.contexts.beingmovement.domain.model

import java.util.*

typealias BMArea = Area

interface Area {
    fun getTile(at: PixelPoint): Tile

    fun isRectanglePassible(
        location: PixelPoint, size: PixelDimensions, movementType: MovementType,
        checkForBeing: Boolean, forEntity: Entity?, forEntity2: Entity?,
        checkForHarmfulTerrains: Boolean
    ): IsRectanglePassibleResult

    fun moveEntity(entity: Entity, to: PixelPoint)

    fun getTerrainsInRectangle(location: PixelPoint, size: PixelDimensions): List<Terrain>

    fun canSee(
        from: PixelPoint, to: PixelPoint, pixelsPerStep: Int,
        adjustFromToTileCenter: Boolean, beingsBlockLOS: Boolean, fromBeing: Being?,
        toBeing: Being?
    ): Boolean

    fun isLinePassible(
        from: PixelPoint, to: PixelPoint, movementType: MovementType,
        moverSize: PixelDimensions, fromEntity: Entity?, toEntity: Entity?,
        checkForHarmfulTerrains: Boolean
    ): IsLinePassibleResult

    fun findPassiblePath(
        from: PixelPoint?, to: PixelPoint, movingEntity: Entity, goalEntity: Entity?,
        maxLength: TileDistance, movementType: MovementType, shouldFindBestPath: Boolean,
        checkForHarmfulTerrains: Boolean
    ): FindPassiblePathResult
}

typealias BMIsRectanglePassibleResult = IsRectanglePassibleResult

interface IsRectanglePassibleResult {
    val passible: Boolean
    val harmful: Boolean
}

typealias BMIsLinePassibleResult = IsLinePassibleResult

interface IsLinePassibleResult {
    val passible: Boolean
    val harmful: Boolean
}

typealias BMFindPassiblePathResult = FindPassiblePathResult

interface FindPassiblePathResult {
    val path: ArrayDeque<PixelPoint>?
    val foundHarm: Boolean
    val bestPathReturned: Boolean
}