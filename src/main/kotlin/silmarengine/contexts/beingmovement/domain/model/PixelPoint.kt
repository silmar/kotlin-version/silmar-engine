package silmarengine.contexts.beingmovement.domain.model

typealias BMPixelPoint = PixelPoint

interface PixelPoint {
    val x: Int
    val y: Int
}

typealias BMMutablePixelPoint = MutablePixelPoint

interface MutablePixelPoint : PixelPoint {
    fun set(x: Int, y: Int): MutablePixelPoint
    fun set(p: PixelPoint): MutablePixelPoint = set(p.x, p.y)
}