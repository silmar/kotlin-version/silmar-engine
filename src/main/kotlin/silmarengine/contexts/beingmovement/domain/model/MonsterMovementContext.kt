package silmarengine.contexts.beingmovement.domain.model

import silmarengine.contexts.beingmovement.domain.functions.MoveTowardsGoalListener
import java.awt.geom.Point2D
import java.io.Serializable
import java.util.*

class MonsterMovementContext(
    val maxPathDistanceToGoal: TileDistance, val noGoalValue: PixelPoint,
    val moveTowardsGoalListener: MoveTowardsGoalListener,
    createMutablePixelPoint: () -> MutablePixelPoint,
    val getUnitVector: (from: PixelPoint, to: PixelPoint, use: Point2D.Float) -> Point2D.Float,
    val getDistance: (from: PixelPoint, to: PixelPoint, use: PixelDistance) -> PixelDistance,
    val isDistanceAtMost: (from: PixelPoint, to: PixelPoint, atMost: PixelDistance) -> Boolean,
    val reclaimPath: (path: ArrayDeque<PixelPoint>) -> Unit
) : Serializable {
    val immediateGoal = createMutablePixelPoint()
    var path: ArrayDeque<PixelPoint>? = null
    var pathIndex: Int = 0
    var pathTried: Boolean = false
    var harmfulPathTried: Boolean = false

    fun reset() {
        immediateGoal.set(noGoalValue)
        if (path != null) reclaimPath(path!!)
        path = null
        pathIndex = 0
        pathTried = false
        harmfulPathTried = false
    }
}