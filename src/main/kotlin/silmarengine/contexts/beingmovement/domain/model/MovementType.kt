package silmarengine.contexts.beingmovement.domain.model

typealias BMMovementType = MovementType

enum class MovementType {
    WALKING,
    WALKING_NO_HANDS,
    FLYING,
    FLYING_NO_HANDS,
    EARTHING,
    ETHEREAL
}