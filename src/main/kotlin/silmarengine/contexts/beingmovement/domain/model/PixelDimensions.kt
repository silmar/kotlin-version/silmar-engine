package silmarengine.contexts.beingmovement.domain.model

typealias BMPixelDimensions = PixelDimensions

interface PixelDimensions {
    val width: Int
    val height: Int
}