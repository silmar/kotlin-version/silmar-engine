package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendItemValue
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.util.html.addLineBreak
import java.awt.Color
import java.awt.Dimension
import javax.swing.Box
import javax.swing.JFrame
import javax.swing.JScrollPane
import javax.swing.border.EtchedBorder

/**
 * A modal dialog that displays a set of items the user may select from to purchase, along
 * with the player's items to remind the user of what the player already possesses.
 *
 * @param owner      This dialog's owning frame.
 */
class BuyDialog(
    owner: JFrame, greeting: String, saleItems: List<Item>,
    getEquippedItems: () -> EquippedItems, getInventoryItems: () -> List<Item>,
    private val getPrice: (item: Item) -> Int,
    private val buyItem: (item: Item, price: Int) -> Unit
) : EquippedItemsAndInventoryDialog(owner, "Buy items", greeting, getEquippedItems,
    getInventoryItems) {

    private var saleItemsDisplay: ItemsDisplay? = null

    private val saleItemsDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            buyItem(item, getPrice(item))
        }

        override fun onCanAppendToTooltip(item: Item, text: StringBuffer) {
            addLineBreak(text)
            text.append("price: ")
            appendItemValue(getPrice(item), text)
        }
    }

    init {
        isModal = true

        // add a header
        val headerIndex = mainPanel.components.indexOf(header)
        val header = Label("<html><b>Items for sale</b></html>", Fonts.mediumPlus)
        mainPanel.add(header, headerIndex)
        mainPanel.add(Box.createRigidArea(Dimension(0, 5)), headerIndex + 1)

        // add the sale-items display
        val display = ItemsDisplay({ saleItems })
        saleItemsDisplay = display
        val scrollPane = JScrollPane(display)
        scrollPane.horizontalScrollBarPolicy = JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        scrollPane.border = EtchedBorder(Color.white, Color.lightGray)
        mainPanel.add(scrollPane, headerIndex + 2)

        // pack this dialog's UI so that the sale-items display gets assigned a width
        // before we set its items, below; that way, it can know how tall to size itself
        pack()

        // set the items for sale into the sale-items display
        display.addListener(saleItemsDisplayListener)

        mainPanel.add(Box.createRigidArea(Dimension(0, 5)), headerIndex + 3)

        display.onItemsChanged()
        populateItemDisplays()

        pack()
        setLocationRelativeTo(owner)
    }
}
