package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.model.HandsConfigs
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.domain.model.handsConfigsCount
import silmarengine.contexts.playeritemsmanagement.ui.listeners.HandsConfigsDisplayListener
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import java.awt.Color
import java.awt.Dimension
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JPanel
import javax.swing.border.LineBorder

/**
 * A panel which displays the hands-configurations owned by a player.
 */
class HandsConfigsDisplay(private val configs: HandsConfigs) : JPanel(),
    ReporterMixin<HandsConfigsDisplayListener> {
    override var listeners = HashSet<HandsConfigsDisplayListener>()

    /**
     * The panels that each display one of the player's hands-configs.
     */
    private var configPanels: Array<ConfigPanel>? = null

    init {
        isOpaque = false

        layout = BoxLayout(this, BoxLayout.Y_AXIS)

        // for each config
        configPanels = Array(handsConfigsCount) { i ->
            // add a config panel to this display
            val panel = ConfigPanel(i)
            add(panel)
            add(Box.createRigidArea(Dimension(0, 5)))
            panel
        }

        // get the initial configs displayed
        onConfigChanged()
    }

    /**
     * A panel that displays one hands-config.
     *
     * @param  configIndex       The index of the hands-config this panel is displaying.
     */
    private inner class ConfigPanel(private val configIndex: Int) : JPanel() {
        /**
         * The displays for the individual left- and right-hand items.
         */
        private val leftDisplay: ItemDisplay
        private val rightDisplay: ItemDisplay

        private val itemDisplayListener = object : ItemDisplayListener {
            override fun onItemClicked(item: Item, rightClick: Boolean) {
                reportToListeners { it.onConfigSelected(configIndex) }
            }

            override fun onEmptyClicked() {
                reportToListeners { it.onConfigSelected(configIndex) }
            }
        }

        init {
            isOpaque = false

            layout = BoxLayout(this, BoxLayout.X_AXIS)

            add(Box.createRigidArea(Dimension(1, 0)))

            // add the left-item display
            leftDisplay = ItemDisplay()
            leftDisplay.setReportEmptyClicks(true)
            leftDisplay.border = LineBorder(Color.white, 1)
            add(leftDisplay)
            leftDisplay.addListener(itemDisplayListener)

            add(Box.createRigidArea(Dimension(1, 0)))

            // add the right-item display
            rightDisplay = ItemDisplay()
            rightDisplay.setReportEmptyClicks(true)
            rightDisplay.border = LineBorder(Color.white, 1)
            add(rightDisplay)
            rightDisplay.addListener(itemDisplayListener)
        }

        /**
         * Informs this panel that what items its config holds has changed.
         */
        fun onItemsChanged() {
            // update the left- and right- item-displays with the new current items
            val config = configs.immutableConfigs[configIndex]
            leftDisplay.setItem(config.leftItem)
            rightDisplay.setItem(config.rightItem)
        }

        /**
         * Has this display tell its contained item-displays to update themselves.
         */
        fun updateItems() {
            leftDisplay.repaint()
            rightDisplay.repaint()
        }
    }

    /**
     * Informs this display that one of the configs it is displaying has changed in terms
     * of what items it holds.
     */
    fun onConfigChanged() {
        configPanels?.forEach { it.onItemsChanged() }

        repaint()
    }

    /**
     * Has this display tell all its contained item-displays to update themselves.
     */
    fun updateItems() {
        configPanels?.forEach { it.updateItems() }
    }
}
