package silmarengine.contexts.playeritemsmanagement.ui.functions

import silmarengine.util.html.addLineBreak

fun appendRefusalText(text: String, to: StringBuffer) {
    addLineBreak(to)
    to.append("<b><i>Cannot choose: ")
    to.append(text)
    to.append("</i></b>")
}
