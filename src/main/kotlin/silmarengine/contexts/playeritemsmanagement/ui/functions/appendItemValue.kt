package silmarengine.contexts.playeritemsmanagement.ui.functions

fun appendItemValue(value: Int, to: StringBuffer, unit: String? = "gold") {
    to.append("<b><font style='font-family:sans-serif;font-size:16'>")
    to.append(value)
    to.append("</font></b>")
    if (unit != null) to.append(" $unit")
}