package silmarengine.contexts.playeritemsmanagement.ui.listeners

import silmarengine.contexts.playeritemsmanagement.domain.model.Item

interface ItemDisplayListener {
    fun onItemClicked(item: Item, rightClick: Boolean) {}
    fun onEmptyClicked() {}
    fun onCanAppendToTooltip(item: Item, text: StringBuffer) {}
}
