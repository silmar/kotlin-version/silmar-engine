package silmarengine.contexts.playeritemsmanagement.ui.listeners

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.model.Item

/**
 * Passes item-display events from one or more item-display-listeners onto a given
 * other item-display-listener.
 */
class PassThroughItemDisplayListener(
    private val reporter: ReporterMixin<ItemDisplayListener>
) : ItemDisplayListener {
    override fun onItemClicked(item: Item, rightClick: Boolean) {
        reporter.reportToListeners { it.onItemClicked(item, rightClick) }
    }

    override fun onEmptyClicked() {
        reporter.reportToListeners { it.onEmptyClicked() }
    }

    override fun onCanAppendToTooltip(item: Item, text: StringBuffer) {
        reporter.reportToListeners { it.onCanAppendToTooltip(item, text) }
    }
}
