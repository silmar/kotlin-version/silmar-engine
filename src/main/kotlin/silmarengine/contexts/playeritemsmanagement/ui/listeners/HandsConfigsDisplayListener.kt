package silmarengine.contexts.playeritemsmanagement.ui.listeners

interface HandsConfigsDisplayListener {
    fun onConfigSelected(index: Int) {}
}