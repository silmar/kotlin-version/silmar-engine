package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import java.awt.Color
import java.awt.Dimension
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JPanel
import javax.swing.border.LineBorder

/**
 * Displays the items that are currently in a player's hands, side by side.
 */
class HandsItemsDisplay(private val getHandsItems: () -> Pair<Item?, Item?>) : JPanel() {
    /**
     * The displays for the individual left- and right-hand items.
     */
    private val leftDisplay: ItemDisplay
    private val rightDisplay: ItemDisplay

    init {
        background = Color.black

        layout = BoxLayout(this, BoxLayout.X_AXIS)

        // add the left-item display
        leftDisplay = ItemDisplay()
        leftDisplay.border = LineBorder(Color.white, 1)
        add(leftDisplay)

        add(Box.createRigidArea(Dimension(1, 0)))

        // add the right-item display
        rightDisplay = ItemDisplay()
        rightDisplay.border = LineBorder(Color.white, 1)
        add(rightDisplay)

        onItemsChanged()
    }

    /**
     * Informs this display that what items are in the player's hands has changed.
     */
    fun onItemsChanged() {
        val items = getHandsItems()
        leftDisplay.setItem(items.first)
        rightDisplay.setItem(items.second)
        repaint()
    }

    /**
     * Has this display tell its contained item-displays to update themselves.
     */
    fun updateItems() {
        leftDisplay.repaint()
        rightDisplay.repaint()
    }
}
