package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.events.ReporterMixin
import silmarengine.gui.tooltips.HasTooltip
import silmarengine.gui.tooltips.HasTooltipImpl
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.util.html.addLineBreak
import silmarengine.util.html.breakTextAtSpaces
import silmarengine.util.html.computeColorHexString
import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import java.awt.Graphics
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.text.NumberFormat
import javax.swing.JPanel

val itemDisplaySize = Dimension(32, 32)

/**
 * A panel which shows the picture of an item and may be moused-over for info about that item.
 */
class ItemDisplay : JPanel(), HasTooltip, ReporterMixin<ItemDisplayListener> {
    /**
     * The current item being displayed.
     */
    private var item: Item? = null

    override var listeners = HashSet<ItemDisplayListener>()

    /**
     * The number format used to format decimal values so they don't have too many digits.
     */
    private val format: NumberFormat

    /**
     * Whether this display should report clicks on itself when it is empty of an item.
     */
    private var reportEmptyClicks = false

    /**
     * Provides this component's tooltip implementation.
     */
    override val hasTooltipImpl = HasTooltipImpl(this)

    init {
        isOpaque = false
        background = Color.black

        // create this display's number-format
        format = NumberFormat.getInstance()
        format.maximumFractionDigits = 1

        addMouseListener(object : MouseAdapter() {
            private var rightClick = false
            override fun mouseEntered(e: MouseEvent?) {
                updateTooltip()
            }

            // when the mouse button is first pressed
            override fun mousePressed(e: MouseEvent?) {
                // check to see if the press is a right-click
                rightClick = isRightClick(e!!)
            }

            // when the mouse button is then released
            override fun mouseReleased(e: MouseEvent?) {
                // check again to see if the press is a right-click
                // since it's not clear on which event this information
                // is reported to us
                rightClick = rightClick or isRightClick(e!!)
            }

            // once a full click on this display has occurred
            override fun mouseClicked(e: MouseEvent?) {
                // check again to see if the press is a right-click
                // since it's not clear on which event this information
                // is reported to us
                rightClick = rightClick or isRightClick(e!!)

                // if this display is empty and clicks on empties should be reported
                if (item == null && reportEmptyClicks) {
                    // report this click
                    reportToListeners { it.onEmptyClicked() }
                }

                // otherwise, if this display is not empty
                else if (item != null) {
                    // report this click
                    reportToListeners { it.onItemClicked(item!!, rightClick) }
                }
            }

            /**
             * Returns whether the given mouse event is considered a right click
             * by this display.
             */
            private fun isRightClick(e: MouseEvent): Boolean {
                // allow many ways for the event to signal a right-click,
                // so that Mac users have workarounds for any bugs in their VM's
                // right-click emulation
                return (e.isPopupTrigger || e.isControlDown || e.isAltDown || e.isShiftDown)
            }
        })
    }

    override fun getMaximumSize(): Dimension = itemDisplaySize
    override fun getMinimumSize(): Dimension = itemDisplaySize
    override fun getPreferredSize(): Dimension = itemDisplaySize

    fun setItem(item: Item?) {
        this.item = item
        updateTooltip()
        repaint()
    }

    override fun paint(g: Graphics?) {
        val size = size

        // draw the item
        if (item != null) {
            drawItem(g!!, size.width / 2, size.height / 2)
        }

        // this comes last so this display's border (if any) get painted on top
        super.paint(g)
    }

    /**
     * Builds the tooltip for the item currently displayed.
     */
    override fun updateTooltip() {
        val text = StringBuffer()

        if (item != null) {
            appendItemTooltipText(text)

            reportToListeners { it.onCanAppendToTooltip(item!!, text) }
        }

        hasTooltipImpl.setTooltipText(text.toString(), 0)
    }

    /**
     * Draws this item's image into the given graphics context at the given location.
     */
    fun drawItem(g: Graphics, x: Int, y: Int) {
        // fill the background with color according to this item's condition (unless the
        // item is groupable)
        g.color = if (item!!.type.isGroupable) Color.black else item!!.condition.color
        val size = size
        g.fillRect(x - size.width / 2, y - size.height / 2, size.width, size.height)

        // draw the item
        val image = item!!.image
        g.drawImage(image, x - image.getWidth(null) / 2, y - image.getHeight(null) / 2,
            null)

        // if there is more than one of the item
        val quantity = item!!.quantity
        if (quantity > 1) {
            // draw the item's quantity in a corner of its square
            val text = quantity.toString()
            g.color = Color.white
            g.font = itemQuantityFont
            g.drawString(text,
                x + size.width / 2 - 1 - g.getFontMetrics(itemQuantityFont).stringWidth(
                    text), y + size.height / 2 - 1)
        }
    }

    /**
     * Appends info about the given item in the given value-context to the given string-buffer.
     */
    fun appendItemTooltipText(text: StringBuffer) {
        val item = item ?: return

        // add the item's name
        text.append("<i>")
        text.append(item.name)
        text.append("</i>")

        // if the item is not groupable
        if (!item.type.isGroupable) {
            // add the item's condition
            addLineBreak(text)
            val condition = item.condition
            val color = condition.color
            text.append("condition: <b><font color='#")
            text.append(computeColorHexString(color))
            text.append("'>")
            text.append(condition.displayName)
            text.append("</font></b>")
        }

        // add the item's encumbrance
        addLineBreak(text)
        text.append("encumbrance: <b>")
        text.append(format.format(item.encumbrance))
        text.append("</b>")

        // add the item's info
        val info = item.info
        if (info != null) {
            addLineBreak(text)
            text.append(breakTextAtSpaces(info, 45))
        }

        text.append(item.getAttributeRequirementsInfo())
    }

    fun setReportEmptyClicks(reportNoItemClicks: Boolean) {
        this.reportEmptyClicks = reportNoItemClicks
    }

    companion object {
        /**
         * The font used when displaying this item's quantity.
         */
        private val itemQuantityFont = Font("SansSerif", Font.BOLD, 12)
    }
}
