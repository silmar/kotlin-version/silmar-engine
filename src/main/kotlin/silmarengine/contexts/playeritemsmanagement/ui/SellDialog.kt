package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.gui.dialogs.InputIntDialog
import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendItemValue
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendRefusalText
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.util.html.addLineBreak
import javax.swing.JFrame

/**
 * A modal dialog that displays the player's items to allow the user to select which to sell.
 */
class SellDialog(
    owner: JFrame, greeting: String, getEquippedItems: () -> EquippedItems,
    getInventoryItems: () -> List<Item>, private val getPrice: (item: Item) -> Int,
    private val sellItem: (item: Item, quantity: Int) -> Unit
) : EquippedItemsAndInventoryDialog(owner, "Sell items", greeting, getEquippedItems,
    getInventoryItems) {
    private val itemDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            // cursed or unidentified items may not be sold
            if (item.type.isCursed || !item.isIdentified) return

            // gold may not be sold
            if (item.type.name == "gold") return

            // if the item's quantity is more than one
            var quantity = item.quantity
            if (quantity > 1) {
                // ask the user how many should be donated
                val result =
                    InputIntDialog.show(this@SellDialog, "Sell how many of this item?", 1,
                        quantity, quantity)
                if (result.cancelled) return
                quantity = result.valueEntered
            }

            sellItem(item, quantity)
        }

        override fun onCanAppendToTooltip(item: Item, text: StringBuffer) {
            val refusalText = when {
                !item.isIdentified -> "unidentified"
                item.type.isCursed -> "cursed"
                item.type.name == "gold" -> "is gold"
                else -> null
            }
            if (refusalText != null) {
                appendRefusalText(refusalText, text)
                return
            }

            addLineBreak(text)
            text.append("offered: ")
            appendItemValue(getPrice(item), text)
        }
    }

    init {
        isModal = true

        equippedItemsDisplay.addListener(itemDisplayListener)
        inventoryDisplay.addListener(itemDisplayListener)

        populateItemDisplays()
    }
}
