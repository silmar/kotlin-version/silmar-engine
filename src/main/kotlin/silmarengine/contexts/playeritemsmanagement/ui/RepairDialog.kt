package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendItemValue
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendRefusalText
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.util.html.addLineBreak
import javax.swing.JFrame

/**
 * A modal dialog that displays the player's items to allow the user to select which
 * to have repaired.
 */
class RepairDialog(
    owner: JFrame, greeting: String, getEquippedItems: () -> EquippedItems,
    getInventoryItems: () -> List<Item>,
    private val isItemPristine: (item: Item) -> Boolean,
    private val canFixItem: (item: Item) -> Boolean,
    private val getPrice: (item: Item) -> Int,
    private val repairItem: (item: Item, price: Int) -> Unit
) : EquippedItemsAndInventoryDialog(owner, "Repair items", greeting, getEquippedItems,
    getInventoryItems) {
    private val itemDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            // do nothing for groupable or undamaged items, and also for items
            // that the repairer can't fix
            if (item.type.isGroupable || isItemPristine(item) || !canFixItem(item)) return

            repairItem(item, getPrice(item))
        }

        override fun onCanAppendToTooltip(item: Item, text: StringBuffer) {
            val refusalText = when {
                item.type.isGroupable || isItemPristine(item) -> "undamaged"
                !canFixItem(item) -> "outside of expertise"
                else -> null
            }
            if (refusalText != null) {
                appendRefusalText(refusalText, text)
                return
            }

            addLineBreak(text)
            text.append("price: ")
            appendItemValue(getPrice(item), text)
        }
    }

    init {
        isModal = true

        equippedItemsDisplay.addListener(itemDisplayListener)
        inventoryDisplay.addListener(itemDisplayListener)

        populateItemDisplays()
    }
}
