package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.events.ReporterMixin
import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation
import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.contexts.playeritemsmanagement.ui.listeners.PassThroughItemDisplayListener
import java.awt.Color
import java.awt.Dimension
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.util.*
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.border.LineBorder
import kotlin.math.max
import kotlin.math.min

/**
 * Displays the equipped items on a player in a manner where the equip-locations
 * are laid out like they are on a body.
 */
class EquippedItemsDisplay(private val getEquippedItems: () -> EquippedItems) : JPanel(),
 ReporterMixin<ItemDisplayListener> {
    /**
     * The item-displays that display the equipped items.
     */
    private val itemDisplays = Array(EquipLocation.values().size) { ItemDisplay() }

    /**
     * The labels that go above the item-displays, naming each equip-location.
     */
    private val labels = arrayOfNulls<JLabel>(itemDisplays.size)

    override var listeners = HashSet<ItemDisplayListener>()

    init {
        // configure this display
        layout = null
        background = Color.black
        isOpaque = true
        addComponentListener(object : ComponentAdapter() {
            override fun componentResized(e: ComponentEvent?) {
                layoutDisplays()
            }
        })

        // for each item display to create
        itemDisplays.forEachIndexed { i, display ->
            // create this display
            display.border = LineBorder(Color.white, 1)
            add(display)

            // add this equipped-items-display as a listener to this item-display
            display.addListener(PassThroughItemDisplayListener(this))

            // create the text label for this display
            val label = Label(EquipLocation.values()[i].displayName, Fonts.smallText)
            labels[i] = label
            add(label)
        }

        // this display would like to be at least at tall as 7 items (even though at most 4
        // are in any vertical column; the extra space is for the text headers and for spacing
        // between equip-spots)
        preferredSize = Dimension(300, 7 * itemDisplaySize.height)
    }

    /**
     * Lays out this display's item-displays within itself in a manner such that
     * their locations correspond to where they would be on a body.
     */
    private fun layoutDisplays() {
        // for each item-display
        var headY = 0
        var neckY = 0
        var ringY = 0
        var handY = 0
        val fontMetrics = getFontMetrics(font)
        val fontAscent = fontMetrics.ascent
        val width = width
        val height = height
        val minYBetweenDisplays = itemDisplaySize.height + fontAscent + 2
        EquipLocation.values().forEach { location ->
            // determine this display's location within this equipped-items display
            val x1 = width / 10 + 4
            val x2 = width / 2
            val x3 = 9 * width / 10 - 4
            var x = 0
            var y = 0
            when (location) {
                EquipLocation.HEAD -> {
                    x = x2
                    headY = max(height / 12, fontAscent + 2 + itemDisplaySize.height / 2)
                    y = headY
                }
                EquipLocation.NECK -> {
                    x = x2
                    neckY = headY + minYBetweenDisplays
                    y = neckY
                }
                EquipLocation.BODY -> {
                    x = x2
                    y = max(height / 2, neckY + minYBetweenDisplays)
                }
                EquipLocation.FEET -> {
                    x = x2
                    y = min(11 * height / 12, height - 2 - itemDisplaySize.height / 2)
                }
                EquipLocation.BACK -> {
                    x = 3 * width / 10 + 2
                    y = height / 3
                }
                EquipLocation.LEFT_RING -> {
                    x = x1
                    ringY = 5 * height / 12
                    y = ringY
                }
                EquipLocation.LEFT_HAND -> {
                    x = x1
                    handY = ringY + minYBetweenDisplays
                    y = handY
                }
                EquipLocation.RIGHT_RING -> {
                    x = x3
                    y = ringY
                }
                EquipLocation.RIGHT_HAND -> {
                    x = x3
                    y = handY
                }
            }
            val display = itemDisplays[location.ordinal]
            display.setBounds(x - itemDisplaySize.width / 2,
                y - itemDisplaySize.height / 2,
                itemDisplaySize.width,
                itemDisplaySize.height)

            // determine this display's text xy
            val label = labels[location.ordinal]!!
            val textWidth = fontMetrics.stringWidth(label.text)
            label.setBounds(x - textWidth / 2,
                y - itemDisplaySize.height / 2 - fontAscent - 2,
                textWidth,
                fontMetrics.height)
        }
    }

    /**
     * Informs this display that the set of equipped items it is displaying has changed.
     */
    fun onEquippedItemsChanged() {
        val items = getEquippedItems()
        EquipLocation.values().forEach { location ->
            // set the item at this equip-location into its item-display in this display
            itemDisplays[location.ordinal].setItem(items.itemAt(location))
        }

        repaint()
    }

    /**
     * Has this display tell all its contained item-displays to update themselves.
     */
    fun updateItems() {
        itemDisplays.forEach { it.repaint() }
    }
}
