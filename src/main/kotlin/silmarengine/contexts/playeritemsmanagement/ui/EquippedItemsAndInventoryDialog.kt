@file:Suppress("LeakingThis")

package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Frame
import javax.swing.*
import javax.swing.border.EmptyBorder
import javax.swing.border.LineBorder
import javax.swing.border.MatteBorder

/**
 * A dialog that houses both the player's equipped-items-display, and its inventory display.
 */
open class EquippedItemsAndInventoryDialog(
    owner: Frame, title: String, val greeting: String?,
    getEquippedItems: () -> EquippedItems,
    getInventoryItems: () -> List<Item>
) : JDialog(owner, title) {
    var equippedItemsDisplay: EquippedItemsDisplay
        private set

    var inventoryDisplay: ItemsDisplay

    var mainPanel: JPanel
    val header: Label
    var equippedPanel: JPanel
    var inventoryPanel: JPanel

    init {
        // configure this dialog
        isResizable = false
        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color.black

        // add the main panel
        mainPanel = JPanel()
        mainPanel.isOpaque = false
        mainPanel.layout = BoxLayout(mainPanel, BoxLayout.Y_AXIS)
        pane.add(mainPanel, BorderLayout.CENTER)

        // if a greeting was supplied
        if (greeting != null) {
            // add a text-area containing the greeting
            val greetingLabel = Label("<html>$greeting</html>", Fonts.mediumText)
            greetingLabel.border = EmptyBorder(5, 5, 5, 5)
            greetingLabel.preferredSize = Dimension(300, 100)
            greetingLabel.maximumSize = Dimension(300, 300)
            mainPanel.add(greetingLabel)
        }

        // add a header
        header = Label("<html><b>Your items</b></html>", Fonts.mediumPlus)
        mainPanel.add(header)

        // add the equipped-items panel
        equippedPanel = JPanel()
        equippedPanel.isOpaque = false
        equippedPanel.alignmentX = 0.5f
        equippedPanel.border = MatteBorder(0, 0, 5, 0, Color.lightGray)
        equippedPanel.layout = BoxLayout(equippedPanel, BoxLayout.Y_AXIS)
        mainPanel.add(equippedPanel)

        // add the equipped-items display
        equippedItemsDisplay = EquippedItemsDisplay(getEquippedItems)
        val panel = equippedItemsDisplay
        equippedPanel.add(panel)
        equippedPanel.add(Box.createRigidArea(Dimension(0, 5)))

        // add the inventory panel
        inventoryPanel = JPanel()
        inventoryPanel.isOpaque = false
        inventoryPanel.border = EmptyBorder(5, 5, 5, 5)
        inventoryPanel.layout = BoxLayout(inventoryPanel, BoxLayout.Y_AXIS)
        mainPanel.add(inventoryPanel)

        // add the inventory display
        inventoryDisplay = ItemsDisplay(getInventoryItems, 2)
        val scrollPane = JScrollPane(inventoryDisplay)
        scrollPane.border = LineBorder(Color.white, 1)
        scrollPane.isOpaque = false
        scrollPane.horizontalScrollBarPolicy = JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        scrollPane.verticalScrollBarPolicy = JScrollPane.VERTICAL_SCROLLBAR_ALWAYS
        val bar = scrollPane.verticalScrollBar
        bar.unitIncrement = 32
        bar.blockIncrement = 32
        inventoryPanel.add(scrollPane)

        // set the inventory-display scroll-pane's preferred height to that
        // desired for display at one time by the inventory display
        // (+ the height of the pane's borders)
        val insets = scrollPane.insets
        val height = inventoryDisplay.preferredViewableHeight + insets.top + insets.bottom
        scrollPane.preferredSize = Dimension(preferredSize.width, height)

        pack()

        @Suppress("LeakingThis") setLocationRelativeTo(owner)
    }

    protected fun populateItemDisplays() {
        equippedItemsDisplay.onEquippedItemsChanged()
        inventoryDisplay.onItemsChanged()
    }
}
