package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.gui.windowLayout
import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import java.awt.Frame
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JDialog
import javax.swing.JLabel

/**
 * A dialog that lets the user select to have his player use, remove, and/or drop
 * items that are currently equipped or are in its inventory.
 */
class UseOrDropItemsDialog(
    parent: Frame, getEquippedItems: () -> EquippedItems,
    getInventoryItems: () -> List<Item>,
    val onEquippedItemClicked: (item: Item, rightClick: Boolean) -> Unit,
    val onInventoryItemClicked: (item: Item, rightClick: Boolean, inventoryDialog: JDialog) -> Unit
) : EquippedItemsAndInventoryDialog(parent, "Items", null, getEquippedItems,
    getInventoryItems) {
    private val equippedItemsDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            onEquippedItemClicked(item, rightClick)
        }
    }

    private val inventoryDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            onInventoryItemClicked(item, rightClick, this@UseOrDropItemsDialog)
        }
    }

    init {
        equippedItemsDisplay.addListener(equippedItemsDisplayListener)
        inventoryDisplay.addListener(inventoryDisplayListener)

        addWindowListener(object : WindowAdapter() {
            override fun windowOpened(e: WindowEvent?) {
                // if a location value for this dialog is available, use it
                val location = windowLayout.useOrDropItemsDialogLocation
                if (location != null) setLocation(location)

                // otherwise, center this dialog on its parent
                else setLocationRelativeTo(parent)
            }

            override fun windowClosing(event: WindowEvent?) {
                // store this dialog's location
                windowLayout.useOrDropItemsDialogLocation = location
            }

            override fun windowDeactivated(e: WindowEvent?) = windowClosing(e)
        })

        addEquippedItemsHelpText()
        addInventoryHelpText()

        pack()
    }

    private fun addEquippedItemsHelpText() {
        val label = Label("left-click: use   right-click: remove", Fonts.smallText)
        equippedPanel.add(label)
    }

    private fun addInventoryHelpText() {
        // add the help text
        var label: JLabel = Label("left-click: use   right-click: drop", Fonts.smallText)
        inventoryPanel.add(label)
        label = Label("mouseover for info", Fonts.smallText)
        inventoryPanel.add(label)
    }
}
