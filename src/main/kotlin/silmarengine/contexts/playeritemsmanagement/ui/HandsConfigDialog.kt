package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playeritemsmanagement.domain.model.HandsConfigs
import silmarengine.contexts.playeritemsmanagement.ui.listeners.HandsConfigsDisplayListener
import silmarengine.util.html.labelHeader
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Frame
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JDialog
import javax.swing.JPanel
import javax.swing.border.EmptyBorder

/**
 * A dialog that displays the player's current hands-configs, to allow selection of
 * one of them by the user as its player's new current hands-config in use.
 */
class HandsConfigDialog(
    parent: Frame, configs: HandsConfigs, setConfig: (index: Int) -> Unit,
    useConfig: (index: Int) -> Unit
) : JDialog(parent) {
    val handsConfigsDisplay: HandsConfigsDisplay

    /**
     * Whether this dialog is being shown to change the contents of a hands-config
     * (vs. using a hands-config).
     */
    private var inSetMode: Boolean = false

    /**
     * The label that describes the current purpose of this dialog to the user.
     */
    private var headingLabel: Label? = null

    private val handsConfigsDisplayListener = object : HandsConfigsDisplayListener {
        override fun onConfigSelected(index: Int) {
            // use or set the hands-config, depending on the mode this display is in
            if (!inSetMode) useConfig(index)
            else setConfig(index)

            // hide this dialog
            isVisible = false
        }
    }

    init {
        // configure this dialog
        setSize(100, 275)
        isResizable = false
        setLocationRelativeTo(owner)

        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color.black

        val panel = JPanel()
        panel.isOpaque = false
        panel.border = EmptyBorder(5, 5, 5, 5)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add a descriptive title (since one won't fit in the dialog's title-bar)
        var label = Label("$labelHeader <br> ", Fonts.mediumText)
        headingLabel = label
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 5)))

        // add the hands-configs display
        val display = HandsConfigsDisplay(configs)
        handsConfigsDisplay = display
        panel.add(display)

        // when a hands-config is selected
        display.addListener(handsConfigsDisplayListener)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        // add the instructional labels
        label = Label("mouseover for info,", Fonts.smallText)
        panel.add(label)
        label = Label("click to select", Fonts.smallText)
        panel.add(label)
    }

    /**
     * Shows the dialog in the set or use mode.
     */
    fun display(inSetMode: Boolean) {
        this.inSetMode = inSetMode

        headingLabel!!.text =
            labelHeader + (if (inSetMode) "Set " else "Use ") + "Hands<br>Configuration"

        isVisible = true
    }
}
