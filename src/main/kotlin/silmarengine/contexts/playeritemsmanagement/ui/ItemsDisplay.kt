package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.contexts.playeritemsmanagement.ui.listeners.PassThroughItemDisplayListener
import silmarengine.contexts.gameplay.tiles.tileSize
import java.awt.Color
import java.awt.Dimension
import java.util.*
import javax.swing.JPanel
import kotlin.math.ceil
import kotlin.math.max

/**
 * A panel that displays a rectangular grid of individual item-displays.
 */
class ItemsDisplay(
    private val getItems: () -> List<Item>, val minRowCount: Int? = null
) : JPanel(), ReporterMixin<ItemDisplayListener> {
    /**
     * The item-displays contained within this display.
     */
    private val itemDisplays = ArrayList<ItemDisplay>()

    /**
     * For this display, what is considered to be the default size of an item image, in pixels.
     */
    private val defaultItemSize = tileSize

    override var listeners = HashSet<ItemDisplayListener>()

    /**
     * Returns how much of this display's height it would like displayed at one time
     * within a containing scroll-pane (if there is one).
     */
    val preferredViewableHeight: Int
        get() = 2 * defaultItemSize.height

    init {
        background = Color.black
        layout = null
        determinePreferredHeight()
    }

    fun onItemsChanged() {
        // clear all item-displays from this display
        removeAll()
        itemDisplays.clear()

        // for each item currently to be displayed
        val items = getItems()
        items.forEach { item ->
            // add an item-display for this item to this display
            val display = ItemDisplay()
            display.setItem(item)
            itemDisplays.add(display)
            add(display)

            // add this items-display as a listener to this item-display
            display.addListener(PassThroughItemDisplayListener(this))
        }

        determinePreferredHeight()
        layoutItemDisplays()

        repaint()
    }

    /**
     * Has this display determine and set its preferred height based on how many item-displays it
     * contains.
     */
    private fun determinePreferredHeight() {
        // set this display's preferred height based on its number of rows
        val numItemsAcross = max(1, width / defaultItemSize.width)
        var numRows = ceil((itemDisplays.size.toFloat() / numItemsAcross)).toInt()
        numRows = max(minRowCount ?: 0, numRows)
        val height = numRows * defaultItemSize.height
        preferredSize = Dimension(preferredSize.width, height)
    }

    /**
     * Lays outs this display's item-displays in a grid-fashion within itself.
     */
    private fun layoutItemDisplays() {
        // determine the number of items per row
        val width = width
        val numItemsAcross = max(1, width / defaultItemSize.width)
        val leftX = (width - numItemsAcross * defaultItemSize.width) / 2

        // for each of this display's item-displays
        itemDisplays.forEachIndexed { i, display ->
            // set this display's bounds
            display.setBounds(leftX + defaultItemSize.width * (i % numItemsAcross),
                defaultItemSize.height * (i / numItemsAcross), defaultItemSize.width,
                defaultItemSize.height)
        }

        revalidate()
    }

    /**
     * Has this display tell all its contained item-displays to update themselves.
     */
    fun updateItems() {
        itemDisplays.forEach { it.repaint() }
    }
}
