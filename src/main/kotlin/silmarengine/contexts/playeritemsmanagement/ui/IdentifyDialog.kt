package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendItemValue
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendRefusalText
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.util.html.addLineBreak
import javax.swing.JFrame

/**
 * A modal dialog that displays the player's items to allow the user to select which to
 * have identified.
 */
class IdentifyDialog(
    owner: JFrame, greeting: String, getEquippedItems: () -> EquippedItems,
    getInventoryItems: () -> List<Item>, private val getPrice: (item: Item) -> Int,
    private val identifyItem: (item: Item, price: Int) -> Unit
) : EquippedItemsAndInventoryDialog(owner, "Identify items", greeting, getEquippedItems,
    getInventoryItems) {
    private val itemDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            // do nothing for identified items
            if (item.isIdentified) return

            identifyItem(item, getPrice(item))
        }

        override fun onCanAppendToTooltip(item: Item, text: StringBuffer) {
            if (item.isIdentified) {
                appendRefusalText("identified", text)
                return
            }

            addLineBreak(text)
            text.append("price: ")
            appendItemValue(getPrice(item), text)
        }
    }

    init {
        isModal = true

        equippedItemsDisplay.addListener(itemDisplayListener)
        inventoryDisplay.addListener(itemDisplayListener)

        populateItemDisplays()
    }
}
