package silmarengine.contexts.playeritemsmanagement.ui

import silmarengine.WorkThread
import silmarengine.gui.dialogs.InputIntDialog
import silmarengine.contexts.playeritemsmanagement.domain.model.EquippedItems
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendItemValue
import silmarengine.contexts.playeritemsmanagement.ui.functions.appendRefusalText
import silmarengine.contexts.playeritemsmanagement.ui.listeners.ItemDisplayListener
import silmarengine.util.html.addLineBreak
import javax.swing.JFrame

/**
 * A modal dialog that displays the player's items to allow the user to select
 * which to donate.
 */
class DonateDialog(
    owner: JFrame, greeting: String, getEquippedItems: () -> EquippedItems,
    getInventoryItems: () -> List<Item>, private val getValue: (item: Item) -> Int,
    private val donateItem: (item: Item, quantity: Int) -> Unit
) : EquippedItemsAndInventoryDialog(owner, "Donate items", greeting, getEquippedItems,
    getInventoryItems) {
    private val itemDisplayListener = object : ItemDisplayListener {
        override fun onItemClicked(item: Item, rightClick: Boolean) {
            // cursed or unidentified items may not be donated
            if (item.type.isCursed || !item.isIdentified) return

            // if the item's quantity is more than one
            var quantity = item.quantity
            if (quantity > 1) {
                // ask the user how many should be donated
                val result = InputIntDialog.show(this@DonateDialog,
                    "Donate how many of this item?", 1, quantity, quantity)
                if (result.cancelled) return
                quantity = result.valueEntered
            }

            // hide this modal dialog in case the donation needs to cause the
            // (also modal) gain-level dialog to appear; we can't do this in the
            // new task below because this code is itself running as part of
            // a task; hiding this dialog will cause the current task
            // to finish executing
            isVisible = false

            WorkThread.queueTask {
                // donate the item
                donateItem(item, quantity)

                // re-show this dialog
                isVisible = true
            }
        }

        override fun onCanAppendToTooltip(item: Item, text: StringBuffer) {
            val refusalText = when {
                !item.isIdentified -> "unidentified"
                item.type.isCursed -> "cursed"
                else -> null
            }
            if (refusalText != null) {
                appendRefusalText(refusalText, text)
                return
            }

            addLineBreak(text)
            text.append("experience value: ")
            appendItemValue(getValue(item), text, "")
        }
    }

    init {
        isModal = true

        equippedItemsDisplay.addListener(itemDisplayListener)
        inventoryDisplay.addListener(itemDisplayListener)

        populateItemDisplays()
    }
}
