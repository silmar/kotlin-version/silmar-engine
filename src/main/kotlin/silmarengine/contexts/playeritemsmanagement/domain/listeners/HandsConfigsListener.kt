package silmarengine.contexts.playeritemsmanagement.domain.listeners

import java.io.Serializable

interface HandsConfigsListener : Serializable {
    fun onConfigChanged(index: Int) {}
}