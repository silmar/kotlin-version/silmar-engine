package silmarengine.contexts.playeritemsmanagement.domain.listeners

import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import java.io.Serializable

interface InventoryListener : Serializable {
    fun onItemAdded(item: Item) {}
    fun onItemRemoved(item: Item) {}
}