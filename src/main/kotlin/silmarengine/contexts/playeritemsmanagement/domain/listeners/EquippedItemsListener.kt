package silmarengine.contexts.playeritemsmanagement.domain.listeners

import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation
import silmarengine.contexts.playeritemsmanagement.domain.model.Item
import java.io.Serializable

interface EquippedItemsListener : Serializable {
    fun onEquippedItemChanged(location: EquipLocation, from: Item?, to: Item?)
}