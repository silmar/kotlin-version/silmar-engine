package silmarengine.contexts.playeritemsmanagement.domain.listeners

import java.io.Serializable

interface PlayerItemsListener : Serializable {
    fun onCursedItemCouldNotBeRemoved() {}
}