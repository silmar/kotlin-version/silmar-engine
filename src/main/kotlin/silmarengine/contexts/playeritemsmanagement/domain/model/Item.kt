package silmarengine.contexts.playeritemsmanagement.domain.model

import java.awt.Image

typealias PIMItem = Item

interface Item {
    val type: ItemType
    val name: String
    val image: Image
    var quantity: Int
    val encumbrance: Float
    val condition: ItemCondition
    val isIdentified: Boolean
    val info: String?

    fun getAttributeRequirementsInfo(): String
}
