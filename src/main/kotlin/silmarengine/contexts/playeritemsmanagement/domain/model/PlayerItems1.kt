package silmarengine.contexts.playeritemsmanagement.domain.model

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.listeners.PlayerItemsListener
import silmarengine.util.math.randomInt
import java.io.Serializable

typealias PIMPlayerItems1 = PlayerItems1

class PlayerItems1(
    createGroupableItem: (type: ItemType, quantity: Int) -> Item,
    getItemType: (name: String) -> ItemType,
    private val canItemBeUsed: (item: Item) -> Boolean
) : PlayerItems, ReporterMixin<PlayerItemsListener>, Serializable {
    override val inventory = Inventory1(createGroupableItem, getItemType)

    override val equippedItems = EquippedItems()

    override val handsConfigs = HandsConfigs()

    override var listeners = HashSet<PlayerItemsListener>()

    override fun usePreviousHandsConfig() {
        val previous = handsConfigs.previousConfigUsedIndex
        if (previous >= 0) tryToUseHandsConfig(previous)
    }

    override fun setHandsConfig(index: Int) {
        val leftItem = equippedItems.itemAt(EquipLocation.LEFT_HAND)
        val rightItem = equippedItems.itemAt(EquipLocation.RIGHT_HAND)
        handsConfigs.setConfig(index, leftItem, rightItem)
    }

    override fun tryToUseHandsConfig(index: Int): Boolean {
        // determine the items of the config
        val config = handsConfigs.immutableConfigs[index]
        val leftItem = config.leftItem
        val rightItem = config.rightItem

        // determine the currently-equipped items
        val currentLeftItem = equippedItems.itemAt(EquipLocation.LEFT_HAND)
        val currentRightItem = equippedItems.itemAt(EquipLocation.RIGHT_HAND)

        // if either currently-equipped item is cursed
        if (currentLeftItem != null && currentLeftItem.type.isCursed || currentRightItem != null && currentRightItem.type.isCursed) {
            reportToListeners { it.onCursedItemCouldNotBeRemoved() }
            return false
        }

        // if the player isn't capable of using one of the items
        if (leftItem != null && !canItemBeUsed(
                leftItem) || rightItem != null && !canItemBeUsed(rightItem)) return false

        // add the currently-equipped items (if any) to this player's inventory
        val inventory = inventory
        if (currentLeftItem != null) {
            inventory.addItem(currentLeftItem)
        }
        if (currentRightItem != null && currentRightItem !== currentLeftItem) {
            inventory.addItem(currentRightItem)
        }

        // remove the items-to-equip (if any) from this player's inventory
        if (leftItem != null) inventory.tryToRemoveItem(leftItem)
        if (rightItem != null) inventory.tryToRemoveItem(rightItem)

        // equip the hands-items
        equippedItems.equip(leftItem, EquipLocation.LEFT_HAND)
        equippedItems.equip(rightItem, EquipLocation.RIGHT_HAND)

        handsConfigs.onConfigUsed(index)
        return true
    }

    override fun unequipItem(item: Item) {
        // remove the item from its equip-location
        val location = equippedItems.findLocationOf(item) ?: return
        equippedItems.equip(null, location)

        // if the item took two hands
        if (item.type.takesTwoHands) {
            // empty both this player's hands
            equippedItems.equip(null, EquipLocation.RIGHT_HAND)
            equippedItems.equip(null, EquipLocation.LEFT_HAND)
        }

        // add the removed item to the inventory
        inventory.addItem(item)
    }

    override val randomItem: Item?
        get() = when (randomInt(1, 2)) {
            1 -> equippedItems.randomItem
            else -> inventory.randomItem
        }

    /**
     * @return  Whether the equip succeeded.
     */
    override fun tryToEquipItem(
        item: Item, isAWeapon: (item: Item) -> Boolean, secondWeaponSizeLimit: Float,
        informUserAboutRejection: (message: String) -> Unit,
        removeCursedItemMessage: String
    ): Boolean {
        // if the given item is meant for one hand, and
        // this player's primary hand already holds an item,
        // and this player's off hand holds nothing
        var equipLocation = item.type.equipLocation ?: return false
        if (equipLocation == EquipLocation.RIGHT_HAND && !item.type.takesTwoHands && equippedItems.itemAt(
                EquipLocation.RIGHT_HAND) != null && equippedItems.itemAt(
                EquipLocation.LEFT_HAND) == null) {
            // switch the equip location to the off hand
            equipLocation = EquipLocation.LEFT_HAND
        }

        // if the given item is equipped as a ring, and
        // this player's primary ring slot holds an item,
        // and this player's off-hand ring slot holds nothing
        if (equipLocation == EquipLocation.RIGHT_RING && equippedItems.itemAt(
                EquipLocation.RIGHT_RING) != null && equippedItems.itemAt(
                EquipLocation.LEFT_RING) == null) {
            // switch the equip location to the off ring slot
            equipLocation = EquipLocation.LEFT_RING
        }

        // if the given item takes two hands
        val leftItem = equippedItems.itemAt(EquipLocation.LEFT_HAND)
        val rightItem = equippedItems.itemAt(EquipLocation.RIGHT_HAND)
        if (item.type.takesTwoHands) {
            // if there is an item in either hand which is cursed
            if (leftItem != null && equippedItems.itemAt(
                    EquipLocation.LEFT_HAND)!!.type.isCursed || rightItem != null && equippedItems.itemAt(
                    EquipLocation.RIGHT_HAND)!!.type.isCursed) {
                // don't allow the use
                informUserAboutRejection(removeCursedItemMessage)
                return false
            }
        }

        // if the item is a one-handed weapon that is larger than the given size
        val leftWeapon = if (leftItem != null && isAWeapon(leftItem)) leftItem
        else null
        val rightWeapon = if (rightItem != null && isAWeapon(rightItem)) rightItem
        else null
        if (isAWeapon(
                item) && !item.type.takesTwoHands && item.type.encumbrance > secondWeaponSizeLimit) {
            // if both hands hold either nothing, a short-sword-sized weapon, or a two-hand weapon
            val isLeftWeaponSmallEnough =
                if (leftWeapon != null) leftWeapon.type.encumbrance <= secondWeaponSizeLimit
                else false
            val isRightWeaponSmallEnough =
                if (rightWeapon != null) rightWeapon.type.encumbrance <= secondWeaponSizeLimit
                else false
            val leftTwoHands = leftWeapon?.type?.takesTwoHands ?: false
            val rightTwoHands = rightWeapon?.type?.takesTwoHands ?: false
            when {
                (leftWeapon == null || leftTwoHands || isLeftWeaponSmallEnough) && (rightWeapon == null || rightTwoHands || isRightWeaponSmallEnough) -> {
                    // the equip passes this check
                }

                // else, if the primary hand holds a one-handed weapon that's larger than a short-sword
                rightWeapon != null && !rightTwoHands && !isRightWeaponSmallEnough ->
                    // use the primary hand
                    equipLocation = EquipLocation.RIGHT_HAND

                // else if the offhand holds a one-handed weapon that's larger than a short-sword
                leftWeapon != null && !leftTwoHands && !isLeftWeaponSmallEnough ->
                    // use the offhand
                    equipLocation = EquipLocation.LEFT_HAND

                else -> {
                    // otherwise, don't allow the equip
                    informUserAboutRejection(
                        "When equipping two weapons, one of them must be a dagger or other small weapon.")
                    return false
                }
            }
        }

        // if there is already an item at the given player's
        // equip-location for this kind of item
        val currentItem = equippedItems.itemAt(equipLocation)
        if (currentItem != null) {
            // if the current item is cursed
            if (currentItem.type.isCursed) {
                // don't allow the use
                informUserAboutRejection(removeCursedItemMessage)
                return false
            }

            // if the old item takes two hands
            if (currentItem.type.takesTwoHands) {
                // remove it from both hands
                equippedItems.equip(null, EquipLocation.RIGHT_HAND)
                equippedItems.equip(null, EquipLocation.LEFT_HAND)
            }

            // add the old item to the inventory
            inventory.addItem(currentItem)
        }

        // remove the given item from the inventory
        inventory.tryToRemoveItem(item)

        // put the given item at the proper equip-location
        equippedItems.equip(item, equipLocation)

        // if the given item takes two hands
        if (item.type.takesTwoHands) {
            // if there is already an item in the given
            // player's off hand, and it's not the same
            // one that used to be in the other hand, too
            val offhandItem = equippedItems.itemAt(EquipLocation.LEFT_HAND)
            if (offhandItem != null && offhandItem !== currentItem) {
                // add the old item to the inventory
                inventory.addItem(offhandItem)
            }

            // put the given item in this player's off hand, as well
            equippedItems.equip(item, EquipLocation.LEFT_HAND)
        }

        return true
    }
}