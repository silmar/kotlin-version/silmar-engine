package silmarengine.contexts.playeritemsmanagement.domain.model

typealias PIMPlayerItems = PlayerItems

interface PlayerItems {
    val inventory: Inventory
    val equippedItems: EquippedItems
    val handsConfigs: HandsConfigs

    fun tryToEquipItem(
        item: Item, isAWeapon: (item: Item) -> Boolean, secondWeaponSizeLimit: Float,
        informUserAboutRejection: (message: String) -> Unit,
        removeCursedItemMessage: String
    ): Boolean

    fun unequipItem(item: Item)

    fun setHandsConfig(index: Int)

    /**
     * Selects the previous hands-config employed by this player as the one to use
     * currently.
     */
    fun usePreviousHandsConfig()

    /**
     * Has this player try to equip the hands-configuration of the given index within
     * its list of such configurations.
     *
     * @return  Whether the use succeeded.
     */
    fun tryToUseHandsConfig(index: Int): Boolean

    val randomItem: Item?
}