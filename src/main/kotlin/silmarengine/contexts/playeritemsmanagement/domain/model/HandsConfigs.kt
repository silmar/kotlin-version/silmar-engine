package silmarengine.contexts.playeritemsmanagement.domain.model

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.listeners.HandsConfigsListener
import java.io.Serializable

const val handsConfigsCount = 4

/**
 * An array of hands-configurations from which a user may select to quickly
 * change what this its is holding in its hands.
 */
class HandsConfigs : Serializable, ReporterMixin<HandsConfigsListener> {
    private val configs = Array(handsConfigsCount) { HandsConfig() }
    val immutableConfigs: List<HandsConfig> = configs.toList()

    override var listeners = HashSet<HandsConfigsListener>()

    /**
     * The index of the last hands-config employed.
     */
    private var lastConfigUsedIndex: Int = 0

    /**
     * The index of the hands-config employed just previous to the last one employed.
     */
    var previousConfigUsedIndex = -1
        private set

    fun setConfig(index: Int, leftItem: Item?, rightItem: Item?) {
        val config = configs[index]
        config.leftItem = leftItem
        config.rightItem = rightItem
        reportToListeners { it.onConfigChanged(index) }
   }

    /**
     * Removes the given item from any of the hands-configs it currently belongs to.
     */
    fun removeItemFromAll(item: Item) {
        configs.forEachIndexed { i, config ->
            // if this config's left-item is the item lost
            var removed = false
            if (config.leftItem === item) {
                config.leftItem = null
                removed = true
            }

            // if this config's right-item is the item lost
            if (config.rightItem === item) {
                config.rightItem = null
                removed = true
            }

            // if this config was modified above
            if (removed) {
                reportToListeners { it.onConfigChanged(i) }
            }
        }
    }

    fun onConfigUsed(index: Int) {
        // remember which are the current and the previous hands-configs used, so
        // the player may switch between them if desired
        previousConfigUsedIndex = lastConfigUsedIndex
        lastConfigUsedIndex = index
    }
}