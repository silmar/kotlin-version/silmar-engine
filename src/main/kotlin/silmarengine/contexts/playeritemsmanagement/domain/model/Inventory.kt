package silmarengine.contexts.playeritemsmanagement.domain.model

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.listeners.InventoryListener

interface Inventory : ReporterMixin<InventoryListener> {
    val items: List<Item>

    fun getFirstItemOfType(type: ItemType): Item?

    fun addItem(item: Item)
    fun addGold(amount: Int)

    /**
     * @return  Whether the removal succeeded.
     */
    fun tryToRemoveItem(item: Item): Boolean

    val randomItem: Item?
}