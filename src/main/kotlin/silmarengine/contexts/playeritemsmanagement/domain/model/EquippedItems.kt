package silmarengine.contexts.playeritemsmanagement.domain.model

import silmarengine.events.ReporterMixin
import silmarengine.contexts.playeritemsmanagement.domain.listeners.EquippedItemsListener
import silmarengine.util.list.randomElement
import java.io.Serializable

/**
 * An array of the items this player currently has equipped.  The locations in
 * the array correspond to the indices of the EquipLocation class.
 */
class EquippedItems : ReporterMixin<EquippedItemsListener>, Serializable {
    private val items = arrayOfNulls<Item?>(EquipLocation.values().size)
    val immutableItems: List<Item?> = items.toList()

    override var listeners = HashSet<EquippedItemsListener>()

    fun itemAt(location: EquipLocation): Item? = items[location.ordinal]

    fun isItemEquipped(item: Item): Boolean = findLocationOf(item) != null

    fun isItemOfTypeEquipped(type: ItemType): Boolean =
        getEquippedItemOfType(type) != null

    fun getEquippedItemOfType(type: ItemType): Item? {
        EquipLocation.values().forEach { location ->
            val item = itemAt(location)
            if (item != null && item.type == type) return item
        }
        return null
    }

    fun equip(item: Item?, location: EquipLocation) {
        val previousItem = items[location.ordinal]
        items[location.ordinal] = item
        reportToListeners { it.onEquippedItemChanged(location, previousItem, item) }
    }

    fun findLocationOf(item: Item): EquipLocation? =
        EquipLocation.values().firstOrNull { itemAt(it) == item }

    fun getHandsItems(): Pair<Item?, Item?> =
        Pair(itemAt(EquipLocation.LEFT_HAND), itemAt(EquipLocation.RIGHT_HAND))

    /**
     * An item chosen at random from the equip-locations on this player.
     * Will return null only if there are no such items.
     */
    val randomItem: Item? get() = compress().randomElement()

    /**
     * Returns a list of the items found in this equipped-list object, without any
     * info as to where they are equipped. Two-handed items are returned only once,
     * not twice.
     */
    fun compress(): List<Item> = EquipLocation.values().mapNotNull { location ->
        val item = itemAt(location)
        when {
            item == null -> null
            // don't consider a two-handed item twice
            item.type.takesTwoHands && location == EquipLocation.LEFT_HAND -> null
            else -> item
        }
    }
}