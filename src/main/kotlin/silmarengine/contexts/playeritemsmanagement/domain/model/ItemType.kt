package silmarengine.contexts.playeritemsmanagement.domain.model

typealias PIMItemType = ItemType

interface ItemType {
    val name: String
    val isGroupable: Boolean
    val equipLocation: EquipLocation?
    val encumbrance: Float
    val isCursed: Boolean
    val takesTwoHands: Boolean
    val isNoisy: Boolean
}
