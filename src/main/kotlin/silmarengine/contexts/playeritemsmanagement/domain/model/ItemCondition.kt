package silmarengine.contexts.playeritemsmanagement.domain.model

import java.awt.Color

typealias PIMItemCondition = ItemCondition

interface ItemCondition {
    val displayName: String
    val color: Color
}