package silmarengine.contexts.playeritemsmanagement.domain.model

import silmarengine.contexts.playeritemsmanagement.domain.listeners.InventoryListener
import silmarengine.util.list.randomElement
import java.io.Serializable

/**
 * The set of items on a player that are not equipped by that player.
 */
class Inventory1(
    private val createGroupableItem: (type: ItemType, quantity: Int) -> Item,
    private val getItemType: (name: String) -> ItemType
) : Inventory, Serializable {
    private val mutableItems = ArrayList<Item>()
    override val items: List<Item> = mutableItems

    override var listeners = HashSet<InventoryListener>()

    override fun getFirstItemOfType(type: ItemType): Item? =
        mutableItems.firstOrNull { it.type == type }

    override fun addItem(item: Item) {
        // if the given item is of a groupable type
        if (item.type.isGroupable) {
            // if this inventory already contains an item of the same type
            val sameType = getFirstItemOfType(item.type)
            if (sameType != null) {
                // add the given item's quantity to this item
                sameType.quantity += item.quantity
                return
            }
        }

        // add the given item to this inventory's list of items
        mutableItems.add(item)

        reportToListeners { it.onItemAdded(item) }
    }

    override fun tryToRemoveItem(item: Item): Boolean {
        // try to remove the item
        if (!mutableItems.remove(item)) return false

        reportToListeners { it.onItemRemoved(item) }

        return true
    }

    override fun addGold(amount: Int) {
        // if this player has no gold-item in its inventory
        val type = getItemType("gold")
        var gold = getFirstItemOfType(type)
        if (gold == null) {
            // create a gold-item and add it to the inventory
            gold = createGroupableItem(type, amount)
            addItem(gold)
        }

        else gold.quantity += amount
    }

    override val randomItem: Item? = items.randomElement()
}
