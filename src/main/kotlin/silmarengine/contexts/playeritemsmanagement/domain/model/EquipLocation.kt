package silmarengine.contexts.playeritemsmanagement.domain.model

enum class EquipLocation(val displayName: String) {
    HEAD("Head"),
    NECK("Neck"),
    BODY("Body"),
    BACK("Back"),
    LEFT_RING("Left ring"),
    RIGHT_RING("Right ring"),
    LEFT_HAND("Left hand"),
    RIGHT_HAND("Right hand"),
    FEET("Feet")
}