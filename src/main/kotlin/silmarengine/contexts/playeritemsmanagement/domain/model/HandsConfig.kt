package silmarengine.contexts.playeritemsmanagement.domain.model

import java.io.Serializable

/**
 * A pair of items (or lack thereof) that can be remembered for equipping
 * in a player's hands at the same time, for convenience.
 */
class HandsConfig(var leftItem: Item? = null, var rightItem: Item? = null) : Serializable

