package silmarengine.contexts.gameplay.areas

import silmarengine.contexts.gameplay.areas.extensions.pathfinding.findPassiblePath as coreFindPassiblePath
import silmarengine.contexts.adjacencygraphcreation.adapters.AGCDungeonListener
import silmarengine.contexts.adjacencygraphcreation.domain.model.AGCAdjacencyGraph
import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.areafileloading.domain.model.functions.loadArea
import silmarengine.contexts.areafileloading.services.loadAreaDataFromTiledFile
import silmarengine.contexts.arealighting.domain.model.ALPixelPoint
import silmarengine.contexts.arealighting.domain.model.ALTile
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.beingmovement.domain.model.*
import silmarengine.contexts.dungeongeneration.domain.model.functions.generateDungeon
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing
import silmarengine.contexts.gameplay.entities.beings.toMovementType
import silmarengine.contexts.gameplay.entities.items.ItemEntity
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBBeing
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBPixelPoint
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBTile
import silmarengine.contexts.gameplay.areas.adjacency.AdjacencyGraph
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.extensions.entities.moveEntityExt
import silmarengine.contexts.gameplay.areas.extensions.generation.stock
import silmarengine.contexts.gameplay.areas.extensions.generation.subsumeDungeon
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.containsLocation
import silmarengine.contexts.gameplay.areas.extensions.loading.subsumeArea
import silmarengine.contexts.gameplay.areas.extensions.passibility.isLinePassible
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleQuarterTileLocation
as coreGetNearbyPassibleQuarterTileLocation
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible as coreIsRectanglePassible
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle
import silmarengine.objectpools.tilePointPool
import silmarengine.contexts.pathfinding.domain.model.*
import silmarengine.sounds.Sound
import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles
import silmarengine.contexts.gameplay.tiles.Tiles.outOfBounds
import silmarengine.contexts.pathfinding.domain.model.MovementType
import java.awt.Dimension
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

@Suppress("LeakingThis")
open class Area1 : Area, Serializable {
    /**
     * The beings that currently reside in this area.
     */
    private val mutableBeings = ArrayList<Being>()
    override val beings: List<Being>
        get() = mutableBeings

    /**
     * The monsters that currently reside in this area.  This is a subset of this.beings.
     */
    private val mutableMonsters = ArrayList<Monster>()
    override val monsters: List<Monster>
        get() = mutableMonsters

    /**
     * The items that currently reside in this area.
     */
    private val mutableItems = ArrayList<ItemEntity>()
    override val items: List<ItemEntity>
        get() = mutableItems

    /**
     * The talker-beings that currently reside in this area.
     */
    private val mutableTalkerBeings = ArrayList<TalkerBeing>()
    override val talkerBeings: List<TalkerBeing>
        get() = mutableTalkerBeings

    /**
     * The special terrain entities that currently reside in this area.
     */
    private val mutableTerrains = ArrayList<Terrain>()
    override val terrains: List<Terrain>
        get() = mutableTerrains

    /**
     * The traps that are currently set in this area.  This list is always a subset of the
     * above terrains list.
     */
    private val mutableTraps = ArrayList<Trap>()
    override val traps: List<Trap>
        get() = mutableTraps

    /**
     * The player (if there is one) that currently resides in this area.
     */
    override var player: Player? = null
        protected set

    /**
     * The visual effects that are currently being displayed within this area.
     */
    @Transient
    private var mutableVisualEffects = ArrayList<VisualEffect>()
    override val visualEffects: List<VisualEffect>
        get() = mutableVisualEffects

    /**
     * The light-sources that currently exist within this area.
     */
    private val mutableLightSources = ArrayList<LightSource>()
    override val lightSources: List<LightSource>
        get() = mutableLightSources

    /**
     * The motif used by this area.  It isn't serialized with this area because its
     * values never change.
     */
    @Transient
    override lateinit var motif: AreaMotif
        protected set

    /**
     * The name of the above motif, to be serialized with this object
     * rather than the motif itself (which never changes), to save space.
     */
    private lateinit var motifName: String

    /**
     * The tile-dimensions of this area.  Should only be set (to a real value) once.
     */
    override var size = Dimension(0, 0)
        set(value) {
            field = value
            tiles.setSize(value)
        }

    override fun onSizeDetermined(size: Dimension) = run { this.size = size }

    /**
     * An object which encapsulates this area's tiles matrix.  See TileMatrix inner class.
     */
    private val tiles = TileMatrix()

    /**
     * This area's adjacency-graph.  Only randomly-generated areas will possess one of
     * these.
     */
    override var adjacencyGraph: AdjacencyGraph? = null
        protected set

    override val isSelfLit = false

    @Transient
    override var listeners = HashSet<AreaListener>()

    /**
     * Loads this area from the file of the name given (if one was), otherwise
     * generates this area randomly.
     */
    protected fun loadOrGenerate(fileName: String?) {
        // if the level uses a set area
        if (fileName != null) {
            // load the area from disk
            val area = loadArea(fileName, ::loadAreaDataFromTiledFile)
            subsumeArea(area)
            onLoaded()
        }
        else {
            // otherwise, generate the layout of this area randomly, and have an adjacency
            // graph built during the process
            val graph = AGCAdjacencyGraph()
            val dungeon = generateDungeon(AGCDungeonListener(graph))
            subsumeDungeon(dungeon)
            adjacencyGraph = AdjacencyGraph(this, graph)

            // stock this area randomly with beings, items, terrains, etc.
            stock()
        }
    }

    open fun onLoaded() {}

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(stream: ObjectOutputStream) {
        // store a lookup for this area's motif (rather than the motif itself)
        motifName = motif.name

        stream.defaultWriteObject()
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(stream: ObjectInputStream) {
        stream.defaultReadObject()

        listeners = HashSet()

        // determine this area's motif from its lookup
        motif = AreaMotif.getMotif(motifName) ?: throw Exception(
            "Unrecognized motif name in area")

        // fill in any transient fields
        mutableVisualEffects = ArrayList()
    }

    /**
     * May be overridden by areas which want to use a different out-of-bounds tile.
     */
    open val outOfBoundsTile: Tile = outOfBounds

    /**
     * An object which encapsulates this area's tiles array, because
     * retrieval and modification of the values of that array must
     * be accompanied by other actions.
     */
    private inner class TileMatrix : Serializable {

        /**
         * The rectangular array of tiles that makes up this area.
         */
        private lateinit var tiles: Array<Array<Tile>>

        /**
         * Returns the tile at the given tile-location.
         */
        fun getTile(location: TilePoint): Tile {
            return if (!containsLocation(location)) outOfBoundsTile
            else tiles[location.x][location.y]
        }

        /**
         * Sets the given tile into the given tile-location within this area.
         */
        fun setTile(location: TilePoint, tile: Tile) {
            if (!containsLocation(location)) {
                return
            }

            tiles[location.x][location.y] = tile

            // report this change
            reportToListeners { it.onTileChanged(location, tile) }
        }

        /**
         * Sets the size of the rectangular tiles array.
         */
        fun setSize(size: Dimension) {
            tiles = Array(size.width) { Array(size.height) { Tiles.floor } }
        }
    }

    override fun getTile(location: TilePoint): Tile {
        return tiles.getTile(location)
    }

    override fun setTile(location: TilePoint, tile: Tile) {
        tiles.setTile(location, tile)
    }

    override fun getTile(location: PixelPoint): Tile {
        if (!containsLocation(location)) return outOfBounds
        val tileLocation = tilePointPool.supply()
        location.asTileLocation(tileLocation)
        val tile = getTile(tileLocation)
        tilePointPool.reclaim(tileLocation)
        return tile
    }

    override fun getTile(at: ALPixelPoint): ALTile = getTile(at as PixelPoint)

    override fun getTile(at: LOSBPixelPoint): LOSBTile = getTile(at as PixelPoint)

    override fun getTile(at: BMPixelPoint): BMTile = getTile(at as PixelPoint)

    /**
     * May be overridden by areas which wish to provide other sounds for when an entity
     * is added to this area.
     */
    protected open fun getEntityAddedSound(entity: Entity): Sound? = null

    override fun addEntity(
        entity: Entity, location: PixelPoint, shouldNotify: Boolean
    ) {
        // add the entity to this area's appropriate list(s) of such entities.
        if (entity is Being) mutableBeings.add(entity)
        if (entity is ItemEntity) mutableItems.add(entity)
        if (entity is Terrain) mutableTerrains.add(entity)
        if (entity is TalkerBeing) mutableTalkerBeings.add(entity)
        if (entity is Trap) mutableTraps.add(entity)
        if (entity is Player) player = entity
        if (entity is Monster) mutableMonsters.add(entity)
        if (entity is LightSource) mutableLightSources.add(entity)
        if (entity is VisualEffect) mutableVisualEffects.add(entity)

        entity.placeInArea(this, location)

        // if we are to notify about this addition
        if (shouldNotify) {
            // determine what the associated sound should be
            var sound: Sound? = getEntityAddedSound(entity)
            if (sound == null) {
                when (entity) {
                    is Monster -> sound = Sounds.monsterGenerated
                    is ItemEntity -> sound = Sounds.itemAdded
                }
            }

            // if an associated sound was determined above
            if (sound != null) {
                // issue that sound
                onSoundIssued(sound, location)
            }

            // report this addition
            reportToListeners { it.onEntityPresenceChange(entity) }
        }
    }

    override fun removeEntity(entity: Entity) {
        // remove the entity from this area's appropriate lists that were
        // containing it
        val location = entity.location
        if (entity is Being) {
            mutableBeings.remove(entity)

            if (entity is Player) player = null
            if (entity is Monster) mutableMonsters.remove(entity)
            if (entity is TalkerBeing) mutableTalkerBeings.remove(entity)
        }
        else if (entity is ItemEntity) mutableItems.remove(entity)
        else if (entity is Terrain) {
            mutableTerrains.remove(entity)

            if (entity is Trap) mutableTraps.remove(entity)
        }
        if (entity is LightSource) mutableLightSources.remove(entity)
        if (entity is VisualEffect) mutableVisualEffects.remove(entity)

        // detm what the associated sound should be
        var sound: Sound? = null
        if (entity is ItemEntity) {
            sound = Sounds.itemRemoved
        }

        // if an associated sound was determined above
        if (sound != null) {
            // issue that sound
            onSoundIssued(sound, location)
        }

        // report this removal
        reportToListeners { it.onEntityPresenceChange(entity) }
    }

    override fun considerLightSourceFirst(lightSource: LightSource) {
        mutableLightSources.remove(lightSource)
        mutableLightSources.add(0, lightSource)
    }

    override val openDoorSound get() = Sounds.doorOpens
    override val closeDoorSound get() = Sounds.doorCloses

    override fun canSee(from: ALPixelPoint, to: ALPixelPoint): Boolean =
        canSeeExt(from as PixelPoint, to as PixelPoint).canSee

    override fun canSee(
        from: BMPixelPoint, to: BMPixelPoint, pixelsPerStep: Int,
        adjustFromToTileCenter: Boolean, beingsBlockLOS: Boolean, fromBeing: BMBeing?,
        toBeing: BMBeing?
    ): Boolean = canSeeExt(from as PixelPoint, to as PixelPoint, pixelsPerStep,
        adjustFromToTileCenter, beingsBlockLOS, fromBeing as Being?,
        toBeing as Being?).canSee

    override fun getBeingAt(at: LOSBPixelPoint): LOSBBeing? =
        getFirstEntityAt(at as PixelPoint, beings)

    override fun getNearbyPassibleQuarterTileLocation(
        from: PFPixelPoint, forEntitySize: PFPixelDimensions,
        movementType: PFMovementType, forEntity: PFEntity?, toEntity: PFEntity?
    ): PFPixelPoint? = coreGetNearbyPassibleQuarterTileLocation(from as PixelPoint,
        forEntitySize as PixelDimensions, movementType.toMovementType(),
        forEntity as Entity?, toEntity as Entity?)

    override fun isRectanglePassible(
        location: PFPixelPoint, size: PFPixelDimensions, movementType: MovementType,
        checkForBeing: Boolean, forEntity: PFEntity?, forEntity2: PFEntity?,
        checkForHarmfulTerrains: Boolean
    ): PFIsRectanglePassibleResult =
        coreIsRectanglePassible(location as PixelPoint, size as PixelDimensions,
            movementType.toMovementType(), checkForBeing, forEntity as Entity?,
            forEntity2 as Entity?, checkForHarmfulTerrains)

    override fun isRectanglePassible(
        location: BMPixelPoint, size: BMPixelDimensions, movementType: BMMovementType,
        checkForBeing: Boolean, forEntity: BMEntity?, forEntity2: BMEntity?,
        checkForHarmfulTerrains: Boolean
    ): BMIsRectanglePassibleResult =
        coreIsRectanglePassible(location as PixelPoint, size as PixelDimensions,
            movementType.toMovementType(), checkForBeing, forEntity as Entity?,
            forEntity2 as Entity?, checkForHarmfulTerrains)

    override fun moveEntity(entity: BMEntity, to: BMPixelPoint) {
        moveEntityExt(entity as Entity, to as PixelPoint)
    }

    override fun getTerrainsInRectangle(
        location: BMPixelPoint, size: BMPixelDimensions
    ): List<BMTerrain> =
        terrains.getEntitiesInRectangle(location as PixelPoint, size as PixelDimensions)

    override fun isLinePassible(
        from: BMPixelPoint, to: BMPixelPoint, movementType: BMMovementType,
        moverSize: BMPixelDimensions, fromEntity: BMEntity?, toEntity: BMEntity?,
        checkForHarmfulTerrains: Boolean
    ): IsLinePassibleResult = isLinePassible(from as PixelPoint, to as PixelPoint,
        movementType.toMovementType(), moverSize as PixelDimensions,
        fromEntity as Entity?, toEntity as Entity?, checkForHarmfulTerrains)

    override fun findPassiblePath(
        from: BMPixelPoint?, to: BMPixelPoint, movingEntity: BMEntity,
        goalEntity: BMEntity?, maxLength: BMTileDistance, movementType: BMMovementType,
        shouldFindBestPath: Boolean, checkForHarmfulTerrains: Boolean
    ): FindPassiblePathResult {
        val result = coreFindPassiblePath(from as PixelPoint?, to as PixelPoint,
            movingEntity as Entity, goalEntity as Entity?, maxLength as TileDistance,
            movementType.toMovementType(), shouldFindBestPath, checkForHarmfulTerrains)
        return object : FindPassiblePathResult {
            override val bestPathReturned = result.bestPathReturned
            override val foundHarm = result.foundHarm
            @Suppress("UNCHECKED_CAST")
            override val path = result.path as ArrayDeque<BMPixelPoint>?
        }
    }
}

