package silmarengine.contexts.gameplay.areas.adjacency

import silmarengine.contexts.adjacencygraphcreation.domain.model.*
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.areOrthogonalDistancesAtMost
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelRectangle
import silmarengine.objectpools.pixelDistancePool
import silmarengine.contexts.gameplay.tiles.extensions.isDoor
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.io.Serializable
import java.util.*

/**
 * Stores a graph representation of an area in terms of which of its rooms and hallways are
 * connected to one another, for the purpose of quickly determine'ing if there is a path of such
 * enclosures between any two locations in the area.
 */
class AdjacencyGraph(
    /**
     * The area for which this is an adjacency graph.
     */
    private val area: Area, graph: AGCAdjacencyGraph
) : Serializable {
    /**
     * The spaces (see Space class) within this area, sorted by their x-values (with an
     * arbitrary sub-ordering of spaces whose x-values tie).
     */
    private val spaces = ArrayList<Space>()

    init {
        // transfer the data in the given adjacency-graph into this adjacency-graph
        val graphSpaces = graph.immutableSpaces
        spaces.addAll(graphSpaces.map { Space(it) })
        spaces.forEach { space ->
            val graphSpace = graphSpaces.first { it.id == space.id }
            graphSpace.links.forEach { space.links.add(Link(it, spaces)) }
        }
    }

    /**
     * Returns whether there is an adjacency path between
     * the spaces containing the given two locations that is at most of the given pixel-length.
     *
     * @param allowDoors    Whether paths are obstructed by closed doors.
     */
    fun getPathExists(
        from: PixelPoint, to: PixelPoint, maxLength: PixelDistance, allowDoors: Boolean
    ): GetPathExistsResult {
        // if the two locations are further apart than the given max-distance, no path
        // can exist
        val result = GetPathExistsResult(false)
        if (!isDistanceAtMost(from, to, maxLength)) return result

        // determine the containing spaces of the given locations
        val fromSpace = getContainingSpace(from)
        val toSpace = getContainingSpace(to)

        // if the given locations are both contained within the same space, a path
        // automatically exists
        if (fromSpace == toSpace) {
            result.pathExists = true
            result.pathLength.distance = 0
            return result
        }

        // it may be that one or both of the given locations are not located within a
        // space (e.g. a ghost moaning from inside walls), in which case no path exists
        if (fromSpace == null || toSpace == null) return result

        // mark all spaces in this graphs's list of spaces as not yet having been searched
        spaces.forEach { it.searched = false }

        getPathExists(fromSpace, toSpace, from, to, maxLength, result, allowDoors)
        return result
    }

    /**
     * Returns whether there is an adjacency path between the given two spaces.
     *
     * @param fromLocation  The location from which the search for a path is to start, which is
     * assumed to be within (or along the border of) the from-space.
     * @param toLocation    The final location to which a path is sought, which is
     * assumed to be within the to-space.
     * @param maxLength     The maximum pixel-length the path may be, from fromLocation
     * to toLocation.
     * @param result        The object which is to be used as the result of this call.
     * @param allowDoors    Whether paths are obstructed by closed doors.
     */
    private fun getPathExists(
        from: Space, to: Space, fromLocation: PixelPoint, toLocation: PixelPoint,
        maxLength: PixelDistance, result: GetPathExistsResult, allowDoors: Boolean
    ) {
        // for each of the links from the from-space
        from.links.forEach nextLink@{ link ->
            // if closed doors obstruct paths, and the link between the two spaces is a doorway,
            // rather than an open space
            if (!allowDoors && link.usesDoors) {
                // if both of the doors comprising the link are shut, then skip this link
                if (area.getTile(link.intersection1).isDoor && area.getTile(
                        link.intersection2).isDoor) return@nextLink
            }

            // determine the next-space to which this link leads
            val next = if (link.space1 == from) link.space2 else link.space1

            // if the next-space has already been searched, skip it
            if (next.searched) return@nextLink
            next.searched = true

            // if the next-space is the given to-space
            if (next == to) {
                // if the distance from the link to the goal to-location is less than
                // that given
                val lastDistance = pixelDistancePool.supply()
                getDistance(link.intersection1, toLocation, lastDistance)
                if (lastDistance.distance <= maxLength.distance) {
                    // a path exists
                    result.pathExists = true

                    // add in the distance covered by this final portion of the path to
                    // the result
                    result.pathLength.distance += lastDistance.distance
                }
                pixelDistancePool.reclaim(lastDistance)
                return
            }

            // for each of the vertices of the next-space's rectangle
            next.vertices.forEach { vertex ->
                // if a rough estimate indicates this vertex is within the given distance;
                // this should eliminate many calls to the slower getDistance below
                if (areOrthogonalDistancesAtMost(fromLocation, vertex, maxLength)) {
                    // if this vertex is definitely within the given distance
                    val distanceToNext = getDistance(fromLocation, vertex)
                    if (distanceToNext.distance <= maxLength.distance) {
                        // continue the search through this space
                        getPathExists(next, to, vertex, toLocation, PixelDistance(
                            maxLength.distance - distanceToNext.distance), result,
                            allowDoors)

                        // if a positive result was achieved by the above search
                        if (result.pathExists) {
                            // add in the distance covered by this portion of the path to
                            // the result
                            result.pathLength.distance += distanceToNext.distance

                            // we are done
                            return
                        }
                    }
                }
            }
        }
    }

    /**
     * The return value of getPathExists, above.
     */
    class GetPathExistsResult(pathExists: Boolean) {

        /**
         * Whether the routine found a path.
         */
        var pathExists = false

        /**
         * An approximation of the pixel-distance covered by the path (if one was found).
         */
        var pathLength = PixelDistance()

        init {
            this.pathExists = pathExists
        }
    }

    /**
     * Returns the space (see Space inner class) whose rectangle contains the given location.
     */
    private fun getContainingSpace(
        location: PixelPoint
    ): Space? {
        // estimate where to start looking in this graph's sorted list of spaces
        // according to the fraction that the given location's x-value is of the area's width
        val areaPixelWidth = area.size.width * tileWidth
        val index = (location.x.toFloat() / areaPixelWidth * spaces.size).toInt()

        // keep doing this
        var back = index
        var forward = index + 1
        while (true) {
            // check to see if we have searched the whole list of spaces
            if (back < 0 && forward >= spaces.size) return null

            // if there are still spaces left going backwards in the list
            if (back >= 0) {
                // check to see if the next space going backwards contains the location
                val space = spaces[back--]
                if (space.rect.contains(location)) return space
            }

            // if there are still spaces left going forwards in the list
            if (forward < spaces.size) {
                // check to see if the next space going forwards contains the location
                val space = spaces[forward++]
                if (space.rect.contains(location)) return space
            }
        }
    }

    /**
     * Adds the given space to those contained by this adjacency graph.
     */
    private fun addSpace(space: Space) {
        // insert the given space where its x-value indicates it should be in the list
        // (which is sorted by the spaces' rectangle's x-value)
        val index = spaces.indexOfFirst { space.rect.x < it.rect.x }
        if (index >= 0) spaces.add(index, space) else spaces.add(space)
    }

    /**
     * A room or a hall in an area.
     */
    class Space(from: AGCSpace) : Serializable {
        val id = from.id

        /**
         * Links to the other spaces that are adjacent to this one.
         */
        val links = ArrayList<Link>()

        /**
         * The pixel-rectangle covered by this space within the area.
         */
        val rect = MutablePixelRectangle()

        /**
         * The vertices of this object's rect field.  These are calculated
         * when rect is set.
         */
        val vertices = Array(4) { MutablePixelPoint() }

        /**
         * Whether this space has been processed during the current search.
         */
        var searched: Boolean = false

        init {
            val rect = from.rect
            setRect(PixelRectangle(PixelPoint(rect.x, rect.y),
                PixelDimensions(rect.width, rect.height)))
        }

        fun setRect(to: PixelRectangle) {
            rect.set(to)

            // determine and cache the rectangle's vertices
            vertices[0].set(rect.x, rect.y)
            vertices[1].set(rect.x + rect.width, rect.y)
            vertices[2].set(rect.x, rect.y + rect.height)
            vertices[3].set(rect.x + rect.width, rect.y + rect.height)
        }
    }

    /**
     * Associates two spaces as adjacent.
     */
    class Link(other: AGCLink, spaces: List<Space>) : Serializable {
        /**
         * The two spaces associated as adjacent with one another by this link.
         */
        val space1: Space = spaces.first { other.space1.id == it.id }
        val space2: Space = spaces.first { other.space2.id == it.id }

        /**
         * Whether the two spaces are connected by doors.
         */
        var usesDoors: Boolean = other.usesDoors

        /**
         * The pixel-locations of the center of the two tiles connecting the two spaces
         * (for example, to test whether doors (if present) are open).
         */
        val intersection1 = other.intersection1.toPixelPoint()
        val intersection2 = other.intersection2.toPixelPoint()
    }
}

private fun AGCPixelPoint.toPixelPoint() = PixelPoint(x, y)