package silmarengine.contexts.gameplay.areas.generation

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * An entity-type of which instances may be generated for placement in areas.
 */
interface Stockable {
    /**
     * A value from 0 to 10 (or higher) that specifies how often entities of this type
     * are randomly generated, relative to others.  0 means the entity
     * is never generated.  5 is considered an average amount.
     */
    val frequency: Int

    /**
     * Returns whether instances of this stockable may be generated for placement
     * on the given area.
     */
    fun isValidForArea(area: Area): Boolean

    /**
     * Returns whether the given location in the given area is a valid one
     * at which an instance of this stockable may be placed.
     */
    fun isPlacementLocationValid(area: Area, location: PixelPoint): Boolean

    /**
     * Creates an instance of this stockable for stocking purposes.
     */
    fun createStockInstance(): Entity
}
