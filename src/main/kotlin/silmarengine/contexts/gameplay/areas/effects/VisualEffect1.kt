package silmarengine.contexts.gameplay.areas.effects

import silmarengine.contexts.gameplay.entities.Entity1
import silmarengine.getImage
import silmarengine.contexts.gameplay.areas.extensions.entities.onEntityChangedAppearance
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.util.image.getImageSizeWithoutLoading
import java.awt.Image
import java.util.*

/**
 * An entity which is just an image to be displayed at some location in the area.
 * The image has no actual bearing on the game other than its visual presence.
 * The image can be modified over time to produce an animation effect.
 */
open class VisualEffect1(imageName: String?) : VisualEffect, Entity1() {
    override var imageName = imageName
        set(value) {
            field = value

            determineSize()

            onImageToChange()
            area.onEntityChangedAppearance()
        }

    init {
        if (imageName != null) determineSize()
    }

    @Transient
    override var image: Image? = null
        get() = if (field != null) field
        else {
            val imageName = imageName
            field = if (imageName != null) getImage(imageName) else null
            field
        }

    override val isVisible: Boolean
        get() = imageName != null

    /**
     * Has this entity determine its size (in pixels).
     */
    private fun determineSize() {
        val imageNameVal = imageName ?: return

        // use the cached size if there is one, otherwise determine the size from
        // the image-name and cache it
        val cachedSize = sizes[imageNameVal]
        mutableBounds.setSize(if (cachedSize != null) cachedSize
        else {
            val imageSize = PixelDimensions(getImageSizeWithoutLoading(imageNameVal))
            sizes[imageNameVal] = imageSize
            imageSize
        })
    }

    companion object {
        /**
         * A cache of the image-sizes (in pixels) of the various visual-effects, by their
         * image-names.
         */
        private val sizes = HashMap<String, PixelDimensions>()
    }
}
