package silmarengine.contexts.gameplay.areas.effects

import silmarengine.contexts.gameplay.entities.Entity

interface VisualEffect : Entity {
    /**
     * The filename of the current image to display for this effect.  A null value means
     * this effect should not currently display.
     */
    val imageName: String?
}