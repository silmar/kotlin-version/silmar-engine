package silmarengine.contexts.gameplay.areas.effects

import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance

class LightSourceVisualEffect(
    imageName: String?,
    /**
     * How far this lightsource shines.
     */
    override val lightRadius: PixelDistance
) : VisualEffect1(imageName), LightSource {

    override val isEmittingLight: Boolean
        get() = true
}
