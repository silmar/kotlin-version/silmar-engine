package silmarengine.contexts.gameplay.areas

import silmarengine.contexts.gameplay.tiles.Tiles.roomSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.roomWall
import silmarengine.contexts.gameplay.tiles.Tiles.secretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.wall
import silmarengine.util.image.loadImage
import java.awt.Image
import java.util.*

/**
 * A motif, or visual style, of an area.  Such a motif specifies
 * what images to use for the tiles in an area.
 */
@Suppress("LeakingThis")
open class AreaMotif(
    /**
     * The textual name of this motif.
     */
    val name: String,
    /**
     * The filenames of the images to load for the
     * tiles in any area to which this motif is being applied.
     * These images will be loaded into the above images array.
     */
    private val tileImageNames: Array<String?>
) {

    /**
     * The images to use for the tiles in any area to which this
     * motif is being applied.  The element of any given index within this array
     * corresponds to the image for the tile with that index in Tile.
     */
    lateinit var tileImages: Array<Image?>
        private set

    init {
        motifsMap[name] = this

        loadTileImages()
    }

    /**
     * Loads this motif's tile images from the disk.
     */
    private fun loadTileImages() {
        // for each tile-image-name in the our names-list
        tileImages = arrayOfNulls(tileImageNames.size)
        tileImageNames.forEachIndexed { i, name ->
            // load the tile-image with this name
            if (name != null) tileImages[i] = loadImage("tiles/$name")
        }

        // for certain tiles, use the images of others
        tileImages[secretDoor.index] = tileImages[wall.index]
        tileImages[roomSecretDoor.index] = tileImages[roomWall.index]
    }

    /**
     * Returns whether this motif is the same as the one given.
     *
     * @param   other       The motif to compare for equality with this one.
     */
    override fun equals(other: Any?): Boolean {
        return other is AreaMotif && (other === this || other.name == name)
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    companion object {
        /**
         * This holds all the flyweight motifs so that they may be
         * retrieved by name.  This allows an area to write out just the name
         * of its motif when serialized, rather than the whole motif.
         * When deserialized, the area can then find its motif here using
         * the motif's name.
         */
        private val motifsMap = HashMap<String, AreaMotif>()

        /**
         * Returns the motif of the given name.
         *
         * @param   name        The name of the motif to return.
         */
        fun getMotif(name: String): AreaMotif? {
            return motifsMap[name]
        }
    }
}

