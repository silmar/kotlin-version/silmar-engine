package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.areas.Area

/**
 * Informs this area that the given entity (which is assumed to be in this area) has
 * changed its appearance in some notable way.
 */
fun Area.onEntityChangedAppearance() {
    reportToListeners { it.onEntityChangedAppearance() }
}