package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInLOSAndRange
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileSize

/**
 * Returns a new version of the given target location that is skewered from that location
 * by up to the given max-skewer distance, and is also passible and in los of the given
 * from- and target- locations. If no skewered location is found, the given target location
 * is returned.
 */
fun getSkeweredTargetLocation(
    area: Area, from: PixelPoint, target: PixelPoint, maxSkewer_: PixelDistance
): PixelPoint {
    // limit the max-skewer to the range between the two locations
    val range = getDistance(from, target)
    val maxSkewer = if (maxSkewer_.distance > range.distance) range else maxSkewer_

    // try this a certain number of times
    var skewered: PixelPoint?
    for (i in 0..4) {
        // find a location within the given range of the given target that is
        // passible and in los of the target
        // note: we can use the tile-size as the rectangle-size, since we don't
        // care about an errant fireball fitting into any particular space
        skewered = area.getRandomPassibleRectangleLocationInLOSAndRange(target, tileSize,
            maxSkewer, 50, MovementType.FLYING_NO_HANDS, false)

        // if no location was found above, or the location found can't be seen from
        // the given from-location, try again
        if (skewered == null || !area.canSeeExt(from, skewered).canSee) continue

        // we've found a valid skewered location
        return skewered
    }

    return target
}
