package silmarengine.contexts.gameplay.areas.extensions.passibility

import silmarengine.contexts.beingmovement.domain.model.BMIsLinePassibleResult
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.pixelPointPool
import silmarengine.objectpools.pixelRectanglePool
import silmarengine.objectpools.floatPointPool
import kotlin.math.floor
import kotlin.math.roundToInt

/**
 * The return value of isLinePassible().
 */
data class IsLinePassibleResult(
    /**
     * Whether the line was passible.
     */
    override var passible: Boolean = false,
    /**
     * Whether there was something harmful (to a given entity) on the line.
     */
    override var harmful: Boolean = false
) : BMIsLinePassibleResult

/**
 * Returns whether the line across this area between the given from- and to- pixel-locations
 * is passible for a mover of the given size and employing the given movement type.
 * Beings on the line besides those given are considered to block passibility.
 * Harmful terrains (to the given from-entity, which must then be a non-null monster)
 * are considered, if specified.
 *
 * NOTE: The points on the line tested must be the same ones tested by
 * doMoveTowardsGoal(), since this routine is called to give the ok
 * for that one to be called.  This means that the two routines must be kept in sync.
 *
 * @return  See IsLinePassibleResult.  The returned object is reused with each call
 * and therefore should not be cached.
 */
fun Area.isLinePassible(
    from: PixelPoint, to: PixelPoint, movementType: MovementType,
    moverSize: PixelDimensions, fromEntity: Entity?, toEntity: Entity?,
    checkForHarmfulTerrains: Boolean
): IsLinePassibleResult {
    // if the given from- and to- location are the same, the line is passible
    val result = IsLinePassibleResult()
    if (from == to) {
        result.passible = true
        return result
    }

    // determine the delta vector to be travelled during each step along the line
    val direction = getUnitVector(from, to)
    val pixelsPerStep = 4
    direction.x *= pixelsPerStep.toFloat()
    direction.y *= pixelsPerStep.toFloat()

    // determine how many steps it will take to get to the end of the line; we
    // won't test the ending fractional step (if there is one), though
    val numSteps =
        floor((getDistance(from, to).distance / pixelsPerStep).toDouble()).toInt()

    // for each step along the line
    val current = floatPointPool.supply()
    current.setLocation(from.x.toFloat(), from.y.toFloat())
    val testRectLocation = pixelPointPool.supply()
    for (i in 0 until numSteps) {
        // if this step's location is harmful (if specified)
        current.x += direction.x
        current.y += direction.y
        testRectLocation.set(current.x.roundToInt(), current.y.roundToInt())
        val rectangleResult =
            isRectanglePassible(testRectLocation, moverSize, movementType, true,
                fromEntity, toEntity, checkForHarmfulTerrains)
        if (checkForHarmfulTerrains && rectangleResult.harmful) {
            // set a 'harmful' result
            result.harmful = true
            break
        }

        // if this step's location is impassible
        if (!rectangleResult.passible) {
            // set in the result whether this step's rectangle intersects
            // the to-entity's bounds
            val rectangle = pixelRectanglePool.supply()
            rectangle.setLocation(testRectLocation.x, testRectLocation.y)
            rectangle.translate(-moverSize.width / 2, -moverSize.height / 2)
            rectangle.setSize(moverSize)
            result.passible = toEntity != null && toEntity.bounds.intersects(rectangle)
            pixelRectanglePool.reclaim(rectangle)
            break
        }

        // if this was the last step, the whole line is passible (and non-harmful, if
        // that was a consideration)
        if (i == numSteps - 1) result.passible = true
    }

    floatPointPool.reclaim(current)
    pixelPointPool.reclaim(testRectLocation)
    return result
}

