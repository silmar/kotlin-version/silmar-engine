package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import kotlin.math.abs

/**
 * Returns the location of the corner of the tile at the given of-location that is closest
 * to the given from-location.  The result is stored into the given use parameter.
 */
fun PixelPoint.getNearestTileCorner(
    from: PixelPoint, use: MutablePixelPoint
): PixelPoint {
    use.set(x - abs(x % tileWidth) + if (x < from.x) tileWidth - 1 else 0,
        y - abs(y % tileHeight) + if (y < from.y) tileHeight - 1 else 0)
    return use
}
