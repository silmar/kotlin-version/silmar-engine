package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import kotlin.math.roundToInt
import kotlin.math.sqrt

fun getDistance(from: PixelPoint, to: PixelPoint): PixelDistance {
    val use = PixelDistance()
    getDistance(from, to, use)
    return use
}

/**
 * Computes the distance between the given two locations, storing the result in the given
 * use-object.
 */
fun getDistance(
    from: PixelPoint, to: PixelPoint, use: PixelDistance
): PixelDistance {
    val a = (to.y - from.y).toFloat()
    val b = (to.x - from.x).toFloat()
    use.distance = sqrt((a * a + b * b).toDouble()).toFloat().roundToInt()
    return use
}