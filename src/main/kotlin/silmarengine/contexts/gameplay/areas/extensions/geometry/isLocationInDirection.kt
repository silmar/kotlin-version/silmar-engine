package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.geometry.vectors.isLocationInDirection
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.geom.Point2D

fun isLocationInDirection(
    from: PixelPoint, to: PixelPoint, direction: Point2D.Float
): Boolean = isLocationInDirection(from.x, from.y, to.x, to.y, direction)