package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import kotlin.math.abs

/**
 * Returns whether the orthogonal distance between the given two locations is at most
 * the given value.  Is designed to be a quick test for eliminating many of the
 * times when the much slower getDistance might be called.
 */
fun areOrthogonalDistancesAtMost(
    from: PixelPoint, to: PixelPoint, distance: PixelDistance
): Boolean {
    if (abs(from.x - to.x) > distance.distance) return false
    return abs(from.y - to.y) <= distance.distance
}