package silmarengine.contexts.gameplay.areas.extensions.passibility

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.containsPixelRectangle
import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.objectpools.pixelPointPool
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Returns one of the four upper-left locations of quarter-tiles that immediately surround
 * (i.e. the locations surround) the given location (which is itself presumed to be
 * passible), as long as the rectangle of the given size that is centered on at least one
 * of those locations is passible according to the given movement-type, ignoring the given
 * moving-entity and to-entity. The four locations are tried in distance order, closest to
 * furthest, from either the to-entity (if one is given) or the given location, although
 * the 2nd and 3rd are ordered correctly only half the time.
 *
 * Note: The upper-left location is returned, rather than the center, because
 * every other such location corresponds to the center of a whole area-tile, and
 * we need to test map-tile-centers for passibility when determining such movements
 * as a bat (which cannot work a door) trying to pass through a single open door.
 */
fun Area.getNearbyPassibleQuarterTileLocation(
    location: PixelPoint, size: PixelDimensions, movementType: MovementType,
    forEntity: Entity?, toEntity: Entity?
): PixelPoint? {
    // if the given location is already a quarter-tile upper-left location,
    // presume it is passible and return it
    val quarterTileWidth = tileWidth / 2
    val quarterTileHeight = tileHeight / 2
    val xMod = location.x % quarterTileWidth
    val yMod = location.y % quarterTileHeight
    if (xMod == 0 && yMod == 0) return pixelPointPool.supply().set(location)

    // determine the upper-left corner location of the quarter-tile that covers the
    // given location
    val coveringQuarterTileX = location.x - xMod
    val coveringQuarterTileY = location.y - yMod

    // for each of the four upper-left locations of quarter-tiles that immediately
    // surround the given location, starting with closest and ending with the furthest
    // from either the given to-entity or the given location (although, the middle
    // two might have only a 50% chance of being in the right
    // distance-order just between themselves, which shouldn't be of much importance)
    val test = pixelPointPool.supply()
    val leftXCloser = if (toEntity != null) toEntity.location.x < location.x
    else location.x % quarterTileWidth < quarterTileWidth / 2
    val topYCloser = if (toEntity != null) toEntity.location.y < location.y
    else location.y % quarterTileHeight < quarterTileHeight / 2
    val closerX = coveringQuarterTileX + if (leftXCloser) 0 else quarterTileWidth
    val closerY = coveringQuarterTileY + if (topYCloser) 0 else quarterTileHeight
    val furtherX = coveringQuarterTileX + if (!leftXCloser) 0 else quarterTileWidth
    val furtherY = coveringQuarterTileY + if (!topYCloser) 0 else quarterTileHeight
    for (i in 0..4) {
        // determine the coordinates of this upper-left location
        when (i) {
            0 -> test.set(closerX, closerY)
            1 -> test.set(closerX, furtherY)
            2 -> test.set(furtherX, closerY)
            3 -> test.set(furtherX, furtherY)
        }

        // if the rectangle centered at the upper-left corner of this
        // quarter-tile is passible
        if (isRectanglePassible(test, size, movementType, true, forEntity, toEntity,
                false).passible) {
            // if the line between the given location and this quarter-tile upper-left corner
            // is also passible (otherwise, the result will likely do the caller no good)
            if (isLinePassible(location, test, movementType, size, forEntity,
                    toEntity, false).passible) {
                // return this upper-left quarter-tile corner
                return test
            }
        }
    }

    pixelPointPool.reclaim(test)
    return null
}

/**
 * Returns a pixel-location found near the one given that represents the center
 * of a pixel-rectangle (of the given size) which is all passible.  The key thing about
 * this method is that it starts at the given location and searches progressively outward
 * to the given search range until it finds a location.  Therefore, this is the routine
 * to call (with a large search range) when it's absolutely necessary to find a passible
 * location (like, when placing the player upon arrival on a map).
 */
@JvmOverloads
fun Area.getNearbyPassibleRectangleLocation(
    location: PixelPoint, size: PixelDimensions, movementType: MovementType,
    searchRange: PixelDistance = PixelDistance.maxValue,
    checkForBeing: Boolean = true, forEntity: Entity? = null,
    forEntity2: Entity? = null
): PixelPoint? {
    // for each box of pixel locations centered around the given location, with
    // that box expanding by one pixel on each side for each iteration, until
    // a suitable location is found
    val center = MutablePixelPoint()
    val testRect = MutablePixelRectangle()
    testRect.setSize(size)
    fun check(dx: Int, dy: Int): Boolean {
        center.set(location.x + dx, location.y + dy)
        testRect.setLocation(center.x - size.width / 2, center.y - size.height / 2)
        return containsPixelRectangle(testRect) && isRectanglePassible(center,
            size, movementType, checkForBeing, forEntity, forEntity2, false).passible
    }
    for (range in 0..searchRange.distance) {
        // check the locations along the box's sides for suitability, from the
        // center of each side out to the corners
        for (i in 0..range) {
            when {
                check(-i, -range) -> return center
                check(i, -range) -> return center
                check(-i, range) -> return center
                check(i, range) -> return center
                check(-range, -i) -> return center
                check(-range, i) -> return center
                check(range, -i) -> return center
                check(range, i) -> return center
            }
        }
    }

    return null
}

