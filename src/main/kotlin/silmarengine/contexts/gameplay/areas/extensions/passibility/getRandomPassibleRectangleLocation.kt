package silmarengine.contexts.gameplay.areas.extensions.passibility

import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.containsPixelRectangle
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt

/**
 * The size to pass to the getRandomPassibleRectangle... methods below when only a
 * passible pixel is sought.
 */
val onePixelRectangle = PixelDimensions(1, 1)

/**
 * Returns the center location (if one is found) of a passible pixel-rectangle
 * (of the given size) within this area.
 */
fun Area.getRandomPassibleRectangleLocation(
    rectSize: PixelDimensions
): PixelPoint? {
    // keep doing this (but avoid an endless loop)
    val test = MutablePixelPoint()
    var result: PixelPoint? = null
    for (i in 0..100000) {
        // choose a point at random within this area
        test.set(randomInt(0, size.width * tileWidth - 1 - rectSize.width),
            randomInt(0, size.height * tileHeight - 1 - rectSize.height))

        // if the rectangle, defined by the location determined above along with the given
        // size, is passible
        if (isRectanglePassible(test, rectSize, MovementType.WALKING, true, null, null,
                false).passible) {
            // return the center location of the rectangle just tested
            result = test
            break
        }
    }

    return result
}

/**
 * Returns the location (if one is found within the given number of tries) of the center
 * of a passible pixel-rectangle (of the given size) within the given range from the
 * given location.
 *
 * @param movementType      See isRectanglePassible
 * @param checkForBeing     See isRectanglePassible
 */
fun Area.getRandomPassibleRectangleLocationInRange(
    location: PixelPoint, rectSize: PixelDimensions, range: PixelDistance,
    numTries: Int, movementType: MovementType, checkForBeing: Boolean
): PixelPoint? {
    // try this the given number of times
    val center = MutablePixelPoint()
    val upLeft = MutablePixelPoint()
    val testRect = MutablePixelRectangle()
    testRect.setSize(rectSize)
    for (i in 0 until numTries) {
        // choose a point at random to be the test-rectangle's center
        // from within the given range from the given location
        val distance = range.distance
        center.set(location.x + randomInt(-distance, distance),
            location.y + randomInt(-distance, distance))

        // if the rectangle centered on the above location is not entirely within
        // this area's boundaries, don't consider it
        upLeft.set(center.x - rectSize.width / 2, center.y - rectSize.height / 2)
        testRect.setLocation(upLeft)
        if (!containsPixelRectangle(testRect)) continue

        // if the pixel at the rectangle center is passible
        if (isRectanglePassible(center, onePixelRectangle, movementType, checkForBeing,
                null, null, false).passible) {
            // if the whole rectangle isn't passible
            return if (!isRectanglePassible(center, rectSize, movementType, checkForBeing,
                    null, null, false).passible) {
                // if we can find a nearby passible quarter-tile location for the
                // rectangle, then return that, otherwise, move on to trying another location
                getNearbyPassibleQuarterTileLocation(center, rectSize, movementType, null,
                    null) ?: continue
            }
            else center

        }
    }

    return null
}

/**
 * See getRandomPassibleLocationUsingLOS
 */
fun Area.getRandomPassibleRectangleLocationInLOSAndRange(
    location: PixelPoint, rectSize: PixelDimensions, range: PixelDistance,
    numTries: Int, movementType: MovementType, checkForBeing: Boolean
): PixelPoint? {
    return getRandomPassibleRectangleLocationUsingLOSAndRange(location, location,
        rectSize, true, range, numTries, movementType, checkForBeing)
}

/**
 * Returns the center location (if one is found within the given number of tries)
 * of a passible pixel-rectangle in/not-in LOS of the given location.
 *
 * @param rangeFrom   The pixel-location around which to search.
 * @param losFrom     The location from which the result must/mustn't be in LOS.
 * @param inLOS       Whether the result must be in direct LOS, or must not be in LOS
 * @param range       See getRandomPassibleLocationInRange
 * @param checkForBeing     See getRandomPassibleLocationInRange
 */
fun Area.getRandomPassibleRectangleLocationUsingLOSAndRange(
    rangeFrom: PixelPoint, losFrom: PixelPoint, rectSize: PixelDimensions,
    inLOS: Boolean, range: PixelDistance, numTries: Int, movementType: MovementType,
    checkForBeing: Boolean
): PixelPoint? {
    // do this the given number of times
    for (i in 0 until numTries) {
        // find a passible rectangle randomly within our max range of the given location
        val test = getRandomPassibleRectangleLocationInRange(rangeFrom, rectSize, range,
            numTries, movementType, checkForBeing) ?: break

        // the above call does many repetitions to find a location; so if it doesn't
        // return any location at all, none likely exists, so don't go through more iterations
        // here, otherwise slowdown will occur

        // if the center-location of the passible rectangle is also visible/not-visible
        // (according to inLOS) from the given location
        if (canSeeExt(losFrom, test).canSee == inLOS) {
            return test
        }
    }

    return null
}

