package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Returns the first terrain found within this area that is
 * of the given type.
 */
fun Area.getTerrainOfType(type: TerrainType): Terrain? =
    terrains.firstOrNull { it.isOfType(type) }

/**
 * Returns whether there is a terrain of the given type whose center is
 * within the given rectangular pixel-region of this area.
 */
fun Area.isTerrainOfTypeInRectangle(
    type: TerrainType, location: PixelPoint, size: PixelDimensions
): Boolean =
    terrains.getEntitiesInRectangle(location, size).any { it.isOfType(type) }

