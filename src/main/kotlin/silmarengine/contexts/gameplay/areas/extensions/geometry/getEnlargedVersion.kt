package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelRectangle

/**
 * Returns a new created rectangle whose bounds are determined by scaling those of the given
 * rectangle by the given factor, keeping the rectangle centers the same.
 */
fun getEnlargedVersion(r: PixelRectangle, factor: Float): PixelRectangle {
    val newWidth = (r.width * factor).toInt()
    val newHeight = (r.height * factor).toInt()
    return PixelRectangle(PixelPoint(r.x + r.width / 2 - newWidth / 2,
        r.y + r.height / 2 - newHeight / 2), PixelDimensions(newWidth, newHeight))
}