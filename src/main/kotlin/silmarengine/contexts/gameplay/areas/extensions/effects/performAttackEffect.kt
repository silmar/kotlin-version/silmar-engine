package silmarengine.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineLocations
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread

/**
 * Performs an attack effect between the given pixel-locations using the
 * given attack-type.
 */
fun Area.performAttackEffect(
    from: PixelPoint, to: PixelPoint, attackType: AttackType
) {
    if (from == to) return

    // determine the locations on the line between the given two locations
    val locations =
        getLineLocations(from, to, tileWidth / 4, null, 0)

    // issue the given attack type's accompanying sound
    onSoundIssued(attackType.sound, from)

    // add a shot visual effect at the from location, that may or may not emit light,
    // depending on the dictate of the given attack type
    val effect =
        if (attackType.lightTileRadius > 0) LightSourceVisualEffect(
            attackType.imageName,
            PixelDistance(TileDistance(attackType.lightTileRadius)))
        else VisualEffect1(attackType.imageName)
    addEntity(effect, from)

    // for each location in the list determined above
    locations.forEach { location ->
        // move the shot effect to this location
        val startTime = System.currentTimeMillis()
        moveEntity(effect, location)

        // time how long it took to display this step of the shot's movement
        val timeTaken = System.currentTimeMillis() - startTime

        // if this step of the shot's movement didn't take at least a minimum amount of time
        val minimumMovementStepTime = attackType.minAnimationDelay
        if (timeTaken < minimumMovementStepTime) {
            // sleep for a bit, so the movement won't occur too rapidly on speedy machines
            sleepThread(minimumMovementStepTime - timeTaken)
        }
    }

    // remove the visual effect from the map
    removeEntity(effect)
}
