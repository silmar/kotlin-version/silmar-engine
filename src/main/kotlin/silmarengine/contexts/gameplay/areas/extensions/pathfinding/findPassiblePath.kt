package silmarengine.contexts.gameplay.areas.extensions.pathfinding

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.getSquaredDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.pathfinding.domain.model.PFArea
import silmarengine.contexts.pathfinding.domain.model.functions.FindPassiblePathResult
import silmarengine.contexts.pathfinding.domain.model.functions.findPassiblePath

fun Area.findPassiblePath(
    from_: PixelPoint?, to_: PixelPoint, movingEntity: Entity, goalEntity: Entity?,
    maxLength: TileDistance, movementType: MovementType, shouldFindBestPath: Boolean,
    checkForHarmfulTerrains: Boolean
): FindPassiblePathResult =
    (this as PFArea).findPassiblePath(from_, to_, movingEntity, goalEntity, maxLength,
        movementType.toPFMovementType(), shouldFindBestPath, checkForHarmfulTerrains,
        { from, to, atMost ->
            isDistanceAtMost(from as PixelPoint, to as PixelPoint,
                atMost as PixelDistance)
        }, { from, to, use ->
            getSquaredDistance(from as PixelPoint, to as PixelPoint, use as PixelDistance)
        })