package silmarengine.contexts.gameplay.areas.extensions.generation

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.monsters.types.monsterTypes
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.talkerBeingTypes
import silmarengine.contexts.gameplay.entities.items.ItemEntity
import silmarengine.contexts.gameplay.entities.items.extensions.checkToPutSelfInChest
import silmarengine.contexts.gameplay.entities.items.types.*
import silmarengine.contexts.gameplay.entities.items.weapons.types.weaponTypes
import silmarengine.contexts.gameplay.entities.terrains.extensions.checkForBeingTrapped
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.entities.terrains.types.nonTrapTypes
import silmarengine.contexts.gameplay.entities.terrains.types.trapTypes
import silmarengine.contexts.gameplay.entities.types.EntityType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.generation.Stockable
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.items.types.armorTypes
import silmarengine.contexts.gameplay.entities.items.types.shieldTypes
import silmarengine.util.list.concat
import silmarengine.util.math.randomInt
import java.util.*
import kotlin.math.roundToInt

/**
 * Stocks the given area with all the different kinds of entities.
 */
fun Area.stock() {
    // determine how big the area is relative to the average area size (the size
    // factor is squared because we are considering a ratio of areas)
    val averageGeneratedAreaWidth = 50
    var areaSizeFactor = size.width.toFloat() / averageGeneratedAreaWidth
    areaSizeFactor *= areaSizeFactor

    // determine from the above size factor how many items total should be stocked
    val averageNumItems = 20
    val numItems = averageNumItems * areaSizeFactor

    // stock the gold
    fun portion(percent: Float) = (numItems * percent).roundToInt()
    stockTypes(listOf(GroupableItemTypes.gold.entityType), portion(.20f))

    // stock the weapons
    stockTypes(weaponTypes.map { it.itemType.entityType }, portion(.20f))

    // stock the ammos
    stockTypes(ammoTypes.map { it.entityType }, portion(.05f))

    // stock the treasures
    stockTypes(treasureTypes.map { it.entityType }, portion(.20f))

    // stock the armors
    stockTypes(armorTypes.map { it.entityType }, portion(.10f))

    // stock the shields
    stockTypes(shieldTypes.map { it.entityType }, portion(.10f))

    // stock the other items (which are mainly the misc. magic items)
    stockTypes(nonGroupableItemTypes.map { it.entityType }, portion(.15f))

    // stock the monsters
    val averageNumMonsters = 30
    stockTypes(monsterTypes, (averageNumMonsters * areaSizeFactor).roundToInt())

    // stock the terrains and talker-beings
    val averageNumTerrains = 2
    stockTypes(concat(nonTrapTypes, talkerBeingTypes),
        (averageNumTerrains * areaSizeFactor).roundToInt())

    // stock the empty chests; the quantity takes into account that on average, only
    // half of items in the area are in a chest, and we want only half as many empty
    // chests as occupied ones
    val chests = stockTypes(listOf(TerrainTypes.chest), (numItems / 2 / 2).roundToInt())

    // check each empty chest stocked above for being trapped
    chests.forEach { (it as Chest).checkForBeingTrapped() }

    // stock the traps
    val averageNumTraps = 8
    stockTypes(trapTypes, (averageNumTraps * areaSizeFactor).toInt())

    stockGameSpecificEntities(this, areaSizeFactor)
}

@ExtensionPoint
var stockGameSpecificEntities: (area: Area, areaSizeFactor: Float) -> Unit = { _, _ -> }

/**
 * Returns a list of those stockables from the given list that are
 * valid for the current area being stocked.  Also, repeats the entries
 * in this list a number of times according to their frequency
 * of appearance values.
 *
 * @param stockables    The list of stockables from which to cull.
 */
private fun Area.cullValidStockables(stockables: List<Stockable>): List<Stockable> {
    // for each stockable in the given list of stockables
    val valids = ArrayList<Stockable>()
    stockables.forEach { stockable ->
        // if this stockable is valid for the given area
        if (stockable.isValidForArea(this)) {
            // for as many times as this stockable's frequency
            // value indicates
            for (j in 0 until stockable.frequency) {
                // add this stockable to our list of valid stockables
                valids.add(stockable)
            }
        }
    }

    return valids
}

/**
 * Stocks the given number of instances of stockable within the area,
 * choosing the type for each one randomly from the given list.
 *
 * @return                  A list of the entities that were stocked.
 */
fun Area.stockTypes(stockables: List<EntityType>, numberToStock: Int): List<Entity> {
    val stocked = ArrayList<Entity>()

    // if there are no valid stockables in the given list
    val valids = cullValidStockables(stockables)
    if (valids.isEmpty()) {
        return stocked
    }

    // for each stockable instance to place
    for (i in 0 until numberToStock) {
        // choose what type of stockable this is randomly
        // from our list of valid stockables
        val stockable = valids[randomInt(0, valids.size - 1)]

        // create an instance of the stockable type determined above
        val entity = stockable.createStockInstance()

        // keep doing this
        var location: PixelPoint?
        while (true) {
            // choose a random passible location within the area
            location = getRandomPassibleRectangleLocation(entity.size)
            if (location == null) continue

            // if this stockable says the location is ok
            if (stockable.isPlacementLocationValid(this, location)) {
                // we have chosen a location
                break
            }
        }

        if (location == null) continue

        // place the instance of the stockable type at the chosen location
        addEntity(entity, location)
        stocked.add(entity)

        onEntityStocked(entity)
    }

    return stocked
}

/**
 * Stocks the given list of locations within the given area with instances
 * of stockable types from the given list.
 */
fun Area.stockLocations(locations: List<PixelPoint>, stockables: List<Stockable>) {
    // if there are no valid stockables in the given list
    val valids = cullValidStockables(stockables)
    if (valids.isEmpty()) {
        return
    }

    // for each location in the given list
    locations.forEach { location ->
        // choose what type of stockable this is randomly
        // from our list of valid stockables
        val stockable = valids[randomInt(0, valids.size - 1)]

        // place an instance of the stockable at the chosen location
        val entity = stockable.createStockInstance()
        addEntity(entity, location)

        onEntityStocked(entity)
    }
}

/**
 * Informs this stocker that the given entity was just stocked into an area.
 */
private fun onEntityStocked(stocked: Entity) {
    // if the stocked entity is an item
    if (stocked is ItemEntity) {
        // have the item check to see if it should go in a chest
        stocked.checkToPutSelfInChest()
    }
}
