package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.geometry.vectors.getUnitVector
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.geom.Point2D

fun getUnitVector(
    from: PixelPoint, to: PixelPoint, use: Point2D.Float = Point2D.Float()
): Point2D.Float = getUnitVector(from.x, from.y, to.x, to.y, use)