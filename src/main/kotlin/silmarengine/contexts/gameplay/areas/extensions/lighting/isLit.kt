package silmarengine.contexts.gameplay.areas.extensions.lighting

import silmarengine.contexts.arealighting.domain.model.ALArea
import silmarengine.contexts.arealighting.domain.model.ALPixelPoint
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.arealighting.domain.model.functions.isLit
import silmarengine.gui.views.areaview.LitStatus
import silmarengine.gui.views.areaview.toLitStatus
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.getNearestTileCorner
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.alPixelDistancePool
import silmarengine.objectpools.alPixelPointPool
import silmarengine.objectpools.alTilePointPool

fun Area.isLit(
    location: PixelPoint, viewpoint: PixelPoint? = null,
    inLosLightSource: LightSource? = null
): LitStatus = (this as ALArea).isLit(location as ALPixelPoint, viewpoint, inLosLightSource,
    alPixelPointPool, alTilePointPool, alPixelDistancePool, { from, to, most ->
        isDistanceAtMost(from as PixelPoint, to as PixelPoint,
            most as PixelDistance)
    }, { from, to, use ->
        (to as PixelPoint).getNearestTileCorner(from as PixelPoint,
            use as MutablePixelPoint)
    }).toLitStatus()
