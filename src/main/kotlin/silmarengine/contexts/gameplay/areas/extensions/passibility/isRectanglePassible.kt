package silmarengine.contexts.gameplay.areas.extensions.passibility

import silmarengine.contexts.beingmovement.domain.model.BMIsRectanglePassibleResult
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityInRectangle
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstHarmfulTerrainInRectangle
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstImpassibleTerrainInRectangle
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileRectangle
import silmarengine.contexts.gameplay.areas.geometry.MutableTilePoint
import silmarengine.objectpools.pixelPointPool
import silmarengine.contexts.pathfinding.domain.model.PFIsRectanglePassibleResult
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import kotlin.math.max
import kotlin.math.min

/**
 * The return value of isRectanglePassible().
 */
class IsRectanglePassibleResult (
    /**
     * Whether the rectangle was passible.
     */
    override var passible: Boolean = false,

    /**
     * Whether there was something harmful (to a given entity) in the rectangle.
     */
    override var harmful: Boolean = false
) : PFIsRectanglePassibleResult, BMIsRectanglePassibleResult {

    /**
     * Clears this object's fields, so it may be reused.
     */
    fun init() {
        passible = false
        harmful = false
    }
}

/**
 * Returns whether the rectangular pixel-region of the given center-location and size
 * within this area is entirely passible (although in truth, only a small fraction of
 * the pixels are tested for passibility) for the given movement-type, taking into
 * account beings (if specified, and excluding the given for-entities) and
 * harmful terrains (if specified, in which case forEntity must be a non-null monster).
 *
 * @return  See IsRectanglePassibleResult.  The returned object is reused with each call
 * and therefore should not be cached.
 */
fun Area.isRectanglePassible(
    location: PixelPoint, size: PixelDimensions, movementType: MovementType,
    checkForBeing: Boolean, forEntity: Entity?, forEntity2: Entity?,
    checkForHarmfulTerrains: Boolean
): IsRectanglePassibleResult {
    // clear the result object we are going to reuse
    val result = IsRectanglePassibleResult()
    result.init()

    // for each tile location in the given rectangle
    val test = pixelPointPool.supply()
    val upperLeftX = location.x - size.width / 2
    val upperLeftY = location.y - size.height / 2
    var i = 0
    while (i < size.width) {
        var j = 0
        while (j < size.height) {
            // if the tile at this location isn't passible
            test.set(upperLeftX + i, upperLeftY + j)
            val testTile = getTile(test)
            if (!testTile.isPassible(movementType)) {
                // the rectangle is not passible
                pixelPointPool.reclaim(test)
                return result
            }
            j += max(1, min(size.height - j - 1, tileHeight))
        }
        i += max(1, min(size.width - i - 1, tileWidth))
    }
    pixelPointPool.reclaim(test)

    if (checkForBeing) {
        // check for beings within the rectangle
        if (getFirstEntityInRectangle(location, size, beings, null, forEntity,
                forEntity2) != null) {
            return result
        }
    }

    if (checkForHarmfulTerrains) {
        // check for harmful terrains within the rectangle
        if (getFirstHarmfulTerrainInRectangle(location, size,
                forEntity as Monster) != null) {
            result.harmful = true
            return result
        }
    }

    // check for impassible terrains within the rectangle
    if (getFirstImpassibleTerrainInRectangle(location, size, movementType) != null) {
        return result
    }

    // if we make it here, the rectangle is passible (and not harmful, if that matters)
    result.passible = true
    return result
}

/**
 * Returns whether the tiles at all tile-locations within the given
 * rectangle are impassible.
 *
 * @param rectangle     The tile-rectangle to test.
 */
fun Area.isRectanglePlusBoundaryAllImpassible(rectangle: TileRectangle): Boolean {
    // for each tile location within the given rectangle, as well as
    // on the 1-tile boundary around the rectangle
    val location = MutableTilePoint()
    for (i in rectangle.x - 1..rectangle.x + rectangle.width) {
        for (j in rectangle.y - 1..rectangle.y + rectangle.height) {
            // if the tile at this location is passible
            location.set(i, j)
            val tile = getTile(location)
            if (tile.isPassible) return false
        }
    }

    return true
}
