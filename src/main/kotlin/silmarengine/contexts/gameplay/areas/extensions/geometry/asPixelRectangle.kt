package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.TileRectangle
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelRectangle
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Returns a new copy of the given rectangle with its values adjusted from
 * tile- to pixel-scale.  The new pixel-values enclose the full area of the outmost tiles
 * of the tile-rectangle.
 */
fun TileRectangle.asPixelRectangle(): MutablePixelRectangle {
    val location = TilePoint(x, y).getUpperLeftPixelLocation()
    return MutablePixelRectangle().set(location.x, location.y, width * tileWidth,
        height * tileHeight)
}