package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import kotlin.math.roundToInt

/**
 * Computes the square of the distance between the given two locations, storing the result in the given
 * use-object.  This is a much quicker computation than determined the distance (unsquared), since no square
 * root is involved.
 */
fun getSquaredDistance(
    from: PixelPoint, to: PixelPoint, use: PixelDistance
): PixelDistance {
    val a = (to.y - from.y).toFloat()
    val b = (to.x - from.x).toFloat()
    use.distance = (a * a + b * b).roundToInt()
    return use
}