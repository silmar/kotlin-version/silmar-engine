package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.Area

/**
 * Returns all monsters in this area that are within the given range, and for which
 * a passible path of at most the given range (in length) is found.
 */
fun Area.getMonstersInPassibleRange(
    location: PixelPoint, range: PixelDistance
): List<Monster> {
    // get all monsters that are within the given range
    val monsters = monsters.getEntitiesInRange(location, range)

    // if this area has no adjacency graph, assume that a passible path exists to all
    // the monsters found above
    if (adjacencyGraph == null) return monsters

    // return those monsters in range to which this area's adjacency graph says there is
    // a passible path
    return monsters.filter { monster ->
        adjacencyGraph!!.getPathExists(location, monster.location, range, true)
            .pathExists
    }
}
