package silmarengine.contexts.gameplay.areas.extensions.generation

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.dungeongeneration.domain.model.DGTilePoint
import silmarengine.contexts.dungeongeneration.domain.model.DGTiles
import silmarengine.contexts.dungeongeneration.domain.model.Dungeon
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles
import java.awt.Dimension
import java.awt.Point

fun Area.subsumeDungeon(dungeon: Dungeon) {
    onSizeDetermined(Dimension(dungeon.size.width, dungeon.size.height))
    for (i in 0 until size.width) {
        for (j in 0 until size.height) {
            val tile = convertDungeonTile(dungeon, i, j)
            setTile(TilePoint(i, j), tile)
        }
    }
}

private fun Area.convertDungeonTile(dungeon: Dungeon, x: Int, y: Int): Tile =
    when (val tile = dungeon.getTile(DGTilePoint(x, y))) {
        DGTiles.floor -> Tiles.floor
        DGTiles.wall -> Tiles.wall
        DGTiles.roomFloor -> Tiles.roomFloor
        DGTiles.roomWall -> Tiles.roomWall
        DGTiles.door -> Tiles.door
        DGTiles.secretDoor -> Tiles.secretDoor
        DGTiles.roomSecretDoor -> Tiles.roomSecretDoor
        DGTiles.entrance, DGTiles.exit -> {
            val exit = if (tile == DGTiles.entrance) createGameSpecificAreaEntrance()
            else createGameSpecificAreaExit()
            addEntity(exit, TilePoint(x, y))
            getNearbyFloorTile(dungeon, x, y)
        }
        else -> throw Exception("Unrecognized dungeon tile")
    }

private fun getNearbyFloorTile(dungeon: Dungeon, x: Int, y: Int): Tile {
    val directions = arrayOf(Point(0, -1), Point(-1, 0), Point(1, 0), Point(0, 1))
    directions.forEach {
        when (dungeon.getTile(DGTilePoint(x + it.x, y + it.y))) {
            DGTiles.floor -> return Tiles.floor
            DGTiles.roomFloor -> return Tiles.roomFloor
        }
    }
    throw Exception("Could not find nearby floor tile")
}

@ExtensionPoint
var createGameSpecificAreaEntrance: () -> Terrain = { throw NotImplementedError() }

@ExtensionPoint
var createGameSpecificAreaExit: () -> Terrain = { throw NotImplementedError() }