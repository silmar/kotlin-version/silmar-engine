package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.geometry.points.getDirectionString
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

fun getDirectionString(from: PixelPoint, to: PixelPoint): String =
    getDirectionString(from.x, from.y, to.x, to.y)