package silmarengine.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sound
import silmarengine.sounds.Sounds.powerCircle
import silmarengine.util.sleepThread

fun performPowerDischargeEffect(entity: Entity) {
    entity.area.performPowerDischargeEffect(entity.location)
}

/**
 * Does a discharge effect at the given pixel-location, with the given delay (in ms)
 * between each step of the discharge.
 *
 * @param frameDelay    Note that a zero value for this parameter indicates to use a default delay.
 * @param sound         The sound emitted by the discharge.
 */
fun Area.performPowerDischargeEffect(
    location: PixelPoint, frameDelay: Int = 0, sound: Sound = powerCircle
) {
    // issue an accompanying sound
    onSoundIssued(sound, location)

    // if the player can see this discharge
    if (player?.isInLOSOf(location) == true) {
        // use a default value for the frame-delay, if requested
        val finalFrameDelay = if (frameDelay != 0) frameDelay else 150

        // add a discharge visual effect at the given location
        val effect = LightSourceVisualEffect(null, PixelDistance(TileDistance(8)))
        addEntity(effect, location)

        // for each step in the discharge
        for (i in 1..5) {
            // perform this step
            effect.imageName = "powerUsed$i"

            // pause a bit for the step to be seen
            sleepThread(finalFrameDelay.toLong())
        }

        // remove this discharge's visual effect from the area
        removeEntity(effect)
    }
}
