package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

fun TilePoint.getUpperLeftPixelLocation(): PixelPoint {
    val result = MutablePixelPoint()
    getUpperLeftPixelLocation(result)
    return result
}

/**
 * Returns the result of converting the given tile-location to a pixel-location.
 * The pixel-location corresponding to the upper-left of the tile at the given tile-location
 * is returned.
 *
 * @param use               The object in which to place the results.
 */
fun TilePoint.getUpperLeftPixelLocation(
    use: MutablePixelPoint
) {
    asPixelLocation(use)
    use.translate(-tileWidth / 2, -tileHeight / 2)
}