package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.items.ItemEntity
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Returns the item-containing entity nearest to the given location that is
 * at least of the given value.  Item-containing chests *are* considered.
 */
fun Area.getNearestItem(location: PixelPoint, minValue: Int): Entity? {
    // start out with the items in this area
    val itemEntitiesAndChests = ArrayList<Entity>(items)

    // add all the items in chests
    val chestItems = terrains.filter { it is Chest && it.item != null }
    itemEntitiesAndChests.addAll(chestItems)

    // filter all the items of at least the given value
    val candidates = itemEntitiesAndChests.filter {
        val item = if (it is ItemEntity) it.item else (it as Chest).item!!
        item.getValue(ItemValueContext.PLACEMENT) >= minValue
    }

    // return the nearest item
    return if (candidates.isNotEmpty()) {
        var minDistance = getDistance(location, candidates[0].location)
        return candidates.reduce { nearest, next ->
            val distance = getDistance(location, next.location)
            if (distance.distance < minDistance.distance) {
                minDistance = distance
                return next
            }
            return nearest
        }
    }
    else null
}
