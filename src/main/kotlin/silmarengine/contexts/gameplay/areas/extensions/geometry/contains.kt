package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelRectangle
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.TileRectangle
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Returns whether this area contains the given tile location.
 */
fun Area.containsLocation(location: TilePoint): Boolean {
    return (location.x >= 0 && location.y >= 0 && location.x < size.width && location.y < size.height)
}

/**
 * Returns whether this area contains the given pixel location.
 */
fun Area.containsLocation(location: PixelPoint): Boolean {
    return (location.x >= 0 && location.y >= 0 && location.x < size.width * tileWidth && location.y < size.height * tileHeight)
}

/**
 * Returns whether this area (minus its outer one-tile boundary)
 * completely contains the given rectangular tile region.
 */
fun Area.containsTileRectangle(r: TileRectangle): Boolean {
    // note that this enforces a one-tile boundary around this area's edges
    return (r.x > 0 && r.y > 0 && r.x + r.width - 1 < size.width - 1 && r.y + r.height - 1 < size.height - 1)
}

/**
 * Returns whether this area completely contains the given rectangular pixel region.
 */
fun Area.containsPixelRectangle(r: PixelRectangle): Boolean {
    val x = r.location.x
    val y = r.location.y
    return (x > 0 && y > 0 && x + r.size.width - 1 < size.width * tileWidth - 1 && y + r.size.height - 1 < size.height * tileHeight - 1)
}

