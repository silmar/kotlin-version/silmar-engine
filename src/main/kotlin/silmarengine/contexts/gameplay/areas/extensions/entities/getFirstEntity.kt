package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.contains
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.pixelRectanglePool

/**
 * Returns the first (if any) entity at the given pixel-location
 * on this area.  Note that this method precludes the need to have
 * a separate isThereAnEntityAt() method - just test the return
 * value of this method against null to achieve the same result.
 * This eliminates the pattern of calling isThereAnEntityAt() and
 * then this method to find out what the entity is, where duplicate
 * work would be performed.
 *
 * @param   entities    The list of entities in which to search for the
 * first one that is at the given location.
 * @param   predicate   If specified, a criterion that an entity
 * at the location must match to be returned.
 * @param   ignore      If specified, an entity whose presence at the given
 * location will be ignored (usually, this is the entity
 * that is interested in the result of this call in
 * the first place).
 */
fun <E : Entity> getFirstEntityAt(
    location: PixelPoint, entities: List<E>,
    predicate: ((Entity) -> Boolean)? = null, ignore: E? = null
): E? = entities.firstOrNull { entity ->
    entity != ignore && (predicate == null || predicate(entity)) && entity.contains(
        location)
}

/**
 * Returns the first (if any) entity from the given list found in this area whose center is
 * within the rectangular pixel-region of the given center location and size.
 *
 * See getFirstEntityAt() for descriptions of similar parameters.
 */
fun <E : Entity> getFirstEntityInRectangle(
    location: PixelPoint, size: PixelDimensions, entities: List<E>,
    predicate: ((Entity) -> Boolean)? = null, ignore: E? = null, ignore2: E? = null
): E? {
    val rectangle = pixelRectanglePool.supply()
    rectangle.setLocation(location.x - size.width / 2, location.y - size.height / 2)
    rectangle.setSize(size)
    val result = entities.firstOrNull { entity ->
        entity != ignore && entity != ignore2 && (predicate == null || predicate(
            entity)) && rectangle.intersects(entity.impassibleBounds)
    }
    pixelRectanglePool.reclaim(rectangle)
    return result
}

/**
 * Returns the first (if any) impassible terrain found whose bounds intersect the given
 * rectangular pixel-region of this area.
 */
fun Area.getFirstImpassibleTerrainInRectangle(
    location: PixelPoint, size: PixelDimensions, movementType: MovementType
): Terrain? = getFirstEntityInRectangle(location, size, terrains,
    { entity -> !(entity as Terrain).isPassible(movementType) })

/**
 * Returns the first (if any) terrain found whose bounds intersect the given
 * rectangular pixel-region of this area and which is considered harmful to
 * the given to-monster.
 */
fun Area.getFirstHarmfulTerrainInRectangle(
    location: PixelPoint, size: PixelDimensions, toMonster: Monster
): Terrain? = getFirstEntityInRectangle(location, size, terrains,
    { entity -> (entity as Terrain).isHarmfulTo(toMonster) })

