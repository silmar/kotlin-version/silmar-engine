package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelRectangle
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelRectangle

fun union(r: PixelRectangle, with: PixelRectangle): PixelRectangle {
    var tx2 = r.width.toLong()
    var ty2 = r.height.toLong()
    if (tx2 or ty2 < 0) {
        // This rectangle has negative dimensions...
        // If r has non-negative dimensions then it is the answer.
        // If r is non-existent (has a negative dimension), then both
        // are non-existent and we can return any non-existent rectangle
        // as an answer.  Thus, returning r meets that criterion.
        // Either way, r is our answer.
        return with
    }
    var rx2 = with.width.toLong()
    var ry2 = with.height.toLong()
    if (rx2 or ry2 < 0) {
        return r
    }
    var tx1 = r.x
    var ty1 = r.y
    tx2 += tx1.toLong()
    ty2 += ty1.toLong()
    val rx1 = with.x
    val ry1 = with.y
    rx2 += rx1.toLong()
    ry2 += ry1.toLong()
    if (tx1 > rx1) tx1 = rx1
    if (ty1 > ry1) ty1 = ry1
    if (tx2 < rx2) tx2 = rx2
    if (ty2 < ry2) ty2 = ry2
    tx2 -= tx1.toLong()
    ty2 -= ty1.toLong()
    // tx2,ty2 will never underflow since both original rectangles
    // were already proven to be non-empty
    // they might overflow, though...
    if (tx2 > Integer.MAX_VALUE) tx2 = Integer.MAX_VALUE.toLong()
    if (ty2 > Integer.MAX_VALUE) ty2 = Integer.MAX_VALUE.toLong()
    return MutablePixelRectangle().set(tx1, ty1, tx2.toInt(), ty2.toInt())
}
