package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.extensions.proximity.contains
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Returns a list of the entities from the given list that are at
 * given location.
 */
fun <E : Entity> getEntitiesAt(
    location: PixelPoint, entities: List<E>
): List<E> = entities.filter { it.contains(location) }

/**
 * Returns a list of the entities from the given list that are in direct line-of-sight of the
 * given location.
 */
fun <E : Entity> Area.getEntitiesInLOS(
    location: PixelPoint, entities: List<E>
): List<E> = entities.filter { entity ->
    canSeeExt(location, entity.location).canSee
}

/**
 * See getEntitiesInLOS() and getEntitiesInRange().
 */
fun <E : Entity> Area.getEntitiesInLOSAndRange(
    location: PixelPoint, range: PixelDistance, entities: List<E>
): List<E> {
    val inRange = entities.getEntitiesInRange(location, range)
    return if (inRange.isEmpty()) inRange else getEntitiesInLOS(location, inRange)
}
