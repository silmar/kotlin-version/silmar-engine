package silmarengine.contexts.gameplay.areas.extensions.tiles

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.extensions.associatedDoor
import silmarengine.contexts.gameplay.tiles.extensions.isOpenDoor

/**
 * Replaces any open-door tile at the given pixel-location with
 * the corresponding closed-door tile.
 *
 * @param   location    The pixel-location at which to close an open door
 * (if one is present).
 */
fun Area.closeDoor(location: PixelPoint, issueSoundToMonsters: Boolean) {
    // if there is an open door tile at the given location, replace it with the
    // associated closed door tile
    val tile = getTile(location)
    if (tile.isOpenDoor) {
        val closedDoor = tile.associatedDoor
        closedDoor?.let {
            setTile(location, closedDoor)
            onSoundIssued(closeDoorSound, location, !issueSoundToMonsters)
        }
    }
}

