package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Returns the result of converting the given tile-location to a pixel-location.
 * The pixel-location corresponding to the center of the tile at the given tile-location
 * is returned.
 *
 * @param use               The object in which to place the results.
 */
fun TilePoint.asPixelLocation(use: MutablePixelPoint = MutablePixelPoint()) =
    use.set(x * tileWidth + tileWidth / 2, y * tileHeight + tileHeight / 2)
