package silmarengine.contexts.gameplay.areas.extensions.visibility

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBArea
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBBeing
import silmarengine.contexts.lineofsightblocking.domain.model.functions.CanSeeResult
import silmarengine.contexts.lineofsightblocking.domain.model.functions.canSee
import silmarengine.contexts.lineofsightblocking.domain.model.tileWidth
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.adjustToTileCenter
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.floatPointPool
import silmarengine.objectpools.losbPixelPointPool

fun Area.canSeeExt(
    from: PixelPoint, to: PixelPoint, pixelsPerStep: Int = tileWidth,
    adjustFromToTileCenter: Boolean = false, beingsBlockLOS: Boolean = false,
    fromBeing: Being? = null, toBeing: Being? = null
): CanSeeResult = (this as LOSBArea).canSee(from, to, losbPixelPointPool, floatPointPool,
    { location, use ->
        adjustToTileCenter(location as PixelPoint, use as MutablePixelPoint)
    }, { from_, to_, use ->
        getUnitVector(from_ as PixelPoint, to_ as PixelPoint, use)
    }, { from_, to_, use ->
        getDistance(from_ as PixelPoint, to_ as PixelPoint, use as PixelDistance)
    }, pixelsPerStep, adjustFromToTileCenter, beingsBlockLOS, fromBeing as LOSBBeing?,
    toBeing as LOSBBeing?)
