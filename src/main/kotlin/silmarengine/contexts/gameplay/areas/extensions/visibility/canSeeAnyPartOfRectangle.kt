package silmarengine.contexts.gameplay.areas.extensions.visibility

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.pixelPointPool

/**
 * Returns whether any part of the pixel-rectangle whose center is the given to-location,
 * and is of the given size, can be seen from the given from-location.  In actuality, just
 * the four corners and the center of the rectangle are tested for visibility.
 */
fun Area.canSeeAnyPartOfRectangle(
    from: PixelPoint, to_: PixelPoint, rectangleSize: PixelDimensions
): Boolean {
    // make a copy of the given to-location, and use that instead of the original,
    // to avoid causing a side effect
    val to = pixelPointPool.supply()
    to.set(to_)

    // test whether the center of the rectangle can be seen
    if (canSeeExt(from, to).canSee) return true

    // test whether the upper-left corner of the rectangle can be seen
    val x = to.x
    val y = to.y
    val halfWidth = rectangleSize.width / 2
    val halfHeight = rectangleSize.height / 2
    to.set(x - halfWidth, y - halfHeight)
    fun canSee(to: PixelPoint): Boolean = canSeeExt(from, to).canSee

    val result = canSee(to) || canSee(to.set(x - halfWidth, y - halfHeight)) || canSee(
        to.set(x + halfWidth, y + halfHeight)) || canSee(
        to.set(x - halfWidth, y + halfHeight)) || canSee(
        to.set(x + halfWidth, y - halfHeight))
    pixelPointPool.reclaim(to)
    return result
}
