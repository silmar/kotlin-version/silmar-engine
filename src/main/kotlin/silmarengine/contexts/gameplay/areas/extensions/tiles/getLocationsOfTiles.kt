package silmarengine.contexts.gameplay.areas.extensions.tiles

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.asPixelLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.containsLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.getNearestTileCorner
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Returns a list of the pixel-locations of all tiles matching
 * one of the given types within the given range of the given
 * location.
 *
 * @param tiles         The tiles for which to search.
 * @param location      The location around which to search.
 * @param range         The maximum horizontal or vertical tile-range
 * (as the search area is a square) from the given location
 * in which to search.
 * @param inLOS         Whether the tiles returned must also be in direct LOS of the given
 * location.
 */
fun Area.getLocationsOfTilesInRange(
    tiles: List<Tile>, location: PixelPoint, range: TileDistance, inLOS: Boolean
): List<PixelPoint> {
    // for each location in the box around the given location
    // out to the given range
    val test = MutablePixelPoint()
    val found = ArrayList<PixelPoint>()
    val distance = range.distance
    for (i in -distance..distance) {
        for (j in -distance..distance) {
            test.set(location.x + i * tileWidth, location.y + j * tileHeight)

            // if this area doesn't contain this location
            if (!containsLocation(test)) continue

            // if it is specified that candidate tiles must be in LOS
            if (inLOS) {
                // if the nearest corner of the tile at this location isn't in LOS
                // (taking into account whether any LOS blockage is on the last step,
                // since the sought-after tile-types may themselves block LOS)
                test.getNearestTileCorner(location, test)
                val result = canSeeExt(location, test)
                if (!result.canSee && !result.failedOnLastStep) {
                    // skip this tile
                    continue
                }
            }

            // if this tile at this location is one of those given, add the
            // pixel-location of its tile-location to our result
            val tile = getTile(test)
            if (tiles.contains(tile)) {
                val tileLocation = test.asTileLocation()
                found.add(tileLocation.asPixelLocation())
            }
        }
    }

    return found
}

/**
 * See getLocationsOfTilesInRange
 */
fun Area.getLocationsOfTilesInLOSAndRange(
    tiles: List<Tile>, location: PixelPoint, range: TileDistance
): List<PixelPoint> {
    return getLocationsOfTilesInRange(tiles, location, range, true)
}

