package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.MutableTilePoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

fun PixelPoint.asTileLocation(): TilePoint {
    val result = MutableTilePoint()
    asTileLocation(result)
    return result
}

/**
 * Returns the result of converting the given pixel-location to a tile-location.
 *
 * @param use               The object in which to place the results.
 */
fun PixelPoint.asTileLocation(use: MutableTilePoint) {
    use.set(x / tileWidth, y / tileHeight)
}