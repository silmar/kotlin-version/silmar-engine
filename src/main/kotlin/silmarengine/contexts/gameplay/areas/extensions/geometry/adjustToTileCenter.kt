package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.tilePointPool

fun adjustToTileCenter(location: PixelPoint, use: MutablePixelPoint): PixelPoint {
    val tileFrom = tilePointPool.supply()
    location.asTileLocation(tileFrom)
    val result = tileFrom.asPixelLocation(use)
    tilePointPool.reclaim(tileFrom)
    return result
}
