package silmarengine.contexts.gameplay.areas.extensions.loading

import silmarengine.contexts.areafileloading.domain.model.AFLArea
import silmarengine.contexts.areafileloading.domain.model.AFLPixelPoint
import silmarengine.contexts.areafileloading.domain.model.EntityKind
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmarengine.contexts.gameplay.entities.beings.monsters.types.nameToMonsterTypeMap
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.createTalkerBeing
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.nameToTalkerBeingTypeMap
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.types.nameToTerrainTypeMap
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles
import java.awt.Dimension

fun Area.subsumeArea(area: AFLArea) {
    // size this area to match the area
    val (width, height) = area.size
    onSizeDetermined(Dimension(width, height))

    // copy over the tiles
    for (x in 0 until width) for (y in 0 until height) {
        val tile = Tile.idToTileMap[area.getTile(x, y).id]
        setTile(TilePoint(x, y), tile ?: Tiles.floor)
    }

    // copy over the entities
    area.immutableEntities.forEach {
        val entity = when (it.kind) {
            EntityKind.TERRAIN -> {
                val type = nameToTerrainTypeMap[it.typeName]
                if (type != null) createTerrain(type) else null
            }
            EntityKind.TALKER_BEING -> {
                val type = nameToTalkerBeingTypeMap[it.typeName]
                if (type != null) createTalkerBeing(type) else null
            }
            EntityKind.MONSTER -> {
                val type = nameToMonsterTypeMap[it.typeName]
                if (type != null) createMonster(type) else null
            }
        }
        if (entity != null) addEntity(entity, it.location.toPixelPoint(), false)
    }
}

private fun AFLPixelPoint.toPixelPoint() = PixelPoint(x, y)