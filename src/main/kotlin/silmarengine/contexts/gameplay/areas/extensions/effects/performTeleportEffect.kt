package silmarengine.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

/**
 * Performs a teleport effect at the given pixel-location.
 *
 * @param out               Whether the teleport is a teleport-out (vs. a teleport-in).
 * @param leaveFinalFrame   Whether to not remove the last frame of the
 * (presumably, teleport-out) effect, so that the
 * caller may remove it manually, after the actual moving of the
 * entity (for the teleport) has occurred, thus producing a
 * more proper-looking teleport-out effect.
 *
 * @return                  The last frame, if leaveFinalFrame specifies to
 *                          to leave it in the area.
 */
fun Area.performTeleportEffect(
    location: PixelPoint, out: Boolean, leaveFinalFrame: Boolean
): VisualEffect? {
    // issue an accompanying sound
    onSoundIssued(if (out) Sounds.teleportOut else Sounds.teleportIn, location)

    // add a teleport visual effect at the given location
    val effect = LightSourceVisualEffect(null,
        PixelDistance(TileDistance(6)))
    addEntity(effect, location)

    // for each step in the effect
    for (i in 1..5) {
        // perform this step
        effect.imageName = "teleport" + if (out) i else 6 - i

        // pause a bit for the step to be seen
        sleepThread(200)
    }

    // return the effect if we are to leave the final frame, so that the caller may
    // manually remove it later
    if (leaveFinalFrame) return effect

    // remove this discharge's visual effect from the area
    removeEntity(effect)

    return null
}
