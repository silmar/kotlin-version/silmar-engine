package silmarengine.contexts.gameplay.areas.extensions.entities

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.extensions.associatedOpenDoor
import silmarengine.contexts.gameplay.tiles.extensions.isDoor
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

fun Area.moveEntityExt(entity: Entity, location: PixelPoint) {
    // determine whether the movement will place the center of the entity
    // over a different tile than the current one
    val oldLocation = entity.location
    val tileChanged =
        location.x / tileWidth != oldLocation.x / tileWidth || location.y / tileHeight != oldLocation.y / tileHeight

    // move the entity
    entity.moveTo(location)

    // report this movement
    reportToListeners { it.onEntityPresenceChange(entity) }

    // if the entity is a being which is moving to a different tile
    if (entity is Being && tileChanged) {
        // if the being is moving onto a door, and it's movement type says it can work doors
        val tile = getTile(location)
        if (tile.isDoor && entity.movementType.allowsWorkingDoors && tile.associatedOpenDoor != null) {
            val openDoor = tile.associatedOpenDoor
            openDoor?.let {
                // open that door
                setTile(location, openDoor)
                onSoundIssued(openDoorSound, location, entity is Monster)
            }
        }
    }
}