package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint

/**
 * Returns a point down the line that is the reflection off the given
 * collision pixel-location of a line with the given start and last-reached
 * pixel-locations.
 */
fun getLineBearingAfterCollision(
    area: Area, start_: PixelPoint, collision: PixelPoint, lastReached: PixelPoint
): PixelPoint {
    // if the start and last-reached locations are the same
    val start = if (start_ == lastReached) {
        // make a new start location that is backed away from the collision along the
        // start-collision line, so that mirroring start coordinates
        // across the last-reached location can work
        PixelPoint(2 * start_.x - collision.x, 2 * start_.y - collision.y)
    }
    else start_

    // if the collision differs in y from the point last reached
    val bearing = MutablePixelPoint(start)
    val spot = MutablePixelPoint()
    if (collision.y != lastReached.y) {
        // if the tile at the collision's x and lastReached's y
        // doesn't block LOS
        spot.set(collision.x, lastReached.y)
        if (!area.getTile(spot).blocksLOS) {
            // mirror the x ordinate of the start
            bearing.set(lastReached.x - (start.x - lastReached.x), bearing.y)
        }
    }

    // if the collision-location differs in x from the point last reached
    if (collision.x != lastReached.x) {
        // if the tile at lastReached's x and collision's y
        // doesn't block LOS
        spot.set(lastReached.x, collision.y)
        if (!area.getTile(spot).blocksLOS) {
            // mirror the y ordinate of the start
            bearing.set(bearing.x, lastReached.y - (start.y - lastReached.y))
        }
    }

    // if both x and y start ordinates were mirrored
    if (bearing.x != start.x && bearing.y != start.y) {
        // what we really mean to do in this case is to reflect the line
        // back onto itself
        bearing.set(start)
    }

    return bearing
}
