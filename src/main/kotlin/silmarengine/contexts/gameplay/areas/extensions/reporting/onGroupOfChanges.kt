package silmarengine.contexts.gameplay.areas.extensions.reporting

import silmarengine.contexts.gameplay.areas.Area

fun Area.onGroupOfChangesToOccur() {
    reportToListeners { it.onGroupOfChangesToOccur() }
}

fun Area.onGroupOfChangesOccurred() {
    reportToListeners { it.onGroupOfChangesOccurred() }
}
