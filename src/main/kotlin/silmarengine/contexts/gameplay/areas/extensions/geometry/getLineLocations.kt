package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.Point
import java.awt.geom.Point2D
import java.util.*
import kotlin.math.max

/**
 * Returns a list of locations from the given from- location to the given to-location
 * (inclusive only on the to- side, and the last location is not necessarily equal to the
 * to-location), where every two successive locations have the given distance between
 * them.
 *
 * @param maxDistance_       If non-zero, dictates the maximum distance the line may
 * travel from the from-location.
 * @param maxLocations      If non-zero, dictates the maximum number of locations
 * the line may travel from the from-location, for contexts
 * in which the line should proceed past the to-location.
 */
fun getLineLocations(
    from: PixelPoint, to: PixelPoint, spacing: Int,
    maxDistance_: PixelDistance?, maxLocations: Int
): List<PixelPoint> {
    // determine the vector to add at each step along the line between the given locations
    val direction = getUnitVector(from, to)
    direction.x *= spacing.toFloat()
    direction.y *= spacing.toFloat()

    // determine how many moves it will take to reach the given maximum distance,
    // possibly overriding this value with the given max-number-of-locations
    val maxDistance = maxDistance_ ?: getDistance(from, to)
    var numMoves = (maxDistance.distance / spacing).toFloat()
    if (maxLocations != 0) numMoves = max(numMoves, maxLocations.toFloat())

    // for each location along the line between the given from- and to- locations,
    // up to the given max-distance
    val current = Point2D.Float(from.x.toFloat(), from.y.toFloat())
    val rounded = Point(from.x, from.y)
    val locations = ArrayList<PixelPoint>()
    var i = 0
    while (i < numMoves) {
        // add the direction vector we determined above to our current location along the line
        current.x += direction.x
        current.y += direction.y

        // add the rounded form of this location to our list of line locations
        rounded.move(current.x.toInt(), current.y.toInt())
        locations.add(PixelPoint(rounded))
        i++
    }

    return locations
}
