package silmarengine.contexts.gameplay.areas.extensions.reporting

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onSoundHeard
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.sounds.Sound

/**
 * Informs this area that the given sound has issued forth from the given location.
 *
 * @param forPlayerOnly        Whether the given sound only needs to be heard by
 * players, and not other beings.  A monster's movement
 * sound would fall into this category.
 */
@JvmOverloads
fun Area.onSoundIssued(
    sound: Sound, location: PixelPoint, forPlayerOnly: Boolean = false
) {
    // find those beings (or just the player, if specified) that are within the
    // given range
    var beings = beings
    if (forPlayerOnly) beings = if (player != null) listOf(player!!) else return
    val range = sound.range
    beings = beings.getEntitiesInRange(location, range)

    // for each being found above
    beings.forEach { being ->
        // if this area all open and connected, or there is LOS between the sound location
        // and the being
        val beingLocation = being.location
        var distance: PixelDistance? = null
        if (isGameSpecificAreaOpenAndConnected(this) || canSeeExt(location, beingLocation).canSee) {
            // determine the distance between the sound and the being
            distance = getDistance(location, beingLocation)
        }
        // else, if this area has an adjacency graph
        else if (adjacencyGraph != null) {
            // if the adjacency graph says there is a path between the
            // sound location and this being
            val result =
                adjacencyGraph!!.getPathExists(location, beingLocation, range, false)
            if (result.pathExists) {
                // remember the path distance as the distance between the sound and the being
                distance = result.pathLength
            }
        }

        // if we determined above that this being is within earshot,
        // inform this being of the sound
        if (distance != null) being.onSoundHeard(sound, location, distance)
    }
}

@ExtensionPoint
var isGameSpecificAreaOpenAndConnected: (area: Area) -> Boolean = { _ -> false }