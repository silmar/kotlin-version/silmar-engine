package silmarengine.contexts.gameplay.areas.extensions.geometry

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.objectpools.pixelDistancePool

/**
 * Returns whether the distance between the given two locations is at most
 * the given value.  Is designed to be much quicker than using getDistance() for this purpose.
 */
fun isDistanceAtMost(
    from: PixelPoint, to: PixelPoint, atMost: PixelDistance
): Boolean {
    if (!areOrthogonalDistancesAtMost(from, to, atMost)) return false

    val atMostSquared = atMost.distance * atMost.distance
    val squaredDistance = pixelDistancePool.supply()
    getSquaredDistance(from, to, squaredDistance)
    val result = squaredDistance.distance <= atMostSquared
    pixelDistancePool.reclaim(squaredDistance)
    return result
}