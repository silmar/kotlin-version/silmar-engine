package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.events.ReporterMixin
import silmarengine.contexts.gameplay.areas.geometry.listeners.DimensionsChangedListener
import java.awt.Dimension
import java.io.Serializable

open class TileDimensions(open val width: Int, open val height: Int) : Serializable {
    constructor() : this(0, 0)
    constructor(other: TileDimensions) : this(other.width, other.height)
    constructor(d: Dimension) : this(d.width, d.height)

    override fun toString(): String {
        return "width=$width height=$height"
    }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is TileDimensions && width == other.width && height == other.height)
    }

    override fun hashCode(): Int {
        return 31 * width + height
    }
}

open class MutableTileDimensions(width: Int, height: Int) :
    TileDimensions(width, height), ReporterMixin<DimensionsChangedListener> {
    final override var width = width
        private set
    final override var height = height
        private set

    override var listeners = HashSet<DimensionsChangedListener>()

    constructor() : this(0, 0)
    constructor(other: TileDimensions) : this(other.width, other.height)
    constructor(d: Dimension) : this(d.width, d.height)

    fun set(width: Int, height: Int): MutableTileDimensions {
        if (width == this.width && height == this.height) return this

        val oldWidth = this.width
        val oldHeight = this.height
        this.width = width
        this.height = height
        reportToListeners { it(oldWidth, oldHeight) }
        return this
    }

    fun set(other: TileDimensions): MutableTileDimensions =
        set(other.width, other.height)
}
