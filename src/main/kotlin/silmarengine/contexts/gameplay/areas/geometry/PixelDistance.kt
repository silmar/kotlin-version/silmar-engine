package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.contexts.arealighting.domain.model.ALPixelDistance
import silmarengine.contexts.beingmovement.domain.model.BMPixelDistance
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBPixelDistance
import silmarengine.contexts.pathfinding.domain.model.PFPixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.io.Serializable

/**
 * Represents a distance of pixels in an area setting.
 */
class PixelDistance : Serializable, ALPixelDistance, LOSBPixelDistance, PFPixelDistance,
    BMPixelDistance {
    /**
     * The number of pixels of distance this object represents.
     */
    override var distance: Int = 0

    constructor()

    constructor(distance: Int) {
        this.distance = distance
    }

    constructor(tileDistance: TileDistance) {
        distance = tileDistance.distance * tileWidth
    }

    override fun toString(): String = distance.toString()

    companion object {
        /**
         * The maximum distance representable by an object of this class.
         */
        var maxValue = PixelDistance(Integer.MAX_VALUE)
    }
}