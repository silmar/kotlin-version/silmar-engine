package silmarengine.contexts.gameplay.areas.geometry.listeners

typealias PointChangedListener = (oldX: Int, oldY: Int) -> Unit
