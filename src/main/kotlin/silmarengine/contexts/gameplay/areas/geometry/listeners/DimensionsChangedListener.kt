package silmarengine.contexts.gameplay.areas.geometry.listeners

typealias DimensionsChangedListener = (oldWidth: Int, oldHeight: Int) -> Unit
