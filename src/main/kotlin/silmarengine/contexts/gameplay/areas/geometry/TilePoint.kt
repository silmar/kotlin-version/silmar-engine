package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.contexts.arealighting.domain.model.ALMutableTilePoint
import silmarengine.contexts.arealighting.domain.model.ALTilePoint
import silmarengine.events.ReporterMixin
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBMutableTilePoint
import silmarengine.contexts.gameplay.areas.geometry.listeners.PointChangedListener
import java.awt.Point
import java.io.Serializable

/**
 * Represents a specific tile-location within an area.
 */
open class TilePoint(override val x: Int, override val y: Int) : ALTilePoint,
    Serializable {
    constructor() : this(0, 0)
    constructor(other: TilePoint) : this(other.x, other.y)
    constructor(p: Point) : this(p.x, p.y)

    override fun toString(): String {
        return "x=$x y=$y"
    }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is TilePoint && x == other.x && y == other.y)
    }

    override fun hashCode(): Int {
        return 31 * x + y
    }
}

open class MutableTilePoint(x: Int, y: Int) : TilePoint(x, y),
    ReporterMixin<PointChangedListener>, ALMutableTilePoint, LOSBMutableTilePoint {
    final override var x = x
        private set
    final override var y = y
        private set

    override var listeners = HashSet<PointChangedListener>()

    constructor() : this(0, 0)
    constructor(other: TilePoint) : this(other.x, other.y)
    constructor(p: Point) : this(p.x, p.y)

    override fun set(x: Int, y: Int): MutableTilePoint {
        if (x == this.x && y == this.y) return this

        val oldX = this.x
        val oldY = this.y
        this.x = x
        this.y = y
        reportToListeners { it(oldX, oldY) }
        return this
    }

    fun set(p: TilePoint): MutableTilePoint = set(p.x, p.y)

    fun translate(dx: Int, dy: Int): MutableTilePoint =
        set(x + dx, y + dy)
}
