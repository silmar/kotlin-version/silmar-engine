package silmarengine.contexts.gameplay.areas.geometry.listeners

typealias RectangleBoundsChangedListener = () -> Unit
