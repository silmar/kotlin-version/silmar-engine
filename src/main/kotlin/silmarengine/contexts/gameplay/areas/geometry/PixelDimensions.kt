package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.contexts.beingmovement.domain.model.BMPixelDimensions
import silmarengine.events.ReporterMixin
import silmarengine.contexts.gameplay.areas.geometry.listeners.DimensionsChangedListener
import silmarengine.contexts.pathfinding.domain.model.PFPixelDimensions
import java.awt.Dimension
import java.io.Serializable

open class PixelDimensions(override val width: Int, override val height: Int) :
    Serializable, PFPixelDimensions, BMPixelDimensions {
    constructor() : this(0, 0)
    constructor(other: PixelDimensions) : this(other.width, other.height)
    constructor(d: Dimension) : this(d.width, d.height)

    override fun toString(): String {
        return "width=$width height=$height"
    }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is PixelDimensions && width == other.width && height == other.height)
    }

    override fun hashCode(): Int {
        return 31 * width + height
    }
}

open class MutablePixelDimensions(width: Int, height: Int) :
    PixelDimensions(width, height), ReporterMixin<DimensionsChangedListener> {
    final override var width = width
        private set
    final override var height = height
        private set

    override var listeners = HashSet<DimensionsChangedListener>()

    constructor() : this(0, 0)
    constructor(other: PixelDimensions) : this(other.width, other.height)
    constructor(d: Dimension) : this(d.width, d.height)

    fun set(width: Int, height: Int): PixelDimensions {
        if (width == this.width && height == this.height) return this

        val oldWidth = this.width
        val oldHeight = this.height
        this.width = width
        this.height = height
        reportToListeners { it(oldWidth, oldHeight) }

        return this
    }
}
