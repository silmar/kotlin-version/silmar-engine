package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.contexts.beingmovement.domain.model.BMTileDistance
import silmarengine.contexts.pathfinding.domain.model.PFTileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.io.Serializable

/**
 * Represents a distance of tiles in an area setting.
 */
class TileDistance : Serializable, PFTileDistance, BMTileDistance {
    /**
     * The number of tiles of distance this object represents.
     */
    override var distance: Int = 0

    constructor(distance: Int) {
        this.distance = distance
    }

    constructor(pixelDistance: PixelDistance) {
        distance = pixelDistance.distance / tileWidth
    }
}
