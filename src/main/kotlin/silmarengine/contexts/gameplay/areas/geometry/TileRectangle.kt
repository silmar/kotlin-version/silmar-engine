package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.events.*
import silmarengine.contexts.gameplay.areas.geometry.listeners.RectangleBoundsChangedListener
import java.io.Serializable

@Suppress("LeakingThis")
open class TileRectangle {
    open val location: TilePoint
    open val size: TileDimensions

    val x: Int
        get() = location.x
    val y: Int
        get() = location.y

    val width: Int
        get() = size.width
    val height: Int
        get() = size.height

    constructor() {
        location = TilePoint(0, 0)
        size = TileDimensions()
    }

    constructor(location: TilePoint, size: TileDimensions) {
        this.location = location
        this.size = size
    }

    constructor(other: TileRectangle) : this(other.location, other.size)

    constructor(x: Int, y: Int, w: Int, h: Int) : this(TilePoint(x, y),
        TileDimensions(w, h))

    override fun toString(): String =
        "x=${location.x} y=${location.y} size=${size.width} x ${size.height}"

    override fun equals(other: Any?): Boolean {
        return other === this || (other is TileRectangle && location == other.location && size == other.size)
    }

    override fun hashCode(): Int {
        return 31 * location.hashCode() + size.hashCode()
    }
}

@Suppress("LeakingThis")
class MutableTileRectangle() : TileRectangle(), Serializable,
    ReporterMixin<RectangleBoundsChangedListener> {
    val mutableLocation = MutableTilePoint()
    val mutableSize = MutableTileDimensions()

    override val location: TilePoint get() = mutableLocation
    override val size: TileDimensions get() = mutableSize

    override var listeners = HashSet<RectangleBoundsChangedListener>()

    constructor(other: TileRectangle) : this() {
        mutableLocation.set(other.location)
        mutableSize.set(other.size)
    }

    fun setLocation(x: Int, y: Int): MutableTileRectangle =
        set(x, y, size.width, size.height)

    fun setLocation(to: TilePoint): MutableTileRectangle =
        setLocation(to.x, to.y)

    fun setSize(w: Int, h: Int): MutableTileRectangle =
        set(location.x, location.y, w, h)

    fun setSize(size: TileDimensions): MutableTileRectangle =
        setSize(size.width, size.height)

    fun set(x: Int, y: Int, w: Int, h: Int): MutableTileRectangle {
        mutableLocation.set(x, y)
        mutableSize.set(w, h)
        reportToListeners { it() }
        return this
    }

    fun set(other: TileRectangle): MutableTileRectangle =
        set(other.location.x, other.location.y, other.size.width, other.size.height)
}
