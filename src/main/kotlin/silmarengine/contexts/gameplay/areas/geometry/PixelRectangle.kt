package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.events.ReporterMixin
import silmarengine.geometry.rectangles.containsPoint
import silmarengine.geometry.rectangles.intersects
import silmarengine.contexts.gameplay.areas.geometry.listeners.RectangleBoundsChangedListener
import silmarengine.contexts.pathfinding.domain.model.PFMutablePixelRectangle
import silmarengine.contexts.pathfinding.domain.model.PFPixelDimensions
import silmarengine.contexts.pathfinding.domain.model.PFPixelPoint
import silmarengine.contexts.pathfinding.domain.model.PFPixelRectangle
import java.io.Serializable

@Suppress("LeakingThis")
open class PixelRectangle : PFPixelRectangle {
    open val location: PixelPoint
    open val size: PixelDimensions

    override val x: Int
        get() = location.x
    override val y: Int
        get() = location.y

    override val width: Int
        get() = size.width
    override val height: Int
        get() = size.height

    constructor() {
        location = PixelPoint(0, 0)
        size = PixelDimensions()
    }

    constructor(location: PixelPoint, size: PixelDimensions) {
        this.location = location
        this.size = size
    }

    fun contains(p: PixelPoint): Boolean = containsPoint(x, y, width, height, p.x, p.y)

    override fun toString(): String =
        "x=${location.x} y=${location.y} size=${size.width} x ${size.height}"

    override fun equals(other: Any?): Boolean {
        return other === this || (other is PixelRectangle && location == other.location && size == other.size)
    }

    override fun hashCode(): Int {
        return 31 * location.hashCode() + size.hashCode()
    }

    fun intersects(r: PixelRectangle): Boolean =
        intersects(x, y, width, height, r.x, r.y, r.width, r.height)

    override fun intersects(other: PFPixelRectangle): Boolean =
        intersects(other as PixelRectangle)
}

@Suppress("LeakingThis")
class MutablePixelRectangle : PixelRectangle(), Serializable,
    ReporterMixin<RectangleBoundsChangedListener>, PFMutablePixelRectangle {
    val mutableLocation = MutablePixelPoint()
    val mutableSize = MutablePixelDimensions()

    override val location: PixelPoint get() = mutableLocation
    override val size: PixelDimensions get() = mutableSize

    override var listeners = HashSet<RectangleBoundsChangedListener>()

    fun setLocation(x: Int, y: Int): MutablePixelRectangle =
        set(x, y, size.width, size.height)

    fun setLocation(to: PixelPoint): MutablePixelRectangle = setLocation(to.x, to.y)

    override fun setLocation(to: PFPixelPoint): MutablePixelRectangle =
        setLocation(to as PixelPoint)

    override fun translate(dx: Int, dy: Int): MutablePixelRectangle {
        mutableLocation.translate(dx, dy)
        return this
    }

    override fun setSize(width: Int, height: Int): MutablePixelRectangle =
        set(location.x, location.y, width, height)

    fun setSize(size: PixelDimensions): MutablePixelRectangle =
        setSize(size.width, size.height)

    override fun setSize(size: PFPixelDimensions): MutablePixelRectangle =
        setSize(size as PixelDimensions)

    fun set(x: Int, y: Int, w: Int, h: Int): MutablePixelRectangle {
        mutableLocation.set(x, y)
        mutableSize.set(w, h)
        reportToListeners { it() }
        return this
    }

    fun set(other: PixelRectangle): MutablePixelRectangle =
        set(other.location.x, other.location.y, other.size.width, other.size.height)

    override fun set(other: PFPixelRectangle): MutablePixelRectangle =
        set(other as PixelRectangle)

    fun centerOn(other: PixelPoint) {
        set(other.x - size.width / 2, other.y - size.height / 2, size.width, size.height)
    }
}
