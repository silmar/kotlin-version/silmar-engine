package silmarengine.contexts.gameplay.areas.geometry

import silmarengine.contexts.arealighting.domain.model.ALMutablePixelPoint
import silmarengine.contexts.arealighting.domain.model.ALPixelPoint
import silmarengine.contexts.beingmovement.domain.model.BMMutablePixelPoint
import silmarengine.contexts.beingmovement.domain.model.BMPixelPoint
import silmarengine.events.*
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBMutablePixelPoint
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBPixelPoint
import silmarengine.contexts.gameplay.areas.geometry.listeners.PointChangedListener
import silmarengine.contexts.pathfinding.domain.model.PFMutablePixelPoint
import silmarengine.contexts.pathfinding.domain.model.PFPixelPoint
import java.awt.Point
import java.io.Serializable

/**
 * Represents a specific pixel-location within an area.
 */
open class PixelPoint(override val x: Int, override val y: Int) :
    Serializable, ALPixelPoint, LOSBPixelPoint, PFPixelPoint, BMPixelPoint {

    constructor(other: PixelPoint) : this(other.x, other.y)
    constructor(p: Point) : this(p.x, p.y)

    override fun toString(): String {
        return "x=$x y=$y"
    }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is PixelPoint && x == other.x && y == other.y)
    }

    override fun hashCode(): Int {
        return 31 * x + y
    }
}

open class MutablePixelPoint(x: Int, y: Int) : PixelPoint(x, y),
    ReporterMixin<PointChangedListener>, ALMutablePixelPoint,
    LOSBMutablePixelPoint, PFMutablePixelPoint, BMMutablePixelPoint {
    final override var x = x
        private set
    final override var y = y
        private set

    override var listeners = HashSet<PointChangedListener>()

    constructor() : this(0, 0)
    constructor(other: PixelPoint) : this(other.x, other.y)
    constructor(p: Point) : this(p.x, p.y)

    override fun set(x: Int, y: Int): MutablePixelPoint {
        if (x == this.x && y == this.y) return this

        val oldX = this.x
        val oldY = this.y
        this.x = x
        this.y = y
        reportToListeners { it(oldX, oldY) }
        return this
    }

    fun set(other: PixelPoint): MutablePixelPoint = set(other.x, other.y)

    override fun translate(dx: Int, dy: Int): MutablePixelPoint =
        set(x + dx, y + dy)
}
