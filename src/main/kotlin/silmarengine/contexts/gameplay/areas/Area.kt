package silmarengine.contexts.gameplay.areas

import silmarengine.contexts.arealighting.domain.model.ALArea
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.beingmovement.domain.model.BMArea
import silmarengine.contexts.gameplay.areas.adjacency.AdjacencyGraph
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.extensions.geometry.asPixelLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing
import silmarengine.contexts.gameplay.entities.items.ItemEntity
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.events.ReporterMixin
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBArea
import silmarengine.contexts.pathfinding.domain.model.PFArea
import silmarengine.sounds.Sound
import silmarengine.contexts.gameplay.tiles.Tile
import java.awt.Dimension

/**
 * An area in the game.  Consists of a rectangular grid of tiles, plus entities
 * of various sorts which reside within the area's bounds.
 */
interface Area : ReporterMixin<AreaListener>, ALArea, LOSBArea, PFArea, BMArea {
    override val size: Dimension
    fun onSizeDetermined(size: Dimension)

    val beings: List<Being>
    val monsters: List<Monster>
    val items: List<ItemEntity>
    val terrains: List<Terrain>
    val traps: List<Trap>
    val talkerBeings: List<TalkerBeing>
    val player: Player?
    val visualEffects: List<VisualEffect>
    override val lightSources: List<LightSource>
    val motif: AreaMotif
    val adjacencyGraph: AdjacencyGraph?
    override val isSelfLit: Boolean

    /**
     * Returns the tile at the given tile-location within this area.
     */
    fun getTile(location: TilePoint): Tile

    fun getTile(location: PixelPoint): Tile

    /**
     * Sets the given tile into the given tile-location within this area.
     */
    fun setTile(location: TilePoint, tile: Tile)

    fun setTile(location: PixelPoint, tile: Tile) =
        setTile(location.asTileLocation(), tile)

    /**
     * Adds the given entity to this area at the given location.
     *
     * @param shouldNotify  Whether the area should notify listeners about the addition.
     *                      (An example of when it would not want to is during this
     *                      area's creation.)
     */
    fun addEntity(
        entity: Entity, location: PixelPoint, shouldNotify: Boolean = true
    )

    fun addEntity(
        entity: Entity, location: TilePoint, shouldNotify: Boolean = true
    ) = addEntity(entity, location.asPixelLocation(), shouldNotify)

    /**
     * Removes the given entity from this area.
     */
    fun removeEntity(entity: Entity)

    /**
     * Moves the given light-source to the head of this area's list of light-sources,
     * so it gets processed in subsequent operations first, since it is presumed
     * most likely to bring a positive result.
     */
    override fun considerLightSourceFirst(lightSource: LightSource)

    val openDoorSound: Sound
    val closeDoorSound: Sound
}

