package silmarengine.contexts.gameplay.areas

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.tiles.Tile

interface AreaListener {
    fun onTileChanged(location: TilePoint, to: Tile) {}
    fun onEntityPresenceChange(entity: Entity) {}
    fun onEntityChangedAppearance() {}
    fun onGroupOfChangesToOccur() {}
    fun onGroupOfChangesOccurred() {}
}