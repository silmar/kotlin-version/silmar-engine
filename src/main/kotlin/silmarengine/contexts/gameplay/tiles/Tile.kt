package silmarengine.contexts.gameplay.tiles

import silmarengine.contexts.arealighting.domain.model.ALTile
import silmarengine.contexts.beingmovement.domain.model.BMTile
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.MovementType.*
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBTile
import java.io.Serializable

/**
 * A tile in an area.  Actually, flyweight objects of this class
 * are used to represent types of tiles, so the same flyweight tile
 * object may be referenced by multiple locations in an area.
 */
class Tile(
    /**
     * Whether this tile is normally passible (i.e. can it be moved over?).
     */
    val isPassible: Boolean,

    /**
     * Whether this tile blocks line-of-sight (as would a wall, for example).
     */
    override val blocksLOS: Boolean
) : Serializable, ALTile, LOSBTile, BMTile {

    /**
     * The index (or ID) of this tile amongst all others.  This
     * field lets us quickly and correctly check for equality
     * between two tiles.
     */
    val index: Int

    /**
     * Returns the cost in movement points for moving over this tile.
     * The "normal" cost for movement over an ordinary tile is 1.
     */
    override val movementCost: Int
        get() = if (!equals(Tiles.slow)) 1 else 3

    init {
        index = lastIndex++
        idToTileMap[index] = this
    }

    override fun toString() = "Tile($index)"

    /**
     * Returns whether this tile is passible for the given
     * movement-type.
     *
     * @param   movementType    The movement-type to consider when
     * reporting this tile's passibility.
     */
    fun isPassible(movementType: MovementType): Boolean {
        if (movementType == WALKING_NO_HANDS || movementType == FLYING_NO_HANDS) {
            if (blocksLOS) {
                return false
            }
        }

        if (movementType == FLYING || movementType == FLYING_NO_HANDS) {
            return isPassible || !blocksLOS
        }

        return if (movementType == EARTHING || movementType == ETHEREAL) {
            true
        }
        else isPassible

    }

    /**
     * Returns whether the given object is the same as this tile.
     */
    override fun equals(other: Any?): Boolean {
        return other is Tile && index == other.index
    }

    override fun hashCode(): Int {
        return index
    }

    companion object {
        val idToTileMap = HashMap<Int, Tile>()

        /**
         * A counter for assigning indices to newly created tiles.
         */
        private var lastIndex = 0
    }
}