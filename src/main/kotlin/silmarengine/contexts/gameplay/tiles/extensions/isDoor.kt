package silmarengine.contexts.gameplay.tiles.extensions

import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles.door
import silmarengine.contexts.gameplay.tiles.Tiles.openDoor
import silmarengine.contexts.gameplay.tiles.Tiles.openRoomSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.openSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.roomSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.secretDoor

/**
 * Returns whether this tile represents some form of door.
 */
val Tile.isDoor: Boolean
    get() = when (this) {
        door, secretDoor, roomSecretDoor -> true
        else -> false
    }

/**
 * Returns whether this tile represents some form of open doorway.
 */
val Tile.isOpenDoor: Boolean
    get() = when (this) {
        openDoor, openSecretDoor, openRoomSecretDoor -> true
        else -> false
    }

