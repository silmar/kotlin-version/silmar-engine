package silmarengine.contexts.gameplay.tiles.extensions

import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles.door
import silmarengine.contexts.gameplay.tiles.Tiles.openDoor
import silmarengine.contexts.gameplay.tiles.Tiles.openRoomSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.openSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.roomSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.secretDoor

/**
 * Returns this tile's corresponding closed-door tile, if this tile represents an open
 * door.
 */
val Tile.associatedDoor: Tile?
    get() = when (this) {
        openDoor -> door
        openSecretDoor -> secretDoor
        openRoomSecretDoor -> roomSecretDoor
        else -> null
    }

/**
 * Returns this tile's corresponding open-door tile, if this tile represents a closed
 * door.
 */
val Tile.associatedOpenDoor: Tile?
    get() = when (this) {
        door -> openDoor
        secretDoor -> openSecretDoor
        roomSecretDoor -> openRoomSecretDoor
        else -> null
    }

