package silmarengine.contexts.gameplay.tiles

import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import java.awt.Dimension

/**
 * The pixel-size of each tile in an area.
 */
const val tileWidth = 32
const val tileHeight = 32
val tileSizeDimension = Dimension(tileWidth, tileHeight)
val tileSize = PixelDimensions(tileWidth, tileHeight)

/**
 * The orthogonal distance covered by one (or a half) tile.
 */
val tileDistance = PixelDistance(tileSize.width)
val halfTileDistance = PixelDistance(tileSize.width / 2)
