package silmarengine.contexts.gameplay.tiles

/**
 * The flyweight tile types.
 */
@Suppress("unused")
object Tiles {
    val floor = Tile(isPassible = true, blocksLOS = false)
    val wall = Tile(isPassible = false, blocksLOS = true)
    val roomFloor = Tile(isPassible = true, blocksLOS = false)
    val roomWall = Tile(isPassible = false, blocksLOS = true)
    val door = Tile(isPassible = true, blocksLOS = true)
    val secretDoor = Tile(isPassible = true, blocksLOS = true)
    val roomSecretDoor = Tile(isPassible = true, blocksLOS = true)
    val openDoor = Tile(isPassible = true, blocksLOS = false)
    val openSecretDoor = Tile(isPassible = true, blocksLOS = false)
    val openRoomSecretDoor = Tile(isPassible = true, blocksLOS = false)
    val water = Tile(isPassible = false, blocksLOS = false)
    val waterNW = Tile(isPassible = true, blocksLOS = false)
    val waterNE = Tile(isPassible = true, blocksLOS = false)
    val waterSW = Tile(isPassible = true, blocksLOS = false)
    val waterSE = Tile(isPassible = true, blocksLOS = false)
    val road = Tile(isPassible = true, blocksLOS = false)
    val roadNW = Tile(isPassible = true, blocksLOS = false)
    val roadNE = Tile(isPassible = true, blocksLOS = false)
    val roadSW = Tile(isPassible = true, blocksLOS = false)
    val roadSE = Tile(isPassible = true, blocksLOS = false)
    val roomWindow = Tile(isPassible = false, blocksLOS = false)
    val slow = Tile(isPassible = true, blocksLOS = false)
    val outOfBounds = Tile(isPassible = false, blocksLOS = false)
}