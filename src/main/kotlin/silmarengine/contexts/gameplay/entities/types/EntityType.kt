package silmarengine.contexts.gameplay.entities.types

import silmarengine.contexts.gameplay.areas.generation.Stockable
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions

/**
 * Holds attributes which are static for all instances of an entity of a particular type.
 */
interface EntityType : Stockable {

    /**
     * What to call instances of this entity-type.
     */
    val name: String

    /**
     * The relative path (+ name) (if any) of the file containing the image for instances
     * of this entity-type.
     */
    val imagePath: String?

    /**
     * The dimensions (in pixels) of a entity of this type when it is depicted onscreen.
     */
    val size: PixelDimensions
}