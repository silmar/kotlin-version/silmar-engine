package silmarengine.contexts.gameplay.entities.types

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.tiles.tileSize
import silmarengine.util.image.getImageSizeWithoutLoading

abstract class EntityType1(name: String, imagePath: String?, override val frequency: Int)
    : EntityType, Cloneable {

    override var name: String = name
        protected set

    override var imagePath: String? = imagePath
        protected set

    override lateinit var size: PixelDimensions
        protected set

    override fun toString() = name

    override fun isPlacementLocationValid(area: Area, location: PixelPoint): Boolean {
        // test each corner of the rectangle centered about the given location
        // and of the dimensions specified by this item-type's size field
        val x1 = location.x - size.width / 2
        val x2 = location.x + size.width / 2 - 1
        val y1 = location.y - size.height / 2
        val y2 = location.y + size.height / 2 - 1
        val test = MutablePixelPoint()
        return isPresenceLocationValid(area, test.set(x1, y1)) && isPresenceLocationValid(
            area, test.set(x2, y2)) && isPresenceLocationValid(area,
            test.set(x2, y1)) && isPresenceLocationValid(area, test.set(x1, y2))
    }

    /**
     * Returns whether an entity of this entity-type may be present at the
     * given location on the given area
     */
    protected abstract fun isPresenceLocationValid(
        area: Area, location: PixelPoint
    ): Boolean

    /**
     * Determines the size (in pixels) of entities of this type, usually by examining
     * the size of the image used to represent such entities.
     */
    protected fun determineSize() {
        // if an image path was given
        val imagePath = imagePath
        size = if (imagePath != null) {
            // determine the dimensions of an item of this type from the image at that path
            PixelDimensions(getImageSizeWithoutLoading(imagePath))
        }
        // otherwise, use a default size
        else tileSize
    }
}
