package silmarengine.contexts.gameplay.entities.talkers

import silmarengine.contexts.gameplay.entities.items.Item

interface ItemFixer : Talker {
    /**
     * Returns whether this fixer is able (and willing) to fix the given item.
     */
    fun getCanFixItem(item: Item): Boolean

    /**
     * Returns this fixer's adjusted price for the given item with the given normal price.
     */
    fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int
}
