package silmarengine.contexts.gameplay.entities.talkers

import java.util.*

/**
 * A service that may be performed for a player by one of the talkers in the game.
 */
class Service(
    /**
     * The name of this service.
     */
    val name: String,
    /**
     * The average price it costs to have this service performed.
     */
    val price: Int
) {
    init {
        servicesMap[name] = this
    }

    companion object {
        /**
         * A mapping of all the services by their names.
         */
        var servicesMap = HashMap<String, Service>()

        /**
         * The price per level for priestly services.  This is computed
         * according to the following formula:
         *
         * [Spell level *] 100 * 3    (per literature for scroll values)
         * / 2                 (since it is a right-now casting, and not portable like a scroll)
         * / 4                 (since the priest is charging for materials only)
         *
         * ~= 40
         */
        const val priestServiceCostPerLevel = 40
    }
}