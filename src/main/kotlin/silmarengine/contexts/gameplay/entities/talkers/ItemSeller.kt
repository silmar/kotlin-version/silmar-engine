package silmarengine.contexts.gameplay.entities.talkers

import silmarengine.contexts.gameplay.entities.items.Item

/**
 * Describes an entity which sells items services.
 */
interface ItemSeller : Talker {

    /**
     * Returns the complete list of what items this seller has for sale.
     */
    val itemsForSale: List<Item>

    /**
     * Returns this seller's adjusted price for the given item with the given normal price.
     */
    fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int
}
