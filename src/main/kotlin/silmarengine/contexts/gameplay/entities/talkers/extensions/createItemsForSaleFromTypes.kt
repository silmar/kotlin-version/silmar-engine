package silmarengine.contexts.gameplay.entities.talkers.extensions

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.types.ItemType

/**
 * Returns a list of items for sale generated using the given array of item-types,
 * where each item (i.e. element) in the returned array is of the item-type
 * of the corresponding element in the types array.
 */
fun createItemsForSaleFromTypes(types: Array<ItemType>): List<Item> =
    // for each of the given item-types
    types.map { type ->
        // create an identified item of the given type
        val item = createItem(type, ItemCondition.EXCELLENT)
        item.identify()

        // if the item-type is groupable
        if (item.type.isGroupable) {
            // make the item sold in fives
            item.quantity = 5
        }

        item
    }