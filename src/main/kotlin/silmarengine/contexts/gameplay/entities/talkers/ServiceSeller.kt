package silmarengine.contexts.gameplay.entities.talkers

import silmarengine.contexts.gameplay.entities.beings.player.Player

/**
 * Describes an entity which sells services.
 */
interface ServiceSeller : Talker {

    /**
     * Returns the complete list of what services this seller has for sale.
     */
    val servicesForSale: Array<Service>

    /**
     * Returns this seller's price for providing the given service for the given player.
     */
    fun getServicePrice(service: Service, forPlayer: Player): Int
}
