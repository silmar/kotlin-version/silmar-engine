package silmarengine.contexts.gameplay.entities.talkers

/**
 * The flyweight services.
 */
object Services {
    val curePoison = Service("cure poison", 4 * Service.priestServiceCostPerLevel)
    val cureDisease = Service("cure disease", 3 * Service.priestServiceCostPerLevel)
    val removeCursedItems =
        Service("remove cursed items", 3 * Service.priestServiceCostPerLevel)
    val healWounds = Service("heal wounds", 1 * Service.priestServiceCostPerLevel)
    val healFully = Service("heal fully", 6 * Service.priestServiceCostPerLevel)
    val restoreLevels = Service("restore levels", 7 * Service.priestServiceCostPerLevel)
}