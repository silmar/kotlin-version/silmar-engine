package silmarengine.contexts.gameplay.entities.talkers

import silmarengine.contexts.gameplay.entities.beings.player.Player

/**
 * Describes an ability for an entity to talk to the player.
 */
interface Talker {

    /**
     * Returns the initial greeting received when talking to this talker.
     */
    val greeting: String

    /**
     * Informs this talker that its current conversation is now over.
     */
    fun onTalkedTo()

    /**
     * Informs this talker that its greeting has been presented to the given player.
     */
    fun onGreetingHeardByPlayer(player: Player)
}
