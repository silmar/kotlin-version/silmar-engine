package silmarengine.contexts.gameplay.entities.extensions.proximity

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.extensions.geometry.getEnlargedVersion
import silmarengine.contexts.gameplay.areas.extensions.geometry.union
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Returns whether this entity is next to the given entity in the same area.
 */
fun Entity.isNextTo(entity: Entity): Boolean {
    // if the given entity isn't in the same area
    if (area != entity.area) return false

    // return whether the orthogonal distance between this entity's bounds and the
    // given entity's bounds is at most a small amount (which is slightly greater than
    // the pixel's covered during one step of an entity's movement, to avoid situations
    // whether one entity can't get close enough to another to attack it)
    val entityBounds = entity.bounds
    val union = union(bounds, entityBounds)
    return union.width <= bounds.width + entityBounds.width + 6 && union.height <= bounds.height + entityBounds.height + 6
}

/**
 * Returns whether this entity is next to the given pixel-location.
 */
fun Entity.isNextToLocation(location: PixelPoint): Boolean {
    // return whether this entity's bounds, when almost doubled in dimension, contain the given
    // location
    val enlarged = getEnlargedVersion(bounds, 2.75f)
    return enlarged.contains(location)
}

