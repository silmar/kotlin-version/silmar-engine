package silmarengine.contexts.gameplay.entities.extensions.proximity

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost

/**
 * Returns a list of the entities from the given list that are within the given
 * pixel-range of the given location.
 */
fun <E : Entity> List<E>.getEntitiesInRange(
    location: PixelPoint, range: PixelDistance
): List<E> = filter { entity ->
    isDistanceAtMost(location, entity.location, range)
}

