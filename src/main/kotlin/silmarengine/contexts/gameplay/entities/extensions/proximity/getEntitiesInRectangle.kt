package silmarengine.contexts.gameplay.entities.extensions.proximity

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelRectangle

/**
 * Returns a list of the entities from the given list found whose center is
 * within the given rectangular pixel-region of this area.
 *
 * See getFirstEntityAt() for descriptions of similar parameters.
 */
fun <E : Entity> List<E>.getEntitiesInRectangle(
    location: PixelPoint, size: PixelDimensions,
    predicate: ((Entity) -> Boolean)? = null, ignore: E? = null, ignore2: E? = null
): List<E> {
    val rectangle = MutablePixelRectangle()
    rectangle.setLocation(location.x - size.width / 2, location.y - size.height / 2)
    rectangle.setSize(size)
    return filter { entity ->
        entity !== ignore && entity !== ignore2 && (predicate == null || predicate(
            entity)) && rectangle.intersects(entity.impassibleBounds)
    }
}
