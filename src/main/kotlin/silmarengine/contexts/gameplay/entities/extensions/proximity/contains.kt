package silmarengine.contexts.gameplay.entities.extensions.proximity

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Returns whether this entity contains the given area pixel-location.
 */
fun Entity.contains(location: PixelPoint): Boolean {
    return bounds.contains(location)
}

