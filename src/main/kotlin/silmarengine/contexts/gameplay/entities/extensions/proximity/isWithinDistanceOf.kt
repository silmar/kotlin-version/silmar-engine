package silmarengine.contexts.gameplay.entities.extensions.proximity

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost

fun Entity.isWithinDistanceOf(
    of: PixelPoint, distance: PixelDistance
): Boolean = isDistanceAtMost(location, of, distance)