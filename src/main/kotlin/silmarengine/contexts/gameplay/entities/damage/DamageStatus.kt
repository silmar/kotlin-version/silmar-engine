package silmarengine.contexts.gameplay.entities.damage

import silmarengine.getImage

import java.awt.Image

/**
 * A description of how badly damaged a being is overall.
 */
class DamageStatus(
    /**
     * The filename of the image to use to indicate a being's new current damage-status
     * when it gets hit for damage.
     */
    val hitImageName: String, markerImageName: String?
) {

    /**
     * The image to use to indicate a being's current damage-status as it is moving around.
     */
    val markerImage: Image? =
        if (markerImageName != null) getImage(markerImageName) else null

    companion object {
        /**
         * The flyweight damage-statuses
         */
        val undamaged = DamageStatus("undamaged", null)
        val ok = DamageStatus("ok", "x-green")
        val serious = DamageStatus("serious", "x-yellow")
        val critical = DamageStatus("critical", "x-red")
    }
}
    
