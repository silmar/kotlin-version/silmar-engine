package silmarengine.contexts.gameplay.entities.damage

/**
 * The flyweight damage forms.
 */
object DamageForms {
    val pierce = DamageForm(affectsItems = true, isLessenedByToughExterior = true)
    val slash = DamageForm(affectsItems = true, isLessenedByToughExterior = true)
    val bluntImpact =
        DamageForm(affectsItems = true, isLessenedByToughExterior = true)
    val electricity =
        DamageForm(affectsItems = true, isLessenedByToughExterior = false)
    val fire = DamageForm(affectsItems = true, isLessenedByToughExterior = false)
    val explosion = DamageForm(affectsItems = true, isLessenedByToughExterior = true)
    val swallow = DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val acid = DamageForm(affectsItems = true, isLessenedByToughExterior = true)
    val decapitation =
        DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val deathFog = DamageForm(affectsItems = true, isLessenedByToughExterior = false)
    val exertion = DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val choking = DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val poison = DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val starvation =
        DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val attributeAtZero =
        DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val holyPower = DamageForm(affectsItems = true, isLessenedByToughExterior = false)
    val disintegration =
        DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val laser = DamageForm(affectsItems = true, isLessenedByToughExterior = true)
    val radiation =
        DamageForm(affectsItems = false, isLessenedByToughExterior = false)
    val crushing = DamageForm(affectsItems = false, isLessenedByToughExterior = false)
}