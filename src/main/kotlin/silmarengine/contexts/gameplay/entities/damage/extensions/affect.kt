package silmarengine.contexts.gameplay.entities.damage.extensions

import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForm
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.weapons.extensions.canHit
import kotlin.math.roundToInt

/**
 * If the given damage is of the given form, multiplies the damage amount
 * by the given factor.
 */
fun Damage.affectForm(formToAffect: DamageForm, factor: Float) {
    // if the given form is the one to affect
    if (form == formToAffect) {
        // multiply the given damage amount by the given factor
        amount = (amount * factor).toInt()
    }
}

/**
 * If the given damage is of a non-attack form, multiplies the damage amount
 * by the given factor.
 */
fun Damage.affectForNonAttacks(factor: Float) {
    // if the damage isn't from an attack
    if (form != DamageForms.slash && form != DamageForms.pierce && form != DamageForms.bluntImpact) {
        amount = (amount * factor).toInt()
    }
}

/**
 * If the given damage is of an attack form, multiplies the damage amount
 * by the given factor.
 */
fun Damage.affectForAttacks(factor: Float) {
    // if the damage is from an attack
    if (form == DamageForms.slash || form == DamageForms.pierce || form == DamageForms.bluntImpact) {
        amount = (amount * factor).toInt()
    }
}

/**
 * If the weapon mentioned in the given damage object does meet the given plus-value
 * needed to hit, zeros the damage amount.
 */
fun Damage.affectByWeaponPlusNeededToHit(plusNeeded: Int) {
    // if a plus is indeed needed and the weapon can't hit that plus
    if (plusNeeded > 0 && weapon != null && !weapon!!.canHit(plusNeeded)) {
        amount = 0
    }
}

/**
 * If the given damage is due to an edged weapon, multiplies the damage amount
 * by the given factor.
 */
fun Damage.affectForEdgedWeapons(factor: Float) {
    if (weapon != null && weapon!!.weaponType.isEdged) {
        amount = (amount * factor).roundToInt()
    }
}
