package silmarengine.contexts.gameplay.entities.damage

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

/**
 * An object describing an occurrence of damage, including the form and
 * the amount.  This is a struct-style class, with public data fields
 * instead of access methods.
 */
class Damage(
    /**
     * The number of hit points of damage done.
     */
    var amount: Int,
    /**
     * The form of the damage.
     */
    var form: DamageForm,
    /**
     * The being (if any) that inflicted the damage.
     */
    var foe: Being? = null,
    /**
     * The weapon (if any) that inflicted the damage.
     */
    var weapon: Weapon? = null,
    /**
     * The monster attack (if any) that inflicted the damage.
     */
    var attack: MonsterAttack? = null
)
