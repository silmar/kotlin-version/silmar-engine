package silmarengine.contexts.gameplay.entities.damage

/**
 * Describes a form of damage.
 */
class DamageForm(
    /**
     * Whether this form of damage is applicable to items.
     */
    val affectsItems: Boolean,
    /**
     * Whether damage of this form is lessened by the victim possessing a tough exterior.  Note that armor
     * does not lessen damage, as it is presumed that the armor makes it harder for the wearer to dodge the
     * cause of the damage. Instead, magical and special armor offers a bonus to avoidance.
     */
    val isLessenedByToughExterior: Boolean
) {
    /**
     * This damage-form's index (or ID) amongst all those in existence.
     */
    val index: Int

    init {
        index = maxIndex++
    }

    /**
     * Returns whether the given form is the same as this one.
     */
    override fun equals(other: Any?): Boolean {
        return other is DamageForm && other.index == index
    }

    override fun hashCode(): Int {
        return index
    }

    companion object {
        /**
         * The maximum index assigned to a damage form so far.
         */
        private var maxIndex = 0
    }
}
