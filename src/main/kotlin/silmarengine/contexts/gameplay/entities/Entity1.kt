package silmarengine.contexts.gameplay.entities

import silmarengine.annotations.DeserializationOverride
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.beingmovement.domain.functions.pixelsPerMove
import silmarengine.contexts.beingmovement.domain.model.BMMovementType
import silmarengine.contexts.beingmovement.domain.model.BMPixelPoint
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.sounds.Sound
import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.gui.views.areaview.AreaView
import silmarengine.gui.views.areaview.extensions.geometry.doesRectangleIntersectViewArea
import java.awt.Image
import java.io.IOException
import java.io.ObjectInputStream
import java.io.Serializable

abstract class Entity1 : Entity, Serializable {
    final override lateinit var area: Area
        private set

    override fun placeInArea(area: Area, location: PixelPoint) {
        this.area = area
        mutableLocation.set(location)
        onPlacedInArea()
    }

    open fun onPlacedInArea() {}

    protected val mutableLocation = MutablePixelPoint()
    override val location: PixelPoint = mutableLocation

    override fun moveTo(location: PixelPoint) {
        mutableLocation.set(location)
    }

    protected val mutableBounds = MutablePixelRectangle()
    override val bounds: PixelRectangle = mutableBounds

    /**
     * This entity's count of movement steps it has taken since the last time a movement
     * sound was issued for it.  This lets us issue the sounds with a uniform interval.
     * When this is reset to 0, a movement sound will be issued with this entity's
     * next movement step.  This is transient because we want to have this value start
     * at zero each time this entity is loaded from disk.
     */
    @Transient
    protected var movementSoundCounter = 0

    /**
     * Per how many steps of movement this entity emits a movement sound.
     */
    protected open val movementSoundInterval
        get() = 4 * tileWidth / pixelsPerMove

    @Transient
    override var image: Image? = null

    /**
     * Returns the bounds of this entity that is impassible (if the entity itself is considered
     * impassible).  For most entities, this should not be overwritten.  One exception is
     * terrains, which often take up too much space if their normal bounds are used for this
     * purpose.
     */
    override val impassibleBounds: PixelRectangle
        get() = bounds

    override val movementType: MovementType
        get() = MovementType.WALKING_NO_HANDS

    override val bmMovementType: BMMovementType get() = movementType.toBMMovementType()

    /**
     * Returns the sound this entity makes when it moves.
     */
    protected open val movementSound: Sound?
        get() = if (movementType.isQuiet) null else Sounds.movement

    override val shouldDrawOnTop: Boolean
        get() = false

    override val isVisible = true

    init {
        setupListeners()
    }

    private fun setupListeners() {
        // update this entity's bounds when its location changes
        mutableLocation.addListener { _, _ ->
            mutableBounds.centerOn(location)
            mutableBounds.setSize(size)
        }
        mutableBounds.addListener { onBoundsChanged() }
    }

    override fun onImageToChange() {
        image = null
    }

    /**
     * Informs this entity that its bounds have changed.
     */
    protected open fun onBoundsChanged() {}

    /**
     * Checks to see if this entity should emit a sound for having just
     * completed a step of its current movement.
     */
    override fun checkForMovementSound() {
        // if it's time to produce a sound to go along with the current movement
        if (movementSoundCounter == 0) {
            // if this entity has an associated movement sound
            val sound = movementSound
            if (sound != null) {
                // issue that sound
                area.onSoundIssued(sound, location, this !is Player)
            }
        }

        // advance this entity's movement-sound-counter
        movementSoundCounter = when (movementSoundCounter) {
            movementSoundInterval -> 0
            else -> movementSoundCounter + 1
        }
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(stream: ObjectInputStream) {
        stream.defaultReadObject()

        setupListeners()
    }

    fun isInLOSOf(of: PixelPoint): Boolean = area.canSeeExt(of, location).canSee

    override fun isInLOSOf(location: BMPixelPoint): Boolean =
        isInLOSOf(location as PixelPoint)

    override fun isVisibleToUser(): Boolean =
        AreaView.instance?.doesRectangleIntersectViewArea(
            bounds) == true && area.player?.isInLOSOf(location) == true
}