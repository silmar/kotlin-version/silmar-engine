package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInRange
import silmarengine.contexts.gameplay.areas.extensions.passibility.isLinePassible
import silmarengine.objectpools.pixelDistancePool
import silmarengine.objectpools.pixelPointPool
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.awt.geom.Point2D

/**
 * Returns a location to serve as the next goal for this unactivated monster.
 * The goal is determined randomly by finding a passible location in the direction of
 * this monster's last movement (or, just around the monster if the preceding has
 * already failed), where the line between this monster and that location is also
 * all passible (so that a path to that location will not have to be found).
 */
fun Monster.computeUnactivatedGoal(
    lastDirectionMoved: Point2D.Float?
): PixelPoint? {
    // start out the location around which to look for a new goal at this monster's
    // current location
    var goal: PixelPoint? = null
    val spot = pixelPointPool.supply()
    spot.set(location)

    // the range around the spot to look for a new goal is only half the monster's
    // max-movement, because it's in no hurry, and choosing a smaller range also
    // increases the likelihood of finding a passible location in the direction
    // of last movement, which is done below
    val range = pixelDistancePool.supply()
    range.distance = maxMovementPoints * tileWidth / 2

    // if this monster has a last-direction-moved value
    if (lastDirectionMoved != null) {
        // move the spot around which to look for a new goal in the direction of
        // last movement (out to the max range determined above, so that the spot
        // chosen will have to be in that direction)
        spot.set((spot.x + lastDirectionMoved.x * range.distance).toInt(),
            (spot.y + lastDirectionMoved.y * range.distance).toInt())
    }

    // try this a few times
    val maxTries = 2
    for (i in 0..maxTries) {
        // look around the spot determined above for a random passible goal location
        // to which to move
        goal = area.getRandomPassibleRectangleLocationInRange(spot, size, range, 10,
            type.movementType, false)

        // if no potential goal location at all was found during the above call,
        // don't bother trying anymore, as that call makes many attempts to find one
        if (goal == null) break

        // if the line from this monster to the goal is passible (taking into account
        // any harmful terrains)
        if (area.isLinePassible(location, goal, movementType, size, this, null,
                true).passible) {
            // the goal is valid
            break
        }

        // if we get here, the goal is invalid
        goal = null
    }

    return goal
}

