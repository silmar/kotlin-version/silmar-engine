package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.util.math.randomBellCurveInt

/**
 * Informs this player that it is to receive a heal-wounds service from the given
 * seller.
 *
 * @return      Whether the service was performed.
 */
fun Player.onHealWoundsService(seller: Entity): Boolean {
    // if this player is not damaged
    if (!isDamaged) {
        onMessage("You aren't hurt!", MessageType.error)
        return false
    }

    // heal this player a small amount
    performPowerDischargeEffect(seller)
    takeHealing(randomBellCurveInt(1, 4, 4))

    return true
}

