package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.WorkThread
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.Buyer
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

/**
 * Has this player try to sell the given quantity of the given item to the given buyer.
 */
@JvmOverloads
fun Player.sellItem(item: Item, buyer: Buyer, quantity: Int = item.quantity) {
    // if the item is equipped
    if (items.isItemEquipped(item)) {
        // if the item cannot be un-equipped
        if (!tryToUnequipItem(item, false, false)) {
            // don't allow the sale
            return
        }
    }

    // if not all of the item is to be sold
    var itemToSell = item
    if (item.quantity > quantity) {
        // reduce the quantity of the given item to what will be left
        item.quantity -= quantity

        // create a new item with the quantity to be sold, so we can use it
        // below to detm the value of the quantity sold
        itemToSell = createGroupableItem(item.type, quantity)
    }
    else {
        // remove the item from this player's inventory (which is where it should be at this point
        // whether it was equipped above or not)
        items.inventory.tryToRemoveItem(itemToSell)
    }

    // add the price to this player's gold
    val price = buyer.getAdjustedItemOffering(itemToSell,
        itemToSell.getValue(ItemValueContext.SELL))
    items.inventory.addGold(price)

    onItemLost(itemToSell, ItemLostReason.sold)

    area.onSoundIssued(Sounds.itemSold, location)

    // if the buyer leaves after making a purchase
    if (buyer.leavesAfterPurchase) {
        // report that the buyer is leaving
        reportToListeners { it.onBuyerLeft() }
    }

    WorkThread.queueTask { buyer.onItemBought() }
}

