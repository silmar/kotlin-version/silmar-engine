package silmarengine.contexts.gameplay.entities.beings.extensions.damage

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

/**
 * Applies the given amount of healing to this being.
 */
fun Being.takeHealing(amount: Int) {
    // determine how much of the healing can be applied without healing
    // this being beyond full hit points
    val actual = Integer.min(amount, maxHitPoints - hitPoints)

    // if no healing can be applied, skip this
    if (actual <= 0) return

    // add a healing visual effect at the given location
    val effect = LightSourceVisualEffect("healing", PixelDistance(TileDistance(4)))
    area.addEntity(effect, location)

    // if this being is a player
    val pausePerHitPoint = 60
    if (this is Player) {
        // display the amount healed at this player's location
        onMessage("" + actual, MessageType.normal, actual * pausePerHitPoint)
    }

    // issue an accompanying sound
    area.onSoundIssued(Sounds.heal, location)

    // pause a bit for the effect to be seen
    sleepThread((actual * pausePerHitPoint).toLong())

    // apply the healing
    addHitPoints(actual)

    // remove this healing's visual effect from the area
    area.removeEntity(effect)
}

fun Being.healFully() {
    takeHealing(maxHitPoints - hitPoints)
}
