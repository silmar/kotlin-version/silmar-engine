package silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.updateEncumbrance
import silmarengine.contexts.gameplay.entities.items.Item

fun Player.onInventoryItemAdded(item: Item) {
    item.onHeldBy(this)

    reportToListeners { it.onItemAddedToInventory(item) }

    updateEncumbrance()
}

fun Player.onInventoryItemRemoved(item: Item) {
    reportToListeners { it.onItemRemovedFromInventory(item) }

    updateEncumbrance()
}

