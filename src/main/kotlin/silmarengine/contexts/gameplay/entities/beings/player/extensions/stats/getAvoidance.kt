package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider

fun Player.getAvoidance(base: Int): Int {
    // add each equipped item's avoidance modifier to the rating
    var avoidance = base
    val items = items.compressEquippedItems()
    items.forEach { item -> avoidance -= item.type.avoidanceModifier }

    val provider = PlayerAttributeModifierProvider
    avoidance -= provider.avoidanceModifierForAgility(attributeValues.agility)

    return avoidance
}