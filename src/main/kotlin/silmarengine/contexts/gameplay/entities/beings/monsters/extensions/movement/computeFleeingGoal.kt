package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.geometry.vectors.getAngleBetween
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInRange
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Return's this fleeing monster's next goal to which to flee.
 */
fun Monster.computeFleeingGoal(): PixelPoint? {
    // try this a certain number of times
    var goal: PixelPoint?
    val maxDistance = PixelDistance(maxMovementPoints * tileWidth)
    val maxPathDistance = PixelDistance(maxDistance.distance * 2)
    var furthestGoal: PixelPoint? = null
    var furthestDistance: PixelDistance? = null
    val maxTries = 10
    for (i in 0..maxTries) {
        // look for a random passible goal location to which to move
        goal =
            area.getRandomPassibleRectangleLocationInRange(location, size, maxDistance, 50,
                type.movementType, false)

        // if the call above could not produce any potential goal location, don't
        // try anymore, as that call makes a lot of attempts
        if (goal == null) break

        // if the goal location is on the same side of this monster as the location
        // of the flee-from-player
        if (getAngleBetween(getUnitVector(location, goal),
                getUnitVector(location, fleeFromPlayer!!.location)) < Math.PI / 2) {
            // the goal is invalid
            continue
        }

        // if a path doesn't exist to the goal determined above
        val adjacencyGraph = area.adjacencyGraph
        if (adjacencyGraph != null && !adjacencyGraph.getPathExists(location, goal,
                maxPathDistance, movementType.allowsWorkingDoors).pathExists) {
            // the goal is invalid
            continue
        }

        // if we make it here, the goal is valid

        // if the distance from the monster to this goal is the furthest so far
        val distance = getDistance(location, goal)
        if (furthestGoal == null || distance.distance > furthestDistance!!.distance) {
            // remember this goal as the furthest (and therefore, best)
            furthestGoal = goal
            furthestDistance = distance
        }
    }

    // use the furthest goal found as this monster's goal
    goal = furthestGoal

    return goal
}

