package silmarengine.contexts.gameplay.entities.beings.monsters

import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance

/**
 * A monster that emits light.
 */
open class LightSourceMonster : Monster1(), LightSource {
    override val isEmittingLight: Boolean
        get() = true

    override val lightRadius: PixelDistance
        get() = type.lightRadius!!
}