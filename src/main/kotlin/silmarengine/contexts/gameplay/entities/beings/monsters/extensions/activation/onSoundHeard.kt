package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.contains
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.sounds.Sound
import silmarengine.sounds.Sounds

fun Monster.onSoundHeard(sound: Sound, location: PixelPoint) {
    // if the sound came from this monster, ignore it
    if (location == this.location) return

    // if the sound doesn't activate monsters, ignore it
    if (!sound.activatesMonsters) return

    // if the sound is one of a few common types, and no player is at the sound's location,
    // or the sound's location can't be seen by this monster, then ignore it
    if ((sound == Sounds.movement || sound == Sounds.doorOpens || sound == Sounds.doorCloses) && (area.player?.contains(
            location) == true || !isInLOSOf(location))) return

    // if this monster's location is lit, and the sound's location isn't lit,
    // then ignore it
    val shouldIgnore = area.isLit(this.location).isHalfOrMoreLit && !area.isLit(location,
        this.location).isHalfOrMoreLit
    if (shouldIgnore) return

    // activate this monster
    if (!isActivated) activate()

    seekLocationIfNotFocusedOnPlayer(location)
}

