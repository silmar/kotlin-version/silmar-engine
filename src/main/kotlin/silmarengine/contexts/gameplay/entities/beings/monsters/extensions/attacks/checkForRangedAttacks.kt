package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.beingmovement.domain.model.BMCheckForRangedAttacksResult
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextTo
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit
import silmarengine.util.math.randomInt

class CheckForRangedAttacksResult(
    override val madeAttack: Boolean, override val couldMakeAttack: Boolean
) : BMCheckForRangedAttacksResult

private typealias Result = CheckForRangedAttacksResult

private val falseResult = Result(false, false)

/**
 * Has this monster check to see if it is time to try to perform ranged attacks,
 * and if it is, has it do so (if it can).
 */
fun Monster.checkForRangedAttacksExt(): CheckForRangedAttacksResult {
    // if this monster is not focused on the player, make no attacks
    val player = if (isFocusedOnPlayer) (area.player ?: return falseResult)
    else return falseResult

    // if this monster isn't fleeing, is activated, has ranged attacks, feels it's time
    // to use them, and is in range of the player (but is not in melee range,
    // unless it has no melee attacks)
    if (!isFleeing && isActivated && type.rangedAttacks != null && isInRange(
            player.location) && (!isNextTo(player) || type.meleeAttacks == null)) {
        // if this monster needs a light to see, or is currently lit
        if (this is LightSource || area.isLit(location).isHalfOrMoreLit) {
            // if the player is not lit, ranged attacks are prevented
            if (!area.isLit(player.location, location).isHalfOrMoreLit) {
                return falseResult
            }
        }

        // if a random chance says it's time for this monster to execute a ranged attack;
        // note that we remember that we've made it to this test this turn,
        // so that no more ranged attacks by this monster will be considered
        // for the rest of the turn's duration
        if (randomInt(1, 10) <= type.rangedAttackFrequency) {
            makeRangedAttacks(player)

            endTurn()

            return Result(true, true)
        }

        return Result(false, true)
    }

    return falseResult
}