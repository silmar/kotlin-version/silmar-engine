package silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.ItemLostReason
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onItemLost

/**
 * Has this player lose a non-groupable item chosen at random from its inventory, for
 * the given reason.
 */
fun Player.loseRandomNonGroupableInventoryItem(reason: ItemLostReason) {
    // if there are any non-groupable items in this player's inventory
    val groupableItems = items.filterInventory { !it.type.isGroupable }
    if (groupableItems.isNotEmpty()) {
        // remove a random such item from the inventory
        val item = groupableItems.random()
        items.inventory.tryToRemoveItem(item)
        onItemLost(item, 1, reason)
    }
}

