package silmarengine.contexts.gameplay.entities.beings.player.extensions.gold

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.tiles.tileDistance
import kotlin.math.min

/**
 * Removes the given amount of gold from this player's inventory.  If the inventory
 * doesn't contain enough to cover the amount, the gold will be taken from gold-coinage
 * items at or very close to this player's location, which are presumed to exist and contain
 * the remainder.
 */
fun Player.removeGold(amount: Int) {
    // remove whichever number is smaller of this player's gold: the given amount, or the
    // amount currently being held
    val gold = items.inventory.getFirstItemOfType(GroupableItemTypes.gold) as Item?
    val held = gold?.quantity ?: 0
    if (gold != null) gold.quantity -= min(amount, held)

    // if the amount held wasn't enough to cover the given amount
    var remainder = amount - held
    if (remainder > 0) {
        // for each item at or very close to this player's location
        val itemEntities = area.items.getEntitiesInRange(location, tileDistance)
        itemEntities.filter { it.item.isOfType(GroupableItemTypes.gold) }.forEach { itemEntity ->
            // remove whichever number is smaller of this item's gold: the remainder,
            // or the item's quantity
            val item = itemEntity.item
            val itemQuantity = item.quantity
            val newRemainder = remainder - itemQuantity
            item.quantity -= min(remainder, itemQuantity)
            remainder = newRemainder

            // if this item covered the remainder, then we are done
            if (remainder <= 0) return
        }
    }
}

