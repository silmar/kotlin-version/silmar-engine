package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.incrementBaseAttribute
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes

/**
 * Informs this player that it has just used a potion-of-<attribute>.
 */
fun Player.onPotionOfAttributeUsed(potion: Item) {
    // detm which attribute the potion affects
    val attribute = when (potion.type) {
        NonGroupableItemTypes.potionOfStrength -> PlayerAttributes.strength
        NonGroupableItemTypes.potionOfIntelligence -> PlayerAttributes.intelligence
        NonGroupableItemTypes.potionOfJudgement -> PlayerAttributes.judgement
        NonGroupableItemTypes.potionOfAgility -> PlayerAttributes.agility
        else -> PlayerAttributes.endurance
    }

    // do a power discharge
    performPowerDischargeEffect(this)

    // increment that attribute
    incrementBaseAttribute(attribute)
}

