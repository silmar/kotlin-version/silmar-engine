package silmarengine.contexts.gameplay.entities.beings.player

/**
 * The flyweight player attributes.
 */
object PlayerAttributes {
    val strength = PlayerAttribute("strength")
    val intelligence = PlayerAttribute("intelligence")
    val judgement = PlayerAttribute("judgement")
    val agility = PlayerAttribute("agility")
    val endurance = PlayerAttribute("endurance")
}