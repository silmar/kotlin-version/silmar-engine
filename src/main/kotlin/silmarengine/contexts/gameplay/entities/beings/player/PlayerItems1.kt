package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.hasAttributesToUseItem
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.updateEncumbrance
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.items.types.itemTypesByName
import silmarengine.contexts.playeritemsmanagement.domain.listeners.EquippedItemsListener
import silmarengine.contexts.playeritemsmanagement.domain.listeners.HandsConfigsListener
import silmarengine.contexts.playeritemsmanagement.domain.listeners.PlayerItemsListener
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMPlayerItems1
import java.io.IOException
import java.io.ObjectInputStream
import java.io.Serializable
import kotlin.math.max

class PlayerItems1(override val player: Player) : PlayerItems, Serializable {
    override val playerItems = PIMPlayerItems1(
        { type, quantity -> createGroupableItem(type as ItemType, quantity) }, { name ->
            itemTypesByName[name] ?: throw Exception("Item type not found")
        }, { player.hasAttributesToUseItem(it as Item) })

    override fun getEquippedItem(at: EquipLocation): Item? =
        playerItems.equippedItems.itemAt(at) as Item?

    override fun getInventoryItems(): List<Item> = inventory.items.map { it as Item }

    override fun filterInventory(predicate: (item: Item) -> Boolean): List<Item> =
        inventory.items.filter { predicate(it as Item) }.map { it as Item }

    override var maxEncumbrance = 0
        private set(value) {
            field = value
            player.reportVitalStatChange()
        }

    override var encumbrance = 0
        private set(value) {
            field = value
            player.reportVitalStatChange()
        }

    init {
        setupListeners()
    }

    private fun setupListeners() {
        playerItems.equippedItems.addListener(object : EquippedItemsListener {
            override fun onEquippedItemChanged(
                location: EquipLocation, from: PIMItem?, to: PIMItem?
            ) = onEquippedItemChanged(to as Item?)
        })
        playerItems.handsConfigs.addListener(object : HandsConfigsListener {
            override fun onConfigChanged(index: Int) {
                player.reportToListeners { it.onHandsConfigChanged() }
            }
        })
        playerItems.addListener(object : PlayerItemsListener {
            override fun onCursedItemCouldNotBeRemoved() {
                player.onMessage(
                    "You have a cursed item equipped that you cannot unequip!",
                    MessageType.error)
            }
        })
    }

    override fun equipItem(item: Item?, equipLocation: EquipLocation) {
        playerItems.equippedItems.equip(item, equipLocation)
    }

    private fun onEquippedItemChanged(to: Item?) {
        player.updateAttributes()
        player.updateEncumbrance()
        updateGameSpecificPlayerOnItemEquipped(player)
        player.reportToListeners { it.onItemEquipped(to) }
    }

    override fun updateMaxEncumbrance() {
        // adjust what would be the value for average strength and endurance ratings by
        // the modifiers for this player's strength and endurance
        val provider = PlayerAttributeModifierProvider
        val values = player.attributeValues
        maxEncumbrance = max(0,
            (10 + 10) / 2 * 20 + provider.maxEncumbranceModifierForStrength(
                values.strength) + provider.maxEncumbranceModifierForEndurance(
                values.endurance))
    }

    override fun onEncumbranceComputed(amount: Int) {
        encumbrance = amount
    }

    override val numEquippedCursedItems: Int
        get() = compressEquippedItems().count { it.type.isCursed }

    override val equippedUnidentifiedCursedItemsTotalIdentificationValue: Int
        get() = compressEquippedItems().filter { !it.isIdentified && it.type.isCursed }.sumBy {
            it.getValue(ItemValueContext.IDENTIFY)
        }
}

@ExtensionPoint
var updateGameSpecificPlayerOnItemEquipped: (player: Player) -> Unit = { _ -> }