package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

/**
 * Returns the standard bonus or penalty associated within the given attribute score.
 */
fun getStandardModifier(score: Int): Int {
    return (score - 10) / 2
}

/**
 * Returns a dampened version of the standard modifier for the given score.
 */
fun getDampenedStandardModifier(score: Int): Int {
    return getStandardModifier(score) * 3 / 5
}

