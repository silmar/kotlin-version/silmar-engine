package silmarengine.contexts.gameplay.entities.beings.player.extensions.experience

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

fun Player.onLevelChange(oldValue: Int, newValue: Int) {
    // detm whether this call is merely for the initialization of this value,
    // as it will affect which parts are done of those below
    val forInit = oldValue == 0

    // if a level or levels were lost
    var levelGained = false
    if (newValue < oldValue) {
        area.onSoundIssued(Sounds.levelLost, location)
        onMessage("You lose a level!")
    }
    // else, if a level or levels were gained, and this isn't an initialization call
    else if (newValue > oldValue && !forInit) {
        levelGained = true

        havePlayerGainGameSpecificLevels(this, newValue)

        // do a power discharge at the player
        onMessage("You gain a level!")
        performPowerDischargeEffect(this)
    }

    onGameSpecificPlayerLevelChanged(this, forInit, levelGained)

    // if this isn't an initialization call
    if (!forInit) {
        // report this level change to listeners of this player's events
        reportToListeners { it.onLevelChanged() }
    }

    levelValues.fillOutHitPointsByLevel(newValue)

    updateMaxHitPoints()
    updateAttributes()
    updateGameSpecificPlayerOnLevelChange(this)
}

@ExtensionPoint
var havePlayerGainGameSpecificLevels: (player: Player, toLevel: Int) -> Unit = { _, _ -> }

@ExtensionPoint
var onGameSpecificPlayerLevelChanged: (player: Player, forInit: Boolean, levelGained: Boolean) -> Unit =
    { _, _, _ -> }

@ExtensionPoint
var updateGameSpecificPlayerOnLevelChange: (player: Player) -> Unit = { _ -> }
