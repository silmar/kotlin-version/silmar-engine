package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Informs this player that it has bought a remove-cursed-items service from the given
 * seller.
 *
 * @return      Whether the service was performed.
 */
fun Player.onRemoveCursedItemsService(seller: Entity): Boolean {
    // for each cursed equipped item
    var hadCursedItem = false
    items.compressEquippedItems().filter { it.type.isCursed }.forEach { item ->
        performPowerDischargeEffect(seller)
        performPowerDischargeEffect(this)

        // un-equip this item, so that this player may manually remove it from
        // its inventory
        tryToUnequipItem(item, false, true)

        hadCursedItem = true
    }

    if (!hadCursedItem) {
        onMessage("You don't have any cursed items equipped!", MessageType.error)
        return false
    }

    return true
}

