package silmarengine.contexts.gameplay.entities.beings.player.extensions.harm

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds as EngineSounds

fun Player.dieAsPlayer(dieAsBeing: () -> Unit) {
    // remember this value, as it will be changed by super.doDie() below
    val area = area

    area.onSoundIssued(EngineSounds.playerDead, location)

    if (isGameSpecificPlayerDeathAverted(this)) return

    dieAsBeing()

    // create a bones terrain where this player died
    val bones = createTerrain(TerrainTypes.bones)
    area.addEntity(bones, location)

    reportToListeners { it.onBecameBones(bones) }
    reportToListeners { it.onDead() }

    // stop the current game
    Game.currentGame!!.stopProcessingTurns()
}

@ExtensionPoint
var isGameSpecificPlayerDeathAverted: (player: Player) -> Boolean = { _ -> false }