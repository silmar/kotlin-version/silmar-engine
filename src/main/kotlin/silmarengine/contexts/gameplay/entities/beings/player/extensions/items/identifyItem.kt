package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.removeGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.tallyGold
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * Has this player try to pay to have the given item identified.
 */
fun Player.identifyItem(item: Item, price: Int) {
    // if this player doesn't have enough gold
    if (tallyGold(true) < price) {
        // don't allow the identification
        onMessage(Player.notEnoughGoldMessage, MessageType.error)
        return
    }

    area.onSoundIssued(Sounds.itemIdentified, location, true)

    // inform the item
    item.identify()

    removeGold(price)
}

