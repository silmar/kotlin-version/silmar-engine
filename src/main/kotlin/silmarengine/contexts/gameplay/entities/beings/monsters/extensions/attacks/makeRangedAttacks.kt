package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.makeRangedAttack
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.modifyTarget
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onAttackedAndMissed
import silmarengine.contexts.gameplay.entities.beings.monsters.Hider
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.effects.performAttackEffect
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.util.math.randomInt

/**
 * Has this monster make all of its available ranged attacks on the given player, who
 * is assumed to be within range of those attacks.
 */
fun Monster.makeRangedAttacks(player: Player) {
    // if this monster doesn't make ranged attacks
    val attacks = type.rangedAttacks ?: return

    // for each ranged attack this monster can make
    val playerLocation = player.location
    attacks.forEach { attack ->
        // give the player a short description of the attack if it's in LOS
        area.player?.receiveMessageIfInLOSOf(location,
            type.name + ": " + attack.description)

        // make this ranged attack on the given player
        val target =
            modifyTarget(location, playerLocation, attack.maxRange, attack.accuracy)
        val result = makeRangedAttack(this, area, location, target, attack.maxRange,
            attack.attackType, numberToHitDefense0)

        // if the attack hit a being
        var damage = 0
        if (result!!.hit) {
            // detm the damage
            damage = randomInt(attack.minDamage, attack.maxDamage)
        }

        // depict the attack
        val impact = if (result.beingImpacted != null) result.beingImpacted!!.location
        else result.impactLocation!!
        if (!depictGameSpecificMonsterRangedAttack(area, location, impact,
                attack)) area.performAttackEffect(location, impact, attack.attackType)

        // if the attack hit a being
        val being = result.beingImpacted
        if (result.hit) {
            // inform the being of the hit
            being?.onHitByMonster(this, attack, damage, result.hitBy)

            onMonsterAttackHitBeing(this, being!!, attack, result.hitBy)
        }
        else being?.onAttackedAndMissed()

        if (this is Hider) becomeVisible()
    }
}

/**
 * @return  Whether the attack was depicted (vs. should instead be depicted by the caller).
 */
@ExtensionPoint
var depictGameSpecificMonsterRangedAttack: (area: Area, from: PixelPoint, to: PixelPoint, attack: MonsterAttack) -> Boolean =
    { _, _, _, _ -> false }