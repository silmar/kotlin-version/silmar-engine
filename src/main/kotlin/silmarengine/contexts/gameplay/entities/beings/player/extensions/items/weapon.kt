package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

/**
 * The primary weapon currently employed by this player.
 */
val Player.weapon: Weapon get() = run {
    // if there's a weapon in the right hand
    val rightHandItem = items.getEquippedItem(EquipLocation.RIGHT_HAND)
    if (rightHandItem != null && rightHandItem is Weapon) {
        // return it
        return rightHandItem
    }

    // if there's a weapon in the left hand
    val leftHandItem = items.getEquippedItem(EquipLocation.LEFT_HAND)
    if (leftHandItem != null && leftHandItem is Weapon) {
        // return it
        return leftHandItem
    }

    return Weapon.hand
}

/**
 * The offhand weapon currently employed by this player.
 */
val Player.offhandWeapon: Weapon? get() = run {
    // if the same weapon is in both hands (and that weapon isn't
    // the hands themselves)
    val leftHandItem = items.getEquippedItem(EquipLocation.LEFT_HAND)
    val rightHandItem = items.getEquippedItem(EquipLocation.RIGHT_HAND)
    if (leftHandItem === rightHandItem && leftHandItem != null) {
        return null
    }

    // if there's a separate weapon in each hand
    if (leftHandItem != null && leftHandItem is Weapon && rightHandItem != null && rightHandItem is Weapon) {
        // return the weapon in the left-hand
        return leftHandItem
    }

    // if the player is using a weapon/hand or hand/hand configuration
    if ((leftHandItem != null && leftHandItem is Weapon && rightHandItem == null) || (rightHandItem != null && rightHandItem is Weapon && leftHandItem == null) || (leftHandItem == null && rightHandItem == null)) {
        return Weapon.hand
    }

    return null
}
