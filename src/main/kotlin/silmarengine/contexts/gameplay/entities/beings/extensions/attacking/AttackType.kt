package silmarengine.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.sounds.Sound

/**
 * Describes a type of attack.
 */
open class AttackType(
    /**
     * The filename of the image to display for an attack of this type.
     */
    val imageName: String,
    /**
     * The sound to issue for an attack of this type.
     */
    val sound: Sound,
    /**
     * The minimum number of milliseconds between movements of the animation-effect for this
     * attack type.
     */
    val minAnimationDelay: Int = defaultAttackTypeMinAnimationDelay,
    /**
     * How far (in tiles, and if at all) an attack of this type shines light as it
     * travels from source to target.
     */
    val lightTileRadius: Int = 0
)

const val defaultAttackTypeMinAnimationDelay = 15
