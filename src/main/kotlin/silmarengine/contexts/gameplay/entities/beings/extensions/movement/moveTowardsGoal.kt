package silmarengine.contexts.gameplay.entities.beings.extensions.movement

import silmarengine.contexts.beingmovement.domain.functions.MoveTowardsGoalListener
import silmarengine.contexts.beingmovement.domain.functions.MoveTowardsGoalResult
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.beingmovement.domain.functions.moveTowardsGoal as emMoveTowardsGoal

fun Being.moveTowardsGoal(
    goal: PixelPoint, movementPoints: Float, listener: MoveTowardsGoalListener?,
    shouldSeekDetours: Boolean
): MoveTowardsGoalResult =
    emMoveTowardsGoal(goal, movementPoints, listener, shouldSeekDetours,
        { from_, to_, use ->
            getUnitVector(from_ as PixelPoint, to_ as PixelPoint, use)
        }, { from_, to_, use ->
            getDistance(from_ as PixelPoint, to_ as PixelPoint, use as PixelDistance)
        })