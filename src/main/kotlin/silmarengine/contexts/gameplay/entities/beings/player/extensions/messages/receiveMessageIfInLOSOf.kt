package silmarengine.contexts.gameplay.entities.beings.player.extensions.messages

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

fun Player.receiveMessageIfInLOSOf(
    of: PixelPoint, message: String, type: MessageType = MessageType.normal,
    duration: Int = 0
) {
    if (!isInLOSOf(of)) return

    val adjustedType = if (type == MessageType.normal) LocationalMessageType(of)
    else type
    onMessage(message, adjustedType, duration)
}