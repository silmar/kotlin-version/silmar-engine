package silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.specializations.Torch
import silmarengine.contexts.playeritemsmanagement.domain.model.Inventory

/**
 * Returns the first lit torch (if any) found within this inventory.
 */
val Inventory.firstLitTorch: Torch?
    get() = items.firstOrNull {
        val item = it as Item
        item is Torch && item.isLit
    } as Torch?

