package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes

/**
 * Makes this player lose a ration if it has one, for the given reason.
 *
 * @return      Whether a ration was actually lost.
 */
fun Player.loseRation(reason: ItemLostReason): Boolean {
    // if this player's inventory contains rations
    val rations = items.getFirstInventoryItemOfType(GroupableItemTypes.ration)
    if (rations != null) {
        // lose one
        rations.quantity--
        onItemLost(rations, 1, reason)
        return true
    }

    return false
}
