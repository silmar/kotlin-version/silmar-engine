package silmarengine.contexts.gameplay.entities.beings.player.extensions.messages

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Types used to classify messages received by this player.
 */
open class MessageType {

    companion object {
        /**
         * The flyweight message types.
         */
        var normal = MessageType()
        var error = MessageType()
        var status = MessageType()
        var important = MessageType()
        var gameFinished = MessageType()
    }
}

/**
 * A message type that indicates the message is emanating from a location other than
 * that of this player.
 *
 * @param location  The location from which this message is emanating.
 */
class LocationalMessageType(var location: PixelPoint) : MessageType()

/**
 * Informs this player that it has received a message of the given type that should
 * be displayed for the given duration (in ms, and if applicable).
 */
fun Player.onMessage(
    message: String, type: MessageType = MessageType.normal, duration: Int = 0
) {
    reportToListeners { it.onMessageReceived(message, type, duration) }
}

