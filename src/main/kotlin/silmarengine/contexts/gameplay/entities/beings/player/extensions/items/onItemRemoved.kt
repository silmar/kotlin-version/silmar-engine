package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item

/**
 * Informs this player that the given item was removed from it.
 */
fun Player.onItemRemoved(item: Item) {
    // if the item is equipped on this player
    if (items.isItemEquipped(item)) {
        // unequip it
        tryToUnequipItem(item, false, true)
    }

    // the item must now be in this player's inventory, so remove it from there
    items.inventory.tryToRemoveItem(item)

    onItemLost(item, null)
}

