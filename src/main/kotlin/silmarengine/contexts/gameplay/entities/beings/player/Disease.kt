package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.decrementBaseAttribute
import java.io.Serializable
import java.util.*

/**
 * A disease that may afflict a player.
 *
 * @param turnInterval  How many game turns elapse before the next time this disease affects its host.
 */
class Disease(
    val name: String, val attributeAffected: PlayerAttribute, val turnInterval: Int
) : Serializable {
    init {
        diseases.add(this)
    }

    fun affectPlayer(player: Player) {
        if (!affectPlayerAsGameSpecificDisease(this,
                player)) player.decrementBaseAttribute(attributeAffected)
    }

    override fun equals(other: Any?): Boolean {
        return other === this || (other is Disease && other.name == name)
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    companion object {
        /**
         * A list of all the diseases.
         */
        val diseases = ArrayList<Disease>()
    }
}

/**
 * @return  Whether this call resulted in affecting the given player.
 */
@ExtensionPoint
var affectPlayerAsGameSpecificDisease: (disease: Disease, player: Player) -> Boolean =
    { _, _ -> false }

/**
 * Describes an affliction of a disease that is currently affecting a player.
 */
class DiseaseAffliction : Serializable {
    /**
     * The disease afflicting this player.
     */
    var disease: Disease? = null

    /**
     * The game turn upon which this affliction started.
     */
    var turnAfflicted: Int = 0
}

