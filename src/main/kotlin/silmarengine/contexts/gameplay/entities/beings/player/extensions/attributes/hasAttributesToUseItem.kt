package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.Item

/**
 * Returns whether this player's attributes are good enough to allow its usage of the
 * given item.
 */
fun Player.hasAttributesToUseItem(item: Item): Boolean {
    // for each player attribute
    PlayerAttribute.attributes.forEach { attribute ->
        // if this player's score for this attribute is too low to use the item
        val requirement = item.type.getAttributeRequirement(attribute)
        if (requirement > attributeValues.getAttribute(attribute)) {
            // inform the player
            onMessage("You need a $requirement ${attribute.name} to use this item.",
                MessageType.important)

            return false
        }
    }

    return true
}

