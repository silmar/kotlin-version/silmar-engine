package silmarengine.contexts.gameplay.entities.beings.monsters.types

import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.types.EntityType
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.sounds.Sound

interface MonsterType : EntityType {
    /**
     * An abstract rating of overall challenge to defeat monsters of this type,
     * compared to that of other types.
     */
    val powerRating: Int

    /**
     * The alignment of monsters of this type.
     */
    val alignment: Alignment

    /**
     * How much experience is earned for vanquishing monsters of this type.
     */
    val experienceValue: Int

    /**
     * How many movement points a monster of this type receives per game turn.
     */
    val movementPoints: Int

    /**
     * Whether monsters of this type are considered living dead (i.e. undead).
     */
    val isLivingDead: Boolean

    /**
     * The melee attacks that may be employed by monsters of this type.
     */
    val meleeAttacks: Array<MonsterAttack>?

    /**
     * The ranged attacks that may be employed by monsters of this type.
     */
    val rangedAttacks: Array<MonsterAttack>?

    /**
     * A number from 0 to 10 that describes how often monsters of this type use ranged
     * attacks (when possible) instead of melee attacks.  0 means never, 10 means always.
     */
    val rangedAttackFrequency: Int

    /**
     * The mode of movement employed by monsters of this type.
     */
    val movementType: MovementType

    /**
     * The weapon-plus necessary to hit monsters of this type.
     */
    val weaponPlusNeededToHit: Int

    /**
     * The modifier to avoidance rolls that monsters of this type always receive.
     */
    val avoidanceModifier: Int

    /**
     * Whether monsters of this type are less vulnerable to edged weapons (vs. other
     * types of weapons).
     */
    val isLessVulnerableToEdgedWeapons: Boolean

    /**
     * The sound that monsters of this type might sometimes make as they move.
     */
    val sound: Sound?

    /**
     * The probability that a monster of this type will issue this sound when moving (vs. the normal movement sound).
     */
    val soundFrequency: Float

    /**
     * Whether monsters of this type cannot move.
     */
    val isStationary: Boolean

    /**
     * The defense rating of monsters of this type. Zero means no defense.
     */
    val defense: Int

    /**
     * Whether a monster of this type must see a player to be able use its pre-movement
     * power.
     */
    val mustSeePlayerToUsePower: Boolean

    /**
     * The distance that monsters of this type shed light, likely due to needing a
     * torch to see in the dark.
     */
    val lightRadius: PixelDistance?
}

/**
 * The relative path to the image files for monster types.
 */
const val monsterTypeImagePath = "monsters/"
