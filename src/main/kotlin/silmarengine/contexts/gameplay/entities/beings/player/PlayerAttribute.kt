package silmarengine.contexts.gameplay.entities.beings.player

import java.io.Serializable
import java.util.*

/**
 * A measure of some aspect of a player's characteristics.
 */
class PlayerAttribute(val name: String) : Serializable {
    /**
     * The index of this attribute, amongst all others.
     */
    val index: Int

    init {
        attributes.add(this)
        index = attributes.size - 1
    }

    override fun equals(other: Any?): Boolean {
        return other is PlayerAttribute && other.index == index
    }

    override fun hashCode(): Int {
        return index
    }

    companion object {
        /**
         * A list of all attributes.
         */
        var attributes = ArrayList<PlayerAttribute>()

        /**
         * Returns an attribute at random from the list of all attributes.
         */
        val randomAttribute: PlayerAttribute get() = attributes.random()

        /**
         * Returns the attribute of the given index within the attributes list.
         */
        fun getAttribute(index: Int): PlayerAttribute = attributes[index]
    }
}