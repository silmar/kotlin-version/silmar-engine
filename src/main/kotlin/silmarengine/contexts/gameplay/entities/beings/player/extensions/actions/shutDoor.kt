package silmarengine.contexts.gameplay.entities.beings.player.extensions.actions

import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.geometry.asPixelLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.extensions.tiles.closeDoor
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileSize

/**
 * Has this player shut the door (if any) at the given pixel-location, which is assumed
 * to be within this player's reach.
 */
fun Player.shutDoor(location: PixelPoint) {
    // don't allow shutting the door if the door's tile area is not passible to this player
    // (likely due to the presence of a being on the door tile)
    if (!area.isRectanglePassible(
            location.asTileLocation().asPixelLocation(),
            tileSize, MovementType.WALKING, true, this, null, false).passible) return

    area.closeDoor(location, true)
}

