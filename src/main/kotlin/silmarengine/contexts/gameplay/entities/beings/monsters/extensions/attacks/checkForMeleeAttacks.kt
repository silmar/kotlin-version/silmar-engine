package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextTo

/**
 * Has this monster check to see if it can perform melee attacks at this juncture,
 * and if it can, has it do so.
 *
 * @return      Whether melee attacks were performed.
 */
fun Monster.checkForMeleeAttacksExt(): Boolean {
    if (!canCurrentlyMakeMeleeAttacks) return false

    // if this monster is not focused on the player, make no attacks
    val player = if (isFocusedOnPlayer) (area.player ?: return false) else return false

    // if this monster isn't fleeing, is activated, has melee attacks,
    // and is in melee attack range of the player
    if (!isFleeing && isActivated && type.meleeAttacks != null && isNextTo(player)) {
        makeMeleeAttacks(player)

        endTurn()

        return true
    }

    return false
}

