package silmarengine.contexts.gameplay.entities.beings

/**
 * The different types of moral orientations of beings.
 */
class Alignment {

    companion object {
        /**
         * The flyweight alignments.
         */
        val good = Alignment()
        val neutral = Alignment()
        val evil = Alignment()
    }
}
