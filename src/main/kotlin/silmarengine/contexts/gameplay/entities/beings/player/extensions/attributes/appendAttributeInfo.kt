package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider
import silmarengine.util.html.addLineBreakPlusIndent
import silmarengine.util.text.formatModifier

enum class Attribute {
    STRENGTH, INTELLIGENCE, JUDGEMENT, AGILITY, ENDURANCE
}

fun appendAttributeInfo(attribute: Attribute, value: Int, to: StringBuffer) {
    val provider = PlayerAttributeModifierProvider
    fun append(text: String) = to.append(text)
    when (attribute) {
        Attribute.STRENGTH -> {
            addLineBreakPlusIndent(to)
            append("melee attack to-hit modifier: ")
            append(formatModifier(provider.toHitModifierForStrength(value)))

            addLineBreakPlusIndent(to)
            append("melee attack damage modifier: ")
            append(formatModifier(provider.damageModifierForStrength(value)))

            addLineBreakPlusIndent(to)
            append("max encumbrance modifier: ")
            append(formatModifier(provider.maxEncumbranceModifierForStrength(value)))

            addLineBreakPlusIndent(to)
            append("base movement rate modifier: ")
            append(
                formatModifier(provider.maxMovementPointsModifierForStrength(value)))
        }
        Attribute.AGILITY -> {
            addLineBreakPlusIndent(to)
            append("ranged attack to-hit modifier: ")
            append(
                formatModifier(provider.rangedAttackToHitModifierForAgility(value)))

            addLineBreakPlusIndent(to)
            append("base movement rate modifier: ")
            append(
                formatModifier(provider.maxMovementPointsModifierForAgility(value)))

            addLineBreakPlusIndent(to)
            append("to-hit penalty (regular/offhand) with two weapons: ")
            append(provider.toHitWithTwoWeaponsModifierForAgility(value, false).toString())
            append("/")
            append(provider.toHitWithTwoWeaponsModifierForAgility(value, true).toString())

            addLineBreakPlusIndent(to)
            append("defense modifier: ")
            append(formatModifier(provider.defenseModifierForAgility(value)))

            addLineBreakPlusIndent(to)
            append("avoidance check modifier: ")
            append(formatModifier(provider.avoidanceModifierForAgility(value)))
        }
        Attribute.ENDURANCE -> {
            addLineBreakPlusIndent(to)
            append("hit points per level modifier: ")
            append(formatModifier(provider.hitPointsModifierForEndurance(value)))

            addLineBreakPlusIndent(to)
            append("max encumbrance modifier: ")
            append(
                formatModifier(provider.maxEncumbranceModifierForEndurance(value)))
        }
        else -> Unit
    }
    appendGameSpecificAttributeInfo(attribute, value, to)
}

@ExtensionPoint
var appendGameSpecificAttributeInfo: (attribute: Attribute, value: Int, to: StringBuffer) -> Unit =
    { _, _, _ -> }
