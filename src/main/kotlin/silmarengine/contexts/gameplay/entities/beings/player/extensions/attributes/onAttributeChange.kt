package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes

fun Player.onAttributeChange(attribute: PlayerAttribute) {
    reportToListeners { it.onAttributeChanged(attribute) }

    when (attribute) {
        PlayerAttributes.strength -> items.updateMaxEncumbrance()
        PlayerAttributes.endurance -> {
            updateMaxHitPoints()
            items.updateMaxEncumbrance()
        }
    }

    updateGameSpecificPlayerForAttributeChange(this, attribute)
}

@ExtensionPoint
var updateGameSpecificPlayerForAttributeChange: (player: Player, attribute: PlayerAttribute) -> Unit =
    { _, _ -> }