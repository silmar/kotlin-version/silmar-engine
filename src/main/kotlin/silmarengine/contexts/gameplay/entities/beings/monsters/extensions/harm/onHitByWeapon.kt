package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.extensions.affectByWeaponPlusNeededToHit
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForEdgedWeapons
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

fun Monster.onHitByWeapon(weapon: Weapon, wielder: Being, damageAmount: Int) {
    val damage = Damage(damageAmount, weapon.weaponType.damageForm, wielder, weapon)

    damage.affectByWeaponPlusNeededToHit(type.weaponPlusNeededToHit)

    // if the given damage amount was > 0, but has since been modified to 0, and the
    // wielder is a player
    if (damageAmount != 0 && damage.amount == 0 && wielder is Player) {
        // inform the player that its weapon might not be able to damage this monster
        wielder.onMessage("Your weapon may not be able to damage this monster.",
            MessageType.status)
    }

    if (type.isLessVulnerableToEdgedWeapons) {
        damage.affectForEdgedWeapons(0.5f)
    }

    takeDamage(damage)
}

