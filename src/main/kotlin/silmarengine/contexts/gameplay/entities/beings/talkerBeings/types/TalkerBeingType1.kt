package silmarengine.contexts.gameplay.entities.beings.talkerBeings.types

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.createTalkerBeing
import silmarengine.contexts.gameplay.entities.types.EntityType1
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.generation.Stockable
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

@Suppress("LeakingThis")
open class TalkerBeingType1(name: String, imageName: String, frequency: Int,
    override val movementPoints: Int, override val greeting: String) : TalkerBeingType,
    EntityType1(name, talkerBeingTypeImagePath + imageName, frequency), Stockable {
    init {
        determineSize()

        talkerBeingTypes.add(this)
        nameToTalkerBeingTypeMap[name] = this
    }

    /**
     * Returns whether the given talker-being type is the same as thie one.
     *
     * @param   other    The talker-being type to compare to this one.
     */
    override fun equals(other: Any?): Boolean {
        return other === this || (other is TalkerBeingType1 && other.name == name)
    }

    override fun isValidForArea(area: Area) = true

    override fun createStockInstance(): Entity {
        return createTalkerBeing(this)
    }

    override fun isPresenceLocationValid(area: Area, location: PixelPoint): Boolean {
        // if there is already a being at the given location
        if (getFirstEntityAt(location, area.beings) != null) {
            return false
        }

        // if the tile at the given location isn't passible or blocks los
        val tile = area.getTile(location)
        return !(!tile.isPassible || tile.blocksLOS)

    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}
