package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomFloat

/**
 * Returns the location of the nearest player to this monster, if the
 * this monster's adjacency graph says there is a path to it.
 */
fun Monster.computeGoalFromAdjacencyGraph(): PixelPoint? {
    // if the area's adjacency graph says there is a path between this monster and the
    // location of the nearest player, and a random chance which decreases
    // with the pixel-length of that path (linearly, from 1 to 0.3) says "yes"
    val player = area.player ?: return null
    var goal: PixelPoint? = null
    val playerLocation = player.location
    val maxAdjacencyPathDistance = PixelDistance(20 * tileWidth)
    val result = area.adjacencyGraph!!.getPathExists(location, playerLocation,
        maxAdjacencyPathDistance, true)
    if (result.pathExists && randomFloat < 1 - result.pathLength.distance / tileWidth * .015f) {
        // make the player's location this monster's goal
        goal = playerLocation
    }

    return goal
}

