package silmarengine.contexts.gameplay.entities.beings.player.extensions.attacks

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.isAHit
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.makeRangedAttack
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.modifyTarget
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onAttackedAndMissed
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm.onHitByWeapon
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.ItemLostReason
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onItemLost
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.computeDamageModifier
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.getAccuracy
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.getToHitModifier
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextTo
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.areas.extensions.effects.performAttackEffect
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.util.math.randomInt
import kotlin.math.max

/**
 * Has this player make an attack on the given pixel-location.
 */
fun Player.attack(on: PixelPoint) {
    val from = location

    // cache this value as it may change during this method due to this player's death
    val area = area

    // if this player is the first being found at the given location
    if (getFirstEntityAt(on, area.beings) === this) {
        // then abort this attack, which is likely the user's intention for
        // selecting this player as the target; the rest of the player's turn must also
        // be aborted, since attacks may already have been made this turn by this player
        endTurn()
        return
    }

    val adjustedOn = MutablePixelPoint(on)
    if (isConfused) adjustedOn.set(modifyLocationDueToConfusion(area, on, from))

    // if this player's weapon requires ammo
    val offhand = isCurrentAttackOffhand
    val weapon = if (!offhand) weapon else offhandWeapon
    if (weapon!!.requiresAmmo) {
        // if this player's inventory has none of that ammo
        val ammo = weapon.getAmmo(items.inventory)
        if (ammo == null) {
            // abort the attack
            onMessage("Out of ammunition!", MessageType.status)

            // if this player has made any previous attack this turn, end the turn, since
            // finishing his attacks is the only thing he is currently allowed to do, but
            // he can't finish them without ammo
            if (hasAttackedThisTurn) endTurn()

            return
        }

        // remove one of the ammo from this player's inventory
        ammo.quantity--
        onItemLost(ammo, 1, ItemLostReason.used)
    }

    // if this player has movement points left
    if (movementPoints > 0) {
        endMovementForThisTurn()
    }

    // if this player is making a melee attack
    val being: Being?
    var hit = false
    val impactLocation: PixelPoint?
    val weaponType = weapon.weaponType
    var attackType = weaponType.attackType
    if (!weaponType.isRanged) {
        impactLocation = adjustedOn

        // if there is a monster at the given location, and the monster is in
        // melee range of this player
        being = getFirstEntityAt(adjustedOn, area.monsters)
        if (being != null && isNextTo(being)) {
            // detm if the attack hits
            val roll = randomInt(1, 20)
            hit = when (roll) {
                1 -> false
                20 -> true
                else -> being.isAHit(roll + getToHitModifier(offhand,
                    being) - stats.numberToHitZeroDefense, attackType, this).hit
            }
        }
    }
    // otherwise, this is a ranged attack
    else {
        // detm the attack-type
        attackType = weaponType.attackType

        if (checkToAbortGameSpecificPlayerRangedAttack(this, weapon)) {
            onAttackMade()
            return
        }

        // detm the result of the attack
        val maxRange = weaponType.maxRange
        val modified = modifyTarget(from, adjustedOn, maxRange, getAccuracy(offhand))
        adjustedOn.set(modified)
        val result = makeRangedAttack(this, area, from, adjustedOn, maxRange, attackType,
            stats.numberToHitZeroDefense)
        hit = result!!.hit
        being = result.beingImpacted
        impactLocation = result.impactLocation
    }

    weapon.onMadeAttack()

    val isSurpriseAttack = being is Monster && !being.isActivated

    // if the attack is impacting on a location
    if (impactLocation != null) {
        // depict the attack with a visual effect
        area.performAttackEffect(from, impactLocation, attackType)
    }

    // if the attack is on a being
    if (being != null) {
        // if the attack hit
        var damage: Int
        if (hit) {
            // detm the base damage
            damage = randomInt(weaponType.minDamage, weaponType.maxDamage)

            // add this player's bonus to the damage
            damage += computeDamageModifier(offhand)

            // make sure the damage is still positive
            damage = max(damage, 1)

            // if the attack is a surprise
            if (isSurpriseAttack) {
                // double its damage, and activate the victim
                damage *= 2
                being as Monster
                being.activate()
            }

            if (being is Monster) being.onHitByWeapon(weapon, this, damage)

            onAttackHitVictim(being, damage, offhand)
        }
        else being.onAttackedAndMissed()
    }

    onAttackMade()
}

/**
 * @return  Whether the attack with the given weapon should be aborted.
 */
@ExtensionPoint
var checkToAbortGameSpecificPlayerRangedAttack: (player: Player, weapon: Weapon) -> Boolean =
    { _, _ -> false }