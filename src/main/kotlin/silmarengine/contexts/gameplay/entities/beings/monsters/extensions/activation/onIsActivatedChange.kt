package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

fun Monster.onIsActivatedChange(newValue: Boolean) {
    // if this monster is fleeing, don't allow its activation
    if (isFleeing && newValue) return

    // if now activated (and visible), this monster issues a sound which will
    // activate the others near it
    if (newValue && isVisible) {
        area.onSoundIssued(Sounds.monsterActivation, location, false)
    }
}

