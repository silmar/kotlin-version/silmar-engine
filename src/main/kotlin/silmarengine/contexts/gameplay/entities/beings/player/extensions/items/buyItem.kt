package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.removeGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.tallyGold
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * Has this player try to buy the given item.
 */
fun Player.buyItem(item: Item, price: Int) {
    // if this player doesn't have enough gold
    if (tallyGold(true) < price) {
        // don't allow the purchase
        onMessage(Player.notEnoughGoldMessage, MessageType.error)
        return
    }

    // if this player can't carry the added encumbrance of the item
    if (item.encumbrance > items.maxEncumbrance - items.encumbrance) {
        // don't allow the purchase
        onMessage("You can't carry that!", MessageType.error)
        return
    }

    // subtract the purchase cost from this player's gold
    if (price > 0) removeGold(price)

    // add a clone of the purchased item to this player's inventory
    val clone = createItem(item.type, item.condition)
    clone.quantity = item.quantity
    items.inventory.addItem(clone)

    area.onSoundIssued(Sounds.itemBought, location)
}

