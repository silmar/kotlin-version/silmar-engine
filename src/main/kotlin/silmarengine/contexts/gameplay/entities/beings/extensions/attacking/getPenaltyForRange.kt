package silmarengine.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import kotlin.math.round

/**
 * Returns the penalty incurred when trying to hit a target at the given pixel-range,
 * according to the given maximum pixel-range of the attack being employed.
 */
fun getPenaltyForRange(range: PixelDistance, maxRange: PixelDistance): Int {
    return round(
        (7 * (range.distance.toFloat() / maxRange.distance)).toDouble()).toInt()
}