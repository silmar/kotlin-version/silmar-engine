package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation

import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Creates a new monster of the given type someplace randomly chosen around the
 * given location, but only if the given number of game turns has
 * elapsed.
 *
 * @return      Whether a monster was created.
 */
fun generateMonster(
    area: Area, location: PixelPoint, generateType: MonsterType, interval: Int
): Boolean {
    // if it's time to generate
    val game = Game.currentGame
    if (game!!.turn % interval == 0) {
        createMonsterForReason(area, location, generateType, CreateMonsterReasons.generation)
        return true
    }

    return false
}