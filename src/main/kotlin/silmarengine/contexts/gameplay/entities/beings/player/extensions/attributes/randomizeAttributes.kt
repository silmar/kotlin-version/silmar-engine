package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeValues
import silmarengine.util.list.removeRandomElement
import kotlin.random.Random

/**
 * Makes a random adjustment to each of this player's base attributes.
 * The sum total of the adjustments is zero.
 */
fun PlayerAttributeValues.randomizeBaseAttributes() {
    // for each player attribute, in a random order
    val attributes = PlayerAttribute.attributes.toMutableList()
    var totalAdjustment = 0
    while (attributes.isNotEmpty()) {
        val attribute = attributes.removeRandomElement()

        // determine the adjustment for this attribute randomly, in the opposite
        // direction of the total adjustment so far; but if this is tne final
        // attribute left, its adjustment is the opposite of the total adjustment,
        // so that the final total adjustment is zero
        val maxAdjustment = 6
        val adjustment = when {
            attributes.isNotEmpty() -> Random.nextInt(0,
                (maxAdjustment + 1)) * (if (totalAdjustment > 0) -1 else 1)
            else -> -totalAdjustment
        }
        addToBaseAttribute(attribute, adjustment)
        totalAdjustment += adjustment
    }
}