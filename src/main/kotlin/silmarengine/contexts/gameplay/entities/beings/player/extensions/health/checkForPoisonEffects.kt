package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.game.Game
import kotlin.math.max

/**
 * Checks to see if its time for any poisoning this player is currently
 * suffering to affect its well-being.
 */
fun Player.checkForPoisonEffects() {
    // if this player is poisoned
    val game = Game.currentGame
    if (statuses.isPoisoned) {
        // if it's time for the current level of poison to affect this player
        if (game!!.turn % max(1, 10 / statuses.poisonLevel) == 0) {
            onMessage("POISON!")
            takeDamage(Damage(1, DamageForms.poison))
        }
    }
}

