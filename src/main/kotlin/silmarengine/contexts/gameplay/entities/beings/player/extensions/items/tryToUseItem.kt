package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.Player.Companion.removeCursedItemMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.hasAttributesToUseItem
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOSAndRange
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Has this player try to use the given item.
 *
 * @return  Whether the use succeeded.
 */
fun Player.tryToUseItem(item: Item): Boolean {
    if (isParalyzed) return false

    if (!hasAttributesToUseItem(item)) return false

    // detm if the item is in the inventory (vs. is equipped)
    val inInventory = items.getInventoryItems().contains(item)

    // if the item is in the inventory and must be equipped
    if (inInventory && item.type.equipLocation != null) {
        // if the item is an armor, and there are monsters in LOS
        if (item.type.isArmor && area.getEntitiesInLOS(location, area.monsters)
                .isNotEmpty()) {
            // don't allow the equip, as it would be unrealistic
            onMessage("You cannot put on armor with monsters in sight.",
                MessageType.error)
            return false
        }

        return items.playerItems.tryToEquipItem(item, { it is Weapon },
            WeaponTypes.shortSword.itemType.encumbrance, {
                onMessage(it, MessageType.important)
            }, removeCursedItemMessage)
    }
    // else, if the item is a torch, and there are monsters nearby and in LOS
    else if (item.type == NonGroupableItemTypes.torch && area.getEntitiesInLOSAndRange(
            location, PixelDistance(3 * tileWidth), area.monsters).isNotEmpty()) {
        // don't allow the torch lighting/extinguishing, as it would be unrealistic
        onMessage(
            "You cannot do that during close combat.  Separate yourself from your nearby enemies, then try again.  Or, attempt to vanquish them by clicking where their attack-description messages appear.",
            MessageType.error)
        return false
    }
    // else, if the item is in the inventory and doesn't need
    // to be equipped, or, the item is already equipped
    else if (inInventory && item.type.equipLocation == null || !inInventory) {
        // if the item has no immediate use effect
        return if (item !is Item.Usable) {
            // inform the user
            onMessage("No effect!", MessageType.important)
            false
        }
        else {
            // this will get the inventory display dialog hidden
            reportToListeners { it.onUsedImmediateEffectItem() }

            // have the item do its thing
            (item as Item.Usable).onUsed(this)
            true
        }
    }

    return false
}
