package silmarengine.contexts.gameplay.entities.beings.monsters

import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackTypes.meleeAttack
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackTypes.oldProjectile
import silmarengine.contexts.gameplay.entities.damage.DamageForm
import silmarengine.contexts.gameplay.areas.geometry.TileDistance

/**
 * Describes details about an attack that may be issued by a monster.
 */
open class MonsterAttack(
    /**
     * A brief, textual description of this attack.
     */
    val description: String,
    /**
     * The minimum damage this attack may do.
     */
    val minDamage: Int,
    /**
     * The maximum damage this attack may do.
     */
    val maxDamage: Int, maxTileRange: Int, halfAccuracy: Int,
    /**
     * The form of damage caused by this attack.
     */
    val damageForm: DamageForm
) {
    /**
     * The maximum tile-range at which this attack may be of effect.
     */
    val maxRange = TileDistance(maxTileRange)

    /**
     * If ranged, how accurate this attack is, on average.
     */
    val accuracy: Int = halfAccuracy * 2

    /**
     * Returns the attack-type of this attack.  Attack-type is a higher-level
     * classification than instances of this class.
     */
    open val attackType: AttackType
        get() = when {
            maxRange.distance > 1 -> oldProjectile
            else -> meleeAttack
        }
}
