package silmarengine.contexts.gameplay.entities.beings.extensions.damage

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.damage.DamageStatus

/**
 * Returns this being's current damage-status.
 */
val Being.damageStatus: DamageStatus
    get() {
        var status = DamageStatus.critical
        when {
            hitPoints >= maxHitPoints -> status = DamageStatus.undamaged
            hitPoints > 2 * maxHitPoints / 3 -> status = DamageStatus.ok
            hitPoints > 1 * maxHitPoints / 3 -> status = DamageStatus.serious
        }

        return status
    }
