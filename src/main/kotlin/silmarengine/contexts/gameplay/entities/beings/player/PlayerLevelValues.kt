package silmarengine.contexts.gameplay.entities.beings.player

import java.util.*

interface PlayerLevelValues {
    /**
     * This player's current experience level.
     */
    val level: Int

    fun updateLevel()

    /**
     * A list of how many hit-points was added to this player's maximum value with each
     * experience level gained.  This tells us how many max-hit-points to subtract when
     * this player loses a level.
     */
    val hitPointsByLevel: ArrayList<Int>

    fun fillOutHitPointsByLevel(toLevel: Int)

    /**
     * Has this player immediately jump to the beginning of the next experience level.
     */
    fun gainExperienceLevel()

    /**
     * How many levels this player has currently lost due to level draining.
     */
    val numLevelsLost: Int

    fun onExperienceLevelLost(newExperienceAmount: Int)

    /**
     * Informs this player that its experience levels lost due to level draining have
     * been restored.
     */
    fun onLevelsRestored()

    /**
     * How many experience points this player has.
     */
    val experience: Int

    /**
     * Informs this player that it has gained the given amount of experience.
     */
    fun onExperienceGained(amount: Int)
}