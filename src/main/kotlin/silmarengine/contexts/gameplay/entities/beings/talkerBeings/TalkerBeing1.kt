package silmarengine.contexts.gameplay.entities.beings.talkerBeings

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.Being1
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingType
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.nameToTalkerBeingTypeMap
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.getImage
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.movement.moveTowardsGoal
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.halfTileDistance
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import java.awt.Image
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

open class TalkerBeing1 : TalkerBeing, Being1() {
    /**
     * The location to which this talker-being is currently trying to move.
     */
    private val goalLocation = MutablePixelPoint()

    /**
     * Whether or not the above goal-location is currently a valid goal.
     */
    private var hasGoalLocation = false

    /**
     * Stores attributes that are constant across all talker-beings of this type.  Since
     * these values are immutable, we serialize only the type's name.
     */
    @Transient
    private lateinit var mutableType: TalkerBeingType
    override val type: TalkerBeingType
        get() = mutableType

    /**
     * The name of the above talker-being type, to be serialized for this object instead of
     * the whole type.
     */
    private var typeName: String? = null

    /**
     * Returns the image that should be used to represent this talker-being.
     */
    @Transient
    override var image: Image? = null
        get() {
            val imagePath = type.imagePath
            return if (field != null || imagePath == null) field
            else {
                field = getImage(imagePath)
                field
            }
        }

    /**
     * A flag that signals this talker-being that its current game turn is over.
     */
    private var turnOver: Boolean = false

    /**
     * The kind of tile which is the only one this being is allowed to move to.
     */
    private var movementTile: Tile? = null

    override val greeting: String
        get() = type.greeting

    // talker-beings get only a nominal power rating, since they cannot attack or be
    // attacked
    override val powerRating: Int
        get() = 1

    override val maxMovementPoints: Int
        get() = type.movementPoints

    override val movementType: MovementType
        get() = MovementType.WALKING

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(s: ObjectOutputStream) {
        // store the name of this talker-being's type (instead of the type itself)
        typeName = type.name

        s.defaultWriteObject()
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(s: ObjectInputStream) {
        try {
            s.defaultReadObject()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }

        // detm this talker-being's type from its type-name
        mutableType = nameToTalkerBeingTypeMap[typeName] as TalkerBeingType
    }

    /**
     * Informs this talker-being it has been created with the given talker-being-type.
     */
    override fun onCreated(type: TalkerBeingType) {
        mutableType = type

        // set this entity's initial size from its type
        mutableBounds.setSize(type.size)

        // talker-beings get only a nominal amount of hit points, because they cannot be
        // damaged
        maxHitPoints = 1
        hitPoints = 1
    }

    override fun onGreetingHeardByPlayer(player: Player) {}

    override fun onTalkedTo() {}

    override fun act() {
        turnOver = false

        // we'd like this talker-being to emit a movement sound as soon as it starts moving
        movementSoundCounter = 0

        // give this being its full allotment of movement points for this turn
        movementPoints = maxMovementPoints.toFloat()

        // while this being's turn-is-over flag has not been set
        while (!turnOver) {
            // if this being doesn't have a goal location to move towards
            val current = location
            if (!hasGoalLocation) {
                // if this being hasn't yet stored what kind of tile it's allowed to move to
                if (movementTile == null) {
                    // store the tile that starts out underneath it
                    movementTile = area.getTile(current)
                }

                // for up to a certain number of tries
                val maxTries = 5
                val maxTileDistance = 4
                for (i in 0 until maxTries) {
                    // randomly pick a nearby location that is a tile-sized delta away
                    // from this being's current location
                    goalLocation.set(current.x + randomInt(-maxTileDistance,
                        maxTileDistance) * tileWidth,
                        current.y + randomInt(-maxTileDistance,
                            maxTileDistance) * tileHeight)

                    // if the test location is the current location, try again
                    if (goalLocation == current) continue

                    // if the tile at the test location isn't of the type to which this being
                    // is allowed to move, try again
                    if (area.getTile(goalLocation) != movementTile!!) continue

                    // we have found a valid goal location
                    hasGoalLocation = true
                    break
                }

                // if we couldn't come up with a location above
                if (!hasGoalLocation) {
                    // end movement for this turn
                    break
                }
            }

            // if this being hasn't yet reached its current goal location
            if (!isDistanceAtMost(current, goalLocation, halfTileDistance)) {
                // move towards the goal
                val result = moveTowardsGoal(goalLocation, movementPoints, null, false)

                // if this being's movement was blocked
                if (result.impass) {
                    // this being will need another goal next turn
                    hasGoalLocation = false
                    break
                }
                else if (result.notEnoughMovementPoints) {
                    // this being will keep trying for to reach the current goal next turn
                    break
                }
                else {
                    // this being now needs another goal
                    hasGoalLocation = false
                } // else, this being reached the goal
                // else, if this being ran out of movement points
            }
            else {
                // this being now needs another goal
                hasGoalLocation = false
            }
        }
    }

    override fun endTurn() {
        turnOver = true
    }

    override fun getDefense(attackType: AttackType, attacker: Being?): Int {
        // talker-beings need no defense value as they cannot be attacked or otherwise harmed.
        return 0
    }

    override fun affectDamage(damage: Damage) {
        // don't have talker-beings take any damage; the player shouldn't be hurting them,
        // anyway, and some of them are vital to the player's progress and so must not die
        damage.amount = 0
    }
}
