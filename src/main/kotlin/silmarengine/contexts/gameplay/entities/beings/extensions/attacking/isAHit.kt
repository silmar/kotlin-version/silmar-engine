package silmarengine.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.contexts.gameplay.entities.beings.Being

/**
 * Describes whether an attack resulted in a hit.
 */
data class IsAHitResult(
    /**
     * Whether the attack result was a hit.
     */
    val hit: Boolean = false,
    /**
     * If the attack was a hit, how much it hit by.
     */
    val hitBy: Int = 0,
    /**
     * If the attack was a miss, how much it missed by.
     */
    val missedBy: Int = 0
)

/**
 * Decides whether the given final attack value for the given attack-type by the
 * given attacker results in a hit on this being.
 */
fun Being.isAHit(number: Int, attackType: AttackType, attacker: Being?): IsAHitResult {
    val missedBy =
        getDefense(attackType, attacker) - (number + modifierOnIncomingAttack)
    val hit = missedBy <= 0
    val hitBy = -missedBy
    return IsAHitResult(hit, hitBy, missedBy)
}
