package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.annotations.ExtensionPoint
import java.io.Serializable
import java.util.*
import kotlin.math.min

open class PlayerStatuses1(val player: Player) : PlayerStatuses, Serializable {
    override val diseaseAfflictions = ArrayList<DiseaseAffliction>()
    override fun onAfflictedWithDisease(affliction: DiseaseAffliction) {
        diseaseAfflictions.add(affliction)
    }

    override fun onDiseaseCured() {
        diseaseAfflictions.removeAt(diseaseAfflictions.size - 1)
    }

    final override var poisonLevel = 0
        private set(value) {
            field = min(value, maxPlayerPoisonLevel)
            if (value in 0..1) player.reportVitalStatChange()
        }

    override fun onPoisoned(degree: Int) {
        if (isGameSpecificPlayerImmuneToPoison(player)) return

        poisonLevel += degree
    }

    override fun reducePoisonLevel() {
        poisonLevel--
    }
}

@ExtensionPoint
var isGameSpecificPlayerImmuneToPoison: (player: Player) -> Boolean = { _ -> false }