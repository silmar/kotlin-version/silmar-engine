package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.LocationalMessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.ItemEntity
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.gui.dialogs.InputIntDialog
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import javax.swing.JFrame
import kotlin.math.floor

/**
 * Has this player try to take the given item (or, some portion of it) into its possession.
 */
fun Player.pickupItem(itemEntity: ItemEntity, parentFrame: JFrame) {
    // if the item is groupable, there is more than just a few of it,
    // and there are monsters in LOS
    val item = itemEntity.item
    if (item.type.isGroupable && item.quantity > 3 && area.getEntitiesInLOS(location,
            area.monsters).isNotEmpty()) {
        // don't allow the get, as it would be unrealistic
        onMessage("You cannot retrieve this item with enemies in sight.",
            MessageType.error)
        return
    }

    // if the item's quantity is more than one
    var quantity = item.quantity
    if (quantity > 1) {
        // ask the user how many should be picked up
        val result = InputIntDialog.show(parentFrame,
            "Get how many of this item (" + item.type.pluralName + ")?", 1, quantity,
            quantity)
        if (result.cancelled) return
        quantity = result.valueEntered
    }

    // if the player can't carry any more of this item
    val canCarry = floor(
        ((items.maxEncumbrance - items.encumbrance) / item.type.encumbrance).toDouble()).toInt()
    if (canCarry < 1) {
        onMessage(
            "You cannot carry " + (if (quantity > 1) "any more of " else "") + "this item.")
        return
    }

    // make the quantity to be picked up as many as this player can carry
    val quantityToGet = if (quantity > canCarry) canCarry else quantity

    // if all of the item is being gotten
    val itemLocation = itemEntity.location
    var itemToGet = item
    if (quantityToGet == item.quantity) {
        // remove the item from the area
        area.removeEntity(itemEntity)
    }
    else {
        // adjust the quantity of the item remaining in this area
        item.quantity -= quantityToGet

        // create a new item of the given quantity and the
        // area item's type
        itemToGet = createGroupableItem(item.type, quantity)
    }

    // add the item to this player's inventory
    items.inventory.addItem(itemToGet)

    // display the item's quantity (if > 1) and name at the spot where it was gotten
    onMessage((if (quantityToGet > 1) "$quantityToGet " else "") + itemToGet.name,
        LocationalMessageType(itemLocation))

    // if more than a few of the item were picked up, end the player's turn
    if (quantityToGet > 3) endTurn()
}

