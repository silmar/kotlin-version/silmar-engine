package silmarengine.contexts.gameplay.entities.beings.talkerBeings

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingType

/**
 * Returns a newly created instance of the given talker-being-type.
 */
fun createTalkerBeing(type: TalkerBeingType): TalkerBeing {
    val being = createGameSpecificTalkerBeing(type) ?: TalkerBeing1()
    being.onCreated(type)
    return being
}

@ExtensionPoint
var createGameSpecificTalkerBeing: (type: TalkerBeingType) -> TalkerBeing? = { _ -> null }