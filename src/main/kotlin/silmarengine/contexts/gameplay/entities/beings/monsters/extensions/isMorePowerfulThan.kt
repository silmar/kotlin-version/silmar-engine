package silmarengine.contexts.gameplay.entities.beings.monsters.extensions

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster

/**
 * Returns whether the power rating of this monster is greater than that of the
 * given monster.
 */
fun Monster.isMorePowerfulThan(monster: Monster): Boolean {
    return type.powerRating > monster.type.powerRating
}

