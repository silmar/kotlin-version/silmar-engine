package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.maxPlayerPoisonLevel
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes
import silmarengine.util.math.randomBellCurveInt

/**
 * Informs this player that it has just used the given item.
 */
fun Player.onUsedItem(item: Item) {
    when {
        item.isOfType(NonGroupableItemTypes.potionOfHealing) -> takeHealing(
            randomBellCurveInt(1, 8, 4))
        item.isOfType(NonGroupableItemTypes.potionOfPoison) -> {
            // poison this player
            statuses.onPoisoned(maxPlayerPoisonLevel)
            onMessage("Aack!!")
        }
        item.isOfType(NonGroupableItemTypes.potionOfStrength) || item.isOfType(
            NonGroupableItemTypes.potionOfIntelligence) || item.isOfType(
            NonGroupableItemTypes.potionOfJudgement) || item.isOfType(
            NonGroupableItemTypes.potionOfAgility) || item.isOfType(
            NonGroupableItemTypes.potionOfEndurance) -> onPotionOfAttributeUsed(item)
    }
}

