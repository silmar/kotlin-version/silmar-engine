package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.isAHit
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onAttackedAndMissed
import silmarengine.contexts.gameplay.entities.beings.monsters.Hider
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.areas.extensions.effects.performAttackEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.IsAHitResult
import silmarengine.util.math.randomInt

/**
 * Has this monster make all of its available melee attacks on the given player, who
 * is assumed to be within range of those attacks.
 */
fun Monster.makeMeleeAttacks(player: Player) {
    // if this monster doesn't make melee attacks
    val attacks = type.meleeAttacks ?: return

    // for each melee attack this monster can make
    val playerLocation = player.location
    attacks.forEach { attack ->
        // give the player a short description of the attack if it's in LOS
        area.player?.receiveMessageIfInLOSOf(location,
            type.name + ": " + attack.description)

        // if the attack hits
        val result = when (val roll = randomInt(1, 20)) {
            1 -> IsAHitResult(false)
            20 -> IsAHitResult(true)
            else -> {
                val total = roll + getMonsterToHitModifier(this, player,
                    attack) - numberToHitDefense0
                player.isAHit(total, attack.attackType, this)
            }
        }
        var damage = 0
        if (result.hit) {
            // detm the damage
            damage = randomInt(attack.minDamage, attack.maxDamage)
        }

        area.performAttackEffect(location, playerLocation, attack.attackType)

        // if the attack hit
        if (result.hit) {
            // inform the player of the hit
            player.onHitByMonster(this, attack, damage, result.hitBy)

            onMonsterAttackHitBeing(this, player, attack, result.hitBy)
        }
        else {
            player.onAttackedAndMissed()
        }

        if (this is Hider) becomeVisible()
    }
}

@ExtensionPoint
var getMonsterToHitModifier: (monster: Monster, player: Player, attack: MonsterAttack) -> Int =
    { _, _, _ -> 0 }

/**
 * Informs the given monster that its attack of the given type hit the given being
 * by the given amount.
 */
@ExtensionPoint
var onMonsterAttackHitBeing: (monster: Monster, being: Being, attack: MonsterAttack, hitBy: Int) -> Unit =
    { _, _, _, _ -> }
