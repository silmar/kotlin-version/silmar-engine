package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.getExperienceNeededForLevel
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.getLevelForExperience
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.onLevelChange
import silmarengine.util.math.randomInt
import java.io.Serializable
import java.util.*
import kotlin.math.max

open class PlayerLevelValues1(val player: Player) : PlayerLevelValues, Serializable {
    override val hitPointsByLevel = ArrayList<Int>()

    final override var level = 0
        private set(value) {
            val old = field
            field = max(value, 1)
            player.onLevelChange(old, field)
        }

    override var numLevelsLost = 0
        protected set

    final override var experience = 0
        private set(value) {
            field = value
            updateLevel()
            player.reportVitalStatChange()
        }

    override fun gainExperienceLevel() {
        experience = getExperienceNeededForLevel(level + 1)
    }

    override fun onExperienceLevelLost(newExperienceAmount: Int) {
        experience = newExperienceAmount
        numLevelsLost++
    }

    override fun onLevelsRestored() {
        // for each level lost
        for (i in 0 until numLevelsLost) {
            // set this player's experience to just above the amount
            // needed for the next level
            experience = getExperienceNeededForLevel(level + 1)
        }

        numLevelsLost = 0
    }

    override fun onExperienceGained(amount: Int) {
        experience += amount
    }

    override fun updateLevel() {
        level = getLevelForExperience(experience)
    }

    override fun fillOutHitPointsByLevel(toLevel: Int) {
        // while this player doesn't have as many entries in its
        // hit-points-by-level list as it does experience levels
        while (toLevel >= hitPointsByLevel.size) {
            // detm the hit points gained for the new level and add the amount to our list
            val maxHitPointsPerLevel = 10
            val averageHitPointsPerLevel = 6
            val amount = if (hitPointsByLevel.size >= startingPlayerLevel) randomInt(1,
                maxHitPointsPerLevel)
            else averageHitPointsPerLevel
            hitPointsByLevel.add(amount)
        }
    }
}