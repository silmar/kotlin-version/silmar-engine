package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.game.Game

/**
 * Checks to see if it's time for this player to consume a ration.
 */
fun Player.checkForRationConsumption() {
    // if it's time to eat
    val eatingInterval = 200
    val game = Game.currentGame
    if (game!!.turn % eatingInterval == 0) {
        // if this player can't eat a ration
        if (!loseRation(ItemLostReason.used)) {
            // this player is starving
            onMessage("STARVATION!")
            takeDamage(Damage(hitPoints / 10, DamageForms.starvation))
        }
        else {
            onMessage("You consume a ration.")
        }
    }
}
