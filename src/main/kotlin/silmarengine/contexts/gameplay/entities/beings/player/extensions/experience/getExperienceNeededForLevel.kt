package silmarengine.contexts.gameplay.entities.beings.player.extensions.experience

import silmarengine.annotations.ExtensionPoint

/**
 * Returns the total amount of experience necessary for a player to reach the
 * given experience level.
 */
fun getExperienceNeededForLevel(level: Int): Int {
    val numLevels = experienceRequiredForPlayerLevel.size
    return if (level >= numLevels) Integer.MAX_VALUE else experienceRequiredForPlayerLevel[level]
}

/**
 * Returns the highest experience level achieved for the given amount of experience.
 */
fun getLevelForExperience(experience: Int): Int {
    // for each level
    val numLevels = experienceRequiredForPlayerLevel.size
    for (i in 0 until numLevels) {
        // if the given experience is less than the amount needed for this level,
        // return the previous level
        if (experience < getExperienceNeededForLevel(i)) return i - 1
    }

    return numLevels
}

/**
 * The amount of experience (total, inclusive of all previous levels) needed
 * by the player to reach each experience level.
 */
@ExtensionPoint
var experienceRequiredForPlayerLevel: IntArray = IntArray(0)

