package silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.entities.items.Item

open class Buyer : TalkerBeing1() {
    /**
     * Returns whether this buyer disappears from the game once it buys an item.
     */
    open val leavesAfterPurchase: Boolean
        get() = false

    /**
     * Returns this buyer's adjusted offering for the given item with the given normal
     * offering value.
     */
    open fun getAdjustedItemOffering(item: Item, normalOffering: Int): Int {
        return normalOffering
    }

    /**
     * Informs this buyer that it has bought an item.
     */
    open fun onItemBought() {}
}