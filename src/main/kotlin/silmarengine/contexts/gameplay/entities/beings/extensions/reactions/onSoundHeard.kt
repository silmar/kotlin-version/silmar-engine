package silmarengine.contexts.gameplay.entities.beings.extensions.reactions

import silmarengine.sounds.Sound
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation.onSoundHeard
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Informs this being that it has heard the given sound coming from the given location.
 */
fun Being.onSoundHeard(
    sound: Sound, location: PixelPoint, distance: PixelDistance = PixelDistance(0)
) {
    when (this) {
        is Monster -> onSoundHeard(sound, location)
        is Player -> reportToListeners { it.onSoundHeard(sound, location, distance) }
    }
}

