package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sounds.curePoison
import silmarengine.util.sleepThread

/**
 * Informs this player that a cure poison spell has been cast upon it.
 */
fun Player.onCurePoisonReceived() {
    // if this player is poisoned
    if (statuses.isPoisoned) {
        // issue an accompanying sound
        area.onSoundIssued(curePoison, location)

        // add a cure visual effect at the given location
        val effect = LightSourceVisualEffect(null, PixelDistance(TileDistance(5)))
        area.addEntity(effect, location)

        // for each step in the effect
        for (i in 1..3) {
            // perform this step
            effect.imageName = "curePoison$i"

            // pause a bit for this step to be seen
            sleepThread(200)
        }

        // remove this curing's visual effect from the area
        area.removeEntity(effect)

        // lower this player's poison level by one
        statuses.reducePoisonLevel()
    }
}

