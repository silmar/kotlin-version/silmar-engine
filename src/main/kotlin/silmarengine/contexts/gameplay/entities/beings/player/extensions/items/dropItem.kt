package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity

/**
 * Has this player try to drop the given quantity of the given item.
 */
fun Player.dropItem(item: Item, quantity: Int) {
    if (isParalyzed) return

    // if not all of the item is to be dropped
    var itemToDrop = item
    if (item.quantity > quantity) {
        // reduce the quantity of the given item to what will be left
        item.quantity -= quantity

        // create a new item with the quantity to be dropped
        itemToDrop = createGroupableItem(item.type, quantity)
    }
    else {
        // remove the item from the inventory
        items.inventory.tryToRemoveItem(itemToDrop)
    }

    // place the item at this player's location
    area.addEntity(createItemEntity(itemToDrop), location)

    onItemLost(item, ItemLostReason.dropped)
}

