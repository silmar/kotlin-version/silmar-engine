package silmarengine.contexts.gameplay.entities.beings.player.extensions.experience

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Informs this player that it has bought a restore-levels service from the given
 * seller.
 *
 * @return      Whether the service was performed.
 */
fun Player.onRestoreLevelsService(seller: Entity): Boolean {
    // if this player has not lost any levels
    if (levelValues.numLevelsLost == 0) {
        onMessage("You haven't lost any levels!", MessageType.error)
        return false
    }

    // restore this player's lost levels
    performPowerDischargeEffect(seller)
    levelValues.onLevelsRestored()

    return true
}

