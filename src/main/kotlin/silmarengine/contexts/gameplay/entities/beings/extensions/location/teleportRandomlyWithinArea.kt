package silmarengine.contexts.gameplay.entities.beings.extensions.location

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocation

/**
 * Teleports the given being to another location at random within
 * the given area.
 */
fun Being.teleportRandomlyWithinArea() {
    // find a location randomly to which to teleport the given being
    val to = area.getRandomPassibleRectangleLocation(size) ?: return

    // perform the teleport-out effect
    val lastFrame = area.performTeleportEffect(location, true, true)

    // move the being to the location determined above
    area.moveEntity(this, to)

    // remove the final, covering teleport-out frame
    area.removeEntity(lastFrame!!)

    // perform the teleport-in effect
    area.performTeleportEffect(to, false, false)

    // if the being is a player, cancel any pending movement
    if (this is Player) cancelPendingMove()
}
