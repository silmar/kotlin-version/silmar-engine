package silmarengine.contexts.gameplay.entities.beings

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.entities.Entity1
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeNotParalyzed
import silmarengine.contexts.gameplay.entities.beings.extensions.location.adjustLocationIfNotFitting
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.damage.Damage
import java.lang.Integer.min

/**
 * A being in the game.
 */
abstract class Being1 : Being, Entity1() {
    override var hitPoints = 0
        protected set

    override var maxHitPoints = 0
        protected set(value) {
            field = value
            hitPoints = min(hitPoints, value)
        }

    /**
     * How many game turns this being has left to be paralyzed, if any.
     */
    protected open var paralyzedTurnsLeft = 0

    override val isParalyzed: Boolean
        get() = paralyzedTurnsLeft > 0

    /**
     * How many game turns this being has left to be confused, if any.
     */
    protected open var confusedTurnsLeft = 0

    override val isConfused: Boolean
        get() = confusedTurnsLeft > 0

    override var movementPoints: Float = 0f
        protected set

    override fun endMovementForThisTurn() = run { movementPoints = 0f }

    abstract override val powerRating: Int

    override val avoidance: Int
        get() = 18 - powerRating * 2 / 3

    abstract override val maxMovementPoints: Int

    override val isDead: Boolean
        get() = hitPoints <= 0

    override val modifierOnIncomingAttack: Int
        get() = 0

    override val isEvil: Boolean
        get() = false

    override val needsLightToSee: Boolean
        get() = this is LightSource

    override fun die(damage: Damage) {
        becomeNotParalyzed()

        area.removeEntity(this)
    }

    override fun affectDamage(damage: Damage) {}

    override fun onDamaged(damage: Damage) {}

    override fun addHitPoints(amount: Int) {
        hitPoints += amount
    }

    override fun endTurn() {}

    override fun setConfusedDuration(duration: Int) {
        confusedTurnsLeft = duration
    }

    override fun setParalyzedDuration(duration: Int) {
        paralyzedTurnsLeft = duration
    }

    abstract override fun getDefense(attackType: AttackType, attacker: Being?): Int

    override fun onHitPointsDetermined(amount: Int) {
        hitPoints = amount
    }

    override fun onTurnOver() {
        super<Entity1>.onTurnOver()

        // if this being is paralyzed, mark off one more turn of paralysis
        if (isParalyzed) paralyzedTurnsLeft--

        // if this being is confused, mark off one more turn of confusion
        if (isConfused) confusedTurnsLeft--
    }

    override fun onImageToChange() {
        super.onImageToChange()

        adjustLocationIfNotFitting()
    }

    override val shouldMovementBeAccelerated = true

    override fun onHitByMonster(monster: Monster, attack: MonsterAttack,
        damageAmount: Int, hitBy: Int) {
        if (onBeingHitByMonster_PreDamage(this, monster, attack, damageAmount,
                hitBy)) return

        // have this being incur the damage caused by the attack
        val damage = Damage(damageAmount, attack.damageForm, monster, null, attack)
        takeDamage(damage)

        onBeingHitByMonster_PostDamage(this, monster, attack, damageAmount, hitBy)
    }

    companion object {
        /**
         * The penalty suffered by this being's defense when it is paralyzed.
         */
        const val paralysisDefenseModifier = -10
    }
}


/**
 * @return  Whether the caller should skip the rest of its processing.
 */
@ExtensionPoint
var onBeingHitByMonster_PreDamage: (
    being: Being, monster: Monster, attack: MonsterAttack, damageAmount: Int, hitBy: Int
) -> Boolean = { _, _, _, _, _ -> false }

@ExtensionPoint
var onBeingHitByMonster_PostDamage: (
    being: Being, monster: Monster, attack: MonsterAttack, damageAmount: Int, hitBy: Int
) -> Unit = { _, _, _, _, _ -> }
