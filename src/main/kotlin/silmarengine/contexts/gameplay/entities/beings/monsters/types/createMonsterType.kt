package silmarengine.contexts.gameplay.entities.beings.monsters.types

import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.sounds.Sound

fun createMonsterType(
    frequency: Int, name: String, imageName: String?, powerRating: Int,
    alignment: Alignment, experienceValue: Int, movementPoints: Int, defense: Int,
    isLivingDead: Boolean, meleeAttacks: Array<MonsterAttack>?,
    rangedAttacks: Array<MonsterAttack>?, rangedAttackFrequency: Int,
    movementType: MovementType, weaponPlusNeededToHit: Int, avoidanceModifier: Int,
    isLessVulnerableToEdgedWeapons: Boolean, sound: Sound?, soundFrequency: Float,
    isStationary: Boolean
): MonsterType =
    MonsterType1(frequency, name, imageName, powerRating, alignment, experienceValue,
        movementPoints, defense, isLivingDead, meleeAttacks, rangedAttacks,
        rangedAttackFrequency, movementType, weaponPlusNeededToHit, avoidanceModifier,
        isLessVulnerableToEdgedWeapons, sound, soundFrequency, isStationary)
