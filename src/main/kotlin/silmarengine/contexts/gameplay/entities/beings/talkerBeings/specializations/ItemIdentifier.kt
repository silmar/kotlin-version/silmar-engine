package silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.entities.items.Item

class ItemIdentifier : TalkerBeing1() {
    /**
     * Returns this identifier's adjusted price for the given item with the given normal price.
     */
    fun getAdjustedItemPrice(
        @Suppress("UNUSED_PARAMETER")
        item: Item, normalPrice: Int
    ): Int {
        return normalPrice
    }
}