package silmarengine.contexts.gameplay.entities.beings.monsters

import silmarengine.contexts.beingmovement.domain.model.BMMonster
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.RangedAttacker
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

interface Monster : Being, RangedAttacker, BMMonster {
    /**
     * Stores attributes that are constant across all monsters of this type.
     */
    val type: MonsterType
    fun isOfType(type: MonsterType) = type == this.type
    fun onCreated(type: MonsterType)

    /**
     * Whether this monster is in an activated state.  Activated means being aware of the
     * player's presence.
     */
    override val isActivated: Boolean
    fun activate()
    fun deactivate()

    /**
     * The player this monster is focused on trying to reach during its act() method.
     */
    override val isFocusedOnPlayer: Boolean
    override fun focusOnPlayer()

    /**
     * Whether this monster is currently in a fleeing mode of conduct.
     */
    override val isFleeing: Boolean

    /**
     * The player this from which this monster is currently fleeing (if any).
     */
    val fleeFromPlayer: Player?
    fun fleeFromPlayer(player: Player)

    fun seekLocationIfNotFocusedOnPlayer(location: PixelPoint)

    val isStationary: Boolean
    fun makeStationary()

    /**
     * Whether this monster has completed it actions for the current game turn.
     */
    override val isTurnOver: Boolean

    val canCurrentlyMakeMeleeAttacks: Boolean get() = true
}