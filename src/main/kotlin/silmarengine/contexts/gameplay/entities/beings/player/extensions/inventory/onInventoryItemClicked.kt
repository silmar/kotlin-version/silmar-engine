package silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory

import silmarengine.WorkThread
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.dropItem
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.tryToUseItem
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.gui.dialogs.InputIntDialog
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import javax.swing.JDialog

fun Player.onInventoryItemClicked(item: PIMItem, rightClick: Boolean,
    inventoryDialog: JDialog) {
    // if the click signals a drop-item request
    item as Item
    if (rightClick) {
        // if the item's quantity is more than 1
        var quantity = item.quantity
        if (quantity > 1) {
            // ask how many the user would like to drop
            val result = InputIntDialog.show(inventoryDialog,
                "Drop how many of this item?", 1, quantity, quantity)
            if (result.cancelled) return
            quantity = result.valueEntered
        }

        // have the items dropped on the work-thread
        val finalQuantity = quantity
        WorkThread.queueTask {
            dropItem(item, finalQuantity)
        }
    }
    else {
        // else, the click signals a use-item request
        WorkThread.queueTask { tryToUseItem(item) }
    }
}