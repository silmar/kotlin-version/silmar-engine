package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.game.Game
import kotlin.math.abs

/**
 * Checks this player's current disease-afflictions to see if it's time for any of
 * them to impose their effects.
 */
fun Player.checkForDiseaseEffects() {
    // for each disease-affliction this player has
    val game = Game.currentGame
    val afflictions = statuses.diseaseAfflictions
    afflictions.forEach { affliction ->
        // if it's time for this disease-affliction to affect this player
        val disease = affliction.disease
        val interval = disease!!.turnInterval
        if (abs(affliction.turnAfflicted - game!!.turn) % interval == interval - 1) {
            onMessage("DISEASE!")
            disease.affectPlayer(this)
        }
    }
}

