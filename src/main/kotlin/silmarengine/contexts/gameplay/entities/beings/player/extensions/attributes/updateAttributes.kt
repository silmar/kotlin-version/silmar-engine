package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms

fun Player.updateAttributes() {
    // for each attribute
    val items = items.compressEquippedItems()
    PlayerAttribute.attributes.forEachIndexed { i, attribute ->
        // start off with this attribute equalling the corresponding base attribute
        var newValue = attributeValues.getBaseAttribute(attribute)

        // add the appropriate attribute modifier of each equipped item
        items.forEach { newValue += it.type.attributeModifiers[i] }

        newValue = updateGameSpecificPlayerAttribute(this, attribute, newValue)

        // if the value of this attribute changed
        if (attributeValues.getAttribute(attribute) != newValue) {
            attributeValues.onAttributeComputed(attribute, newValue)

            // if the attribute is now 0 or less
            if (newValue <= 0) {
                // this players dies
                takeDamage(Damage(hitPoints, DamageForms.attributeAtZero))
            }
        }
    }
}

@ExtensionPoint
var updateGameSpecificPlayerAttribute: (player: Player, attribute: PlayerAttribute, newValueSoFar: Int) -> Int =
    { _, _, newValueSoFar -> newValueSoFar}