package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle

/**
 * Informs the terrains at this monster's current location of its presence.
 */
fun Monster.informTerrainsOfPresence() {
    // for each terrain feature at this monster's location
    val terrains = area.terrains.getEntitiesInRectangle(location, size)
    terrains.firstOrNull first@{ terrain ->
        // inform this terrain of this monster's presence
        terrain.onBeingHere(this)

        // if the contact with this terrain killed this monster, we don't need to
        // inform the rest of the terrains in the list of this monster's presence
        return@first isDead
    }
}

