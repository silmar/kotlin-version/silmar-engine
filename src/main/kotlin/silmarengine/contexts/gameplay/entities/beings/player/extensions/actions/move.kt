package silmarengine.contexts.gameplay.entities.beings.player.extensions.actions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.beingmovement.domain.functions.MoveTowardsGoalListener
import silmarengine.contexts.beingmovement.domain.model.BMPixelPoint
import silmarengine.contexts.beingmovement.domain.model.BMTerrain
import silmarengine.contexts.beingmovement.domain.model.BMTile
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.entities.beings.extensions.movement.moveTowardsGoal
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.tiles.Tile

/**
 * Has this player move towards the given pixel-location.
 */
fun Player.move(to: PixelPoint) {
    // if this player is out of movement points, don't do any of the move
    if (movementPoints <= 0) return

    val adjustedTo =
        if (isConfused) modifyLocationDueToConfusion(area, to, location) else to

    // move towards the requested location
    val startingMovementPoints = movementPoints
    val result =
        moveTowardsGoal(adjustedTo, movementPoints, moveTowardsGoalListener, true)

    // if this player failed to reach the goal due to running out of movement points
    if (result.notEnoughMovementPoints) {
        // this player will do more of this move next turn
        onMovePending(adjustedTo)
    }
    else {
        // there is no move pending for next time
        cancelPendingMove()
    }

    // if the player's amount of movement points was not changed manually for some
    // reason (e.g. set to zero due to being next to a monster) during the above movement
    if (movementPoints == startingMovementPoints) {
        // take the result's count of how many points this player has as its new value
        onMovementPointsRemaining(result.movementPointsLeft)

        // if this player has run out of movement points
        if (movementPoints <= 0) {
            // this player has acted for this turn
            endTurn()
        }
    }
}

class MovePlayerTowardsGoalListener(val player: Player) : MoveTowardsGoalListener {
    override fun onAtTerrain(terrain: BMTerrain): Boolean {
        // if the terrain is a trap that's not set on a chest, halt this player's movement
        return !(terrain is Trap && terrain.chest == null)
    }

    override fun onMovedOntoNewTile(tile: BMTile): Boolean =
        onGameSpecificPlayerMovedOntoNewTile(player, tile as Tile)

    override fun getShouldContinue(oldLocation: BMPixelPoint): Boolean {
        // if the movement step just taken caused this player to die
        return !player.isDead
    }
}

@ExtensionPoint
var onGameSpecificPlayerMovedOntoNewTile: (player: Player, tile: Tile) -> Boolean =
    { _, _ -> false }