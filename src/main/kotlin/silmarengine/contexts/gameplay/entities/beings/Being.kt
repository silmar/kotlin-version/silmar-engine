package silmarengine.contexts.gameplay.entities.beings

import silmarengine.contexts.beingmovement.domain.model.BMBeing
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.lineofsightblocking.domain.model.LOSBBeing

/**
 * A being in the game.  Beings include the player, monsters, and talker-beings.
 */
interface Being : Entity, LOSBBeing, BMBeing {
    /**
     * How many hit points this being currently has.  When this reaches zero or less,
     * this being dies.
     */
    val hitPoints: Int

    /**
     * The maximum amount of hit points this being may currently have.
     */
    val maxHitPoints: Int

    val isDamaged: Boolean get() = hitPoints < maxHitPoints

    /**
     * Whether this being is currently in a paralyzed state.
     */
    val isParalyzed: Boolean

    /**
     * Whether this being is currently in a confused state.
     */
    val isConfused: Boolean

    /**
     * This being's power-rating (i.e. hit dice).
     */
    val powerRating: Int

    /**
     * This being's avoidance (i.e. saving throw) value.
     */
    val avoidance: Int

    /**
     * The amount of movement points this being is allotted, per turn.
     */
    val maxMovementPoints: Int

    /**
     * This being's remaining movement points for the current game turn.
     */
    override val movementPoints: Float

    val isDead: Boolean

    /**
     * The bonus (or penalty) that an attack on this being
     * would receive, due to some special property of this being.
     */
    val modifierOnIncomingAttack: Int

    val isEvil: Boolean

    val needsLightToSee: Boolean

    /**
     * Performs all necessary actions to cause this being to be vanquished
     * from the game.
     *
     * @param   damage      The final damage that resulted in death.
     */
    fun die(damage: Damage)

    /**
     * This is meant to be overridden by subclasses to let them specify
     * special resistances and vulnerabilities to various forms of damage.  The given
     * damage parameter is modified to reflect these special characteristics.
     */
    fun affectDamage(damage: Damage)

    /**
     * Informs this being that it has suffered the given damage.
     */
    fun onDamaged(damage: Damage)

    /**
     * Adds the given amount of hit points to this being's current total.
     */
    fun addHitPoints(amount: Int)

    /**
     * Causes this being to stop acting for the rest of this game turn.
     */
    fun endTurn()

    fun endMovementForThisTurn()

    /**
     * Sets for how many game turns (starting from the current one) this being
     * will be confused.
     */
    fun setConfusedDuration(duration: Int)

    /**
     * Sets for how many game turns (starting from the current one) this being
     * will be paralyzed.
     */
    fun setParalyzedDuration(duration: Int)

    /**
     * Returns this being's defense value versus an attack of the given type made
     * by the giver attacker.
     */
    fun getDefense(attackType: AttackType, attacker: Being?): Int

    /**
     * Is called when some external entity wishes to set this being's
     * (usually, starting) hit points to the given amount.
     */
    fun onHitPointsDetermined(amount: Int)

    /**
     * Informs this being that it has been attacked by the given monster, using the given
     * attack, and was hit for the given amount of damage.
     *
     * @param hitBy     How much the attack hit by.
     */
    fun onHitByMonster(monster: Monster, attack: MonsterAttack, damageAmount: Int,
        hitBy: Int)
}