package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider
import kotlin.math.max
import kotlin.math.roundToInt

fun Player.getMaxMovementPoints(): Int {
    // detm the max points afforded by this player's stats
    val provider = PlayerAttributeModifierProvider
    val values = attributeValues
    var points = (10f / 4 + 10f / 4 + provider.maxMovementPointsModifierForStrength(
        values.strength) + provider.maxMovementPointsModifierForAgility(values.agility))
    points = max(points, 1f)

    // factor in how encumbered this player is
    val fractionNotEncumbered = 1f - items.encumbrance.toFloat() / items.maxEncumbrance
    points *= fractionNotEncumbered

    points = adjustGameSpecificPlayerMaxMovementPoints(this, points)

    return max(1, points.roundToInt())
}

@ExtensionPoint
var adjustGameSpecificPlayerMaxMovementPoints: (player: Player, points: Float) -> Float =
    { _, points -> points }