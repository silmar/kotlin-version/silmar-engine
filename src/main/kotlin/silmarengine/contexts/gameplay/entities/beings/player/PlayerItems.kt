package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.playeritemsmanagement.domain.model.*

interface PlayerItems {
    val player: Player

    val playerItems: PIMPlayerItems
    val equippedItems: EquippedItems get() = playerItems.equippedItems
    val inventory: Inventory get() = playerItems.inventory
    val handsConfigs: HandsConfigs get() = playerItems.handsConfigs

    fun isItemEquipped(item: Item): Boolean = equippedItems.isItemEquipped(item)

    fun isItemOfTypeEquipped(type: ItemType): Boolean =
        equippedItems.isItemOfTypeEquipped(type)

    fun getEquippedItemOfType(type: ItemType): Item? =
        equippedItems.getEquippedItemOfType(type) as Item?

    fun getEquippedItem(at: EquipLocation): Item?

    fun equipItem(item: Item?, equipLocation: EquipLocation)

    fun compressEquippedItems(): List<Item> =
        equippedItems.compress().map { it as Item }

    fun getInventoryItems(): List<Item>

    fun filterInventory(predicate: (item: Item) -> Boolean): List<Item>

    fun getFirstInventoryItemOfType(type: ItemType): Item? =
        inventory.getFirstItemOfType(type) as Item?

    val randomItem: Item? get() = playerItems.randomItem as Item?

    val randomEquippedItem: Item? get() = equippedItems.randomItem as Item?

    /**
     * The maximum amount of encumbrance points this player may currently carry.
     */
    val maxEncumbrance: Int

    fun updateMaxEncumbrance()

    /**
     * How many encumbrance points this player is currently carrying.
     */
    val encumbrance: Int

    fun onEncumbranceComputed(amount: Int)

    val numEquippedCursedItems: Int

    /**
     * The total of the identification values for all the unidentified,
     * cursed items currently equipped by this player.
     */
    val equippedUnidentifiedCursedItemsTotalIdentificationValue: Int
}