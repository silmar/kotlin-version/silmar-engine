package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.contexts.gameplay.entities.beings.player.Player

fun Player.updateEncumbrance() {
    var newValue = 0

    // add the encumbrance of each equipped-item
    items.compressEquippedItems().forEach { newValue += it.encumbrance.toInt() }

    // add the encumbrance of each inventory item
    items.getInventoryItems().forEach { newValue += it.encumbrance.toInt() }

    items.onEncumbranceComputed(newValue)
}

