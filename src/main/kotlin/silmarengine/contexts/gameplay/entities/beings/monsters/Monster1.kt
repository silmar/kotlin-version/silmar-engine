package silmarengine.contexts.gameplay.entities.beings.monsters

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.ExtensionPoint
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.beingmovement.domain.functions.MoveTowardsGoalListener
import silmarengine.contexts.beingmovement.domain.functions.moveTowardGoal
import silmarengine.contexts.beingmovement.domain.model.BMPixelPoint
import silmarengine.contexts.beingmovement.domain.model.BMTerrain
import silmarengine.contexts.beingmovement.domain.model.BMTile
import silmarengine.contexts.beingmovement.domain.model.MonsterMovementContext
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.extensions.tiles.closeDoor
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.Being1
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.pixelsPerShotMove
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation.determineIfShouldActivate
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation.onIsActivatedChange
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.CheckForRangedAttacksResult
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.checkForMeleeAttacksExt
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.checkForRangedAttacksExt
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm.indicateDeath
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement.determineGoalForThisTurn
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement.informTerrainsOfPresence
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType
import silmarengine.contexts.gameplay.entities.beings.monsters.types.nameToMonsterTypeMap
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextTo
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.pathfinding.domain.model.PFMutablePixelPoint
import silmarengine.contexts.pathfinding.domain.model.PFPixelPoint
import silmarengine.getImage
import silmarengine.objectpools.pfPathDequePool
import silmarengine.objectpools.pfPixelPointPool
import silmarengine.sounds.Sound
import silmarengine.util.math.randomBellCurveInt
import silmarengine.util.math.randomFloat
import silmarengine.util.math.randomInt
import java.awt.Image
import java.awt.geom.Point2D
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.util.*

/**
 * A monster in the game.
 */
open class Monster1 : Monster, Being1() {
    /**
     * Returns the image that should be used to represent this monster.
     */
    @Transient
    override var image: Image? = null
        get() {
            val imagePath = type.imagePath
            return if (field != null || imagePath == null) field
            else {
                field = getImage(imagePath)
                field
            }
        }

    /**
     * Whether this monster has already had the opportunity to decide
     * whether to make ranged attacks during the current game turn.  When
     * this is true, no further such checks should be made for this monster
     * during the turn, so it can't make extra sets of ranged attacks.
     */
    override var hadRangedAttackOpportunityThisTurn: Boolean = false

    override var isFocusedOnPlayer = false
    override fun focusOnPlayer() = run { isFocusedOnPlayer = true }

    /**
     * The current goal location (if any) this monster is trying to reach.
     */
    private val goal = MutablePixelPoint(noGoalValue)

    /**
     * A goal location this monster should strive to reach if there are no more-imminent goals.
     */
    override val otherGoal = MutablePixelPoint(noGoalValue)

    /**
     * A vector used to help detm this monster's current facing.
     */
    override val lastDirectionMoved = Point2D.Float()

    /**
     * See MoveTowardsGoalListener inner class.
     */
    private val moveTowardsGoalListener = MoveTowardsGoalListener1()

    /**
     * A true value here will override this monster's type's setting for this value.
     */
    private var mutableIsStationary = false
    override val isStationary: Boolean get() = mutableIsStationary || type.isStationary
    override fun makeStationary() = run { this.mutableIsStationary = true }

    override var isFleeing: Boolean = false
        protected set(value) {
            field = value
            onIsFleeingChange(value)
        }

    override var fleeFromPlayer: Player? = null

    /**
     * If this monster is fleeing, the index of the game turn in which it started doing so.
     */
    private var fleeStartTurn: Int = 0

    override var isTurnOver: Boolean = false

    override fun endTurn() = run { isTurnOver = true }

    /**
     * Stores attributes that are constant across all monsters of this type.  Since
     * these values are immutable, we serialize only the type's name.
     */
    @Transient
    lateinit var mutableType: MonsterType
        protected set
    override val type: MonsterType get() = mutableType

    /**
     * The name of the above monster type, to be serialized for this object instead of
     * the whole type.
     */
    private var typeName: String? = null

    override var isActivated: Boolean = false
        protected set(value) {
            field = value
            onIsActivatedChange(value)
        }

    override fun activate() = run { isActivated = true }
    override fun deactivate() = run { isActivated = false }

    override val avoidance: Int
        get() = super.avoidance - type.avoidanceModifier

    override val modifierOnIncomingAttack: Int
        get() = if (isActivated) 0 else 4

    override val powerRating: Int
        get() = type.powerRating

    override val maxMovementPoints: Int
        get() = type.movementPoints

    override val movementType: MovementType
        get() = type.movementType

    override val movementSound: Sound?
        get() {
            // if this monster has a special movement sound, and we detm that it's time for it to be heard, then
            // return that sound
            val sound = type.sound
            return if (randomFloat <= type.soundFrequency) sound else super.movementSound
        }

    override val isEvil: Boolean
        get() = type.alignment == Alignment.evil

    override val movementContext =
        MonsterMovementContext(maxPathDistanceToGoal, noGoalValue,
            moveTowardsGoalListener, ::MutablePixelPoint, { from_, to_, use ->
                getUnitVector(from_ as PixelPoint, to_ as PixelPoint, use)
            }, { from_, to_, use ->
                getDistance(from_ as PixelPoint, to_ as PixelPoint, use as PixelDistance)
            }, { from, to, atMost ->
                isDistanceAtMost(from as PixelPoint, to as PixelPoint,
                    atMost as PixelDistance)
            }, {
                it.forEach { step ->
                    pfPixelPointPool.reclaim(step as PFMutablePixelPoint)
                }
                it.clear()
                @Suppress("UNCHECKED_CAST") pfPathDequePool.reclaim(
                    it as ArrayDeque<PFPixelPoint>)
            })

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(stream: ObjectInputStream) {
        stream.defaultReadObject()

        // detm this monster's type from its type-name
        mutableType = nameToMonsterTypeMap[typeName]!!
    }

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(s: ObjectOutputStream) {
        // store the name of this monster's type (instead of the type itself)
        typeName = type.name

        s.defaultWriteObject()
    }

    /**
     * Informs this monster it has been created with the given monster-type.
     */
    override fun onCreated(type: MonsterType) {
        this.mutableType = type

        // set this entity's initial size from its type
        mutableBounds.setSize(type.size)

        // if not already done elsewhere, determine this monster's max hit points
        if (maxHitPoints == 0) maxHitPoints = randomBellCurveInt(1, 8, type.powerRating)

        hitPoints = maxHitPoints
    }

    override fun seekLocationIfNotFocusedOnPlayer(location: PixelPoint) {
        // if this monster isn't currently focused on the player
        if (!isFocusedOnPlayer) {
            // make the given location a goal this monster might try to reach
            otherGoal.set(location)
        }
    }

    private fun onIsFleeingChange(newValue: Boolean) {
        // if this monster is now fleeing
        if (newValue) {
            // remember when this monster started fleeing
            val game = Game.currentGame
            fleeStartTurn = game!!.turn
        }
    }

    override fun act() {
        super<Being1>.act()

        isTurnOver = false

        informTerrainsOfPresence()

        // if this monster's turn is now over, or it is dead, due to the preceding code
        if (isTurnOver || isDead) return

        // if this monster is paralyzed
        if (isParalyzed) return

        // if this monster is fleeing
        if (isFleeing) {
            checkForFleeingEnd()
        }

        // we'd like this monster to emit a movement sound as soon as it starts moving
        movementSoundCounter = 0

        // if this monster is not activated and isn't fleeing
        if (!isActivated && !isFleeing) {
            // if it should be activated
            if (determineIfShouldActivate(lastDirectionMoved)) {
                isActivated = true
            }
        }

        // if this monster is activated and isn't focused on the player
        if (isActivated && !isFocusedOnPlayer) {
            // focus on the player if it's in LOS
            val player = area.player
            isFocusedOnPlayer = player?.isInLOSOf(location) == true
        }

        // if this monster is activated, is focused on the player and can see the player,
        // and decides to use a pre-movement power on that player
        val player = area.player ?: return
        val canSeePlayer = isFocusedOnPlayer && area.canSeeExt(location, player.location,
            pixelsPerShotMove, false, true, this, player).canSee
        if (isActivated && (canSeePlayer || !type.mustSeePlayerToUsePower) && (isFocusedOnPlayer && usePreMovementPower(
                player))) {
            // this monster's turn is done
            return
        }

        // give this monster its full allotment of movement points for this turn
        movementPoints = maxMovementPoints.toFloat()

        // determine this monster's goal for this turn; if there is none, end its turn
        val goalThisTurn =
            determineGoalForThisTurn(player, otherGoal, lastDirectionMoved, noGoalValue)
                ?: return
        goal.set(goalThisTurn)

        // have this monster check to see if it should make ranged attacks
        hadRangedAttackOpportunityThisTurn = false
        if (!isFleeing && canSeePlayer) {
            val result3 = checkForRangedAttacks()
            hadRangedAttackOpportunityThisTurn = result3.couldMakeAttack
            if (result3.madeAttack) return
        }

        // if this monster isn't fleeing and is already in melee range of its focus-player
        if (!isFleeing && isFocusedOnPlayer && isNextTo(player)) {
            // have this monster perform its melee attacks to finish out its turn
            checkForMeleeAttacks()
            isTurnOver = true
            return
        }

        // if this monster isn't stationary
        if (!isStationary) {
            // move this monster towards its goal
            moveTowardGoal(goal, player, canSeePlayer)
        }

        // with this monster's turn now over, if it is focused on the player,
        // is not following a path, and cannot see that player
        val isFollowingPath = !isDead && movementContext.path != null
        if (isFocusedOnPlayer && !isFollowingPath && !player.isInLOSOf(location)) {
            // make the player's current location this monster's new goal for next turn
            otherGoal.set(player.location)

            // the player is no longer considered to be focused on
            isFocusedOnPlayer = false
        }

        // if this monster is not activated and isn't fleeing
        if (!isActivated && !isFleeing) {
            // check once more now at the end of this monster's turn to see if the
            // results of all that it did above should cause it to be activated
            if (determineIfShouldActivate(lastDirectionMoved)) {
                isActivated = true
            }
        }
    }

    private inner class MoveTowardsGoalListener1 : MoveTowardsGoalListener, Serializable {
        override fun onMovedOntoNewTile(tile: BMTile): Boolean {
            // if this monster isn't activated and can work doors
            if (!isActivated && type.movementType.allowsWorkingDoors) {
                // close the door (if there is one) at this monster's new location
                area.closeDoor(location, false)
            }

            return true
        }

        override fun getShouldContinue(oldLocation: BMPixelPoint): Boolean {
            // if the movement step just taken caused this monster to die
            return !isDead

        }

        override fun onAtTerrain(terrain: BMTerrain): Boolean {
            return true
        }
    }

    /**
     * Meant to be overridden to specify some action this monster may take
     * at the start of its turn, before it has moved, that will consume the rest of its turn.
     * It is presumed that either the given player, whose presence would be the
     * reason for the power use, is in LOS of this monster, or that monster doesn't need
     * to see the player to use its power.
     *
     * @return      Whether such an action was taken.
     */
    protected open fun usePreMovementPower(player: Player): Boolean {
        return false
    }

    override fun die(damage: Damage) {
        // remember these values for use below, as super.doDie will change them
        val area = area
        val location = location

        super.die(damage)

        indicateDeath(area, location)
    }

    override fun getDefense(attackType: AttackType, attacker: Being?): Int {
        var defense = computeGameSpecificMonsterDefense(this, attackType)

        if (isParalyzed) defense += paralysisDefenseModifier

        return defense
    }

    override fun getRangedToHitModifier(target: Being): Int {
        return 0
    }

    override fun fleeFromPlayer(player: Player) {
        isFleeing = true
        fleeFromPlayer = player
    }

    /**
     * Has this monster check to see if it rallies from its current fleeing status.
     */
    private fun checkForFleeingEnd() {
        // if this monster has been fleeing for at least the minimum flee duration
        val minFleeDuration = 6
        val game = Game.currentGame
        if (game!!.turn - fleeStartTurn >= minFleeDuration) {
            // if this monster makes a rally check
            val roll = randomInt(1, 20)
            if (roll >= 20 - type.powerRating / 2) {
                // this monster is no longer fleeing
                isFleeing = false
            }
        }
    }

    override fun checkForMeleeAttacks() {
        checkForMeleeAttacksExt()
    }

    override fun checkForRangedAttacks(): CheckForRangedAttacksResult =
        checkForRangedAttacksExt()

    companion object {
        /**
         * The maximum distance in tiles that a monster's path to its next goal
         * is allowed to be.
         */
        private val maxPathDistanceToGoal = TileDistance(13)

        val noGoalValue = PixelPoint(0, 0)
    }
}

@ExtensionPoint
var computeGameSpecificMonsterDefense: (monster: Monster1, attackType: AttackType) -> Int =
    { _, _ -> 0 }