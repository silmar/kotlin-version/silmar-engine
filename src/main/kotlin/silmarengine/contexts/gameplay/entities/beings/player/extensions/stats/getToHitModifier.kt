package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes

/**
 * Returns this player's to-hit modifier when attacking the given victim.
 *
 * @param offhand       Whether this player's attack is being made with its off-hand.
 */
fun Player.getToHitModifier(offhand: Boolean, victim: Being): Int {
    var modifier = 0

    // detm the weapon in use
    val offhandWeapon = offhandWeapon
    val weapon = if (!offhand) weapon else offhandWeapon
    modifier += weapon!!.weaponType.toHitModifier

    modifier = adjustGameSpecificPlayerToHitModifier(this, modifier, victim, weapon)

    // if this player is attacking with two weapons, and neither of them is a hand
    val provider = PlayerAttributeModifierProvider
    val values = attributeValues
    if (getAttacksPerTurn(true) > 0 && !isHandBeingUsedAsWeapon) {
        // add the penalty for this player's agility
        modifier += provider.toHitWithTwoWeaponsModifierForAgility(values.agility,
            offhand)
    }

    // if the weapon in use isn't ranged
    modifier += if (!weapon.weaponType.isRanged) provider.toHitModifierForStrength(
        values.strength)
    else provider.rangedAttackToHitModifierForAgility(values.agility)

    return modifier
}

@ExtensionPoint
var adjustGameSpecificPlayerToHitModifier: (player: Player, modifier: Int, victim: Being, weapon: Weapon) -> Int =
    { _, modifier, _, _ -> modifier }

val Player.isHandBeingUsedAsWeapon: Boolean
    get() = weapon.weaponType == WeaponTypes.hand || offhandWeapon?.weaponType == WeaponTypes.hand

