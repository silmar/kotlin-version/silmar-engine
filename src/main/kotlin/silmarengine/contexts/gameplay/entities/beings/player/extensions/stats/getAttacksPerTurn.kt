package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

/**
 * Returns the number of attacks this player gets to make per game turn.
 *
 * @param offhand       Whether the caller is inquiring about off-hand attacks.
 */
fun Player.getAttacksPerTurn(offhand: Boolean): Int {
    var attacks = 0

    val weapon = (if (!offhand) weapon else offhandWeapon) ?: return 0
    attacks += weapon.weaponType.numAttacks

    attacks += computeExtraAttacksPerTurnForGameSpecificPlayer(this, weapon)

    return attacks
}

@ExtensionPoint
var computeExtraAttacksPerTurnForGameSpecificPlayer: (player: Player, weapon: Weapon) -> Int =
    { _, _ -> 0 }