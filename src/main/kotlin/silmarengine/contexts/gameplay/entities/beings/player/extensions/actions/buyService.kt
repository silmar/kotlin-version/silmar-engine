package silmarengine.contexts.gameplay.entities.beings.player.extensions.actions

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.onRestoreLevelsService
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.tallyGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.removeGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCureDiseaseService
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCurePoisonService
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onHealFullyService
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onHealWoundsService
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onRemoveCursedItemsService
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.talkers.Service
import silmarengine.contexts.gameplay.entities.talkers.ServiceSeller
import silmarengine.contexts.gameplay.entities.talkers.Services

/**
 * Has this player try to buy one working of the given service from
 * the given seller.
 *
 * @return Whether the service was actually bought and performed.
 */
fun Player.buyService(service: Service, seller: ServiceSeller): Boolean {
    // if this player doesn't have enough gold
    val price = seller.getServicePrice(service, this)
    if (tallyGold(true) < price) {
        // don't allow the purchase
        onMessage(Player.notEnoughGoldMessage, MessageType.error)
        return false
    }

    // execute the service
    var performed = false
    val sellerEntity = seller as Entity
    when (service) {
        Services.healWounds -> performed = onHealWoundsService(sellerEntity)
        Services.healFully -> performed = onHealFullyService(sellerEntity)
        Services.curePoison -> performed = onCurePoisonService(sellerEntity)
        Services.cureDisease -> performed = onCureDiseaseService(sellerEntity)
        Services.removeCursedItems -> performed =
            onRemoveCursedItemsService(sellerEntity)
        Services.restoreLevels -> performed = onRestoreLevelsService(sellerEntity)
    }

    // if the service was performed
    if (performed) {
        // pay its price
        removeGold(price)
    }

    return performed
}

