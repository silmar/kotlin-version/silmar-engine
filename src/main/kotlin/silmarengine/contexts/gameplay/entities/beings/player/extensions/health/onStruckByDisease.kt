package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Disease
import silmarengine.contexts.gameplay.entities.beings.player.DiseaseAffliction
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.game.Game

/**
 * Informs this player that it's been struck by the given disease.
 */
fun Player.onStruckByDisease(disease: Disease) {
    if (shouldGameSpecificPlayerIgnoreDisease(this, disease)) return

    // if this player already has the given disease, there is nothing to do
    if (statuses.diseaseAfflictions.any { it.disease!! == disease }) return

    // add the disease (as an affliction) to the list of afflictions this player has
    val affliction = DiseaseAffliction()
    affliction.disease = disease
    affliction.turnAfflicted = Game.currentGame!!.turn
    statuses.onAfflictedWithDisease(affliction)

    onMessage("DISEASED!")

    // if the new disease-affliction is the only one affecting this player
    if (statuses.diseaseAfflictions.size == 1) {
        // report that this player's diseased status has changed
        reportToListeners { it.onVitalStatChanged() }
    }
}

@ExtensionPoint
var shouldGameSpecificPlayerIgnoreDisease: (player: Player, disease: Disease) -> Boolean =
    { _, _ -> false }