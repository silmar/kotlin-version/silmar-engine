package silmarengine.contexts.gameplay.entities.beings.monsters.types

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmarengine.contexts.gameplay.entities.types.EntityType1
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sound

/**
 * Describes the attributes of a particular kind of monster.
 */
@Suppress("LeakingThis")
open class MonsterType1(frequency: Int, name: String, imageName: String?,
    override val powerRating: Int, override val alignment: Alignment,
    override val experienceValue: Int, override val movementPoints: Int,
    override val defense: Int, override val isLivingDead: Boolean,
    override val meleeAttacks: Array<MonsterAttack>?,
    override val rangedAttacks: Array<MonsterAttack>?,
    override val rangedAttackFrequency: Int, override val movementType: MovementType,
    override val weaponPlusNeededToHit: Int, override val avoidanceModifier: Int,
    override val isLessVulnerableToEdgedWeapons: Boolean, override val sound: Sound?,
    override val soundFrequency: Float, override val isStationary: Boolean,
    override val mustSeePlayerToUsePower: Boolean = true,
    lightRadiusInTiles: Int? = null) : MonsterType,
    EntityType1(name, if (imageName != null) monsterTypeImagePath + imageName else null,
        frequency) {
    override val lightRadius: PixelDistance? =
        if (lightRadiusInTiles != null) PixelDistance(TileDistance(lightRadiusInTiles))
        else null

    init {
        determineSize()

        monsterTypes.add(this)
        nameToMonsterTypeMap[name] = this
    }

    override fun equals(other: Any?): Boolean {
        return other === this || (other is MonsterType1 && other.name == name)
    }

    override fun isPresenceLocationValid(area: Area, location: PixelPoint): Boolean {
        // if there is already a being at the given location
        if (getFirstEntityAt(location, area.beings) != null) {
            return false
        }

        // if the tile at the given location isn't passible or blocks los
        val tile = area.getTile(location)
        return !(!tile.isPassible || tile.blocksLOS)

    }

    override fun createStockInstance(): Entity {
        return createMonster(this)
    }

    override fun isValidForArea(area: Area) = true

    override fun hashCode(): Int {
        return name.hashCode()
    }
}
