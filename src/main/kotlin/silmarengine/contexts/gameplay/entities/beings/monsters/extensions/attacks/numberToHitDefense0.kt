package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster

/**
 * Returns the number (or higher,,on a 1 to 20 scale) this monster needs to hit a being
 * with a defense rating of zero.
 */
val Monster.numberToHitDefense0: Int
    get() = 9 - type.powerRating - getMonsterModifierToHitDefense0(this)

@ExtensionPoint
var getMonsterModifierToHitDefense0: (monster: Monster) -> Int = { _ -> 0 }
