package silmarengine.contexts.gameplay.entities.beings.extensions.reactions

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

/**
 * Informs this player that an attack directly on it missed.
 */
fun Being.onAttackedAndMissed() {
    // add a no-damage visual effect at the given location
    val effect = VisualEffect1("noDamage")
    area.addEntity(effect, location)

    // issue an accompanying sound
    area.onSoundIssued(Sounds.miss, location)

    // if the player can see this being
    if (area.player?.isInLOSOf(location) == true) {
        // pause a bit for the effect to be seen
        sleepThread(300)
    }

    // remove the no-damage visual effect from the area
    area.removeEntity(effect)
}
