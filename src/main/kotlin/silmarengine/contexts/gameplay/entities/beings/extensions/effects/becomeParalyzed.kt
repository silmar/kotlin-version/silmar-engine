package silmarengine.contexts.gameplay.entities.beings.extensions.effects

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf

/**
 * Puts this being into a paralyzed state for the given number of game turns.
 */
fun Being.becomeParalyzed(duration: Int) {
    if (isBeingImmuneToParalysis(this)) return

    // if this being is a monster which is immune
    if (this is Monster && type.isLivingDead) {
        area.player?.receiveMessageIfInLOSOf(location, "No effect")
        return
    }

    // if this being is already paralyzed, skip this, to be fair
    if (isParalyzed) return

    area.player?.receiveMessageIfInLOSOf(location, "Paralysis!")

    setParalyzedDuration(duration)
}

@ExtensionPoint
var isBeingImmuneToParalysis: (being: Being) -> Boolean = { false }

/**
 * Ends any paralyzed state this being might be in.
 */
fun Being.becomeNotParalyzed() {
    setParalyzedDuration(0)
}

