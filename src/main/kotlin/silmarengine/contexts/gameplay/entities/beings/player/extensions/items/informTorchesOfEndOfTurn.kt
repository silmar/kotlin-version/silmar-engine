package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.specializations.Torch
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes

/**
 * Has this player inform any lit torches it is carrying that another game turn has ended.
 */
fun Player.informTorchesOfEndOfTurn() {
    items.filterInventory { it.isOfType(NonGroupableItemTypes.torch) }.forEach { item ->
        (item as Torch).onEndOfTurn()
    }
}

