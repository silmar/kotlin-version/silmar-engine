package silmarengine.contexts.gameplay.entities.beings.player

/**
 * Note that those stats which the player inherently has as a being are not covered here.
 */
interface PlayerStats {
    val gold: Int

    /**
     * Returns what roll this player needs on a d20 to hit a target with a zero
     * defense rating.
     */
    val numberToHitZeroDefense: Int
}