package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

/**
 * Has this player donate the given item, which it is assumed to possess.
 */
fun Player.donateItem(item: Item, quantity: Int = item.quantity) {
    // if the item is equipped on this player
    var itemToDonate = item
    if (items.isItemEquipped(item)) {
        // if the item cannot be un-equipped
        if (!tryToUnequipItem(item, false, false)) {
            // don't allow the donation
            return
        }
    }

    // if not all of the item is to be donated
    if (item.quantity > quantity) {
        // reduce the quantity of the given item to what will be left
        item.quantity -= quantity

        // create a new item with the quantity to be donated
        itemToDonate = createGroupableItem(item.type, quantity)
    }
    else {
        // remove the item from this player's inventory (which is where it should be at this point
        // whether it was equipped above or not)
        items.inventory.tryToRemoveItem(itemToDonate)
    }

    onItemLost(itemToDonate, ItemLostReason.donated)

    area.onSoundIssued(Sounds.itemDonated, location)

    // add the experience value of the item to this player's total
    val gained = itemToDonate.getValue(ItemValueContext.EXPERIENCE)
    levelValues.onExperienceGained(gained)

    // inform this player what it's getting for its donation
    onMessage("You gained $gained experience points for your donation!",
        MessageType.important)
}

