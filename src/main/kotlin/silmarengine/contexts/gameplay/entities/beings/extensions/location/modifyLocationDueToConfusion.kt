package silmarengine.contexts.gameplay.entities.beings.extensions.location

import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInLOSAndRange
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileSize
import silmarengine.util.math.randomInt

fun modifyLocationDueToConfusion(
    area: Area, location: PixelPoint, from: PixelPoint
): PixelPoint {
    // if a random chance fails, don't modify
    if (randomInt(0, 1) == 0) return location

    // return a random passible rectangle-location that is in LOS of the given
    // from-location, and not farther from the from-location than the given
    // unconfused-location;
    // note: we can use the tile-size as the rectangle size since we don't care about
    // the success of the confused entity being able to do whatever it's trying to do
    // with the confused location
    val modified = area.getRandomPassibleRectangleLocationInLOSAndRange(from, tileSize,
        getDistance(from, location), 50, MovementType.WALKING, false)
    return modified ?: location
}
