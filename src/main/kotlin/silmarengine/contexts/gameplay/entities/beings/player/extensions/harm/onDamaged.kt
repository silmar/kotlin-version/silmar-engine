package silmarengine.contexts.gameplay.entities.beings.player.extensions.harm

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.items.extensions.condition.onHolderDamaged
import kotlin.math.ceil

fun Player.onDamagedAsPlayer(damage: Damage) {
    // cancel any pending movement
    cancelPendingMove()

    onDamagedAsGameSpecificPlayer(this, damage)

    // if the damage affects items, too
    if (damage.form.affectsItems) {
        // for however many items may be affected, which depends on the damage amount
        val numItemsAffected = ceil((damage.amount / 5f).toDouble()).toInt()
        for (i in 0 until numItemsAffected) {
            // choose an item at random on this player (repeats being ok)
            val item = items.randomItem

            // have this item check to see if it damaged
            item?.onHolderDamaged(damage)
        }
    }
}

@ExtensionPoint
var onDamagedAsGameSpecificPlayer: (player: Player, damage: Damage) -> Unit = { _, _ -> }