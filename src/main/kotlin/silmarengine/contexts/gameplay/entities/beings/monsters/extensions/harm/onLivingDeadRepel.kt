package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm

import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

/**
 * Informs this monster that it is within the area of effect of a living-dead-repel
 * power use of the given strength and performed by the given player.
 */
fun Monster.onLivingDeadRepel(repeller: Player, strength: Int) {
    // if this monster is of a living dead type
    if (!type.isLivingDead) return

    // if this monster does not avoid the repel
    val result = avoid(strength)
    if (!result.avoided) {
        // it flees
        fleeFromPlayer(repeller)

        // add a visual effect at this monster's location
        val effect =
            LightSourceVisualEffect("repelled", PixelDistance(TileDistance(4)))
        area.addEntity(effect, location)

        // issue an accompanying sound
        area.onSoundIssued(Sounds.livingDeadRepel, location)

        // pause a bit for the effect to be seen
        sleepThread(700)

        // remove the visual effect from the area
        area.removeEntity(effect)

        // if the save was missed by 8 or more
        if (result.missedBy >= 8) {
            // this monster is destroyed
            takeDamage(Damage(hitPoints, DamageForms.holyPower))
        }

        // focus on the player as the being it is now fleeing from
        focusOnPlayer()
    }
}

