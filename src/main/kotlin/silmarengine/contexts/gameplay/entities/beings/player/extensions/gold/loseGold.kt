package silmarengine.contexts.gameplay.entities.beings.player.extensions.gold

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.ItemLostReason
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onItemLost
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes

/**
 * Makes tha player lose the given quantity of gold (if it has it), for the given reason.
 *
 * @return      Whether any gold was actually lost.
 */
fun Player.loseGold(quantity: Int, reason: ItemLostReason): Boolean {
    // if this player's inventory contains gold
    val gold = items.inventory.getFirstItemOfType(GroupableItemTypes.gold) as Item?
    if (gold != null) {
        // lose up to the given quantity
        gold.quantity -= quantity
        onItemLost(gold, quantity, reason)

        return true
    }

    return false
}

