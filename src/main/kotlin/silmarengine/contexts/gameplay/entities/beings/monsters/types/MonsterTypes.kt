package silmarengine.contexts.gameplay.entities.beings.monsters.types

import java.util.ArrayList
import java.util.HashMap

/**
 * A list of all monster types.
 */
val monsterTypes: MutableList<MonsterType> = ArrayList()

/**
 * A mapping of all monster types by their names.
 */
val nameToMonsterTypeMap = HashMap<String, MonsterType>()

