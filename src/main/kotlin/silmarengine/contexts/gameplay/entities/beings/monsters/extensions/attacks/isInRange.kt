package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Returns whether any of this monster's ranged attacks are capable of reaching
 * the given target location.
 */
fun Monster.isInRange(target: PixelPoint): Boolean {
    val attacks = type.rangedAttacks ?: return false

    // detm the distance to the given location
    val range = getDistance(location, target)

    return attacks.any { attack ->
        attack.maxRange.distance * tileWidth >= range.distance
    }
}

