package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem

fun Player.onEquippedItemClicked(item: PIMItem, rightClick: Boolean) {
    item as Item
    if (rightClick) tryToUnequipItem(item, true, false)
    else tryToUseItem(item)
}