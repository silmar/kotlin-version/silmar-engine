package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.getStandardModifier
import kotlin.math.max

object PlayerAttributeModifierProvider {
    fun maxMovementPointsModifierForStrength(value: Int): Int = (value - 10) / 4
    fun maxMovementPointsModifierForAgility(value: Int): Int = (value - 10) / 4
    fun rangedAttackToHitModifierForAgility(value: Int): Int = getStandardModifier(value)
    fun defenseModifierForAgility(value: Int): Int = getStandardModifier(value)
    fun avoidanceModifierForAgility(value: Int): Int = getStandardModifier(value)
    fun hitPointsModifierForEndurance(value: Int): Int = getStandardModifier(value)
    fun maxEncumbranceModifierForStrength(value: Int): Int = (value - 10) * 10
    fun maxEncumbranceModifierForEndurance(value: Int): Int = (value - 10) * 10
    fun toHitModifierForStrength(value: Int): Int = getStandardModifier(value)
    fun damageModifierForStrength(value: Int): Int = getStandardModifier(value)
    fun toHitWithTwoWeaponsModifierForAgility(value: Int, offhand: Boolean): Int =
        -(max(0, 23 - value) / 4) * if (offhand) 2 else 1
}