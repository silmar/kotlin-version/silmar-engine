package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.tallyGold
import java.io.Serializable

open class PlayerStats1(val player: Player) : PlayerStats, Serializable {
    override val gold: Int
        get() = player.tallyGold(false)

    override val numberToHitZeroDefense: Int get() = 11 - player.levelValues.level
}