package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.movement

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import java.awt.geom.Point2D

/**
 * Note that the lastDirectionMoved argument is modified as a side effect of this method.
 */
fun Monster.determineGoalForThisTurn(
    player: Player, otherGoal: PixelPoint?, lastDirectionMoved: Point2D.Float,
    noGoalValue: PixelPoint
): PixelPoint? {
    // if this monster is stationary
    val goal: PixelPoint?
    if (isStationary) {
        // specify this monster's goal as its current location; we don't give it a null
        // goal, as that would disallow it from making attacks below
        goal = location
    }
    // else, if this monster is fleeing
    else if (isFleeing) {
        goal = computeFleeingGoal()
    }
    // else, if this monster if focused on the player
    else if (isFocusedOnPlayer) {
        // make the player's location this monster's goal for this turn
        goal = player.location
    }
    // else, if some other goal is being sought
    else if (otherGoal != noGoalValue) {
        // make the other goal this monster's goal
        goal = otherGoal
    }
    // else, if this monster is activated, and its area has an associated adjacency-graph
    else if (isActivated && area.adjacencyGraph != null) {
        // try to detm this monster's goal through its adjacency graph
        goal = computeGoalFromAdjacencyGraph()

        // if not even the adjacency graph could detm a goal, de-activate this monster
        if (goal == null) deactivate()
    }
    // otherwise, this monster isn't activated and has no goal
    else {
        goal = computeUnactivatedGoal(lastDirectionMoved)

        // if no goal was found, search around this monster next time, rather than in the
        // direction last moved
        if (goal == null) lastDirectionMoved.setLocation(noLastMovementValue)
    }

    return goal
}

val noLastMovementValue = Point2D.Float(0f, 0f)