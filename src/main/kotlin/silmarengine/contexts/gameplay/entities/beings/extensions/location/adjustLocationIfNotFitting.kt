package silmarengine.contexts.gameplay.entities.beings.extensions.location

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible

fun Being.adjustLocationIfNotFitting() {
    // if this being's image change causes its size to be too big for the spot in this area
    // at which it currently resides
    if (!area.isRectanglePassible(location, size, movementType, true, this, null,
            false).passible) {
        // move this being to a nearby location that is passible for it
        area.moveEntity(this,
            area.getNearbyPassibleRectangleLocation(location, size, movementType,
                PixelDistance.maxValue, true, this, null)!!)
    }
}