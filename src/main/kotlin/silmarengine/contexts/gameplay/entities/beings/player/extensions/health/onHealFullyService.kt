package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Informs this player that it is to receive a heal-fully service from the given
 * seller.
 *
 * @return      Whether the service was performed.
 */
fun Player.onHealFullyService(seller: Entity): Boolean {
    // if this player is not damaged
    if (!isDamaged) {
        onMessage("You aren't hurt!", MessageType.error)
        return false
    }

    // heal this player fully
    performPowerDischargeEffect(seller)
    healFully()

    return true
}

