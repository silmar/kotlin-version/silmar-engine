package silmarengine.contexts.gameplay.entities.beings.extensions.effects

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.util.math.randomInt

/**
 * Holds the return values of a call to Being.avoid().
 */
data class AvoidResult(
    /**
     * Whether the phenomenon was avoided.
     */
    val avoided: Boolean = false,
    /**
     * Whether the phenomenon was super-avoided (often for little or no damage,
     * when otherwise there would be some).
     */
    val superAvoided: Boolean = false,
    /**
     * If the phenomenon wasn't avoided, by how much the check missed.
     */
    val missedBy: Int = 0
)

/**
 * Returns the result of this being making an avoidance check, subject to the given
 * penalty (for which, positive numbers make avoidance less likely, and negative
 * numbers make it more likely).
 */
fun Being.avoid(penalty: Int): AvoidResult {
    // make the avoidance check
    val roll = randomInt(1, 20)
    val avoided = when (roll) {
        1 -> false
        20 -> true
        else -> roll - penalty >= avoidance
    }
    val superAvoided = when (roll) {
        1 -> false
        20 -> true
        else -> roll - penalty - 8 >= avoidance
    }
    val missedBy = when (roll) {
        1 -> 20
        20 -> 0
        else -> avoidance - (roll - penalty)
    }

    // if the avoidance succeeded
    if (avoided) {
        // inform the player if it's in LOS
        area.player?.receiveMessageIfInLOSOf(location,
            if (superAvoided) "super-avoids" else "avoids")
    }

    return AvoidResult(avoided, superAvoided, missedBy)
}

