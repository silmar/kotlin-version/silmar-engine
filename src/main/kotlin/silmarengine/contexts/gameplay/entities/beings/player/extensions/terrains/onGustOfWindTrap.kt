package silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.firstLitTorch
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage

/**
 * Informs this player that it has tripped a gust-of-wind trap.
 */
fun Player.onGustOfWindTrap() {
    onMessage("A strong, wet gust of wind whips past you!")

    // for each lit torched carried by this player
    while (true) {
        val torch = items.inventory.firstLitTorch ?: break

        // this torch is spoiled
        onMessage("Your torch is somehow spoiled!")
        torch.spoil()
    }
}

