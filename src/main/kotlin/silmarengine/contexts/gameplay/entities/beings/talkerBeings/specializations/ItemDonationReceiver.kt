package silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.talkers.Talker

interface ItemDonationReceiver : Talker