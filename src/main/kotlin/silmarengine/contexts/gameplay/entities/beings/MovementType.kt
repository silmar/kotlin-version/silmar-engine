package silmarengine.contexts.gameplay.entities.beings

import silmarengine.contexts.beingmovement.domain.model.BMMovementType
import silmarengine.contexts.pathfinding.domain.model.PFMovementType

/**
 * Classifies a particular mode of movement.
 */
enum class MovementType(
    /**
     * Whether this movement type doesn't produce any movement sound.
     */
    val isQuiet: Boolean,
    /**
     * Whether this movement type allows the opening and closing of doors.
     */
    val allowsWorkingDoors: Boolean
) {
    WALKING(false, true),
    WALKING_NO_HANDS(false, false),
    FLYING(true, true),
    FLYING_NO_HANDS(true, false),
    EARTHING(false, true),
    ETHEREAL(true, false);

    fun toPFMovementType() = PFMovementType.values()[ordinal]

    fun toBMMovementType() = BMMovementType.values()[ordinal]
}

fun PFMovementType.toMovementType() = MovementType.values()[ordinal]

fun BMMovementType.toMovementType() = MovementType.values()[ordinal]