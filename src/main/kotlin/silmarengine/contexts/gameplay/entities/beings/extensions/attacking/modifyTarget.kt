package silmarengine.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import silmarengine.util.math.randomFloat
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin

/**
 * Returns a pixel-location that is randomly modified from the given target pixel-location
 * according to the distance to the target from the given attacker pixel-location,
 * the given maximum tile-range of the attack, and the given accuracy of the attack.
 */
fun modifyTarget(
    attackerLocation: PixelPoint, target: PixelPoint,
    maxRange: TileDistance, accuracy: Int
): PixelPoint {
    // detm the penalty for the range between the attacker and the target
    val range = getDistance(attackerLocation, target)
    val penalty = getPenaltyForRange(range,
        PixelDistance(maxRange.distance * tileWidth))

    // if the attacker fails an accuracy check
    val missedBy = randomInt(1, 20) - accuracy + penalty
    var finalTarget = target
    if (missedBy > 0) {
        // detm the distance and at what angle to modify the target
        // (but don't let it be enough to move the target over or behind the attacker)
        val magnitude = min(range.distance - tileWidth,
            (missedBy / 8f * (range.distance / 8f)).toInt())
        val angle = (randomFloat.toDouble() * 2.0 * Math.PI).toFloat()

        // modify a copy of the target
        finalTarget = MutablePixelPoint(target)
        finalTarget.translate((magnitude * cos(angle.toDouble())).toInt(),
            (magnitude * sin(angle.toDouble())).toInt())
    }

    return finalTarget
}