package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.updateEncumbrance
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

/**
 * The different reasons for which a player may lose one of its items.
 */
class ItemLostReason {

    companion object {
        /**
         * The flyweight item-lost-reasons.
         */
        var dropped = ItemLostReason()
        var used = ItemLostReason()
        var destroyed = ItemLostReason()
        var sold = ItemLostReason()
        var donated = ItemLostReason()
        var stolen = ItemLostReason()
    }
}

fun Player.onItemLost(item: Item, reason: ItemLostReason?) {
    onItemLost(item, item.quantity, reason)
}

/**
 * Informs this player it has lost the given quantity of the given item for the
 * given reason
 */
fun Player.onItemLost(item: Item, quantity: Int, reason: ItemLostReason?) {
    // if the item isn't groupable or has no quantity left, then it is no longer being held
    if (!item.type.isGroupable || item.quantity <= 0) item.onDropped()

    items.handsConfigs.removeItemFromAll(item)

    if (reason === ItemLostReason.destroyed) {
        val message: String = if (!item.type.isGroupable) {
            "Your " + item.name + " is destroyed!"
        }
        else {
            "" + quantity + " of your " + item.name + " are destroyed!"
        }

        area.onSoundIssued(Sounds.itemDamaged, location)
        area.onSoundIssued(Sounds.itemLost, location)

        onMessage(message)
    }
    else if (reason === ItemLostReason.stolen) {
        val message: String = if (!item.type.isGroupable) {
            "Your " + item.name + " is stolen!"
        }
        else {
            ("" + quantity + " of your " + item.type.pluralName + " " + (if (quantity > 1) "are" else "is") + " stolen!")
        }

        area.onSoundIssued(Sounds.itemLost, location)

        onMessage(message)
    }

    updateEncumbrance()
}

