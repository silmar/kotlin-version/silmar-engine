package silmarengine.contexts.gameplay.entities.beings.player

interface PlayerAttributeValues {
    val immutableValues: List<Int>

    fun getBaseAttribute(attribute: PlayerAttribute): Int
    fun addToBaseAttribute(attribute: PlayerAttribute, amount: Int)

    fun getAttribute(attribute: PlayerAttribute): Int
    fun onAttributeComputed(attribute: PlayerAttribute, value: Int)

    val strength: Int
    val intelligence: Int
    val judgement: Int
    val agility: Int
    val endurance: Int
}