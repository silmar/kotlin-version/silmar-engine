package silmarengine.contexts.gameplay.entities.beings.extensions.damage

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

/**
 * Applies the damage described by the given object to this being.
 * This should always be called instead of subtracting hit-points
 * directly.
 *
 * @param   damage      An object describing the damage done.
 */
fun Being.takeDamage(damage: Damage) {
    // if this being is already dead, skip this
    if (isDead) return

    // if this being is paralyzed
    if (isParalyzed) {
        // double the damage
        damage.amount *= 2
    }

    affectDamage(damage)

    // if after being affected, there is no damage, we are done
    if (damage.amount <= 0) return

    // subtract the damage
    addHitPoints(-damage.amount)

    // add a damage visual effect at the given location
    val effect = VisualEffect1(damageStatus.hitImageName)
    area.addEntity(effect, location)

    // if a positive damage amount was given
    val effectDuration = 300
    if (damage.amount > 0) {
        // have that amount displayed to the player if it's in LOS
        area.player?.receiveMessageIfInLOSOf(location, damage.amount.toString(),
            duration = effectDuration)
    }

    // issue an accompanying sound
    area.onSoundIssued(Sounds.damage, location)

    // if the player can see this being
    if (area.player?.isInLOSOf(location) == true) {
        // pause a bit for the effect to be seen
        sleepThread(effectDuration.toLong())
    }

    // remove this damaging's visual effect from the area
    area.removeEntity(effect)

    onDamaged(damage)

    // if this being is now out of hit points
    if (isDead) {
        die(damage)
    }
}

