package silmarengine.contexts.gameplay.entities.beings.talkerBeings

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingType
import silmarengine.contexts.gameplay.entities.talkers.Talker

interface TalkerBeing : Talker, Being {
    val type: TalkerBeingType

    fun onCreated(type: TalkerBeingType)
}