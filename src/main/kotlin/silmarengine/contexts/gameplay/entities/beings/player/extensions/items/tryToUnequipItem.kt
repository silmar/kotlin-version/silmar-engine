package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS

/**
 * Has this player try to unequip the given item.
 *
 * @param checkForMonstersInLOS     Whether to check for the presence of monsters in LOS,
 * before allowing the unequip (as certain items, like armor,
 * shouldn't be allowed to be unequipped during combat).
 * @param ignoreCurse       Whether the item should be unequipped regardless of whether it
 * is cursed.
 *
 * @return      Whether the unequip was successful.
 */
fun Player.tryToUnequipItem(
    item: Item, checkForMonstersInLOS: Boolean, ignoreCurse: Boolean
): Boolean {
    if (isParalyzed) return false

    // if we are to check for monsters in LOS, and the item is an armor,
    // and there are monsters in LOS
    if (checkForMonstersInLOS && item.type.isArmor && area.getEntitiesInLOS(location,
            area.monsters).isNotEmpty()) {
        // don't allow the un-equip, as it would be unrealistic
        onMessage("You cannot remove armor right now.", MessageType.error)
        return false
    }

    // if the item is cursed
    if (!ignoreCurse && item.type.isCursed) {
        // don't allow the removal
        onMessage(Player.removeCursedItemMessage, MessageType.error)
        return false
    }

    items.playerItems.unequipItem(item)

    return true
}

