package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon

/**
 * Returns this player's modifier to damage when making an attack.
 *
 * @param isForOffhand       Whether the attack is being made with this player's off-hand.
 */
fun Player.computeDamageModifier(isForOffhand: Boolean): Int {
    var modifier = 0

    // add the modifier for the weapon in use
    val weapon = if (!isForOffhand) weapon else offhandWeapon
    modifier += weapon!!.weaponType.damageModifier

    modifier += computeGameSpecificPlayerDamageModifier(this, isForOffhand)

    // if the weapon in use isn't ranged, add a strength modifier
    val provider = PlayerAttributeModifierProvider
    if (!weapon.weaponType.isRanged) modifier += provider.damageModifierForStrength(
        attributeValues.strength)

    return modifier
}

@ExtensionPoint
var computeGameSpecificPlayerDamageModifier: (player: Player, isForOffhand: Boolean) -> Int =
    { _, _ -> 0 }
