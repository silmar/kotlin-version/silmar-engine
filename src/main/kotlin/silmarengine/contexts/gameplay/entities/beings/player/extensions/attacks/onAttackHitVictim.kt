package silmarengine.contexts.gameplay.entities.beings.player.extensions.attacks

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

/**
 * Informs this player that one of its attacks hit the given victim for
 * the given amount of damage.
 *
 * @param offhand       Whether the attack was made using the off-hand.
 */
fun Player.onAttackHitVictim(victim: Being, damage: Int, offhand: Boolean) {
    // determine the weapon used
    val weapon = if (!offhand) weapon else offhandWeapon
    weapon!!.onHitVictim(victim, this, damage)

    onGameSpecificPlayerAttackHitVictim(this, weapon, victim, damage)
}

@ExtensionPoint
var onGameSpecificPlayerAttackHitVictim: (player: Player, weapon: Weapon, victim: Being, damage: Int) -> Unit =
    { _, _, _, _ -> }