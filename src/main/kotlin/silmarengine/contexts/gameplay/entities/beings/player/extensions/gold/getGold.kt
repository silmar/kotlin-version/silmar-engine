package silmarengine.contexts.gameplay.entities.beings.player.extensions.gold

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.tiles.tileDistance

/**
 * Returns the quantity of gold this player currently possesses.
 *
 * @param   countItemsUnderneath    Whether to include any gold in gold-coinage items
 * at this player's location.
 */
fun Player.tallyGold(countItemsUnderneath: Boolean): Int {
    // detm how many gold (if any) are in the gold item in this player's inventory
    val goldItem = items.inventory.getFirstItemOfType(GroupableItemTypes.gold)
    var gold = goldItem?.quantity ?: 0

    // if we are to include gold at this player's location, as well
    if (countItemsUnderneath) {
        // add each gold item at (or very close to) this player's location to our total
        val itemEntities = area.items.getEntitiesInRange(location, tileDistance)
        itemEntities.filter { it.item.isOfType(GroupableItemTypes.gold) }
            .forEach { gold += it.item.quantity }
    }

    return gold
}

