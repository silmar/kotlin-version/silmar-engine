package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * Has this player gain one of the given attribute.
 */
fun Player.incrementBaseAttribute(attribute: PlayerAttribute) {
    // play a sound
    area.onSoundIssued(Sounds.attributeGained, location)

    attributeValues.addToBaseAttribute(attribute, 1)

    onMessage("You gain a point of " + attribute.name + "!")
}

