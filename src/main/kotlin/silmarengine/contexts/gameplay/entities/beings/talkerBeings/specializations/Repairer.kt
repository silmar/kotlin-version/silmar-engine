package silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.talkers.ItemFixer

open class Repairer : TalkerBeing1(), ItemFixer {
    override fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int {
        return normalPrice
    }

    override fun getCanFixItem(item: Item): Boolean {
        return item.isIdentified
    }
}