package silmarengine.contexts.gameplay.entities.beings.talkerBeings.types

import silmarengine.contexts.gameplay.entities.types.EntityType

/**
 * Describes the static attributes of a particular kind of talker-being.
 */
interface TalkerBeingType : EntityType {
    /**
     * How many movement points a being of this type receives per game turn.
     */
    val movementPoints: Int

    /**
     * The message with which a being of this type will greet a player.
     */
    val greeting: String
}

/**
 * The relative path to the image files for talker-being types.
 */
const val talkerBeingTypeImagePath = "beings/"