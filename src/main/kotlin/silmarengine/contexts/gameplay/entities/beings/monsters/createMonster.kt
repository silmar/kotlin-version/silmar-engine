package silmarengine.contexts.gameplay.entities.beings.monsters

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType

/**
 * Returns a newly created instance of the given monster-type.
 */
fun createMonster(type: MonsterType): Monster {
    val monster = createGameSpecificMonster(type) ?: when {
        type.lightRadius != null -> LightSourceMonster()
        else -> Monster1()
    }
    monster.onCreated(type)
    return monster
}

@ExtensionPoint
var createGameSpecificMonster: (type: MonsterType) -> Monster? = { _ -> null }