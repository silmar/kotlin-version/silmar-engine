package silmarengine.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.sounds.Sounds

/**
 * The flyweight attack-types.
 */
object AttackTypes {
    val meleeAttack = AttackType("meleeAttack", Sounds.meleeAttack)
    val oldProjectile = AttackType("oldProjectile", Sounds.oldProjectile)
}