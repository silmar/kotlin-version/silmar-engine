package silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.Terrain

/**
 * Has this player interact with the given terrain.
 *
 * @return Whether any meaningful interaction actually occurred.
 */
fun Player.interactWithTerrain(terrain: Terrain): Boolean {
    return when {
        doesPlayerInteractWithGameSpecificTerrain(this, terrain) -> true
        terrain.onInteractionWithPlayer(this) -> {
            endTurn()
            true
        }
        else -> false
    }
}

@ExtensionPoint
var doesPlayerInteractWithGameSpecificTerrain: (player: Player, terrain: Terrain) -> Boolean =
    { _, _ -> false }

