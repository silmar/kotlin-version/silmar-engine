package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.Being1
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider
import silmarengine.contexts.gameplay.entities.items.types.ItemType

/**
 * Returns this player's defense value versus the given attack type made by the given
 * attacker.
 */
fun Player.getDefenseAsPlayer(attackType: AttackType, attacker: Being?): Int {
    var defense = 0

    // add the defense modifier (for this penetration type) of each equipped item
    // to the rating
    items.compressEquippedItems().forEach {
        defense += computeItemDefenseModifierVsGameSpecificAttack(it.type, attackType)
            ?: it.type.defenseModifier
    }

    // add in the modifier for this player's agility
    val provider = PlayerAttributeModifierProvider
    defense += provider.defenseModifierForAgility(attributeValues.agility)

    defense += computeGameSpecificPlayerDefenseModifierVsAttack(this, attackType,
        attacker)

    if (isParalyzed) defense += Being1.paralysisDefenseModifier

    return defense
}

@ExtensionPoint
var computeItemDefenseModifierVsGameSpecificAttack: (itemType: ItemType, attackType: AttackType) -> Int? =
    { _, _ -> null }

@ExtensionPoint
var computeGameSpecificPlayerDefenseModifierVsAttack: (player: Player, attackType: AttackType, attacker: Being?) -> Int =
    { _, _, _ -> 0 }