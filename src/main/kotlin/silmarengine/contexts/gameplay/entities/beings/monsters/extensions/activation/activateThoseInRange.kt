package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Activates each monster within the given tile-range of the given location in the given area.
 */
fun List<Monster>.activateThoseInRange(location: PixelPoint, range: TileDistance
) {
    getEntitiesInRange(location, PixelDistance(range.distance * tileWidth))
        .forEach { it.activate() }
}

