package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.sounds.Sound

interface PlayerListener {
    fun onAreaLoading() {}
    fun onArrivedInArea() {}
    fun onCanActAgain() {}
    fun onMustWaitToAct() {}
    fun onItemEquipped(item: Item?) {}
    fun onVitalStatChanged() {}
    fun onItemAddedToInventory(item: Item) {}
    fun onItemRemovedFromInventory(item: Item) {}
    fun onMessageReceived(message: String, messageType: MessageType, duration: Int) {}
    fun onHandsConfigChanged() {}
    fun onCanMakeAttack() {}
    fun onUsedImmediateEffectItem() {}
    fun onHandsConfigSet(index: Int) {}
    fun onBuyerLeft() {}
    fun onLevelChanged() {}
    fun onAttributeChanged(attribute: PlayerAttribute) {}
    fun onSoundHeard(sound: Sound, location: PixelPoint, distance: PixelDistance) {}
    fun onHeldItemChanged() {}
    fun onBecameBones(bones: Terrain) {}
    fun onDead() {}
}