package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.RangedAttacker
import silmarengine.contexts.beingmovement.domain.functions.MoveTowardsGoalListener
import silmarengine.events.ReporterMixin
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

interface Player : Being, ReporterMixin<PlayerListener>, RangedAttacker, LightSource {
    val attributeValues: PlayerAttributeValues

    val statuses: PlayerStatuses

    val levelValues: PlayerLevelValues

    val items: PlayerItems

    val stats: PlayerStats

    /**
     * This player's character name.
     */
    val name: String

    fun updateMaxHitPoints()
    fun setHitPointsToMax()

    fun reportVitalStatChange()

    /**
     * Whether the attack this player is currently engaged in (if any) is being
     * made with the offhand.
     */
    val isCurrentAttackOffhand: Boolean

    fun onMovePending(destination: PixelPoint)
    fun cancelPendingMove()

    fun onAttackMade()
    val hasAttackedThisTurn: Boolean

    val moveTowardsGoalListener: MoveTowardsGoalListener
    fun onMovementPointsRemaining(to: Float)

    companion object {
        /**
         * How many hands-configurations this player can maintain.  This number really depends
         * only on how many may be comfortably displayed onscreen.
         */
        const val numHandsConfigs = 4

        /**
         * A message that is presented to user in cases where the player is attempting
         * to unequip a cursed item.
         */
        const val removeCursedItemMessage =
            "The equipped item is cursed and won't allow you to remove it!"

        /**
         * A message that is presented to user in cases where the player doesn't have enough
         * gold to purchase an item or service.
         */
        const val notEnoughGoldMessage =
            "You don't have enough gold!<br><br>(Note that gold at your location also counts towards your total.)"
    }
}

/**
 * The experience level at which all players begin the game.
 */
@ExtensionPoint
var startingPlayerLevel = 0
