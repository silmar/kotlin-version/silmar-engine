package silmarengine.contexts.gameplay.entities.beings.extensions.reactions

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeConfused
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid

/**
 * Informs this being that an attempt to confuse it has been made,
 * of the given strength.
 */
fun Being.onConfusionAttempt(strength: Int) {
    if (isConfused) return

    // if this being can't avoid the attempt
    val result = avoid(strength / 2)
    if (!result.avoided) {
        // this being is confused
        becomeConfused(1 + result.missedBy)
    }
}
