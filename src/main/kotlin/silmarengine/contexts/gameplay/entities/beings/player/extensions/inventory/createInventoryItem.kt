package silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory

import silmarengine.contexts.gameplay.entities.beings.player.PlayerItems
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.types.ItemType

/**
 * Creates an item of the given type and adds it to this player's inventory.
 *
 * @return  The item created.
 */
fun PlayerItems.createInventoryItem(type: ItemType, quantity: Int = 1): Item {
    // create the item
    val item = if (type.isGroupable) createGroupableItem(type, quantity)
    else createItem(type)

    inventory.addItem(item)

    // all starting items created this way are considered identified
    item.identify()

    return item
}

