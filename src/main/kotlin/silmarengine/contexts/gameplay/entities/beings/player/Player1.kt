package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.Being1
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.MovePlayerTowardsGoalListener
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.move
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.onHasActedThisTurnChange
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.harm.dieAsPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.harm.onDamagedAsPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.checkForDiseaseEffects
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.checkForPoisonEffects
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.firstLitTorch
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.onInventoryItemAdded
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.onInventoryItemRemoved
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.checkForRationConsumption
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.informTorchesOfEndOfTurn
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.*
import silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains.informTerrainsOfPresence
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.beingmovement.domain.functions.pixelsPerMove
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.specializations.Torch
import silmarengine.contexts.gamecreation.domain.model.GCPlayer
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.playeritemsmanagement.domain.listeners.InventoryListener
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.list.sum
import silmarengine.util.sleepThread
import java.io.IOException
import java.io.ObjectInputStream
import kotlin.math.max

@Suppress("LeakingThis")
open class Player1(fromPlayer: GCPlayer) : Player, Being1() {
    override val name = fromPlayer.name

    override var hitPoints
        get() = super.hitPoints
        set(value) {
            super.hitPoints = value
            reportVitalStatChange()
        }

    final override var maxHitPoints
        get() = super.maxHitPoints
        set(value) {
            super.maxHitPoints = value
            reportVitalStatChange()
        }

    override val attributeValues: PlayerAttributeValues =
        createPlayerAttributeValues(this, fromPlayer.attributeValues.values)

    override val statuses: PlayerStatuses = createPlayerStatuses(this)

    final override val levelValues: PlayerLevelValues = createPlayerLevelValues(this)

    override val items: PlayerItems = PlayerItems1(this)

    override val stats: PlayerStats = createPlayerStats(this)

    /**
     * It is presumed that the only listeners to this player's events will be UI-related
     * elements which get created along with this player and add their listeners.
     * Thus, these listeners don't need to be persisted.
     */
    @Transient
    override var listeners = HashSet<PlayerListener>()

    override var paralyzedTurnsLeft: Int
        get() = super.paralyzedTurnsLeft
        set(value) {
            val old = super.paralyzedTurnsLeft
            super.paralyzedTurnsLeft = value
            if (value == 0 || value != old - 1) reportVitalStatChange()
        }

    override var confusedTurnsLeft: Int
        get() = super.confusedTurnsLeft
        set(value) {
            val old = super.confusedTurnsLeft
            super.confusedTurnsLeft = value
            if (value == 0 || value != old - 1) reportVitalStatChange()
        }

    /**
     * This holds the location this player was trying to move to when its movement
     * was interrupted by the end of its turn.  This player will attempt to reach
     * this destination at the beginning of its next turn. A -1 x-value for this
     * field means there is currently no such destination.
     */
    private val pendingMoveDestination = MutablePixelPoint(-1, -1)

    override fun onMovePending(destination: PixelPoint) {
        pendingMoveDestination.set(destination)
    }

    override fun cancelPendingMove() {
        pendingMoveDestination.set(-1, pendingMoveDestination.y)
    }

    /**
     * The number of attacks already made by this player during the current game turn.
     */
    private var attacksMadeThisTurn: Int = 0
    override val hasAttackedThisTurn: Boolean get() = attacksMadeThisTurn > 0

    /**
     * The number of offhand attacks already made by this player during the current game turn.
     */
    private var offhandAttacksMadeThisTurn: Int = 0

    /**
     * Whether the attack this player is currently engaged in (if any) is being
     * made with the offhand.
     */
    final override var isCurrentAttackOffhand: Boolean = false
        private set

    @Transient
    override var moveTowardsGoalListener = MovePlayerTowardsGoalListener(this)

    /**
     * Whether this player has completed its action(s) for the current game turn.
     */
    private var hasActedThisTurn: Boolean = false
        set(value) {
            val old = field
            field = value
            onHasActedThisTurnChange(old, value)
        }

    /**
     * Returns how many movement points total this player has to spend at the start of the
     * current game turn.
     */
    override val maxMovementPoints: Int
        get() = getMaxMovementPoints()

    override fun onMovementPointsRemaining(to: Float) = run { movementPoints = to }

    override val avoidance: Int get() = getAvoidance(super.avoidance)

    override val powerRating: Int
        get() = levelValues.level

    override fun onPlacedInArea() {
        reportToListeners { it.onArrivedInArea() }
    }

    override val movementType: MovementType
        get() = MovementType.WALKING

    // the interval between player movement sounds is shorter, because a player's
    // movements are displayed less rapidly than those of other beings
    override val movementSoundInterval
        get() = 3 * tileWidth / pixelsPerMove

    override val lightRadius: PixelDistance
        get() = Torch.torchLightRadius

    override val isEmittingLight: Boolean
        get() = items.inventory.firstLitTorch != null

    override val needsLightToSee: Boolean
        get() = area.isLit(location).isHalfOrMoreLit || super.needsLightToSee

    override fun setHitPointsToMax() {
        hitPoints = maxHitPoints
    }

    override val shouldMovementBeAccelerated = false

    init {
        // make this player a listener to events from its own inventory
        items.inventory.addListener(object : InventoryListener {
            override fun onItemAdded(item: PIMItem) {
                onInventoryItemAdded(item as Item)
            }

            override fun onItemRemoved(item: PIMItem) {
                onInventoryItemRemoved(item as Item)
            }
        })

        updateAttributes()
    }

    override fun reportVitalStatChange() = reportToListeners { it.onVitalStatChanged() }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(stream: ObjectInputStream) {
        stream.defaultReadObject()

        listeners = HashSet()

        moveTowardsGoalListener = MovePlayerTowardsGoalListener(this)
    }

    override fun onDamaged(damage: Damage) = onDamagedAsPlayer(damage)

    override fun act() {
        hasActedThisTurn = false

        isCurrentAttackOffhand = false
        attacksMadeThisTurn = 0
        offhandAttacksMadeThisTurn = 0

        movementPoints = maxMovementPoints.toFloat()

        // if this player was still moving to a particular location at the end
        // of its last turn, and it isn't paralyzed
        if (pendingMoveDestination.x >= 0 && !isParalyzed) {
            // try to complete the move
            move(pendingMoveDestination)
        }
        // otherwise, we'd like this player to emit a movement sound as soon as
        // it starts moving
        else movementSoundCounter = 0

        informTerrainsOfPresence()
        checkForDiseaseEffects()
        checkForPoisonEffects()
        checkForRationConsumption()
        checkForGameSpecificPlayerConditions()

        // if, from the above, this player hasn't already fully acted for this turn, and
        // isn't now dead or paralyzed
        val paralyzed = isParalyzed
        if (!hasActedThisTurn && !isDead && !paralyzed) {
            // allow the user to specify new actions for this player for this turn
            reportToListeners { it.onCanActAgain() }
        }
        // else, if this player hasn't already fully acted for this turn, and is paralyzed
        else if (!hasActedThisTurn && paralyzed) {
            // end its turn, but pause a bit first with a message, so turns don't fire
            // off in rapid succession while the paralysis is ongoing
            onMessage("PARALYZED!", duration = 700)
            sleepThread(1000)
            endTurn()
        }
    }

    @ExtensionPoint
    protected open fun checkForGameSpecificPlayerConditions() = Unit

    /**
     * See RangedAttackUtil.RangedAttacker interface method implemented.
     */
    override fun getRangedToHitModifier(target: Being): Int {
        return getToHitModifier(isCurrentAttackOffhand, target)
    }

    override fun die(damage: Damage) = dieAsPlayer { super.die(damage) }

    override fun updateMaxHitPoints() {
        var max = levelValues.hitPointsByLevel.sum(levelValues.level)

        max = applyGameSpecificEffectsOnMaxHitPoints(max)

        // add the bonus for endurance
        max += levelValues.level * PlayerAttributeModifierProvider.hitPointsModifierForEndurance(
            attributeValues.endurance)

        maxHitPoints = max(1, max)
    }

    @ExtensionPoint
    protected open fun applyGameSpecificEffectsOnMaxHitPoints(max: Int): Int = max

    override fun endTurn() {
        hasActedThisTurn = true
    }

    override fun onAttackMade() {
        // there are certain rare cases where a player attack can cause the player to die;
        // check for this so that further attacks are aborted
        if (isDead) {
            endTurn()
            return
        }

        // if it was not an offhand attack
        val attacksPerTurn = getAttacksPerTurn(false)
        val offhand = isCurrentAttackOffhand
        if (!offhand) {
            // if this player has more attacks
            if (++attacksMadeThisTurn < attacksPerTurn) {
                isCurrentAttackOffhand = false
                reportToListeners { it.onCanMakeAttack() }
                return
            }
        }

        // if it was an offhand attack, or this player is out of regular-hand attacks
        val offhandAttacksPerTurn = getAttacksPerTurn(true)
        if (offhand || attacksMadeThisTurn >= attacksPerTurn) {
            if (offhand) {
                offhandAttacksMadeThisTurn++
            }

            // if this player has more offhand attacks
            if (offhandAttacksMadeThisTurn < offhandAttacksPerTurn) {
                isCurrentAttackOffhand = true
                reportToListeners { it.onCanMakeAttack() }
                return
            }
        }

        // if this player is out of regular and offhand attacks
        if (attacksMadeThisTurn >= attacksPerTurn && offhandAttacksMadeThisTurn >= offhandAttacksPerTurn) {
            // this player has finished this player's turn
            endTurn()
        }
    }

    override fun getDefense(attackType: AttackType, attacker: Being?): Int =
        getDefenseAsPlayer(attackType, attacker)

    override fun onTurnOver() {
        super<Being1>.onTurnOver()

        informTorchesOfEndOfTurn()
    }

    override fun isVisibleToUser(): Boolean = true

    override fun onHitByMonster(monster: Monster, attack: MonsterAttack,
        damageAmount: Int, hitBy: Int) {
        if (shouldIgnoreGameSpecificMonsterAttackOnPlayer(this, monster, attack)) return

        super.onHitByMonster(monster, attack, damageAmount, hitBy)

        onPlayerHitByGameSpecificMonster(this, monster, attack, damageAmount, hitBy)
    }
}

@ExtensionPoint
var createPlayerAttributeValues: (player: Player, startingValues: List<Int>) -> PlayerAttributeValues =
    { player, startingValues -> PlayerAttributeValues1(player, startingValues) }

@ExtensionPoint
var createPlayerLevelValues: (player: Player) -> PlayerLevelValues =
    { player -> PlayerLevelValues1(player) }

@ExtensionPoint
var createPlayerStats: (player: Player) -> PlayerStats =
    { player -> PlayerStats1(player) }

@ExtensionPoint
var createPlayerStatuses: (player: Player) -> PlayerStatuses =
    { player -> PlayerStatuses1(player) }

@ExtensionPoint
var shouldIgnoreGameSpecificMonsterAttackOnPlayer: (player: Player, monster: Monster, attack: MonsterAttack) -> Boolean =
    { _, _, _ -> false }

@ExtensionPoint
var onPlayerHitByGameSpecificMonster: (player: Player, monster: Monster, attack: MonsterAttack, damageAmount: Int, hitBy: Int) -> Unit =
    { _, _, _, _, _ -> }
