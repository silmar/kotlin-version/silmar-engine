package silmarengine.contexts.gameplay.entities.beings.player.extensions.actions

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.game.Game

fun Player.onHasActedThisTurnChange(oldValue: Boolean, newValue: Boolean) {
    if (newValue && !oldValue) {
        // inform the game
        val game = Game.currentGame
        game!!.onPlayerHasFinishedTurn()

        reportToListeners { it.onMustWaitToAct() }
    }
}

