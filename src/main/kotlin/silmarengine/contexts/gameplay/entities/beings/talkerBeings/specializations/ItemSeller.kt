package silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.entities.items.Item

abstract class ItemSeller : TalkerBeing1(), silmarengine.contexts.gameplay.entities.talkers.ItemSeller {
    override fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int {
        return normalPrice
    }
}