package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sounds.cureDisease
import silmarengine.util.sleepThread

/**
 * Informs this player that a cure disease spell has been cast upon it.
 */
fun Player.onCureDiseaseReceived() {
    // if this player is diseased
    if (statuses.diseaseAfflictions.isNotEmpty()) {
        // issue an accompanying sound
        area.onSoundIssued(cureDisease, location)

        // add a cure visual effect at the given location
        val effect = LightSourceVisualEffect(null, PixelDistance(TileDistance(4)))
        area.addEntity(effect, location)

        // for each step in the effect
        for (i in 1..9) {
            // perform this step
            val frame = if (i <= 5) i else 10 - i
            effect.imageName = "cureDisease$frame"

            // pause a bit for this step to be seen
            sleepThread(240)
        }

        // remove this curing's visual effect from the area
        area.removeEntity(effect)

        // remove one disease from this player's list of diseases
        statuses.onDiseaseCured()

        if (statuses.diseaseAfflictions.isEmpty()) {
            reportToListeners { it.onVitalStatChanged() }
        }
    }
}

