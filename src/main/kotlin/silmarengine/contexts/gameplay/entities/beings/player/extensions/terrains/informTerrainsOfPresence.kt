package silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle

/**
 * Informs all terrains at this player's location of its presence.
 */
fun Player.informTerrainsOfPresence() {
    val terrains = area.terrains.getEntitiesInRectangle(location, size)
    terrains.forEach { it.onBeingHere(this) }
}

