package silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * Has this player lose one of the given attribute.
 */
fun Player.decrementBaseAttribute(attribute: PlayerAttribute) {
    // play a sound
    area.onSoundIssued(Sounds.attributeLost, location)

    attributeValues.addToBaseAttribute(attribute, -1)

    // inform the user
    onMessage("You lose a point of " + attribute.name + "!")
}

