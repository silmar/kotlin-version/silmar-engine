package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * A reason for calling createMonster().
 *
 * @param message    A message to be displayed to explain why the creation is taking place.
 */
class CreateMonsterReason(val message: String)

object CreateMonsterReasons {
    val generation = CreateMonsterReason("generates!")
    val summoning = CreateMonsterReason("summons!")
    val split = CreateMonsterReason("splits!")
}

/**
 * Up to how far from the given location the createMonster method should look for a place
 * to place the monster created.
 */
private val createMonsterPlacementDistance = PixelDistance(3 * tileWidth)

/**
 * Creates a monster of the given type near to the given location in the given area,
 * for the given reason.
 *
 * @return      The monster created (if any).
 */
fun createMonsterForReason(
    area: Area, location: PixelPoint, type: MonsterType, reason: CreateMonsterReason
): Monster? {
    // find a passible location next to the given location
    val monster = createMonster(type)
    val where =
        area.getNearbyPassibleRectangleLocation(location, monster.size, type.movementType,
            createMonsterPlacementDistance) ?: return null

    area.performPowerDischargeEffect(location)

    // add a new monster of given type at that location
    area.addEntity(monster, where)

    // inform the player if it's in LOS
    area.player?.receiveMessageIfInLOSOf(location, reason.message)

    return monster
}