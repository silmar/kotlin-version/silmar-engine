package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.removeGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.tallyGold
import silmarengine.contexts.gameplay.entities.items.Item

/**
 * Has this player try to pay to have the given item repaired.
 */
fun Player.repairItem(item: Item, price: Int) {
    // if this player doesn't have enough gold
    if (tallyGold(true) < price) {
        // don't allow the repair
        onMessage(Player.notEnoughGoldMessage, MessageType.error)
        return
    }

    // inform the item
    item.onRepaired()

    removeGold(price)
}

