package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.extensions.geometry.isLocationInDirection
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit
import java.awt.geom.Point2D

/**
 * Returns whether this monster should be activated.
 */
fun Monster.determineIfShouldActivate(lastDirectionMoved: Point2D.Float?): Boolean {
    // if there is no player, or this monster can't see the player's location,
    // don't activate
    val player = area.player ?: return false
    if (!player.isInLOSOf(location)) return false

    // if the player is currently emitting light, and this monster isn't
    if (player.isEmittingLight && (this !is LightSource || !(this as LightSource).isEmittingLight)) {
        // this monster notices the player, no matter what
        return true
    }

    // if we know this monster's direction of last movement, and it's
    // not in the general direction of the player, don't activate
    if (lastDirectionMoved != null && !isLocationInDirection(location, player.location,
            lastDirectionMoved)) return false

    // if this monster is lit, and the player is not, don't activate
    val isMonsterLitAndPlayerNot =
        area.isLit(location).isHalfOrMoreLit && !area.isLit(player.location, location,
            if (this is LightSource) this else null).isHalfOrMoreLit
    if (isMonsterLitAndPlayerNot) return false

    if (shouldMonsterNotActivateForGameSpecificReason(this)) return false

    return true
}

@ExtensionPoint
var shouldMonsterNotActivateForGameSpecificReason: (monster: Monster) -> Boolean =
    { _ -> false }