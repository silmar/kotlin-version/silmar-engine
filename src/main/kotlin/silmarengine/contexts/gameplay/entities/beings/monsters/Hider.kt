package silmarengine.contexts.gameplay.entities.beings.monsters

import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.areas.extensions.entities.onEntityChangedAppearance
import java.awt.Image

open class Hider : Monster1() {
    /**
     * Whether this hider has become visible yet.
     */
    override var isVisible = false

    @Suppress("SuspiciousVarProperty")
    override var image: Image? = null
        get() = if (isVisible) super.image else null

    override fun onDamaged(damage: Damage) {
        becomeVisible()
        super.onDamaged(damage)
    }

    /**
     * Has this hider become visible.
     */
    fun becomeVisible() {
        // if this hider is already visible
        if (isVisible) return

        // this hider becomes visible
        isVisible = true
        onImageToChange()
        area.onEntityChangedAppearance()
    }
}

class Assassin : Hider()

