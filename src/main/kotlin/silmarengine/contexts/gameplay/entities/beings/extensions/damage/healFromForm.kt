package silmarengine.contexts.gameplay.entities.beings.extensions.damage

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForm

/**
 * Checks to see if the given damage-form is actually one that heals this being,
 * and if so, applies the healing.
 *
 * @param factor            The factor by which the damage is turned into healing.
 */
fun Being.healFromForm(
    formWhichHeals: DamageForm, damage: Damage, factor: Float
) {
    // if the given form is the one that heals
    if (damage.form == formWhichHeals) {
        // take healing according to the given factor
        takeHealing((damage.amount * factor).toInt())
        damage.amount = 0
    }
}
