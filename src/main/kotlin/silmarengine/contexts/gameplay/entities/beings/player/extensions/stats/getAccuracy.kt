package silmarengine.contexts.gameplay.entities.beings.player.extensions.stats

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon

/**
 * Returns this player's accuracy when making a ranged attack.
 *
 * @param offhand   Whether the caller is concerned with ranged attacks from the off-hand.
 */
fun Player.getAccuracy(offhand: Boolean): Int {
    var accuracy = attributeValues.agility

    val weapon = if (!offhand) weapon else offhandWeapon
    if (weapon != null) accuracy += weapon.weaponType.toHitModifier

    accuracy += computeGameSpecificPlayerAccuracyModifier(this)

    return accuracy
}

@ExtensionPoint
var computeGameSpecificPlayerAccuracyModifier: (player: Player) -> Int = { _ -> 0 }