package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.LocationalMessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.extensions.proximity.isWithinDistanceOf
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Informs this monster's surroundings of its death.
 *
 * Note that this method takes area and location parameters because those of the monster
 * are invalid after its death.
 */
fun Monster.indicateDeath(area: Area, location: PixelPoint) {
    indicateMonsterDeath(this, area, location)

    // if the player is within a certain distance of this monster
    val player = area.player
    if (player?.isWithinDistanceOf(location, PixelDistance(20 * tileWidth)) == true) {
        // inform the player of its experience gain
        val experience = type.experienceValue
        player.onMessage("+$experience exp", LocationalMessageType(location))
        player.levelValues.onExperienceGained(experience)
    }
}

/**
 * Note that this method takes area and location parameters because those of the monster
 * are invalid after its death.
 */
@ExtensionPoint
var indicateMonsterDeath: (monster: Monster, area: Area, location: PixelPoint) -> Unit =
    { _, _, _ -> }