package silmarengine.contexts.gameplay.entities.beings.player.extensions.experience

import silmarengine.contexts.gameplay.entities.beings.player.Player

/**
 * Has this player lose experience such that he is at the midway point of the
 * experience level previous to his current one.
 */
fun Player.loseExperienceLevel() {
    val level = levelValues.level
    if (level <= 1) return

    if (shouldGameSpecificPlayerNotLoseExperienceLevel(this)) return

    // put this player's experience at midway through the next lower level
    val experience =
        (getExperienceNeededForLevel(level) + getExperienceNeededForLevel(level - 1)) / 2
    levelValues.onExperienceLevelLost(experience)
}

var shouldGameSpecificPlayerNotLoseExperienceLevel: (player: Player) -> Boolean =
    { _ -> false }