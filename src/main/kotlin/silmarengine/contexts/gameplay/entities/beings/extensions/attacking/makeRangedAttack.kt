package silmarengine.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import java.awt.geom.Point2D

/**
 * Describes makeRangedAttack()'s view of an entity that can make a ranged attack.
 */
interface RangedAttacker {

    /**
     * Returns this ranged-attacker's modifier for hitting the given being at range.
     */
    fun getRangedToHitModifier(target: Being): Int
}

/**
 * Has the given attacker perform a ranged attack in the given area at the given target
 * pixel-location, where the attack has the given max-tile-range, attack-type, and
 * number-to-hit a defense of zero.
 */
fun makeRangedAttack(attacker: RangedAttacker, area: Area, attackerLocation: PixelPoint,
    target: PixelPoint, maxRange: TileDistance, attackType: AttackType,
    attackerNumberToHitDefense0: Int): MakeRangedAttackResult? {
    // determine the vector to add at each step along the line from the attacker to the target
    val direction = getUnitVector(attackerLocation, target)
    direction.x *= pixelsPerShotMove.toFloat()
    direction.y *= pixelsPerShotMove.toFloat()

    // determine how many moves it will take to reach the given maximum range
    val numMoves = (maxRange.distance * tileWidth / pixelsPerShotMove).toFloat()

    // for each move to make along the line from the attacker to
    // the target, up to the given max-range
    val result = MakeRangedAttackResult()
    val current =
        Point2D.Float(attackerLocation.x.toFloat(), attackerLocation.y.toFloat())
    val rounded = MutablePixelPoint(attackerLocation)
    var i = 0
    while (i < numMoves) {
        // add the direction vector we determined above to our current location along the line
        current.x += direction.x
        current.y += direction.y
        rounded.set(current.x.toInt(), current.y.toInt())

        // if this location contains a being that isn't the attacker
        val being = getFirstEntityAt(rounded, area.beings)
        if (being != null && being !== attacker) {
            // determine the attacker's penalty at this range
            val range = getDistance(attackerLocation, rounded)
            val rangePenalty =
                getPenaltyForRange(range, PixelDistance(maxRange.distance * tileWidth))

            // determine if there should be a penalty for this location not being lit
            val unlitPenalty =
                if (attacker is Being && attacker.needsLightToSee && !area.isLit(rounded,
                        viewpoint = attackerLocation).isHalfOrMoreLit) 4
                else 0

            // determine if the attack hits
            val isAHitResult = when (val roll = randomInt(1, 20)) {
                1 -> IsAHitResult(false)
                20 -> IsAHitResult(true)
                else -> {
                    val total = roll + attacker.getRangedToHitModifier(
                        being) - attackerNumberToHitDefense0 - rangePenalty - unlitPenalty
                    being.isAHit(total, attackType,
                        if (attacker is Being) attacker else null)
                }
            }
            result.hit = isAHitResult.hit
            result.hitBy = isAHitResult.hitBy

            // if the attack hit or did not miss by too much
            if (isAHitResult.missedBy < 6) {
                // the attack stops at this being
                result.beingImpacted = being
                result.impactLocation = rounded
                return result
            }
        }

        // if this location blocks los or is at the max range
        if (area.getTile(rounded).blocksLOS || i.toFloat() == numMoves - 1) {
            // return a location result
            result.impactLocation = rounded
            result.hit = false
            return result
        }
        i++
    }

    return null
}

/**
 * Groups the multiple values returned by makeRangedAttack() into one return value.
 */
class MakeRangedAttackResult {

    /**
     * Whether the attack hits a being.
     */
    var hit: Boolean = false

    /**
     * The being impacted by this attack (if any).
     */
    var beingImpacted: Being? = null

    /**
     * If no being was impacted by this attack, the location where this attack impacted
     * something impassible (e.g. a wall tile).
     */
    var impactLocation: PixelPoint? = null

    /**
     * If this attack hit a being, how much over the minimum value needed to hit
     * that the hit was by.
     */
    var hitBy = 0
}

/**
 * How many pixels a shot moves per step of the movement loop in makeRangedAttack().
 */
var pixelsPerShotMove = 4