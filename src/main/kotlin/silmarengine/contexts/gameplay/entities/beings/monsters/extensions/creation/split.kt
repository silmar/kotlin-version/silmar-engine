package silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster

/**
 * Has this monster split off a copy of itself, where each one has half the hit points
 * of this original monster.
 */
fun Monster.split() {
    // create a new monster of this monster's type
    val monster = createMonsterForReason(area, location, type, CreateMonsterReasons.split)

    // set the hit points of both this monster and the new monster to
    // halve this monster's hit points
    val halfHitPoints = hitPoints / 2
    monster!!.onHitPointsDetermined(halfHitPoints)
}

