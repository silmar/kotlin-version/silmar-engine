package silmarengine.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.onAttributeChange
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttributes
import java.io.Serializable

open class PlayerAttributeValues1(
    val player: Player, startingValues: List<Int>
) : PlayerAttributeValues, Serializable {
    /**
     * This player's intrinsic attribute values (i.e. without item and spell bonuses
     * thrown in).  The indices in this array correspond to those of the
     * PlayerAttribute flyweight objects.
     */
    private val baseAttributes = object : Serializable {
        private val values = Array(PlayerAttribute.attributes.size) { 11 }
        fun get(index: Int) = values[index]
        fun set(index: Int, value: Int, shouldReport: Boolean = true) {
            values[index] = value
            if (shouldReport) player.updateAttributes()
        }
    }

    override fun getBaseAttribute(attribute: PlayerAttribute): Int {
        return baseAttributes.get(attribute.index)
    }

    override fun addToBaseAttribute(attribute: PlayerAttribute, amount: Int) {
        baseAttributes.set(attribute.index, getBaseAttribute(attribute) + amount)
    }

    /**
     * This player's attribute values, with all modifiers from items and spells
     * thrown in.  The indices in this array correspond to those of the
     * PlayerAttribute flyweight objects.
     *
     * Note that we start these attributes out with a different value than the
     * corresponding base-attributes, so that necessary side effects will occur
     * when these attributes are first set to their base counterparts.
     */
    private val attributes = object : Serializable {
        private val values = Array(PlayerAttribute.attributes.size) { 0 }
        val all: List<Int> get() = values.toList()
        fun get(index: Int) = values[index]
        fun set(index: Int, value: Int, shouldReport: Boolean = true) {
            values[index] = value
            if (shouldReport) player.onAttributeChange(
                PlayerAttribute.getAttribute(index))
        }
    }
    override val immutableValues: List<Int> get() = attributes.all

    override fun getAttribute(attribute: PlayerAttribute) =
        attributes.get(attribute.index)

    override fun onAttributeComputed(attribute: PlayerAttribute, value: Int) {
        attributes.set(attribute.index, value)
    }

    override val strength: Int
        get() = attributes.get(PlayerAttributes.strength.index)

    override val intelligence: Int
        get() = attributes.get(PlayerAttributes.intelligence.index)

    override val judgement: Int
        get() = attributes.get(PlayerAttributes.judgement.index)

    override val agility: Int
        get() = attributes.get(PlayerAttributes.agility.index)

    override val endurance: Int
        get() = attributes.get(PlayerAttributes.endurance.index)

    init {
        // copy the starting values into the base attributes
        startingValues.forEachIndexed { i, value -> baseAttributes.set(i, value, false) }
    }
}