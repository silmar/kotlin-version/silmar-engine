package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Informs this player that it has bought a cure-poison service from the given
 * seller at the given cost.
 *
 * @return      Whether the service was performed.
 */
fun Player.onCurePoisonService(seller: Entity): Boolean {
    // if this player is not poisoned
    if (!statuses.isPoisoned) {
        onMessage("You aren't poisoned!", MessageType.error)
        return false
    }

    // while this player is still poisoned
    while (statuses.isPoisoned) {
        // cure this player
        performPowerDischargeEffect(seller)
        onCurePoisonReceived()
    }

    return true
}

