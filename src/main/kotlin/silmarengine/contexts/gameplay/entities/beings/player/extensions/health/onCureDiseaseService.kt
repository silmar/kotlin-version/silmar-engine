package silmarengine.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Informs this player that it has bought a cure-disease service from the given
 * seller at the given cost.
 *
 * @return      Whether the service was performed.
 */
fun Player.onCureDiseaseService(seller: Entity): Boolean {
    // if this player is not diseased
    if (!statuses.isDiseased) {
        onMessage("You aren't diseased!", MessageType.error)
        return false
    }

    // while this player is still diseased
    while (statuses.isDiseased) {
        // cure this player
        performPowerDischargeEffect(seller)
        onCureDiseaseReceived()
    }

    return true
}

