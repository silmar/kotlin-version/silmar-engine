package silmarengine.contexts.gameplay.entities.beings.extensions.effects

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onSoundHeard
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.sounds.Sounds

/**
 * Puts this being into a confused state for the given number of game turns.
 */
fun Being.becomeConfused(duration: Int) {
    if (isBeingImmuneToConfusion(this)) return

    // if this being is already confused, skip this, to be fair
    if (isConfused) return

    performPowerDischargeEffect(this)

    onSoundHeard(Sounds.confused, location)

    setConfusedDuration(duration)
}

@ExtensionPoint
var isBeingImmuneToConfusion: (being: Being) -> Boolean = { false }