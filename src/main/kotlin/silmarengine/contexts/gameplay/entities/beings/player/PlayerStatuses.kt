package silmarengine.contexts.gameplay.entities.beings.player

interface PlayerStatuses {
    /**
     * How badly poisoned this player currently is.
     */
    val poisonLevel: Int
    val isPoisoned: Boolean get() = poisonLevel > 0
    fun reducePoisonLevel()

    /**
     * Informs this player it has been poisoned to the given degree.
     */
    fun onPoisoned(degree: Int)

    /**
     * The disease-afflictions currently suffered by this player.
     */
    val diseaseAfflictions: List<DiseaseAffliction>
    val isDiseased: Boolean get() = diseaseAfflictions.isNotEmpty()
    val numDiseases: Int get() = diseaseAfflictions.size
    fun onAfflictedWithDisease(affliction: DiseaseAffliction)
    fun onDiseaseCured()
}

const val maxPlayerPoisonLevel = 5