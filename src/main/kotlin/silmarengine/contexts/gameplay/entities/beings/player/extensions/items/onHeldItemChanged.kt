package silmarengine.contexts.gameplay.entities.beings.player.extensions.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.updateEncumbrance

/**
 * Informs this player that one of the items it's holding has changed in some way
 * (e.g. quantity).
 */
fun Player.onHeldItemChanged() {
    updateEncumbrance()

    reportToListeners { it.onHeldItemChanged() }
}

