package silmarengine.contexts.gameplay.entities.beings.talkerBeings.types

import java.util.*

/**
 * A list of all talker-being types.
 */
val talkerBeingTypes: MutableList<TalkerBeingType> = ArrayList()

/**
 * A mapping of all talker-being types by their names.
 */
val nameToTalkerBeingTypeMap = HashMap<String, TalkerBeingType>()

/**
 * The flyweight talker-being types.
 */
object TalkerBeingTypes