package silmarengine.contexts.gameplay.entities

import silmarengine.contexts.beingmovement.domain.model.BMEntity
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelRectangle
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.pathfinding.domain.model.PFEntity
import java.awt.Image

/**
 * An entity that exists on a map, but is not actually part of the map itself.
 * Such entities would include the player, monsters, items, talkers, and special
 * terrain elements.  This class can be specialized to represent all these
 * different entity types.
 */
interface Entity : PFEntity, BMEntity {
    /**
     * The map on which this entity currently resides.
     */
    override val area: Area

    /**
     * This location of this entity's center within its map.
     */
    override val location: PixelPoint

    /**
     * This entity's rectangular bounds, which are centered on its location.
     */
    override val bounds: PixelRectangle
    override val size: PixelDimensions get() = bounds.size

    /**
     * The image currently being used to depict this entity.
     */
    val image: Image?

    /**
     * The bounds of this entity that are impassible (if the entity itself is considered
     * impassible).  For most entities, these match the entity's full bounds.  One exception is
     * terrains, which often take up too much space if their normal bounds are used for this
     * purpose.
     */
    val impassibleBounds: PixelRectangle

    /**
     * Returns this entity's mode of movement.
     */
    val movementType: MovementType

    /**
     * Returns whether this entity should be drawn on top of others (e.g. death fog needs
     * to be drawn on top of items and monsters).
     */
    val shouldDrawOnTop: Boolean

    /**
     * Returns whether this entity is currently visible (to players).
     */
    val isVisible: Boolean

    fun placeInArea(area: Area, location: PixelPoint)

    fun moveTo(location: PixelPoint)

    /**
     * Once per game turn, this entity may be given a chance to doAct, at which
     * point this method is called.
     */
    fun act() {}

    /**
     * Informs this entity that its turn has just finished.
     */
    fun onTurnOver() {}

    /**
     * Informs this entity that which image is going to be used to depict it is changing.
     */
    fun onImageToChange()

    /**
     * Checks to see if this entity should emit a sound for having just
     * completed a step of its current movement.
     */
    override fun checkForMovementSound()
}