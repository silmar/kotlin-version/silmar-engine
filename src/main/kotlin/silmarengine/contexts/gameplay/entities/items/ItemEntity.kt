package silmarengine.contexts.gameplay.entities.items

import silmarengine.contexts.gameplay.entities.Entity

/**
 * Wraps an item so it can be placed in an area.
 */
interface ItemEntity : Entity {
    /**
     * The item wrapped by this entity.
     */
    val item: Item
}