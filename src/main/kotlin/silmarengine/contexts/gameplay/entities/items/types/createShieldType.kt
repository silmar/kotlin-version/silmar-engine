package silmarengine.contexts.gameplay.entities.items.types

import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun createShieldType(name: String, imageName: String, unidentifiedName: String?,
    encumbrance: Float, avoidance: Int, defenseModifier: Int, avoidanceModifier: Int,
    isMetal: Boolean, strengthRequired: Int, price: Int): ItemType1 {
    val result = ItemType1(name, imageName, getFrequencyForValue(price), isShield = true,
        unidentifiedName = unidentifiedName, encumbrance = encumbrance,
        equipLocation = EquipLocation.RIGHT_HAND, avoidance = avoidance,
        defenseModifier = defenseModifier, avoidanceModifier = avoidanceModifier,
        isMetal = isMetal, isNoisy = false, strengthRequired = strengthRequired,
        price = price, chanceOfInChest = getChanceOfInChest(price))
    result.register()
    shieldTypes.add(result)
    return result
}

fun createMagicalShieldTypes(normalType: ItemType1, upToPlusAndMinus: Int) {
    for (plus in -upToPlusAndMinus..upToPlusAndMinus) {
        if (plus == 0) continue

        // create the type
        val type = normalType.createMagicalVersion(plus).copy(
            avoidanceModifier = normalType.avoidanceModifier + plus,
            defenseModifier = normalType.defenseModifier + plus
        )
        type.register()
        shieldTypes.add(type)
    }
}
