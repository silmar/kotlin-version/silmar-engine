package silmarengine.contexts.gameplay.entities.items.types

import java.util.ArrayList

val armorTypes: MutableList<ItemType> = ArrayList()

object ArmorTypes {
    val leatherArmor =
        createArmorType("leather armor", "leatherArmor", null, 15f, 9, 2, 0, false, false,
            6, 20)

    init {
        createMagicalArmorTypes(leatherArmor, 3)
    }

    val chainMail =
        createArmorType("chain mail", "chainMail", null, 30f, 7, 5, 0, true, true, 10,
            200)

    init {
        createMagicalArmorTypes(chainMail, 5)
    }

    val plateMail =
        createArmorType("plate mail", "plateMail", null, 45f, 6, 7, 0, true, true, 13,
            1000)

    init {
        createMagicalArmorTypes(plateMail, 5)
    }
}