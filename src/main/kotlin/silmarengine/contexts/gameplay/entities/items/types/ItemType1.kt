package silmarengine.contexts.gameplay.entities.items.types

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType
import silmarengine.contexts.gameplay.entities.types.EntityType1
import silmarengine.contexts.gameplay.tiles.Tiles.floor
import silmarengine.contexts.gameplay.tiles.Tiles.roomFloor
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation
import silmarengine.util.log10
import kotlin.math.*

/**
 * Describes the static attributes of a particular kind of item.
 *
 * Note that this is a data class because we need to be able to use its generated
 * copy() method to declare special item-types which are a copy of another,
 * except for a small number of differences.
 */
data class ItemType1(override val name: String, val imageName: String?,
    val frequency: Int, override val unidentifiedName: String? = null,
    override val pluralName: String? = null, override val encumbrance: Float = 0f,
    override val isGroupable: Boolean = false, override val isMustEquip: Boolean = true,
    override val equipLocation: EquipLocation? = null,
    override val takesTwoHands: Boolean = false,
    override val parentType: ItemType? = null, override val chanceOfInChest: Float = 0f,
    override val avoidance: Int = 0, override val defenseModifier: Int = 0,
    override val avoidanceModifier: Int = 0, override val plus: Int = 0,
    override val isMetal: Boolean = false, override val isNoisy: Boolean = false,
    override val quantityToCreate: Int = 1, override val strengthRequired: Int = 0,
    override val attributeModifiers: IntArray = noAttributeModifiers,
    override val isCursed: Boolean = false, override val isMagical: Boolean = false,
    override val info: String? = null, override val price: Int = 0,
    override val isIndividuallyIdentified: Boolean = false,
    override val isArmor: Boolean = false, override val isShield: Boolean = false) :
    ItemType {

    // this being a data class demands that we specify the related entity type
    // be association, rather than inheritance
    override val entityType = ItemEntityType(name, imageName, frequency)

    override var weaponType: WeaponType? = null

    // this can't be a val because the subtype will always be created after this item-type
    override var subType: ItemSubType? = null

    fun register() {
        itemTypes.add(this)
        itemTypesByName[name] = this
    }

    /**
     * Returns whether the given item-type is the same as this one.
     *
     * @param   other    The item-type to compare to this one.
     */
    override fun equals(other: Any?): Boolean {
        return other === this || (other is ItemType1 && other.name == name)
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    companion object {
        /**
         * This info-string is appended to the info of many different item-types.
         */
        const val degradesInfo = "May degrade with each use."
    }

    /**
     * Returns a copy of this item-type altered by the given magical modifier (i.e. "plus").
     */
    fun createMagicalVersion(plus: Int, shouldModifyPrice: Boolean = true): ItemType1 {
        val modifiedFrequency =
            (getFrequencyForValue(price) * (if (isCursed) .5f else 1f)).roundToInt()

        // if we are to modify the price
        val modifiedPrice = if (shouldModifyPrice) {
            // come up with a price for being +1
            val plusOnePrice = price + when (price) {
                in 1..5 -> 250
                in 6..10 -> 500
                in 11..20 -> 750
                in 21..40 -> 1000
                in 41..100 -> 2000
                in 101..500 -> 3000
                in 501..1500 -> 4000
                else -> 5000
            }

            // determine the price for the given plus as the sum of the prices for each
            // plus through that given plus, where the price for a plus is fractionally
            // higher than for the plus before it
            val absPlus = abs(plus)
            (1..absPlus).sumBy {(plusOnePrice * (1 + (it - 1) * .5f)).toInt()}
        }
        else price

        // determine the unidentified name for the new type
        var quality: String? = null
        val absPlus = abs(plus)
        when {
            absPlus >= 5 -> quality = "magnificent"
            absPlus >= 3 -> quality = "remarkable"
            absPlus >= 1 -> quality = "fine"
        }
        val unidentifiedName = "${if (quality != null) "$quality " else ""}${this.name}"

        return copy(name = alterNameForPlus(name, plus),
            unidentifiedName = unidentifiedName, frequency = modifiedFrequency,
            pluralName = "$pluralName ${if (plus >= 0) "+" else ""}$plus", plus = plus,
            parentType = this,
            chanceOfInChest = 1.0f - (1.0f - chanceOfInChest) / 2.0f.pow(max(0, plus)),
            isCursed = plus < 0, price = modifiedPrice, isMagical = true,
            avoidance = avoidance - plus,
            encumbrance = encumbrance * (1f - abs(plus) * 0.1f),
            strengthRequired = strengthRequired - max(0, abs(plus)),
            isIndividuallyIdentified = true)
    }

    override fun isDescendedFromType(other: ItemType): Boolean {
        // for each related type up the chain from this type
        var ancestor: ItemType? = this
        while (ancestor != null) {
            // if this parent type is the given type
            if (ancestor == other) {
                return true
            }
            ancestor = ancestor.parentType
        }

        return false
    }

    override fun getAttributeRequirement(attribute: PlayerAttribute): Int {
        return if (attribute == PlayerAttributes.strength) strengthRequired
        else 0
    }

    /**
     * As ItemType1 is a data class, we need this inner class in order to
     * specialize EntityType1 for ItemType1's purposes, since we cannot do so
     * through inheritance.
     */
    inner class ItemEntityType(name: String, imageName: String?, frequency: Int) :
        EntityType1(name,
            if (imageName != null) "$itemTypeImagePath/$imageName" else null, frequency) {
        init {
            determineSize()
        }

        override fun isPresenceLocationValid(area: Area, location: PixelPoint): Boolean {
            // return whether the tile at the given location is a floor
            val tile = area.getTile(location)
            return tile == roomFloor || tile == floor

        }

        override fun isValidForArea(area: Area) =
            isItemTypeValidForArea(this@ItemType1, area)

        override fun createStockInstance(): Entity {
            val item = if (isGroupable) createGroupableItem(this@ItemType1)
            else createItem(this@ItemType1)
            return createItemEntity(item)
        }
    }
}

/**
 * Returns the generation frequency that should correspond to the given placement value.
 *
 * @param   value       The item placement value for which to compute
 * the frequency-of-generation value.
 */
fun getFrequencyForValue(value: Int): Int {
    // for every power of 10 above 100 in the given value, the
    // frequency drops by a certain amount
    val frequency = (10f - 4f * max(0f, log10(value.toFloat()) - 2f)).toInt()
    return max(1, frequency)
}

/**
 * Returns the chance-of-in-chest value (see like-named
 * member variable for a description) corresponding to the
 * given placement value.
 *
 * @param   value       The item placement value for which to compute
 * the chance-of-in-chest value.
 */
fun getChanceOfInChest(value: Int): Float {
    // for every power of 10 in the value, the chance goes up by a
    // certain amount
    return min(1f, 0.1f + 0.2f * log10(value.toFloat()))
}

/**
 * Returns given item-type name altered to reflect the type's magical modifier.
 */
fun alterNameForPlus(name: String, plus: Int): String {
    return name + " " + (if (plus >= 0) "+" else "") + plus
}

@ExtensionPoint
var isItemTypeValidForArea: (type: ItemType, area: Area) -> Boolean = { _, _ -> true }

