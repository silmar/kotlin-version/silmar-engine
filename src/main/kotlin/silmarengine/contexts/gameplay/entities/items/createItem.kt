package silmarengine.contexts.gameplay.entities.items

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.items.extensions.quantity.generateRandomQuantity
import silmarengine.contexts.gameplay.entities.items.specializations.*
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.weapons.createWeapon

/**
 * Returns a newly created instance of the given item-type.
 */
fun createItem(type: ItemType, condition: ItemCondition = ItemCondition.random): Item {
    // if the given item-type is in fact a weapon-type, create a weapon
    val weaponType = type.weaponType
    if (weaponType != null) return createWeapon(weaponType, condition)

    // create the actual item object according to the given type
    return createGameSpecificItem(type, condition) ?: when (type) {
        NonGroupableItemTypes.torch -> Torch(condition)
        NonGroupableItemTypes.potionOfHealing, NonGroupableItemTypes.potionOfPoison, NonGroupableItemTypes.potionOfStrength, NonGroupableItemTypes.potionOfIntelligence, NonGroupableItemTypes.potionOfJudgement, NonGroupableItemTypes.potionOfAgility, NonGroupableItemTypes.potionOfEndurance -> Potion(
            type, condition)
        else -> Item1(type, condition)
    }
}

fun createGroupableItem(
    type: ItemType, quantity: Int = type.generateRandomQuantity()
): Item {
    val item = Item1(type, ItemCondition.EXCELLENT)
    item.quantity = quantity
    return item
}

@ExtensionPoint
var createGameSpecificItem: (type: ItemType, condition: ItemCondition) -> Item? = { _, _ -> null }

fun createItemEntity(item: Item): ItemEntity {
    return when (item) {
        is Torch -> TorchEntity(item)
        else -> ItemEntity1(item)
    }
}