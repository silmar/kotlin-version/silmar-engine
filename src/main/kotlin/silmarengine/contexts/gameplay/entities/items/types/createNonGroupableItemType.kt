package silmarengine.contexts.gameplay.entities.items.types

import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun createNonGroupableItemType(name: String, imageName: String, unidentifiedName: String?,
    encumbrance: Float, isMustEquip: Boolean, equipLocation: EquipLocation?,
    avoidance: Int, isMetal: Boolean, price: Int, isCursed: Boolean, info: String?,
    isMagical: Boolean = true, frequency: Int? = null): ItemType1 {
    val result = ItemType1(name, imageName,
        frequency = frequency ?: getFrequencyForValue(price),
        unidentifiedName = unidentifiedName, isIndividuallyIdentified = true,
        isMustEquip = isMustEquip, equipLocation = equipLocation,
        isMetal = isMetal, encumbrance = encumbrance, isCursed = isCursed, info = info,
        chanceOfInChest = getChanceOfInChest(price), price = price, avoidance = avoidance,
        isMagical = isMagical)
    result.register()
    nonGroupableItemTypes.add(result)
    return result
}
