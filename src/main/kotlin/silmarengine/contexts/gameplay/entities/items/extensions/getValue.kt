package silmarengine.contexts.gameplay.entities.items.extensions

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import kotlin.math.round

/**
 * Returns what this item's value is, in the given context.
 */
fun Item.getValue(valueContext: ItemValueContext): Int {
    fun computeValue(factor: Float) =
        round(type.price * factor * valueContext.valueFraction).toInt()
    return when {
        valueContext.isZeroValueIfCursed && type.isCursed -> 0
        valueContext != ItemValueContext.EXPERIENCE && type == GroupableItemTypes.gold -> quantity
        valueContext == ItemValueContext.REPAIR -> computeValue(
            1f - condition.valueFraction)
        valueContext == ItemValueContext.IDENTIFY -> computeValue(1f)
        // note that we have to put the quantity factor inside the call to computeValue(),
        // since that call lops off fractional values
        else -> computeValue(quantity *
            if (!type.isGroupable) condition.valueFraction else 1f)
    }
}

