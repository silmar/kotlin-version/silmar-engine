package silmarengine.contexts.gameplay.entities.items.extensions

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onHeldItemChanged
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.game.Game

fun Item.onIdentified() {
    if (isIdentified) {
        // if items of this item's type aren't individually identified
        val type = type
        if (!type.isIndividuallyIdentified) {
            // inform the game that this item's type has been identified
            Game.currentGame?.storeThatItemTypeIsIdentified(type)
        }
    }

    // inform the holder (if there is one)
    if (holder != null) {
        holder!!.onHeldItemChanged()
    }
}

