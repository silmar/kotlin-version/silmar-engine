package silmarengine.contexts.gameplay.entities.items.weapons

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.Item1
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType
import silmarengine.contexts.playeritemsmanagement.domain.model.Inventory

open class Weapon1(type: WeaponType, condition: ItemCondition) : Weapon,
    Item1(type.itemType, condition) {
    override fun onHitVictim(victim: Being, wielder: Being, damage: Int) {}

    override fun getAmmo(inventory: Inventory): Item? =
        if (weaponType.ammoType != null) inventory.getFirstItemOfType(
            weaponType.ammoType!!) as Item?
        else null
}