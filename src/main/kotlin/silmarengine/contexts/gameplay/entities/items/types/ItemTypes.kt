package silmarengine.contexts.gameplay.entities.items.types

import java.util.*

/**
 * A list of all item types.
 */
val itemTypes = ArrayList<ItemType>()

/**
 * A mapping of all item types by their names.
 */
val itemTypesByName = HashMap<String, ItemType>()

/**
 * Returns the item-type of the given name.
 *
 * @param   name    The name of the item-type desired.
 */
fun getItemType(name: String): ItemType? {
    return itemTypesByName[name]
}

/**
 * Returns an item-type that has been randomly selected from all those
 * in existence.
 */
val randomItemType: ItemType get() = itemTypes.random()
