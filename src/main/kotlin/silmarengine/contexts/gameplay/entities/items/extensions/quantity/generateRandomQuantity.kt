package silmarengine.contexts.gameplay.entities.items.extensions.quantity

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.util.math.randomInt

fun ItemType.generateRandomQuantity(): Int {
    // determine the base quantity
    val quantity: Int =
        if (this == GroupableItemTypes.gold) getGameSpecificAverageGoldItemQuantity()
        else quantityToCreate

    // generate the final quantity value as a random deviation away from the quantity
    // determined above
    return randomInt(2 * quantity / 3, 4 * quantity / 3)
}

@ExtensionPoint
var getGameSpecificAverageGoldItemQuantity: () -> Int = { throw NotImplementedError() }
