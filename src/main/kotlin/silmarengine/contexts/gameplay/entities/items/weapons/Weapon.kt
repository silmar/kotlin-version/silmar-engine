package silmarengine.contexts.gameplay.entities.items.weapons

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.playeritemsmanagement.domain.model.Inventory

/**
 * A weapon in the game.
 */
interface Weapon : Item {

    val weaponType: WeaponType get() = type.weaponType!!
    fun isOfType(weaponType: WeaponType): Boolean = weaponType == type.weaponType

    /**
     * Whether this weapon requires an ammo item in order to function.
     */
    val requiresAmmo: Boolean get() = weaponType.ammoType != null

    /**
     * Returns this weapon's ammo item (if there is any) from the given inventory.
     */
    fun getAmmo(inventory: Inventory): Item?

    /**
     * Informs this weapon that it has just now been used to make an attack.
     */
    fun onMadeAttack() {}

    /**
     * Informs this weapon that it has scored a hit on a being.
     *
     * @param   victim      The being hit.
     * @param   wielder     The wielder of this weapon.
     * @param   damage      The amount of damage done.
     */
    fun onHitVictim(victim: Being, wielder: Being, damage: Int) {}

    companion object {
        /**
         * The hand weapon, which is used by a player when it has no other weapon equipped.
         */
        val hand = Weapon1(WeaponTypes.hand, ItemCondition.EXCELLENT)
    }
}