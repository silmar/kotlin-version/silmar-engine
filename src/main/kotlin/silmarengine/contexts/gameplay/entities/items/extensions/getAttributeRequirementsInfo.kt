package silmarengine.contexts.gameplay.entities.items.extensions

import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.util.html.addLineBreak

fun Item.getAttributeRequirementsInfoExt(): String {
    // for each player attribute
    val text = StringBuffer()
    PlayerAttribute.attributes.forEach { attribute ->
        // if this item has a requirement on this attribute in order to be able to use it
        val requirement = type.getAttributeRequirement(attribute)
        if (requirement > 1) {
            // append that requirement to the tooltip
            addLineBreak(text)
            text.append(attribute.name + " required: " + requirement)
        }
    }
    return text.toString()
}