package silmarengine.contexts.gameplay.entities.items.specializations

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onUsedItem
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.Item1
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

open class Potion(type: ItemType, condition: ItemCondition) : Item1(type, condition),
    Item.Usable {
    override fun onUsed(user: Player) {
        // emit a potion-drunk sound
        user.area.onSoundIssued(Sounds.potionDrunk, user.location)

        user.onUsedItem(this)

        quantity--
    }
}
