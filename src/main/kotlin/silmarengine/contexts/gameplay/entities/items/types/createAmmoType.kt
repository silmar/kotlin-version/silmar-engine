package silmarengine.contexts.gameplay.entities.items.types

fun createAmmoType(name: String, imageName: String, frequency: Int, pluralName: String?,
    unidentifiedName: String?, encumbrance: Float, chanceOfInChest: Float,
    quantityToCreate: Int, price: Int, avoidance: Int): ItemType1 {
    val result = ItemType1(name, imageName, pluralName = pluralName ?: "${name}s",
        unidentifiedName = unidentifiedName, quantityToCreate = quantityToCreate,
        isGroupable = true, avoidance = avoidance, encumbrance = encumbrance,
        price = price, frequency = frequency, chanceOfInChest = chanceOfInChest)
    result.register()
    ammoTypes.add(result)
    return result
}
