package silmarengine.contexts.gameplay.entities.items.extensions.quantity

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onHeldItemChanged
import silmarengine.contexts.gameplay.entities.items.Item

fun Item.onQuantityChange(newValue: Int) {
    // if there is now none of this item left
    if (newValue <= 0) {
        remove()
    }
    // else, if this item is being held
    else if (holder != null) {
        // inform the holder
        holder!!.onHeldItemChanged()
    }
}

