package silmarengine.contexts.gameplay.entities.items.specializations

import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onUsedItem
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.Item1
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.ItemEntity1
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.itemTypeImagePath
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.sounds.Sound
import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.image.loadImage
import java.awt.Image

class Torch(condition: ItemCondition) : Item1(NonGroupableItemTypes.torch, condition),
    Item.Usable {
    var isLit = false

    /**
     * How many game turns this torch can continue to burn.
     */
    var turnsOfLightLeft = 1
        private set(value) {
            field = value
            onTurnsOfLightLeftChange(value)
        }

    private fun onTurnsOfLightLeftChange(newValue: Int) {
        // if this torch is expended for its current condition
        if (newValue <= 0) {
            // if this torch will now be completely expended
            if (condition == ItemCondition.POOR) {
                // issue a goes-out sound
                issueSound(Sounds.torchGoesOut)
            }

            // have it degrade
            degrade(false)

            // start this torch's turns-left counter over
            turnsOfLightLeft = turnsOfLightPerCondition
        }
    }

    override fun onUsed(user: Player) {
        // inform the user
        user.onUsedItem(this)

        // if this torch isn't lit
        if (!isLit) {
            // light this torch
            isLit = true

            // issue a lit sound
            issueSound(Sounds.torchLit)
        }
        else extinguish()
    }

    /**
     * Extinguishes this torch.
     */
    fun extinguish() {
        isLit = false

        // issue a goes-out sound
        issueSound(Sounds.torchGoesOut)
    }

    /**
     * Has this torch issue the given sound into its (or its holder's) area.
     */
    private fun issueSound(sound: Sound) {
        val area = holder?.area ?: entity?.area ?: return
        val location = holder?.location ?: entity?.location ?: return
        area.onSoundIssued(sound, location)
    }

    /**
     * Informs this torch that another game turn has ended.
     */
    fun onEndOfTurn() {
        if (isLit) turnsOfLightLeft--
    }

    /**
     * Spoils this torch so that it no longer may function.
     */
    fun spoil() {
        if (isLit) extinguish()
        destroy(false)
    }

    @Transient
    override var image: Image = super.image
        get() = if (isLit) litImage else field

    companion object {
        /**
         * The distance a torch will shine.
         */
        val torchLightRadius = PixelDistance(TileDistance(9))

        // adding this extra distance onto the torch's light radius produces a more visually pleasing
        // area of effect
        init {
            torchLightRadius.distance += tileWidth / 2
        }

        /**
         * The image to display for this torch when it's lit.
         */
        private val litImage = loadImage("${itemTypeImagePath}/litTorch")

        /**
         * How many turns of light a torch will burn per condition level left.
         */
        private const val turnsOfLightPerCondition = 40
    }
}

class TorchEntity(val torch: Torch) : ItemEntity1(torch), LightSource {
    override val isEmittingLight: Boolean
        get() = torch.isLit

    override val lightRadius: PixelDistance
        get() = Torch.torchLightRadius
}