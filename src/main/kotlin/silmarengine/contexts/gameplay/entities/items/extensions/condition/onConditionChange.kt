package silmarengine.contexts.gameplay.entities.items.extensions.condition

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onHeldItemChanged
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.extensions.quantity.remove

fun Item.onConditionChange(
    oldCondition: ItemCondition, newCondition: ItemCondition
) {
    val isWorse = newCondition.isWorseThan(oldCondition)
    val isDestroyed = newCondition == ItemCondition.DESTROYED

    // if this item is of a groupable type, and its condition is worse, but the item
    // isn't destroyed
    if (isWorse && !isDestroyed && type.isGroupable) {
        // remove from this item a fraction of its quantity determined by the change
        // in conditions
        quantity = (quantity * (1f - newCondition.getQuantityFractionalDifference(
            oldCondition))).toInt()
    }

    // if this item is now destroyed, remove it from its holder or the area
    if (isDestroyed) remove()

    // otherwise, if this item is being held, report a change to the holder
    else if (holder != null) {
        holder!!.onHeldItemChanged()
    }
}
