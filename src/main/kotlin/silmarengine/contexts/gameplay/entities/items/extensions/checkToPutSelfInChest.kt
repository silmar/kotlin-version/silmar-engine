package silmarengine.contexts.gameplay.entities.items.extensions

import silmarengine.contexts.gameplay.entities.items.ItemEntity
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.extensions.checkForBeingTrapped
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.util.math.randomFloat

fun ItemEntity.checkToPutSelfInChest() {
    // if this item is of a type which is at times found in a chest
    val chance = item.type.chanceOfInChest
    if (chance > 0f) {
        // if we randomly detm a chest is called for
        if (randomFloat <= chance) {
            // replace the item in the area with a chest
            area.removeEntity(this)
            val chest = createTerrain(TerrainTypes.chest) as Chest
            area.addEntity(chest, location)

            // put this item into the chest
            chest.item = this.item

            chest.checkForBeingTrapped()
        }
    }
}

