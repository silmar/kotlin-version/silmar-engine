package silmarengine.contexts.gameplay.entities.items.weapons.types

import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackTypes.meleeAttack
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackTypes.oldProjectile
import silmarengine.contexts.gameplay.entities.damage.DamageForm
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.entities.items.types.*
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

/**
 * An object describing the attributes of a particular kind of weapon.
 */
data class WeaponType1(override val itemType: ItemType, override val minDamage: Int = 1,
    override val maxDamage: Int = 1, override val toHitModifier: Int = 0,
    override val damageModifier: Int = 0, override val isRanged: Boolean = false,
    override val maxRange: TileDistance = TileDistance(0),
    override val ammoType: ItemType? = null, override val isEdged: Boolean = false,
    override val numAttacks: Int = 1, override val damageForm: DamageForm,
    override val attackType: AttackType = if (isRanged) oldProjectile else meleeAttack) :
    WeaponType {
    override var weaponSubType: ItemSubType? = null

    init {
        register()
    }

    fun register() {
        weaponTypes.add(this)
        (itemType as ItemType1).weaponType = this
    }

    /**
     * Returns the result of adding to the given info-text remarks about this
     * weapon-type having unusual traits, like being blunt or granting multiple attacks.
     */
    override fun getExtraInfo(): String? {
        var result: String? = null
        val start = { s: String? -> if (s != null) "${s}<br>" else "" }
        if (!isEdged && !isRanged) result = start(result) + bluntInfo
        if (numAttacks > 1) result =
            "${start(result)}Allows $numAttacks attacks per turn."
        val subTypeInfo = weaponSubType?.getExtraInfo()
        if (subTypeInfo != null) result += "<br>$subTypeInfo"

        return result
    }

    companion object {
        /**
         * This info-string is appended to the info of many different weapon-types.
         */
        private const val bluntInfo = "Is a blunt weapon."

        /**
         * Clones and modifies the given weapon-type to create a new magical
         * weapon-type of the given plus.
         *
         * @param   name    The name to give weapons of the type.  If none is provided,
         *                  one will be generated automatically (e.g. "long sword +1").
         */
        fun createMagicalVersion(normalType: WeaponType1,
            itemTypeToUse: ItemType1? = null, plus: Int, name: String? = null,
            price: Int? = null, info: String? = null, appendInfo: Boolean = true,
            numAttacks: Int? = null): WeaponType1 {
            // determine what the info of magical version should be
            var modifiedInfo = normalType.itemType.info
            if (info != null) {
                modifiedInfo =
                    if (appendInfo && modifiedInfo != null) "${modifiedInfo}<br>$info" else info
            }

            // create the modified item-type
            var itemType =
                (itemTypeToUse ?: (normalType.itemType as ItemType1).createMagicalVersion(
                    plus)).copy(info = modifiedInfo)
            if (name != null) itemType = itemType.copy(name = name)
            if (price != null) itemType = itemType.copy(price = price)
            itemType.register()

            // create the modified weapon-type
            val type = normalType.copy(itemType = itemType,
                numAttacks = numAttacks ?: normalType.numAttacks, toHitModifier = plus,
                damageModifier = plus)
            weaponTypes.add(type)

            return type
        }

        /**
         * Creates a range of new magical weapon item-types up to the
         * given plus (and minus) from the given normal weapon item-type.
         */
        fun createMagicalWeaponTypes(normalType: WeaponType1, upToPlusAndMinus: Int) {
            for (plus in -upToPlusAndMinus..upToPlusAndMinus) {
                if (plus == 0) continue
                createMagicalVersion(normalType, plus = plus)
            }
        }
    }
}

/**
 * Returns an item type which is meant to be composed into a weapon type.
 */
fun createWeaponItemType(name: String, imageName: String,
    unidentifiedName: String? = null, encumbrance: Float = 0f,
    takesTwoHands: Boolean = false, avoidance: Int = 0, isMetal: Boolean = false,
    strengthRequired: Int = 0, price: Int = 0, info: String? = null,
    frequency: Int? = null): ItemType1 {
    val result = ItemType1(name = name, imageName = imageName,
        frequency = frequency ?: getFrequencyForValue(price),
        unidentifiedName = unidentifiedName, encumbrance = encumbrance,
        equipLocation = EquipLocation.RIGHT_HAND, takesTwoHands = takesTwoHands,
        chanceOfInChest = getChanceOfInChest(price), avoidance = avoidance,
        isMetal = isMetal, strengthRequired = strengthRequired, price = price,
        info = info)
    result.register()
    return result
}

