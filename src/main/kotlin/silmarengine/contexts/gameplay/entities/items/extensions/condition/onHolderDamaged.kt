package silmarengine.contexts.gameplay.entities.items.extensions.condition

import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.items.Item

/**
 * Informs this item that it's holder has been hit for the given amount of damage.
 */
fun Item.onHolderDamaged(damage: Damage) {
    // this formula produces the following results:
    // with 1 damage, items have no chance of being hurt
    // with 30 damage, items use their normal avoidance value to avoid being hurt
    // with all damage amounts, the modified avoidance value becomes
    //      (amount / 25) * avoidance
    avoidOrDegrade((damage.amount / 25f * type.avoidance).toInt() - type.avoidance)
}

