package silmarengine.contexts.gameplay.entities.items.types

object GroupableItemTypes {
    val ration =
        createGroupableItemType("ration", "ration", 10, null, null, 1f, 0f, 10, 1, 15)
    val gold =
        createGroupableItemType("gold", "gold", 10, "gold", null, 0.05f, 0.5f, 0, 1, 10)
}