package silmarengine.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType
import silmarengine.contexts.gameplay.entities.types.EntityType
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItemType

/**
 * Specifies the static attributes of a particular kind of item.
 */
interface ItemType: PIMItemType {
    /**
     * The more general entity type associated with this type.
     */
    val entityType: EntityType

    /**
     * If this item-type represents a weapon-type, what that type is.
     */
    val weaponType: WeaponType?

    /**
     * The item sub-type (if-any) that is extending this type with additional attributes.
     * This is necessary because ItemTypes are implemented as data classes, so without
     * this field there would be no way to navigate from this type to its extending type.
     */
    val subType: ItemSubType?

    /**
     * What to call an item of this type when this type has not yet been identified.
     * If this is null, it means that items of this type are always identified.
     */
    val unidentifiedName: String?

    /**
     * What to call a grouping of this item (e.g. "arrows").
     */
    val pluralName: String?

    /**
     * The amount of encumbrance points that the burden of one item of this
     * type represents.
     */
    override val encumbrance: Float

    /**
     * Whether or not an item of this type can be part of a group of the same
     * items (e.g. arrow->yes, sword->no).
     */
    override val isGroupable: Boolean

    /**
     * Whether or not an item of this type must be equipped to be used.
     */
    val isMustEquip: Boolean

    /**
     * Where on the body an item of this type is equipped (if anyplace).
     */
    override val equipLocation: EquipLocation?

    /**
     * Whether an item of this type requires being held by both hands in
     * order to be used.
     */
    override val takesTwoHands: Boolean

    /**
     * The item-type (if any) from which this item-type was created.
     */
    val parentType: ItemType?

    /**
     * The percentage chance (expressed as a decimal) that an item of this type
     * when generated, should be placed in a chest due to its value.
     */
    val chanceOfInChest: Float

    /**
     * An item of this type's base avoidance value to avoid harm to itself.
     * Lower numbers mean this type more easily avoids.
     */
    val avoidance: Int

    /**
     * The modifier to defense added by an item of this type.
     */
    val defenseModifier: Int

    /**
     * The modifier afforded by an item of this type to the avoidance value
     * of its holder.
     */
    val avoidanceModifier: Int

    /**
     * If this item is specially-crafted or magical, how much of a "plus" it provides.
     */
    val plus: Int

    /**
     * Whether an item of this type is made of metal.
     */
    val isMetal: Boolean

    /**
     * Whether an item of this type makes noise as its holder moves about.
     */
    override val isNoisy: Boolean

    /**
     * The average amount to create of an item of this type when it is
     * randomly generated.
     */
    val quantityToCreate: Int

    /**
     * The strength that must be possessed by the holder of an item of this type
     * in order to use it.
     */
    val strengthRequired: Int

    /**
     * The modifiers afforded to the attributes of the holder of an item
     * of this type when utilizing it.  See the declaration order of
     * the attributes in the PlayerAttributes class for their order
     * within this array.
     */
    val attributeModifiers: IntArray

    /**
     * Whether an item of this type is cursed.
     */
    override val isCursed: Boolean

    /**
     * Whether an item of this type is magical in nature.
     */
    val isMagical: Boolean

    /**
     * A string of sentences that describes items of this type to the user.
     */
    val info: String?

    /**
     * How much an item of this type would cost in, say, a store.
     * Note that the price of cursed items is the same as that of the non-cursed
     * item that each mimics.  (The isCursed property is consulted when a real price
     * for a cursed-item must be determined.)
     */
    val price: Int

    /**
     * Whether an item of this type must be identified individually, versus it
     * being identified as long as any item of its type has been identified.
     * (e.g. identifying a sword +1 shouldn't cause all such swords to
     * be identified, just that one, whereas identifying one item of pistol bullets
     * should make all pistol bullets identified).
     */
    val isIndividuallyIdentified: Boolean

    val isArmor: Boolean

    val isShield: Boolean

    /**
     * Returns whether this item-type is descended from the one given.
     */
    fun isDescendedFromType(other: ItemType): Boolean

    /**
     * Returns the required attribute value for a player to use an item of this type.
     */
    fun getAttributeRequirement(attribute: PlayerAttribute): Int
}

/**
 * The relative path to the image files for item types.
 */
const val itemTypeImagePath = "items"

/**
 * Set as an item type's attribute modifiers when that type offers none.
 */
val noAttributeModifiers = intArrayOf(0, 0, 0, 0, 0)