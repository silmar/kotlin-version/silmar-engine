package silmarengine.contexts.gameplay.entities.items.types

import java.util.ArrayList

val treasureTypes: MutableList<ItemType> = ArrayList()

object TreasureTypes {
    val turquoise = createTreasureType("turquoise gemstone", "turquoise", 0.1f, 10)
    val obsidian = createTreasureType("obsidian gemstone", "obsidian", 0.1f, 30)
    val carnelian = createTreasureType("carnelian gemstone", "carnelian", 0.1f, 50)
    val sardonyx = createTreasureType("sardonyx gemstone", "sardonyx", 0.1f, 75)
    val amber = createTreasureType("amber gemstone", "amber", 0.1f, 100)
    val jade = createTreasureType("jade gemstone", "jade", 0.1f, 250)
    val pearl = createTreasureType("pearl", "pearl", 0.1f, 500)
    val aquamarineGemstone =
        createTreasureType("aquamarine gemstone", "aquamarine", 0.1f, 700)
    val peridotGemstone = createTreasureType("peridot gemstone", "peridot", 0.1f, 800)
    val opalGemstone = createTreasureType("opal gemstone", "opal", 0.1f, 1000)
    val sapphire = createTreasureType("sapphire", "sapphire", 0.1f, 2000)
    val ruby = createTreasureType("ruby", "ruby", 0.1f, 3000)
    val emerald = createTreasureType("emerald", "emerald", 0.1f, 5000)
    val jacinthGemstone = createTreasureType("jacinth gemstone", "jacinth", 0.1f, 7500)
    val diamond = createTreasureType("diamond", "diamond", 0.1f, 10000)
    val largeSapphire = createTreasureType("large sapphire", "largeSapphire", 0.2f, 15000)
    val largeRuby = createTreasureType("large ruby", "largeRuby", 0.2f, 17500)
    val largeEmerald = createTreasureType("large emerald", "largeEmerald", 0.2f, 20000)
    val largeJacinthGemstone =
        createTreasureType("large jacinth gemstone", "largeJacinth", 0.2f, 22500)
    val largeDiamond = createTreasureType("large diamond", "largeDiamond", 0.2f, 25000)
    val goldBar = createTreasureType("gold bar", "goldBar", 10f, 1000)
    val silverBar = createTreasureType("silver bar", "silverBar", 10f, 250)
    val platinumBar = createTreasureType("platinum bar", "platinumBar", 10f, 4000)
    val ivoryNecklace = createTreasureType("ivory necklace", "ivoryNecklace", 0.5f, 100)
    val silverNecklace = createTreasureType("silver necklace", "silverNecklace", 0.5f, 500)
    val goldNecklace = createTreasureType("gold necklace", "goldNecklace", 0.5f, 1000)
    val coralNecklace = createTreasureType("coral necklace", "coralNecklace", 0.5f, 2000)
    val jadeNecklace = createTreasureType("jade necklace", "jadeNecklace", 0.5f, 3000)
    val platinumNecklace =
        createTreasureType("platinum necklace", "platinumNecklace", 0.5f, 4000)
    val silverNecklaceWithGems =
        createTreasureType("silver necklace with gems", "silverNecklaceWithGems", 0.5f,
            5000)
    val goldNecklaceWithGems =
        createTreasureType("gold necklace with gems", "goldNecklaceWithGems", 0.5f, 8000)
    val platinumNecklaceWithGems =
        createTreasureType("platinum necklace with gems", "platinumNecklaceWithGems", 0.5f,
            11000)
    val silverMedallion =
        createTreasureType("silver medallion", "silverMedallion", 1f, 1000)
    val goldMedallion = createTreasureType("gold medallion", "goldMedallion", 1f, 2000)
    val platinumMedallion =
        createTreasureType("platinum medallion", "platinumMedallion", 1f, 5000)
    val silverChalice = createTreasureType("silver chalice", "silverChalice", 2f, 2000)
    val goldChalice = createTreasureType("gold chalice", "goldChalice", 2f, 4000)
    val platinumChalice =
        createTreasureType("platinum chalice", "platinumChalice", 2f, 6000)
    val silverChaliceWithGems =
        createTreasureType("silver chalice with gems", "silverChaliceWithGems", 3f, 7000)
    val goldChaliceWithGems =
        createTreasureType("gold chalice with gems", "goldChaliceWithGems", 3f, 9000)
    val platinumChaliceWithGems =
        createTreasureType("platinum chalice with gems", "platinumChaliceWithGems", 3f,
            14000)
    val silverSceptre = createTreasureType("silver sceptre", "silverSceptre", 4f, 3000)
    val goldSceptre = createTreasureType("gold sceptre", "goldSceptre", 4f, 5000)
    val platinumSceptre =
        createTreasureType("platinum sceptre", "platinumSceptre", 4f, 7000)
    val silverSceptreWithGems =
        createTreasureType("silver sceptre with gems", "silverSceptreWithGems", 5f, 9000)
    val goldSceptreWithGems =
        createTreasureType("gold sceptre with gems", "goldSceptreWithGems", 5f, 12000)
    val platinumSceptreWithGems =
        createTreasureType("platinum sceptre with gems", "platinumSceptreWithGems", 5f,
            16000)
    val silverCrown = createTreasureType("silver crown", "silverCrown", 5f, 4000)
    val goldCrown = createTreasureType("gold crown", "goldCrown", 5f, 6000)
    val platinumCrown = createTreasureType("platinum crown", "platinumCrown", 5f, 8000)
    val silverCrownWithGems =
        createTreasureType("silver crown with gems", "silverCrownWithGems", 8f, 10000)
    val goldCrownWithGems =
        createTreasureType("gold crown with gems", "goldCrownWithGems", 8f, 18000)
    val platinumCrownWithGems =
        createTreasureType("platinum crown with gems", "platinumCrownWithGems", 8f, 26000)
}