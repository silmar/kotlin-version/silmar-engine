package silmarengine.contexts.gameplay.entities.items.weapons.extensions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType

/**
 * Returns whether this weapon can match the given
 * plus-value-needed-to-hit a particular target.
 */
fun Weapon.canHit(plusNeededToHit: Int): Boolean {
    // if this weapon's to-hit-bonus is high enough
    val type = weaponType
    if (type.toHitModifier >= plusNeededToHit) return true

    return canWeaponTypeHit(type, plusNeededToHit)
}

@ExtensionPoint
var canWeaponTypeHit: (type: WeaponType, plusNeededToHit: Int) -> Boolean =
    { _, _ -> false }