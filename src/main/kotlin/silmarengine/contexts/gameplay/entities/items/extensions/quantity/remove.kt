package silmarengine.contexts.gameplay.entities.items.extensions.quantity

import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.onItemRemoved
import silmarengine.contexts.gameplay.entities.items.Item

/**
 * Removes this item from either its holder, or the area it is in.
 */
fun Item.remove() {
    // if this item is being held
    val entity = entity
    if (holder != null) {
        // inform the holder
        holder!!.onItemRemoved(this)

        // this item is no longer held
        onDropped()
    }
    // otherwise, remove the item's entity from its area
    else entity?.area?.removeEntity(entity)
}