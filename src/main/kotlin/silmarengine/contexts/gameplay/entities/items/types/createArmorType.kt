package silmarengine.contexts.gameplay.entities.items.types

import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun createArmorType(name: String, imageName: String, unidentifiedName: String?,
    encumbrance: Float, avoidance: Int, defenseModifier: Int, avoidanceModifier: Int,
    isMetal: Boolean, isNoisy: Boolean, strengthRequired: Int, price: Int): ItemType1 {
    val result = ItemType1(name, imageName, getFrequencyForValue(price), isArmor = true,
        unidentifiedName = unidentifiedName, encumbrance = encumbrance,
        equipLocation = EquipLocation.BODY, avoidance = avoidance,
        defenseModifier = defenseModifier, avoidanceModifier = avoidanceModifier,
        isMetal = isMetal, isNoisy = isNoisy, strengthRequired = strengthRequired,
        price = price, chanceOfInChest = getChanceOfInChest(price))
    result.register()
    armorTypes.add(result)
    return result
}

fun createMagicalArmorTypes(normalType: ItemType1, upToPlusAndMinus: Int) {
    for (plus in -upToPlusAndMinus..upToPlusAndMinus) {
        if (plus == 0) continue

        // create the type
        val type = normalType.createMagicalVersion(plus).copy(
            avoidanceModifier = normalType.avoidanceModifier + plus,
            defenseModifier = normalType.defenseModifier + plus
        )
        type.register()
        armorTypes.add(type)
    }
}
