package silmarengine.contexts.gameplay.entities.items.weapons.types

import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.types.AmmoTypes
import java.util.*

val weaponTypes: MutableList<WeaponType> = ArrayList()

@Suppress("unused")
object WeaponTypes {
    val hand =
        WeaponType1(createWeaponItemType("hand", "hand", frequency = 0), maxDamage = 2,
            isEdged = false, damageForm = DamageForms.bluntImpact)

    val dagger = WeaponType1(
        createWeaponItemType("dagger", "dagger", encumbrance = 1f, avoidance = 7,
            isMetal = true, strengthRequired = 1, price = 2), maxDamage = 4,
        isEdged = true, damageForm = DamageForms.slash)

    init {
        WeaponType1.createMagicalWeaponTypes(dagger, 5)
    }

    val club = WeaponType1(
        createWeaponItemType("club", "club", encumbrance = 3f, avoidance = 16,
            strengthRequired = 7, price = 1), maxDamage = 5,
        damageForm = DamageForms.bluntImpact)
    val mace = WeaponType1(
        createWeaponItemType("mace", "mace", encumbrance = 10f, avoidance = 7,
            isMetal = true, strengthRequired = 9, price = 10), minDamage = 2,
        maxDamage = 7, damageForm = DamageForms.bluntImpact)

    init {
        WeaponType1.createMagicalWeaponTypes(mace, 5)
    }

    val greatMace = WeaponType1(
        createWeaponItemType("great mace", "greatMace", encumbrance = 20f,
            takesTwoHands = true, avoidance = 6, isMetal = true, strengthRequired = 11,
            price = 20), minDamage = 2, maxDamage = 10,
        damageForm = DamageForms.bluntImpact)

    init {
        WeaponType1.createMagicalWeaponTypes(greatMace, 5)
    }

    val staff = WeaponType1(
        createWeaponItemType("staff", "staff", encumbrance = 5f, takesTwoHands = true,
            avoidance = 18, isMetal = false, strengthRequired = 3, price = 2),
        maxDamage = 6, damageForm = DamageForms.bluntImpact)

    init {
        WeaponType1.createMagicalWeaponTypes(staff, 3)
    }

    val shortSword = WeaponType1(
        createWeaponItemType("short sword", "shortSword", encumbrance = 4f, avoidance = 7,
            isMetal = true, strengthRequired = 7, price = 10), maxDamage = 7,
        isEdged = true, damageForm = DamageForms.slash)

    init {
        WeaponType1.createMagicalWeaponTypes(shortSword, 5)
    }

    val longSword = WeaponType1(
        createWeaponItemType("long sword", "longSword", encumbrance = 6f, avoidance = 7,
            isMetal = true, strengthRequired = 9, price = 17), maxDamage = 10,
        isEdged = true, damageForm = DamageForms.slash)

    init {
        WeaponType1.createMagicalWeaponTypes(longSword, 5)
    }

    val twoHandedSword = WeaponType1(
        createWeaponItemType("two-handed sword", "twoHandedSword", encumbrance = 23f,
            takesTwoHands = true, avoidance = 7, isMetal = true, strengthRequired = 13,
            price = 35), minDamage = 2, maxDamage = 14, isEdged = true,
        damageForm = DamageForms.slash)

    init {
        WeaponType1.createMagicalWeaponTypes(twoHandedSword, 5)
    }

    val sling = WeaponType1(
        createWeaponItemType("sling", "sling", encumbrance = 1f, takesTwoHands = true,
            avoidance = 16, isMetal = false, strengthRequired = 3, price = 1),
        maxDamage = 4, isRanged = true, maxRange = TileDistance(12),
        ammoType = AmmoTypes.slingStone, damageForm = DamageForms.bluntImpact)
    val bow = WeaponType1(
        createWeaponItemType("bow", "bow", encumbrance = 4f, takesTwoHands = true,
            avoidance = 12, strengthRequired = 8, price = 75), maxDamage = 6,
        isRanged = true, maxRange = TileDistance(20), ammoType = AmmoTypes.arrow,
        isEdged = true, numAttacks = 2, damageForm = DamageForms.pierce)

    init {
        WeaponType1.createMagicalWeaponTypes(bow, 5)
    }

    val crossbow = WeaponType1(
        createWeaponItemType("crossbow", "crossbow", encumbrance = 10f,
            takesTwoHands = true, avoidance = 10, isMetal = true, strengthRequired = 9,
            price = 24, info = "Is highly penetrating."), minDamage = 3, maxDamage = 10,
        isRanged = true, maxRange = TileDistance(30), ammoType = AmmoTypes.quarrel,
        isEdged = true, damageForm = DamageForms.pierce)

    init {
        WeaponType1.createMagicalWeaponTypes(crossbow, 5)
    }
}