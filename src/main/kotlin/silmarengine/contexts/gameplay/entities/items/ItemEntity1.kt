package silmarengine.contexts.gameplay.entities.items

import silmarengine.contexts.gameplay.entities.Entity1
import silmarengine.contexts.gameplay.entities.items.specializations.Torch
import java.awt.Image

open class ItemEntity1(final override val item: Item) : ItemEntity, Entity1() {
    override var image: Image?
        get() = item.image
        set(_) = Unit

    init {
        @Suppress("LeakingThis") item.onWrappedByEntity(this)

        // set this entity's initial size from its item's type
        mutableBounds.setSize(item.type.entityType.size)
    }

    override fun onTurnOver() {
        super<Entity1>.onTurnOver()

        val item = item
        if (item is Torch) {
            item.onEndOfTurn()
        }
    }
}