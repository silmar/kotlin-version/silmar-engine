package silmarengine.contexts.gameplay.entities.items

import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItemCondition
import java.awt.Color
import java.io.Serializable

/**
 * Describes a level of how good a shape an item is in.
 */
enum class ItemCondition(
    override val displayName: String,
    /**
     * The color used to graphically represent this condition.
     */
    override val color: Color,
    /**
     * How much item value is retained by an item in this condition (vs. excellent condition).
     */
    val valueFraction: Float
) : PIMItemCondition, Serializable {
    DESTROYED("destroyed", Color.white, 0f),
    POOR("poor", Color(140, 0, 0), .25f),
    FAIR("fair", Color(150, 100, 0), .5f),
    GOOD("good", Color(0, 140, 0), .75f),
    EXCELLENT("excellent", Color.black, 1f);

    /**
     * Returns whether this condition is worse than that given.
     */
    fun isWorseThan(condition: ItemCondition): Boolean = ordinal < condition.ordinal

    /**
     * Returns what fraction of an item of a groupable type is lost when it
     * degrades to this condition from the given old-condition.
     */
    fun getQuantityFractionalDifference(oldCondition: ItemCondition): Float {
        val difference = (oldCondition.ordinal - ordinal).toFloat() / (values().size - 1)
        val excellentToOldRatio = EXCELLENT.ordinal.toFloat() / oldCondition.ordinal
        return difference * excellentToOldRatio
    }

    companion object {
        /**
         * Returns a randomly-selected item-condition, excluding the destroyed condition
         * as a possibility.
         */
        val random: ItemCondition get() = values().drop(1).random()
    }
}