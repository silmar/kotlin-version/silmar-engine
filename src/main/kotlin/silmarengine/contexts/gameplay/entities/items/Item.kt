package silmarengine.contexts.gameplay.entities.items

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.playeritemsmanagement.domain.model.PIMItem
import java.awt.Image

/**
 * An item in the game, such as rations, gold, a weapon, a treasure item, etc.
 */
interface Item : PIMItem {
    /**
     * Stores attributes that are constant across all items of this type.
     */
    override val type: ItemType

    /**
     * The image used to depict this item.
     */
    override val image: Image

    /**
     * If this item currently resides in an area, the entity which wraps it so it may do
     * so.
     */
    val entity: ItemEntity?
    fun onWrappedByEntity(entity: ItemEntity)

    /**
     * The current condition of this item.
     */
    override val condition: ItemCondition

    /**
     * The current quantity of this item's type which is represented by this item.
     * Applies only to items of groupable item-types.
     */
    override var quantity: Int

    /**
     * Whether this item has been identified yet. Items of most types are always identified.
     */
    override val isIdentified: Boolean

    /**
     * The current holder of this item.
     */
    val holder: Player?
    fun onHeldBy(holder: Player)
    fun onDropped()

    /**
     * This item's total encumbrance value.
     */
    override val encumbrance: Float

    /**
     * This item's current name, based on its quantity and
     * whether it has been identified.
     */
    override val name: String

    val isDestroyed: Boolean

    /**
     * Returns a short description of whatever is notable about this item.
     */
    override val info: String?

    fun isOfType(type: ItemType): Boolean

    /**
     * Degrades this item's condition by one level.
     */
    fun degrade(alertHolder: Boolean = true)

    /**
     * Destroys this item.
     */
    fun destroy(alertHolder: Boolean = true)

    fun onRepaired()

    fun identify()

    /**
     * A marking interface to indicate that an item has an immediate effect when used.
     */
    interface Usable {
        /**
         * Informs this item that it has been used by the given user.
         */
        fun onUsed(user: Player)
    }
}
