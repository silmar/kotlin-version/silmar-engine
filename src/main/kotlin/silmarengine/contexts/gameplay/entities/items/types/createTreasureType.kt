package silmarengine.contexts.gameplay.entities.items.types

fun createTreasureType(name: String, imageName: String, encumbrance: Float,
    price: Int): ItemType1 {
    val result = ItemType1(name, imageName, avoidance = 15, isMustEquip = false,
        encumbrance = encumbrance, price = price, frequency = getFrequencyForValue(price),
        chanceOfInChest = getChanceOfInChest(price))
    result.register()
    treasureTypes.add(result)
    return result
}
