package silmarengine.contexts.gameplay.entities.items.types

import java.util.ArrayList

val nonGroupableItemTypes: MutableList<ItemType> = ArrayList()

object NonGroupableItemTypes {
    val torch =
        createNonGroupableItemType("torch", "torch", null, 1f, false, null, 10, false, 1,
            false, null, isMagical = false)
    val potionOfHealing =
        createNonGroupableItemType("potion of healing", "potion", "potion", 1f, false,
            null, 18, false, 400, false, null)

    val potionOfStrength =
        createNonGroupableItemType("potion of strength", "potion", "potion", 1f, false,
            null, 18, false, 3500, false, null)
    val potionOfIntelligence =
        createNonGroupableItemType("potion of intelligence", "potion", "potion", 1f, false,
            null, 18, false, 3500, false, null)
    val potionOfJudgement =
        createNonGroupableItemType("potion of judgement", "potion", "potion", 1f, false,
            null, 18, false, 3500, false, null)
    val potionOfAgility =
        createNonGroupableItemType("potion of agility", "potion", "potion", 1f, false,
            null, 18, false, 3500, false, null)
    val potionOfEndurance =
        createNonGroupableItemType("potion of endurance", "potion", "potion", 1f, false,
            null, 18, false, 3500, false, null)

    // note that we increase the frequency of the potion-of-poison since it has to compete
    // with five like-valued potions-of-[attribute]
    val potionOfPoison =
        createNonGroupableItemType("potion of poison", "potion", "potion", 1f, false, null,
            18, false, 3500, false, "Acts as a super-strength poison.", isMagical = false,
        frequency = potionOfStrength.frequency * 3)
}