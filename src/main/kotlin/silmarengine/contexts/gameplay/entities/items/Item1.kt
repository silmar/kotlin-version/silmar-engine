package silmarengine.contexts.gameplay.entities.items

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.extensions.condition.alertHolderOfConditionChange
import silmarengine.contexts.gameplay.entities.items.extensions.condition.onConditionChange
import silmarengine.contexts.gameplay.entities.items.extensions.getAttributeRequirementsInfoExt
import silmarengine.contexts.gameplay.entities.items.extensions.onIdentified
import silmarengine.contexts.gameplay.entities.items.extensions.quantity.onQuantityChange
import silmarengine.contexts.gameplay.entities.items.types.ItemSubType
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.items.types.getItemType
import silmarengine.contexts.gameplay.game.Game
import silmarengine.getImage
import java.awt.Image
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable

open class Item1(type: ItemType, condition: ItemCondition) : Item, Cloneable,
    Serializable {
    /**
     * Stores attributes that are constant across all items of this type.  Since
     * these values are immutable, we serialize only the type's name.
     */
    @Transient
    var mutableType: ItemType = type
        protected set
    final override val type: ItemType
        get() = mutableType

    /**
     * The name of the above item type, to be serialized for this object instead of
     * the whole type.
     */
    protected lateinit var typeName: String

    override var entity: ItemEntity? = null
    override fun onWrappedByEntity(entity: ItemEntity) = run { this.entity = entity }

    final override var condition: ItemCondition = condition
        private set(value) {
            val old = field
            field = value
            onConditionChange(old, value)
        }

    override var quantity: Int = 1
        set(value) {
            field = value
            onQuantityChange(value)
        }

    final override var isIdentified: Boolean = false
        // note that this checks whether this item's type has no unidentified name,
        // as if it doesn't, this item is always identified
        get() = (field || type.isGroupable || type.unidentifiedName == null || !type.isIndividuallyIdentified && Game.currentGame!!.isItemTypeIdentified(
            type))
        private set(value) {
            field = value
            if (value) onIdentified()
        }
    override fun identify() {
        isIdentified = true
    }

    override var holder: Player? = null
    override fun onHeldBy(holder: Player) = run { this.holder = holder }
    override fun onDropped() = run { holder = null }

    override val encumbrance: Float
        get() = type.encumbrance * quantity

    override val name: String
        get() {
            val identified = isIdentified
            if (quantity > 1 && identified) return type.pluralName!!
            return if (identified) type.name else type.unidentifiedName!!
        }

    override val isDestroyed: Boolean
        get() = condition == ItemCondition.DESTROYED

    override val info: String?
        get() = if (isIdentified) {
            // append any sub-type and/or weapon extra-info to the result
            var result = type.info
            fun append(type: ItemSubType?) {
                val info = type?.getExtraInfo()
                if (info != null) result = if (result != null) "$result<br>$info" else info
            }
            append(type.subType)
            append(type.weaponType)
            result
        } else "Not yet identified."

    /**
     * Returns the image that should be used to represent this item.
     */
    @Transient
    override var image: Image = getImage(type.entityType.imagePath!!)

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(s: ObjectOutputStream) {
        // store the name of this item's type (instead of the type itself)
        typeName = type.name

        s.defaultWriteObject()
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(stream: ObjectInputStream) {
        stream.defaultReadObject()

        // detm this item's type from its type-name
        mutableType = getItemType(typeName)!!

        image = getImage(type.entityType.imagePath!!)
    }

    override fun degrade(alertHolder: Boolean) {
        val newCondition = ItemCondition.values()[condition.ordinal - 1]
        if (alertHolder) alertHolderOfConditionChange(newCondition)
        condition = newCondition
    }

    override fun destroy(alertHolder: Boolean) {
        val newCondition = ItemCondition.DESTROYED
        if (alertHolder) alertHolderOfConditionChange(newCondition)
        condition = newCondition
    }

    override fun isOfType(type: ItemType): Boolean {
        return type == this.type
    }

    override fun onRepaired() {
        val newCondition = ItemCondition.EXCELLENT
        alertHolderOfConditionChange(newCondition)
        condition = newCondition
    }

    override fun getAttributeRequirementsInfo(): String =
        getAttributeRequirementsInfoExt()
}
