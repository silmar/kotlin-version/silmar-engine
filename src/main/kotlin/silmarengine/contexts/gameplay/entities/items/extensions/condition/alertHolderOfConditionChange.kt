package silmarengine.contexts.gameplay.entities.items.extensions.condition

import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

fun Item.alertHolderOfConditionChange(newCondition: ItemCondition) {
    // issue an appropriate sound for the change
    val holder = holder ?: return
    val area = holder.area
    val location = holder.location
    val isWorse = newCondition.isWorseThan(condition)
    area.onSoundIssued(if (isWorse) Sounds.itemDamaged else Sounds.itemRepaired, location)

    // inform the holder
    val willBeDestroyed = newCondition == ItemCondition.DESTROYED
    holder.onMessage(
        "Your " + name + (if (quantity > 1) " are " else " is ") + (if (willBeDestroyed) "destroyed" else if (isWorse) "damaged" else "repaired") + "!")
}