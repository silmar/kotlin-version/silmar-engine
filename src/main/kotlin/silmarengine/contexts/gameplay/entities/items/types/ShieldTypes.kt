package silmarengine.contexts.gameplay.entities.items.types

import java.util.ArrayList

val shieldTypes: MutableList<ItemType> = ArrayList()

object ShieldTypes {
    val smallShield =
        createShieldType("small shield", "shield", null, 5f, 6, 1, 1, true, 8, 10)

    init {
        createMagicalShieldTypes(smallShield, 3)
    }

    val largeShield =
        createShieldType("large shield", "largeShield", null, 10f, 5, 2, 2, true, 12, 40)

    init {
        createMagicalShieldTypes(largeShield, 3)
    }

    val greatShield =
        createShieldType("great shield", "greatShield", null, 20f, 4, 3, 3, true, 17, 80)

    init {
        createMagicalShieldTypes(greatShield, 3)
    }
}