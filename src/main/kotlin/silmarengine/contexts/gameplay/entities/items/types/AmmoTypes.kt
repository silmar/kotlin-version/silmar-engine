package silmarengine.contexts.gameplay.entities.items.types

import java.util.ArrayList

val ammoTypes: MutableList<ItemType> = ArrayList()

object AmmoTypes {
    val slingStone =
        createAmmoType("sling stone", "slingStones", 10, null, null, 0.3f, 0f, 10, 0,
            1)
    val arrow = createAmmoType("arrow", "arrow", 10, null, null, 0.5f, 0f, 10, 1, 15)
    val quarrel =
        createAmmoType("quarrel", "quarrel", 10, null, null, 1f, 0f, 10, 1, 12)
}