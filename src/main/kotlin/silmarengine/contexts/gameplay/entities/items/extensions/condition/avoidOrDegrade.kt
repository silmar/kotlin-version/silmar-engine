package silmarengine.contexts.gameplay.entities.items.extensions.condition

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.util.math.randomInt

/**
 * Performs an avoidance check for this item at the given penalty,
 * and if it fails, degrades this item.  If the check fails by a lot,
 * this item is destroyed.
 */
fun Item.avoidOrDegrade(penalty: Int) {
    // if this item fails an avoidance check at the given
    // penalty by less than a certain amount
    val roll = randomInt(1, 20)
    val failedBy = type.avoidance - (roll + type.avoidanceModifier - penalty)
    if (failedBy in 1..7) {
        // degrade this item's condition
        degrade()
    }
    // else, if this item failed by more than that amount
    else if (failedBy >= 8) {
        // this item is destroyed
        destroy()
    }
}

