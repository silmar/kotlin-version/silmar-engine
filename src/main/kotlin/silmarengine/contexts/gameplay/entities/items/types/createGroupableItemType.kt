package silmarengine.contexts.gameplay.entities.items.types

fun createGroupableItemType(name: String, imageName: String, frequency: Int,
    pluralName: String?, unidentifiedName: String?, encumbrance: Float,
    chanceOfInChest: Float, quantityToCreate: Int, price: Int,
    avoidance: Int): ItemType1 {
    val result = ItemType1(name, imageName, frequency = frequency,
        pluralName = pluralName ?: "${name}s", unidentifiedName = unidentifiedName,
        isGroupable = true, isMustEquip = false, encumbrance = encumbrance,
        chanceOfInChest = chanceOfInChest, quantityToCreate = quantityToCreate,
        price = price, avoidance = avoidance)
    result.register()
    return result
}
