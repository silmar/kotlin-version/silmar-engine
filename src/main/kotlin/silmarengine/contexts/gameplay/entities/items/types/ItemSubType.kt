package silmarengine.contexts.gameplay.entities.items.types

/**
 * Since ItemType1 is a data class, it cannot have proper sub-types.  So instead, an
 * item-type is composed into an instance of a class which represents an associated "subtype".
 * This allows navigation from the "subtype" to the item-type, but not the other
 * direction.  An item-type therefore has a field containing an instance of this
 * interface, into which the "subtype" deposits itself, so that we may navigate
 * from item-type to its associated "subtype".
 */
interface ItemSubType {
    /**
     * Returns informational strings (if any) which the "subtype" would like to append
     * to the info displayed for the item-type.
     */
    fun getExtraInfo(): String?
}