package silmarengine.contexts.gameplay.entities.items.weapons

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType

/**
 * Returns a newly created instance of the given weapon-type.
 */
fun createWeapon(type: WeaponType, condition: ItemCondition): Weapon =
    createGameSpecificWeapon(type, condition) ?: Weapon1(type, condition)

@ExtensionPoint
var createGameSpecificWeapon: (type: WeaponType, condition: ItemCondition) -> Weapon? =
    { _, _ -> null }
