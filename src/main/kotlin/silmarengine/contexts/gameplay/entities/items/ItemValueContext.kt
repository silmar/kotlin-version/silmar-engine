package silmarengine.contexts.gameplay.entities.items

enum class ItemValueContext(
    /**
     * What fraction of the item's value remains in this context.
     */
    val valueFraction: Float,
    /**
     * Whether an item's being cursed renders it valueless in this context.
     */
    val isZeroValueIfCursed: Boolean
) {
    PLACEMENT(1f, false),
    BUY(1f, true),
    SELL(.75f, true),
    REPAIR(.7f, false),
    // the value-fraction for experience makes it more advantageous to donate items
    // directly, rather than sell them and then donate the gold; this is ok, since
    // it takes into account the portability of items vs. that of their value in gold
    // (for transfer to the player's home village)
    EXPERIENCE(.1f, true),
    IDENTIFY(.1f, false)
}