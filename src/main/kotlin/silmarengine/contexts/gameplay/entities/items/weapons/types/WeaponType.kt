package silmarengine.contexts.gameplay.entities.items.weapons.types

import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.damage.DamageForm
import silmarengine.contexts.gameplay.entities.items.types.ItemSubType
import silmarengine.contexts.gameplay.entities.items.types.ItemType

interface WeaponType : ItemSubType {
    /**
     * The more general item type associated with this type.
     */
    val itemType: ItemType

    /**
     * A weapon subtype (if any) associated with this weapon type.
     */
    val weaponSubType: ItemSubType?

    /**
     * The minimum and maximum base damage a weapon of this type can do
     * with one hit.
     */
    val minDamage: Int
    val maxDamage: Int

    /**
     * The modifier to hit an opponent that an attacker gets
     * when striking with a weapon of this type.
     */
    val toHitModifier: Int

    /**
     * The modifier to damage done to an opponent
     * that an attacker gets when striking with a weapon of this type.
     */
    val damageModifier: Int

    /**
     * Whether a weapon of this type can strike at opponents at a distance.
     */
    val isRanged: Boolean

    /**
     * If a weapon of this type is ranged, the max tile-range at which it
     * can strike an opponent.
     */
    val maxRange: TileDistance

    /**
     * If specified, the type of ammo item a weapon of this type fires.
     */
    val ammoType: ItemType?

    /**
     * Whether a weapon of this type is edged.
     */
    val isEdged: Boolean

    /**
    * The base number of attacks an attacker may perform with a weapon
    * of this type in one game turn.
    */
    val numAttacks: Int

    /**
     * The attack type utilized by a weapon of this type.
     */
    val attackType: AttackType

    /**
     * The form of damage inflicted by a weapon of this type.
     */
    val damageForm: DamageForm
}