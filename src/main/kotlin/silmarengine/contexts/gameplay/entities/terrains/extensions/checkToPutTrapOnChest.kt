package silmarengine.contexts.gameplay.entities.terrains.extensions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.entities.terrains.types.trapTypes
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.util.math.randomFloat

/**
 * Has this chest check to see if it should place a trap on itself.
 */
fun Chest.checkForBeingTrapped() {
    // if we randomly detm this chest should be trapped, based on the value of the
    // item it contains (or, just a flat chance if it contains no item)
    val chanceOfTrap = if (item != null) item!!.getValue(ItemValueContext.PLACEMENT)
        .toFloat() / (getGameSpecificAreaMaxItemPlacementValue(area))
    else .25f
    if (randomFloat < chanceOfTrap) {
        // while we haven't yet determined this chest's trap
        while (trap == null) {
            // choose a trap-type at random from all that exist
            val trapType = trapTypes.random()

            // exclude non-damaging traps and those that aren't valid for this
            // chest's area
            if (trapType == TerrainTypes.gustOfWindTrap || !trapType.isValidForArea(
                    area)) continue

            // set the trap as the one that's on this chest
            trap = createTerrain(trapType) as Trap
            trap!!.setOnChest(this)

            // add the trap to the area
            area.addEntity(trap!!, location)
        }
    }
}

@ExtensionPoint
var getGameSpecificAreaMaxItemPlacementValue: (area: Area) -> Int =
    { _ -> throw NotImplementedError() }