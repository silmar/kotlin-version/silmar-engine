package silmarengine.contexts.gameplay.entities.terrains.types

import silmarengine.contexts.gameplay.entities.types.EntityType

/**
 * Describes the attributes of a particular kind of terrain.
 */
interface TerrainType : EntityType {
    /**
     * Whether instances of this terrain-type are passible (i.e. can be moved over).
     */
    val isPassible: Boolean

    /**
     * Whether terrains of this terrain-type generate events when the player
     * runs into them.
     */
    val firesPlayerAtEvents: Boolean

    /**
     * Whether terrains of this terrain-type represent a trap.
     */
    val isTrap: Boolean

    /**
     * A terrain of this type's base avoidance value to avoid harm to itself.
     */
    val avoidance: Int

    /**
     * Whether terrains of this terrain-type talk.
     */
    val isTalker: Boolean

    /**
     * Whether terrains of this terrain-type sell items.
     */
    val isSeller: Boolean

    /**
     * Whether terrains of this terrain-type fix items.
     */
    val isFixer: Boolean
}

/**
 * The relative path to the image files for terrain types.
 */
const val terrainTypeImagePath = "terrain/"