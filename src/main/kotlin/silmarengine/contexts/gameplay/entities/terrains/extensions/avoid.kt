package silmarengine.contexts.gameplay.entities.terrains.extensions

import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.util.math.randomInt

class TerrainAvoidResult(
    /**
     * Whether the avoidance check succeeded.
     */
    val success: Boolean,
    /**
     * By how much the avoidance check failed.
     */
    val failedBy: Int
)

/**
 * Returns whether this terrain passes an avoidance check at the given modifier.
 */
fun Terrain.avoid(modifier: Int): TerrainAvoidResult {
    val roll = randomInt(1, 20)
    val failedBy = -roll + (type.avoidance - modifier)
    val success = failedBy <= 0
    return TerrainAvoidResult(success, failedBy)
}
