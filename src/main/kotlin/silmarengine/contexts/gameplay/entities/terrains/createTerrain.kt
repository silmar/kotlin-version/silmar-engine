package silmarengine.contexts.gameplay.entities.terrains

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.terrains.specializations.*
import silmarengine.contexts.gameplay.entities.terrains.traps.specializations.DamageTrap
import silmarengine.contexts.gameplay.entities.terrains.traps.specializations.GustOfWindTrap
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes

/**
 * Returns a newly created instance of the given terrain-type.
 */
fun createTerrain(type: TerrainType): Terrain {
    // create the actual item object according to the given type
    val terrain = createGameSpecificTerrain(type) ?: when (type) {
        TerrainTypes.chest -> Chest()
        TerrainTypes.monsterActivator -> MonsterActivator()
        TerrainTypes.tempLightSource -> TempLightSource()
        TerrainTypes.eventMarker -> EventMarker()
        TerrainTypes.summoner -> Summoner()
        TerrainTypes.damageTrap -> DamageTrap()
        TerrainTypes.gustOfWindTrap -> GustOfWindTrap()
        else -> Terrain1()
    }

    terrain.onCreated(type)

    return terrain
}

@ExtensionPoint
var createGameSpecificTerrain: (type: TerrainType) -> Terrain? = { _ -> null }