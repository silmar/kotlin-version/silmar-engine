package silmarengine.contexts.gameplay.entities.terrains.traps.specializations

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains.onGustOfWindTrap
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap1

class GustOfWindTrap : Trap1() {
    override fun spring(player: Player) {
        player.onGustOfWindTrap()
    }
}