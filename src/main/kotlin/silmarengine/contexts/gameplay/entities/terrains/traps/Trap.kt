package silmarengine.contexts.gameplay.entities.terrains.traps

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest

interface Trap : Terrain {
    /**
     * Whether this trap has been detected by any player.
     */
    val isDetected: Boolean

    /**
     * The chest on which this trap is set, if any.
     */
    val chest: Chest?
    fun setOnChest(chest: Chest)

    /**
     * Springs this trap on the given player.
     */
    fun spring(player: Player)

    /**
     * Informs this trap that it has been detected.
     */
    fun onDetected()
}