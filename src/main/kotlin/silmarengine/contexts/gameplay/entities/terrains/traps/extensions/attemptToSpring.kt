package silmarengine.contexts.gameplay.entities.terrains.traps.extensions

import silmarengine.annotations.ExtensionPoint
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

/**
 * Attempts to spring this trap on the given player.
 */
fun Trap.attemptToSpring(player: Player) {
    // if this trap hasn't been detected, or the given player fails to disarm it
    if (!isDetected || !shouldTrapNotSpringForGameSpecificReason(this, player)) {
        // issue a sprung sound
        area.onSoundIssued(Sounds.trapSprung, location)

        // inform the player
        player.onMessage("You sprung a trap!")

        spring(player)
    }

    // remove this trap from the area
    area.removeEntity(this)
}

@ExtensionPoint
var shouldTrapNotSpringForGameSpecificReason: (trap: Trap, player: Player) -> Boolean =
    { _, _ -> false }