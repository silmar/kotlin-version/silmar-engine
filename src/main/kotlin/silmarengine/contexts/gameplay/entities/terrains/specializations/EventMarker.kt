package silmarengine.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import java.io.Serializable

/**
 * An invisible terrain feature whose only purpose is to trigger an event
 * when it is stepped on.
 */
class EventMarker : Terrain1() {
    /**
     * The event this marker is to trigger.
     */
    private var event: Event? = null

    fun setEvent(event: Event) {
        this.event = event
    }

    /**
     * Informs this marker that it has been stepped on, which should trigger its event.
     */
    fun onTriggered() {
        event!!.occur(this)
        area.removeEntity(this)
    }

    /**
     * An object must implement this interface to be considered an event that
     * may be triggered by this marker.
     */
    interface Event : Serializable {
        /**
         * Tells this event to occur, as it has been triggered by the given marker.
         */
        fun occur(marker: EventMarker)
    }
}