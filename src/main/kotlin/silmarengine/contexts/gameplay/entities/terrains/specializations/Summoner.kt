package silmarengine.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.types.nameToMonsterTypeMap
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.types.itemTypesByName
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.entities.types.EntityType
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInLOSAndRange
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.io.Serializable

class Summoner : Terrain1() {
    /**
     * The type of entity to be summoned by this summoner. It's transient because
     * entity-types aren't serializable.
     */
    @Transient
    private var summonType: EntityType? = null

    /**
     * The name of the type of entity to be summoned by this summoner.  We serialize
     * this instead of the type itself.
     */
    private var summonTypeName: String? = null

    /**
     * The location to which this summoner should summon entities.
     */
    private var summonLocation: PixelPoint? = null

    /**
     * How many entities this summoner should summon.
     */
    private var summonQuantity: Int = 0

    /**
     * How many game turns should elapse between each summoning by this summoner.
     */
    private var summonInterval: Int = 0

    /**
     * See inner StartCriterion interface.
     */
    private var startCriterion: StartCriterion? = null

    /**
     * Whether this summoner has starting summoning entities yet.
     */
    private var started: Boolean = false

    /**
     * How many entities this summoner has summoned so far.
     */
    private var numSummoned = 0

    /**
     * See inner DoneListener interface.
     */
    private var doneListener: DoneListener? = null

    /**
     * Sets the parameters used by this summoner for summoning.
     */
    fun setSummoningParameters(summonType: EntityType, summonLocation: PixelPoint?,
        summonQuantity: Int, summonInterval: Int, startCriterion: StartCriterion,
        doneListener: DoneListener?) {
        // note that if we use given summon-location (if any) directly (instead of a copy
        // of it), an error will result during de-serialization
        summonTypeName = summonType.name
        this.summonLocation =
            if (summonLocation != null) PixelPoint(summonLocation) else null
        this.summonQuantity = summonQuantity
        this.summonInterval = summonInterval
        this.startCriterion = startCriterion
        this.doneListener = doneListener
    }

    /**
     * Describes an object that can answer for this summoner when it should start summoning.
     */
    interface StartCriterion : Serializable {
        /**
         * Returns whether the given summoner may start summoning based upon some
         * attribute (usually, location) of the given player.
         */
        fun getCanStart(summoner: Terrain, player: Player): Boolean
    }

    /**
     * Describes an object that is interested in learning when this summoner
     * has finished summoning entities.
     */
    interface DoneListener {
        /**
         * Informs this listener that the given summoner has finished summoning entities.
         */
        fun onDone(summoner: Terrain)
    }

    override fun act() {
        // if this summoner hasn't started summoning yet
        if (!started) {
            // if this summoner's start-criterion says that due to some attribute
            // of a player in LOS this summoner may start summoning
            val player = area.player
            if (player?.isInLOSOf(location) == true && startCriterion!!.getCanStart(
                    this@Summoner, player)) {
                // start this summoner summoning
                started = true
            }
        }

        // if this summoner has started summoning and it's time to summon
        val game = Game.currentGame
        if (started && game!!.turn % summonInterval == 0) {
            // if this summoner's summon-type hasn't yet been resolved from its
            // summon-type-name
            if (summonType == null) {
                // resolve it
                summonType = nameToMonsterTypeMap[summonTypeName]
                if (summonType == null) {
                    summonType = itemTypesByName[summonTypeName]?.entityType
                }
            }

            // create the entity to be summoned
            val entity = summonType!!.createStockInstance()

            // if a location was given to summon to
            val nearby: PixelPoint?
            val summonLocationVal = summonLocation
            nearby = if (summonLocationVal != null) {
                // find an empty spot near it
                area.getNearbyPassibleRectangleLocation(summonLocationVal, entity.size,
                    entity.movementType)
            }
            else {
                // otherwise, choose a random passible rectangle-location in LOS
                area.getRandomPassibleRectangleLocationInLOSAndRange(location,
                    entity.size, PixelDistance(10 * tileWidth), 50,
                    MovementType.WALKING_NO_HANDS, true)
            }

            if (nearby == null) return

            // teleport the summonee in
            area.performTeleportEffect(nearby, true, false)
            area.addEntity(entity, nearby)
            area.performTeleportEffect(nearby, false, false)

            // if this summoner has summoned the ask-for quantity
            numSummoned++
            if (numSummoned >= summonQuantity) {
                // inform the done-listener
                if (doneListener != null) {
                    doneListener!!.onDone(this)
                }

                // remove this summoner
                area.removeEntity(this)
            }
        }
    }
}