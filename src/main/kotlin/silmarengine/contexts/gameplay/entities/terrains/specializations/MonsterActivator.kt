package silmarengine.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation.activateThoseInRange
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.geometry.TileDistance

/**
 * When stepped on, activates all monsters within a large distance.
 */
class MonsterActivator : Terrain1() {
    override fun onBeingArrival(being: Being): Boolean {
        // activate all monsters within a large distance
        area.monsters.activateThoseInRange(location, TileDistance(50))
        area.removeEntity(this)

        return false
    }
}