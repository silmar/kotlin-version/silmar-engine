package silmarengine.contexts.gameplay.entities.terrains.types

import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes.chest
import silmarengine.contexts.gameplay.entities.types.EntityType1
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.Tiles.floor
import silmarengine.contexts.gameplay.tiles.Tiles.roomFloor

@Suppress("LeakingThis")
open class TerrainType1(name: String, imageName: String?,
    override val isPassible: Boolean, frequency: Int,
    override val firesPlayerAtEvents: Boolean, override val isTrap: Boolean,
    override val avoidance: Int, override val isTalker: Boolean,
    override val isSeller: Boolean, override val isFixer: Boolean) : TerrainType,
    EntityType1(name, if (imageName != null) terrainTypeImagePath + imageName else null,
        frequency) {
    init {
        determineSize()

        if (isTrap) trapTypes.add(this)
        else nonTrapTypes.add(this)

        terrainTypes.add(this)
        nameToTerrainTypeMap[name] = this
    }

    /**
     * Returns whether the given terrain-type is the same as this one.
     *
     * @param   other    The terrain-type to compare to this one.
     */
    override fun equals(other: Any?): Boolean {
        return other === this || (other is TerrainType1 && other.name == name)
    }

    override fun createStockInstance(): Entity = createTerrain(this)

    override fun isValidForArea(area: Area): Boolean = when {
        equals(TerrainTypes.gustOfWindTrap) && area.isSelfLit -> false
        else -> true
    }

    override fun isPresenceLocationValid(area: Area, location: PixelPoint): Boolean {
        // if there is already some other terrain at the given location
        if (getFirstEntityAt(location, area.terrains) != null) {
            return false
        }

        // if this is the chest terrain-type, and the relevant tile isn't a floor
        val tile = area.getTile(location)
        if (equals(chest) && tile != roomFloor && tile != floor) return false

        // if this terrain-type is a trap type
        if (isTrap) {
            // if the relevant tile isn't floor
            if (tile != floor) return false
        }
        // otherwise, if the relevant tile isn't passible or blocks los
        else if (!tile.isPassible || tile.blocksLOS) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    companion object {
        /**
         * A high avoidance value which signifies that terrains of a particular type are
         * indestructible.
         */
        const val indestructible = -100
    }
}

