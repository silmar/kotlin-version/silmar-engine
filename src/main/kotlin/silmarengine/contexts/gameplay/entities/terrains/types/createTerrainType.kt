package silmarengine.contexts.gameplay.entities.terrains.types

import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType1.Companion.indestructible

fun createTerrainType(
    name: String, imageName: String?, isPassible: Boolean, frequency: Int,
    firesPlayerAtEvents: Boolean, isTrap: Boolean, avoidance: Int = indestructible,
    isTalker: Boolean = false, isSeller: Boolean = false, isFixer: Boolean = false
    ): TerrainType =
    TerrainType1(name, imageName, isPassible, frequency, firesPlayerAtEvents,
        isTrap, avoidance, isTalker, isSeller, isFixer)
