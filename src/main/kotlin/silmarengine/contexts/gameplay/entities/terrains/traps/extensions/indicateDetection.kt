package silmarengine.contexts.gameplay.entities.terrains.traps.extensions

import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.areas.extensions.entities.onEntityChangedAppearance
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * Indicates to its surroundings that this trap has been detected.
 */
fun Trap.indicateDetection() {
    area.onSoundIssued(Sounds.trapDetected, location)

    // this trap will now be visible
    area.onEntityChangedAppearance()
}