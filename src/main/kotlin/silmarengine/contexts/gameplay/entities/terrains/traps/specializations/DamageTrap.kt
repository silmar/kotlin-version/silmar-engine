package silmarengine.contexts.gameplay.entities.terrains.traps.specializations

import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap1
import silmarengine.util.math.randomInt

class DamageTrap : Trap1() {
    override fun spring(player: Player) {
        player.takeDamage(Damage(randomInt(1, 6), DamageForms.bluntImpact))
    }
}