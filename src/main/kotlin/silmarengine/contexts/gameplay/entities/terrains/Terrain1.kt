package silmarengine.contexts.gameplay.entities.terrains

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.beingmovement.domain.model.BMBeing
import silmarengine.contexts.gameplay.entities.Entity1
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType
import silmarengine.contexts.gameplay.entities.terrains.types.nameToTerrainTypeMap
import silmarengine.getImage
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelRectangle
import java.awt.Image
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable

open class Terrain1 : Terrain, Entity1(), Serializable {
    /**
     * Stores attributes that are constant across all terrain of this type.  Since
     * these values are immutable, we serialize only the type's name.
     */
    @Transient
    lateinit var mutableType: TerrainType
        private set
    override val type: TerrainType get() = mutableType

    /**
     * The name of the above terrain type, to be serialized for this object instead of
     * the whole type.
     */
    private var typeName: String? = null

    override val impassibleBounds = MutablePixelRectangle()

    /**
     * Returns the image that should be used to represent this terrain.
     */
    @Transient
    override var image: Image? = null
        get() {
            val imagePath = type.imagePath
            return if (field != null || imagePath == null) field
            else {
                field = getImage(imagePath)
                field
            }
        }

    override val isVisible: Boolean
        get() = type.imagePath != null

    override fun toString(): String {
        return "Terrain1(type=${type.name})"
    }

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(s: ObjectOutputStream) {
        // store the name of this terrain's type (instead of the type itself)
        typeName = type.name

        s.defaultWriteObject()
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(s: ObjectInputStream) {
        try {
            s.defaultReadObject()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }

        // determine this terrain's type from its type-name
        mutableType = nameToTerrainTypeMap[typeName]!!
    }

    override fun onBeingHere(being: Being) {}

    override fun onBeingArrival(being: Being): Boolean {
        return false
    }

    override fun onBeingArrival(being: BMBeing): Boolean = onBeingArrival(being as Being)

    override fun onCreated(type: TerrainType) {
        this.mutableType = type

        // set this entity's initial size from its type
        mutableBounds.setSize(type.size)
    }

    override fun isPassible(movementType: MovementType): Boolean = when (movementType) {
        MovementType.EARTHING, MovementType.ETHEREAL, MovementType.FLYING, MovementType.FLYING_NO_HANDS -> true
        else -> type.isPassible
    }

    override fun onInteractionWithPlayer(player: Player): Boolean {
        return false
    }

    override fun onBoundsChanged() {
        // recalculate this terrain's impassible bounds as being of half the dimensions
        // of its normal bounds
        impassibleBounds.set(bounds)
        val size = bounds.size
        impassibleBounds.translate(size.width / 4, size.height / 4)
        impassibleBounds.setSize(size.width / 2, size.height / 2)
    }

    override fun onDestroyed(avoidanceFailedBy: Int) {}

    override fun isHarmfulTo(monster: Monster): Boolean {
        return false
    }
}
