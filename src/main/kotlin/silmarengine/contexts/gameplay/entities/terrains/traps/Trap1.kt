package silmarengine.contexts.gameplay.entities.terrains.traps

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.traps.extensions.attemptToSpring
import silmarengine.contexts.gameplay.entities.terrains.traps.extensions.indicateDetection
import silmarengine.getImage

open class Trap1 : Trap, Terrain1() {
    var mutableIsDetected = false
        private set
    override val isDetected: Boolean get() = mutableIsDetected

    override var chest: Chest? = null
    override fun setOnChest(chest: Chest) = run {this.chest = chest}

    override val isVisible: Boolean
        get() = isDetected

    /**
     * Meant to be overridden by Trap-extending classes to provide class-specific behavior
     * when this trap is sprung by the given player.
     */
    override fun spring(player: Player) {}

    /**
     * Informs this trap that it has been detected.
     */
    override fun onDetected() {
        mutableIsDetected = true
        image = getImage("detectedTrap")
        indicateDetection()
    }

    override fun onBeingArrival(being: Being): Boolean {
        // if the given being is not a player, or this trap is set on a chest
        if (being !is Player || chest != null) return false

        attemptToSpring(being)

        return true
    }

    override fun onInteractionWithPlayer(player: Player): Boolean {
        attemptToSpring(player)
        return true
    }
}