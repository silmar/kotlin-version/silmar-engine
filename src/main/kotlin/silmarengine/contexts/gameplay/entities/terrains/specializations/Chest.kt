package silmarengine.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.items.extensions.condition.avoidOrDegrade
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.entities.terrains.traps.extensions.attemptToSpring
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

class Chest : Terrain1() {
    /**
     * The item (if any) contained within this chest.
     */
    var item: Item? = null

    /**
     * The trap (if any) set on this chest, which is to go off when this chest is opened.
     */
    var trap: Trap? = null
        internal set

    /**
     * Opens this chest for the given player.
     */
    fun open(player: Player) {
        // issue a chest-opening sound
        area.onSoundIssued(Sounds.chestOpens, location)

        // remember this value, as it will get changed below either by a trap springing, or
        // by the call to area.doRemoveEntity()
        val area = area

        // if this chest is trapped
        if (trap != null) {
            // put this trap into this chest's area at the chest's location, so that
            // it can properly spring
            trap!!.placeInArea(area, location)

            // try to spring the trap on the player
            trap!!.attemptToSpring(player)
            trap = null
        }

        // remove this chest from the area
        area.removeEntity(this)

        // if this chest contains an item
        if (item != null) {
            // put the item where this was in this area
            area.addEntity(createItemEntity(item!!), location)
            item = null
        }
    }

    override fun onDestroyed(avoidanceFailedBy: Int) {
        // if this chest contains an item
        if (item != null) {
            // put the item where this chest was in this area
            area.addEntity(createItemEntity(item!!), location)

            item!!.avoidOrDegrade(avoidanceFailedBy)

            item = null
        }
    }

    companion object {
        const val serialVersionUID = -1295738540866872986
    }
}