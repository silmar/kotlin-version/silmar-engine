package silmarengine.contexts.gameplay.entities.terrains.types

import java.util.ArrayList
import java.util.HashMap

/**
 * A list of all terrain types.
 */
val terrainTypes: MutableList<TerrainType> = ArrayList()

/**
 * A mapping of all terrain-types by their names.
 */
val nameToTerrainTypeMap = HashMap<String, TerrainType>()

/**
 * A list of all trap types.
 */
val trapTypes: MutableList<TerrainType> = ArrayList()

/**
 * A list of all non-trap terrain types.
 */
val nonTrapTypes: MutableList<TerrainType> = ArrayList()

@Suppress("unused")
object TerrainTypes {
    /**
     * The flyweight terrain-types for special terrains.
     */
    val monsterActivator = createTerrainType("monster activator", null, true, 0, false, false)
    val eventMarker = createTerrainType("event marker", null, true, 0, false, false)
    val tempLightSource =
        createTerrainType("", null, true, 0, false, false)
    val summoner =
        createTerrainType("summoner", null, true, 0, false, false)
    val chest = createTerrainType("chest", "chest", true, 1, false, false, 6)

    /**
     * The flyweight terrain-types for decorations.
     */
    val rocks = createTerrainType("rocks", "rocks", false, 0, false, false)
    val bush = createTerrainType("bush", "bush", true, 0, false, false, 14)
    val table = createTerrainType("table", "table", false, 0, false, false, 14)
    val altar = createTerrainType("altar", "altar", false, 0, false, false, 8)
    val crate = createTerrainType("crate", "crate", false, 0, false, false, 10)
    val tallTree = createTerrainType("tall tree", "tallTree", false, 0, false, false, 6)
    val throne = createTerrainType("throne", "throne", true, 0, false, false)
    val pillar = createTerrainType("pillar", "pillar", false, 0, false, false, -10)
    val kiln = createTerrainType("kiln", "kiln", false, 0, false, false, 5)
    val bones = createTerrainType("bones", "bones", true, 0, false, false, 4)

    /**
     * The flyweight terrain-types for traps.
     */
    val damageTrap = createTerrainType("damage trap", null, true, 6, false, true)
    val gustOfWindTrap = createTerrainType("gust of wind trap", null, true, 3, false, true)
}