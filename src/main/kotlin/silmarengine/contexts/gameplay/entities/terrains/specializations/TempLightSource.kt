package silmarengine.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance

/**
 * An invisible, insubstantial terrain that emits light.
 */
class TempLightSource : Terrain1(), LightSource {
    /**
     * How far this lightsource shines.
     */
    override lateinit var lightRadius: PixelDistance
    override val isEmittingLight: Boolean
        get() = true
}