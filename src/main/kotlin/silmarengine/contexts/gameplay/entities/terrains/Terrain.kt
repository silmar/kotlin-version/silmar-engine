package silmarengine.contexts.gameplay.entities.terrains

import silmarengine.contexts.beingmovement.domain.model.BMTerrain
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType

interface Terrain : Entity, BMTerrain {
    /**
     * Stores attributes that are constant across all terrain of this type.
     */
    val type: TerrainType
    fun isOfType(type: TerrainType): Boolean = type == this.type

    /**
     * Informs this terrain it has been created with the given terrain-type.
     */
    fun onCreated(type: TerrainType)

    /**
     * Informs this terrain that the given being has arrived at this terrain.
     *
     * @return Whether this arrival should cause the given being's movement to stop.
     */
    fun onBeingArrival(being: Being): Boolean

    /**
     * Informs this terrain that the given being is residing over this terrain
     * during the current game turn.
     */
    fun onBeingHere(being: Being)

    /**
     * Returns whether this terrain is passible for the given movement-type.
     */
    fun isPassible(movementType: MovementType): Boolean

    /**
     * Informs this terrain that the given player is interacting with it.
     *
     * @return Whether the interaction takes the rest of the player's turn.
     */
    fun onInteractionWithPlayer(player: Player): Boolean

    /**
     * Informs this terrain that it has been destroyed by failing an avoidance check by the
     * given amount.  This is meant to be overridden to allow terrain-specific behavior to
     * execute at this time.
     */
    fun onDestroyed(avoidanceFailedBy: Int)

    /**
     * Returns whether this terrain is considered harmful to the given monster.
     */
    fun isHarmfulTo(monster: Monster): Boolean
}