package silmarengine.contexts.gameplay.game.extensions

import silmarengine.contexts.gameplay.game.Game
import java.io.FileOutputStream
import java.io.IOException
import java.io.ObjectOutputStream

/**
 * Saves this game to disk.
 *
 * @return      Whether the save was successful.
 */
fun Game.save(fileName: String = name): Boolean {
    try {
        val fileStream = FileOutputStream("$path/$fileName")
        val objectStream = ObjectOutputStream(fileStream)
        objectStream.writeObject(this)
        objectStream.flush()
        objectStream.close()
        fileStream.close()
    } catch (e: IOException) {
        e.printStackTrace()
        return false
    }

    return true
}

