package silmarengine.contexts.gameplay.game.extensions

import silmarengine.contexts.gameplay.game.Game

/**
 * Informs this game that it is now the start of its next game turn.
 */
fun Game.startTurn() {
    // note that for the different entity-groups below, we iterate over a copy of the
    // area's terrains lists, since a terrain may remove itself from the area, which
    // would result in a concurrent modification exception)

    // have all terrains in the player's area act
    val area = player.area
    area.terrains.map { it }.forEach { it.act() }

    // have all monsters in the player's area act
    area.monsters.map { it }.forEach {
        it.act()
        it.onTurnOver()
    }

    // have all talker-beings in the player's area act
    area.talkerBeings.map { it }.forEach {
        it.act()
        it.onTurnOver()
    }

    // have all items in the player's area act
    area.items.forEach {
        it.act()
        it.onTurnOver()
    }

    // if the player isn't dead
    if (!player.isDead) {
        // have it act
        player.act()
        player.onTurnOver()
    }
}

