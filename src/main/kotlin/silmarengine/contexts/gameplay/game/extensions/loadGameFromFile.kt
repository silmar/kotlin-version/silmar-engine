package silmarengine.contexts.gameplay.game.extensions

import silmarengine.contexts.gameplay.game.Game
import java.io.FileInputStream
import java.io.ObjectInputStream

/**
 * Loads the game object to be found in the file of the given name.
 */
fun loadGameFromFile(path: String, fileName: String): Game? {
    try {
        // load the game from the file of the given name
        val fileStream = FileInputStream("$path/$fileName")
        val objectStream = ObjectInputStream(fileStream)
        val game = objectStream.readObject() as Game
        objectStream.close()
        fileStream.close()

        // make the name and path of the game the given filename and path;
        // this lets the user rename (or move) a game file and have it
        // stick to using the new name and path when it is saved
        game.useName(fileName)
        game.onPathGiven(path)

        Game.currentGame = game

        return game
    } catch (e: Exception) {
        e.printStackTrace()
        return null
    }
}
