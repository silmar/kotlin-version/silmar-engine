package silmarengine.contexts.gameplay.game

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.game.Game.Companion.currentGame
import silmarengine.contexts.gameplay.game.extensions.startTurn
import silmarengine.contexts.gamecreation.domain.model.GCGame
import silmarengine.util.sleepThread
import java.io.Serializable
import java.util.*

@Suppress("LeakingThis")
open class Game1(fromGame: GCGame, override val player: Player) : Game, Serializable {
    override var name = fromGame.name
        protected set

    override fun useName(name: String) = run { this.name = name }

    override var turn: Int = 0
        protected set

    @Transient
    override var path: String = fromGame.path
        protected set

    override fun onPathGiven(path: String) = run { this.path = path }

    /**
     * A flag that signals to this game when it should stop processing game turns.
     */
    private var gameDone: Boolean = false

    /**
     * Which item types have been identified over the course of this game.  Only the
     * names of the types are stored in this map, since item-types are purposefully
     * not serializable.
     */
    private val itemTypesIdentified = HashMap<String, String>()

    /**
     * Is a flag for telling this game when it's time to start the next game turn.
     */
    private var readyForNextTurn: Boolean = false

    init {
        currentGame = this
    }

    override fun processTurns() {
        // while this game is still going
        while (!gameDone) {
            try {
                readyForNextTurn = false

                turn++
                startTurn()

                // while the current turn has yet to be finished
                while (!readyForNextTurn && !gameDone) {
                    // if the turn isn't over, sleep a little
                    if (!readyForNextTurn) sleepThread(5L)
                }
            } catch (e: Exception) {
                // just print a stack trace; we want the game loop to continue no
                // matter what, so the user doesn't lose his progress
                e.printStackTrace()
            }
        }
    }

    override fun isItemTypeIdentified(type: ItemType): Boolean {
        return type.unidentifiedName == null || itemTypesIdentified[type.name] != null
    }

    override fun storeThatItemTypeIsIdentified(type: ItemType) {
        // store that the given type has been identified
        itemTypesIdentified[type.name] = type.name
    }

    override fun stopProcessingTurns() {
        // this will signal this game's turn processing loop to stop if this is called
        // from the input (vs. the game) thread
        gameDone = true
    }

    override fun onPlayerHasFinishedTurn() {
        // end the current game turn
        readyForNextTurn = true
    }
}
