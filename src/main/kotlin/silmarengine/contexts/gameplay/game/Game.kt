package silmarengine.contexts.gameplay.game

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.types.ItemType

/**
 * Represents the current state of a playing through of the game, across runs of the
 * game program.
 */
interface Game {
    /**
     * The name given to this game by the user, to distinguish it from other such games.
     */
    val name: String
    fun useName(name: String)

    /**
     * The lone player in this game.
     */
    val player: Player

    /**
     * The current game turn index.  (Or, how many turns have elapsed during this game.)
     */
    val turn: Int

    /**
     * The path to this game's file (minus its name) in the filesystem.
     */
    val path: String
    fun onPathGiven(path: String)

    /**
     * Performs the main game loop of iterating through this game's turns.
     */
    fun processTurns()

    fun onPlayerHasFinishedTurn()

    /**
     * Informs this game that the user is stopping his current session with it.
     */
    fun stopProcessingTurns()

    /**
     * Returns whether the given item-type has been identified during this game.
     */
    fun isItemTypeIdentified(type: ItemType): Boolean

    /**
     * Has this game remember that the given item-type has been identified.
     * Item-types are not serialized and therefore cannot themselves
     * be marked as identified.
     */
    fun storeThatItemTypeIsIdentified(type: ItemType)

    companion object {
        /**
         * The current game being played through during this running of the program.
         */
        var currentGame: Game? = null
    }
}