package silmarengine.contexts.playerattributemodification.domain.model

typealias PAMAttribute = Attribute

enum class Attribute(val displayName: String) {
    STRENGTH("Strength"),
    INTELLIGENCE("Intelligence"),
    JUDGEMENT("Judgement"),
    AGILITY("Agility"),
    ENDURANCE("Endurance")
}

typealias PAMAttributeValues = AttributeValues

class AttributeValues(startingValues: List<Int>) {
    private val values: IntArray = startingValues.toIntArray()
    val immutableValues: List<Int> get() = values.toList()

    fun get(attribute: Attribute): Int = get(attribute.ordinal)
    fun get(index: Int): Int = values[index]

    fun add(attribute: Attribute, amount: Int) = add(attribute.ordinal, amount)
    fun add(index: Int, amount: Int) {
        values[index] += amount
    }
}