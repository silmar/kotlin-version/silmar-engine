package silmarengine.contexts.playerattributemodification.ui

import silmarengine.gui.Fonts
import silmarengine.gui.dialogs.MessageDialog
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playerattributemodification.domain.model.Attribute
import silmarengine.contexts.playerattributemodification.domain.model.AttributeValues
import silmarengine.util.getParentFrame
import java.awt.Color
import java.awt.Dimension
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * A panel that displays a player's attribute values, and allows modification to them.
 */
class AttributesDisplay(
    private val attributeValues: AttributeValues,
    private val appendAttributeInfo: (attribute: Attribute, value: Int, to: StringBuffer) -> Unit
) : JPanel() {
    /**
     * The panels which make up the rows of this display, with one row per attribute.
     */
    private val attributePanels = Array(Attribute.values().size) { i ->
        AttributePanel(Attribute.values()[i])
    }

    /**
     * A label for displaying the extraPoints value defined below.
     */
    private val extraPointsLabel: JLabel

    /**
     * How many points are left to add to the attributes, if the
     * display is in a modify mode.
     */
    private var extraPoints = 0

    init {
        background = Color.black
        border = EmptyBorder(5, 5, 5, 5)
        layout = BoxLayout(this, BoxLayout.Y_AXIS)

        // for each player attribute
        Attribute.values().forEachIndexed { i, _ ->
            // add a panel for this attribute
            val panel = attributePanels[i]
            add(panel)
            add(Box.createRigidArea(Dimension(0, 1)))
        }

        // add the extra points label
        extraPointsLabel = Label("", Fonts.smallText)
        val label = extraPointsLabel
        add(label)
        updateExtraPointsLabel()

        // get the attribute values loaded into their display panels
        attributePanels.forEach { it.onValueChanged() }
    }

    /**
     * A panel that displays the value of one attribute, as well as plus and minus buttons
     * (if requested).
     */
    private inner class AttributePanel(private val attribute: Attribute) : JPanel() {
        private var nameLabel: Label
        private var valueLabel: Label
        private var attributeInfo: String = assembleAttributeInfo()

        init {
            background = Color.black
            border = EmptyBorder(0, 1, 0, 1)
            layout = BoxLayout(this, BoxLayout.X_AXIS)

            // add the name label
            var label = Label(attribute.displayName, Fonts.mediumText)
            nameLabel = label
            label.horizontalAlignment = JLabel.LEFT
            add(label)

            add(Box.createHorizontalGlue())
            add(Box.createRigidArea(Dimension(20, 0)))

            // add the value label
            label = Label("--", Fonts.mediumNumber)
            valueLabel = label
            label.horizontalAlignment = JLabel.RIGHT
            add(label)

            add(Box.createRigidArea(Dimension(5, 0)))

            // add the decrement button
            var button: JButton = Button(" - ")
            add(button)
            button.addActionListener(ActionListener {
                if (attributeValues.get(attribute) <= 1) return@ActionListener

                // remove one from this panel's attribute and add it to
                // the extra-points reserve; we subtract from the
                // base-attribute, assuming that the two are equal
                // when undergoing modification
                attributeValues.add(attribute, -1)
                extraPoints++

                onValueChanged()
            })

            // add the increment button
            button = Button(" + ")
            add(button)
            button.addActionListener(ActionListener {
                // don't allow a starting score above a certain limit
                val max = 17
                fun frame() = this@AttributesDisplay.getParentFrame()
                if (attributeValues.get(attribute) >= max) {
                    MessageDialog.show(frame(),
                        "Starting scores above $max are not allowed.")
                    return@ActionListener
                }

                // if there were no extra points to spend
                if (extraPoints <= 0) {
                    MessageDialog.show(frame(),
                        "You must first subtract from another attribute.")
                    return@ActionListener
                }

                // add one to this panel's attribute from the extra-points reserve;
                // we add to the base-attribute, assuming that the two are equal
                // when undergoing modification
                attributeValues.add(attribute, 1)
                extraPoints--

                onValueChanged()
            })
        }

        private fun assembleAttributeInfo(): String {
            // add the name
            val text = StringBuffer()
            text.append("<i>")
            text.append(attribute.displayName)

            // add the value
            text.append(":</i> ")
            text.append("<b>")
            val value = attributeValues.get(attribute)
            text.append(value)

            // add the info
            text.append("</b>")
            appendAttributeInfo(this.attribute, value, text)

            return text.toString()
        }

        /**
         * Informs this panel that its associated attribute value has changed.
         */
        fun onValueChanged() {
            valueLabel.text = attributeValues.get(attribute).toString()
            updateExtraPointsLabel()

            attributeInfo = assembleAttributeInfo()
            nameLabel.hasTooltipImpl.setTooltipText(attributeInfo, 0)
            valueLabel.hasTooltipImpl.setTooltipText(attributeInfo, 0)
        }
    }

    private fun updateExtraPointsLabel() {
        extraPointsLabel.text = "extra points: $extraPoints"
    }
}