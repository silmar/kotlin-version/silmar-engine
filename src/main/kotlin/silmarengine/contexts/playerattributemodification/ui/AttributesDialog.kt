package silmarengine.contexts.playerattributemodification.ui

import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import silmarengine.contexts.playerattributemodification.domain.model.Attribute
import silmarengine.contexts.playerattributemodification.domain.model.AttributeValues
import silmarengine.programName
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import javax.swing.*
import javax.swing.border.CompoundBorder
import javax.swing.border.EmptyBorder
import javax.swing.border.EtchedBorder

/**
 * A dialog that displays a player's attribute values in a manner in which they may be
 * modified by the user.  An "OK" button accepts the changes made.
 */
class AttributesDialog(
    attributeValues: AttributeValues, parent: JFrame,
    appendAttributeInfo: (attribute: Attribute, value: Int, to: StringBuffer) -> Unit
) : JDialog(parent, programName) {

    init {
        isModal = true
        isResizable = false
        setSize(200, 240)
        setLocationRelativeTo(owner)

        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color.black

        // don't allow this dialog's close button to work
        defaultCloseOperation = WindowConstants.DO_NOTHING_ON_CLOSE

        // add the main panel
        val panel = JPanel()
        panel.border = EmptyBorder(5, 5, 5, 5)
        panel.isOpaque = false
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add the prompt label
        val label = Label("Modify your attributes", Fonts.mediumText)
        panel.add(label)

        panel.add(Box.createRigidArea(Dimension(0, 5)))

        // add the attributes display
        val display = AttributesDisplay(attributeValues, appendAttributeInfo)
        display.border =
            CompoundBorder(EtchedBorder(Color.white, Color.lightGray), display.border)
        panel.add(display)

        fun addSpacer(height: Int) = panel.add(Box.createRigidArea(Dimension(0, height)))
        addSpacer(5)

        val label2 = Label("mouseover for info", Fonts.smallText)
        panel.add(label2)

        addSpacer(15)

        // add the ok button
        val button = Button(" Ok ")
        panel.add(button)
        button.addActionListener { isVisible = false }

        addSpacer(10)

        arrayOf("A high strength", "is recommended for",
            "beginning players.").forEach {
            panel.add(Label(it, Fonts.smallText))
        }

        addSpacer(5)

        pack()
    }
}

