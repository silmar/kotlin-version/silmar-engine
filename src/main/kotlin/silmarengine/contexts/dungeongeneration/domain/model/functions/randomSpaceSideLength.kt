package silmarengine.contexts.dungeongeneration.domain.model.functions

import silmarengine.util.math.randomInt

fun randomSpaceSideLength(): Int =
    when (randomInt(1, 42)) {
        in 1..5 -> 3
        in 6..10 -> 4
        in 11..15 -> 5
        in 16..20 -> 6
        in 21..25 -> 7
        in 26..29 -> 8
        in 30..32 -> 9
        in 33..35 -> 10
        36, 37 -> 11
        38, 39 -> 12
        40 -> 13
        41 -> 14
        else -> 15
    }
