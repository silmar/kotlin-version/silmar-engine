package silmarengine.contexts.dungeongeneration.domain.model

data class TileRectangle(val x: Int, val y: Int, val width: Int, val height: Int) {
    constructor(other: TileRectangle) : this(other.x, other.y, other.width, other.height)
}