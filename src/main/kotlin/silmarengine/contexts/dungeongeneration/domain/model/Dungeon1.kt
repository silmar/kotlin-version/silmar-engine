package silmarengine.contexts.dungeongeneration.domain.model

import silmarengine.contexts.dungeongeneration.domain.model.functions.chooseDoorwayLocation
import silmarengine.contexts.dungeongeneration.domain.model.functions.generateRectangleForNewSpace
import silmarengine.util.list.removeRandomElement
import silmarengine.util.math.randomFloat
import silmarengine.util.math.randomInt

class Dungeon1(override val size: DungeonSize) : Dungeon {
    private var tiles: Array<Array<Tile>> =
        Array(size.width) { Array(size.height) { Tiles.wall } }

    override var listeners = HashSet<DungeonListener>()

    override fun getTile(location: TilePoint): Tile = tiles[location.x][location.y]

    override fun setTile(location: TilePoint, tile: Tile) {
        tiles[location.x][location.y] = tile
    }

    override fun fillRectangle(r: TileRectangle, tile: Tile) {
        // for each tile location within the given rectangle
        for (i in r.x until r.x + r.width) {
            for (j in r.y until r.y + r.height) {
                // place the given tile at this location
                val location = TilePoint(i, j)
                setTile(location, tile)
            }
        }
    }

    override fun changeWallsToRoomWalls(room: TileRectangle) {
        // this is used below
        fun checkToConvert(location: TilePoint) {
            if (getTile(location) == Tiles.wall) setTile(location, Tiles.roomWall)
        }

        // check the tiles along the north and south side (and corners) of the room
        for (i in room.x - 1..room.x + room.width) {
            checkToConvert(TilePoint(i, room.y - 1))
            checkToConvert(TilePoint(i, room.y + room.height))
        }

        // check the tiles along the west and east sides of the room
        for (i in room.y until room.y + room.height) {
            checkToConvert(TilePoint(room.x - 1, i))
            checkToConvert(TilePoint(room.x + room.width, i))
        }
    }

    override fun randomRectangleLocation(
        forRectWidth: Int, forRectHeight: Int
    ): TilePoint = TilePoint(randomInt(1, size.width - forRectWidth - 2),
        randomInt(1, size.height - forRectHeight - 2))

    /**
     * Finds (and returns) which directions from the given rectangle might allow additions.
     */
    override fun findLikelyDirectionsFromRectangle(
        rectangle: TileRectangle
    ): List<Direction> {
        // if north is a likely direction
        val result = ArrayList<Direction>()
        var location = TilePoint(rectangle.x + rectangle.width / 2, rectangle.y - 2)
        if (rectangle.y >= 5 && !getTile(TilePoint(location)).isPassible) {
            // add it to our list of directions
            result.add(Directions.north)
        }

        // if south is a likely direction
        location = TilePoint(rectangle.x + rectangle.width / 2,
            rectangle.y + rectangle.height + 1)
        if (rectangle.y + rectangle.height < size.height - 5 && !getTile(
                location).isPassible) {
            // add it to our list of directions
            result.add(Directions.south)
        }

        // if west is a likely direction
        location = TilePoint(rectangle.x - 2, rectangle.y + rectangle.height / 2)
        if (rectangle.x >= 5 && !getTile(location).isPassible) {
            // add it to our list of directions
            result.add(Directions.west)
        }

        // if east is a likely direction
        location = TilePoint(rectangle.x + rectangle.width + 1,
            rectangle.y + rectangle.height / 2)
        if (rectangle.x + rectangle.width < size.width - 5 && !getTile(
                location).isPassible) {
            // add it to our list of directions
            result.add(Directions.east)
        }

        return result
    }

    override fun containsRectangle(r: TileRectangle): Boolean {
        // note that this enforces a one-tile boundary around this dungeon's edges
        return (r.x > 0 && r.y > 0 && r.x + r.width - 1 < size.width - 1 && r.y + r.height - 1 < size.height - 1)
    }

    /**
     * Returns whether the tiles at all tile-locations within the given
     * rectangle are impassible.
     */
    override fun isRectanglePlusBoundaryAllImpassible(rect: TileRectangle): Boolean {
        // for each tile location within the given rectangle, as well as
        // on the 1-tile boundary around the rectangle
        for (i in rect.x - 1..rect.x + rect.width) {
            for (j in rect.y - 1..rect.y + rect.height) {
                // if the tile at this location is passible
                val location = TilePoint(i, j)
                val tile = getTile(location)
                if (tile.isPassible) return false
            }
        }

        return true
    }

    override fun placeDoor(
        doorway: TilePoint, additionIsHallway: Boolean, terminusIsHallway: Boolean,
        down: Boolean
    ) {
        // if both the addition and the terminus are hallways
        val tile: Tile
        val chance = randomFloat
        if (additionIsHallway && terminusIsHallway) {
            // determine the door type appropriately
            tile = when {
                chance < .90 -> Tiles.floor
                chance < .95 -> Tiles.secretDoor
                else -> Tiles.door
            }
        }
        else {
            // determine the door type appropriately
            tile = when {
                chance < .75 -> Tiles.door
                chance < .85 -> Tiles.roomSecretDoor
                else -> Tiles.floor
            }
        }

        // place the door of the chosen type, as well as the second tile of the door
        setTile(doorway, tile)
        if (down) setTile(TilePoint(doorway.x, doorway.y + 1), tile)
        else setTile(TilePoint(doorway.x + 1, doorway.y), tile)
    }

    override fun print() {
        fun Tile.toChar(): Char = when (this) {
            Tiles.floor -> '.'
            Tiles.wall -> '#'
            Tiles.roomFloor -> '-'
            Tiles.roomWall -> '@'
            Tiles.door -> '%'
            Tiles.secretDoor -> '*'
            Tiles.roomSecretDoor -> '$'
            Tiles.entrance -> '^'
            Tiles.exit -> 'v'
            else -> ' '
        }
        tiles.forEach { it ->
            it.forEach { print(it.toChar()) }
            println()
        }
    }

    override fun addTileToRectangle(tile: Tile, rectangle: TileRectangle) {
        val x = rectangle.x + randomInt(0, rectangle.width - 1)
        val y = rectangle.y + randomInt(0, rectangle.height - 1)
        setTile(TilePoint(x, y), tile)
    }

    override fun createStartingTerminus(): Terminus {
        // create a terminus to start out this path
        val isHallway = randomFloat < 0.5
        val rectangle = generateRectangleForNewSpace(this, null, null, isHallway, null)
        val terminus = Terminus(rectangle)

        // hollow out the terminus
        val fillTile = if (isHallway) Tiles.floor else Tiles.roomFloor
        fillRectangle(rectangle, fillTile)

        if (!isHallway) changeWallsToRoomWalls(rectangle)

        // add an entrance tile randomly within the terminus
        addTileToRectangle(Tiles.entrance, rectangle)

        // report the creation of the new space within the dungeon
        reportToListeners { it.onSpaceCreated(this, rectangle, null, null, false) }

        return terminus
    }

    override fun createAddition(
        atTerminus: Terminus, directionChoices: MutableList<Direction>,
        isForHallway: Boolean
    ): Terminus? {
        // keep doing this
        var direction: Direction? = null
        var rectangle: TileRectangle? = null
        var doorway: TilePoint? = null
        while (directionChoices.isNotEmpty()) {
            // randomly choose one of the directions to try
            val trialDirection = directionChoices.removeRandomElement()

            // determine where the doorway would go between this path's terminus and
            // the next addition
            doorway =
                chooseDoorwayLocation(atTerminus.rectangle, trialDirection, isForHallway)

            // determine the boundaries of the next addition
            rectangle =
                generateRectangleForNewSpace(this, atTerminus.rectangle, trialDirection,
                    isForHallway, doorway)

            // if the addition's rectangle isn't completely within the dungeon
            if (!containsRectangle(rectangle)) {
                // this direction has failed
                continue
            }

            // if the addition's rectangle isn't completely impassible
            if (!isRectanglePlusBoundaryAllImpassible(rectangle)) {
                // this direction has failed
                continue
            }

            // we have found a successful addition direction and rectangle
            direction = trialDirection
            break
        }

        // if we could not find a direction or rectangle for an addition, above,
        // an addition cannot be created
        if (direction == null || rectangle == null || doorway == null) return null

        // hollow out the addition
        fillRectangle(rectangle, if (isForHallway) Tiles.floor else Tiles.roomFloor)

        // if the addition is a room
        if (!isForHallway) changeWallsToRoomWalls(rectangle)

        // place the door
        val terminusIsHallway =
            atTerminus.rectangle.width == 2 || atTerminus.rectangle.height == 2
        val isDoorwayVertical = direction.x != 0 && direction.y == 0
        placeDoor(doorway, isForHallway, terminusIsHallway, isDoorwayVertical)

        // report the creation of the new space within the dungeon
        reportToListeners {
            it.onSpaceCreated(this, rectangle, atTerminus.spaceRectangle, doorway,
                isDoorwayVertical)
        }

        // if the addition is to be a hallway
        return if (isForHallway) {
            // the new terminus is the 2x2 square at the away side of the addition
            var (x, y) = rectangle
            if (direction.x == 1 && direction.y == 0) x += rectangle.width - 2
            else if (direction.x == 0 && direction.y == 1) y += rectangle.height - 2
            Terminus(TileRectangle(x, y, 2, 2), rectangle)
        }

        // otherwise, the new terminus is the addition itself
        else Terminus(rectangle)
    }
}