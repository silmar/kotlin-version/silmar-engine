package silmarengine.contexts.dungeongeneration.domain.model

class Direction(val x: Int, val y: Int)

object Directions {
    val north = Direction(0, -1)
    val west = Direction(-1, 0)
    val east = Direction(1, 0)
    val south = Direction(0, 1)
}