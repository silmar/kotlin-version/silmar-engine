package silmarengine.contexts.dungeongeneration.domain.model

typealias DGTilePoint = TilePoint

data class TilePoint(val x: Int, val y: Int) {
    constructor(other: TilePoint) : this(other.x, other.y)
}