package silmarengine.contexts.dungeongeneration.domain.model.functions

import silmarengine.contexts.dungeongeneration.domain.model.Direction
import silmarengine.contexts.dungeongeneration.domain.model.TilePoint
import silmarengine.contexts.dungeongeneration.domain.model.TileRectangle
import silmarengine.util.math.randomInt

/**
 * Randomly chooses (and returns) the location of a doorway on the side of the given
 * rectangle determined by the given direction.
 *
 * @param isHallway     Whether or not the rectangle represents part of a hallway.
 */
fun chooseDoorwayLocation(
    rectangle: TileRectangle, direction: Direction, isHallway: Boolean
): TilePoint {
    // start with the northwest corner of the rectangle
    var doorwayX = rectangle.x - 1
    var doorwayY = rectangle.y - 1

    // if we are dealing with a hallway
    var offsetX = 0
    var offsetY = 0
    if (isHallway) {
        // there is no random offset
        if (direction.x != 0 && direction.y == 0) offsetY = 1
        else offsetX = 1
    }
    else {
        // randomly determine how far south or east to put the doorway
        if (direction.x != 0 && direction.y == 0) {
            offsetY = randomInt(1, rectangle.height - 1)
        }
        else {
            offsetX = randomInt(1, rectangle.width - 1)
        }
    }

    // translate from the northwest corner to get the correct
    // doorway location
    if (direction.x == -1 && direction.y == 0) {
        doorwayY += offsetY
    }
    else if (direction.x == 1 && direction.y == 0) {
        doorwayX += rectangle.width + 1
        doorwayY += offsetY
    }
    else if (direction.x == 0 && direction.y == -1) {
        doorwayX += offsetX
    }
    else if (direction.x == 0 && direction.y == 1) {
        doorwayX += offsetX
        doorwayY += rectangle.height + 1
    }

    return TilePoint(doorwayX, doorwayY)
}

