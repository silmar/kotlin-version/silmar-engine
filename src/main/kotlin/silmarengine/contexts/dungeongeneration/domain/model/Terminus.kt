package silmarengine.contexts.dungeongeneration.domain.model

/**
 * A rectangle into which a path is currently expanding.
 */
class Terminus {
    /**
     * The expansion rectangle this terminus represents.
     */
    val rectangle: TileRectangle

    /**
     * For hallways, the terminus rectangle is only one end of the hallway rectangle.
     * This remembers the space's rectangle the terminus may be a part of.
     */
    val spaceRectangle: TileRectangle

    /**
     * Whether new paths have already been generated out from this
     * terminus.
     */
    var checkedForNewPaths = false

    constructor(
        rectangle: TileRectangle,
        spaceRectangle: TileRectangle? = null
    ) {
        this.rectangle = rectangle
        this.spaceRectangle = spaceRectangle ?: TileRectangle(this.rectangle)
    }

    /**
     * A copy constructor, to use when starting a new branching path.
     */
    constructor(other: Terminus) {
        rectangle = TileRectangle(other.rectangle)
        spaceRectangle = TileRectangle(other.spaceRectangle)
    }
}

