package silmarengine.contexts.dungeongeneration.domain.model

import silmarengine.util.math.randomFloat

/**
 * A path that burrows through the dungeon in random directions.
 */
class Path(val dungeon: Dungeon) {
    /**
     * The current rectangle at which this path is expanding.
     */
    var terminus: Terminus? = null
        private set

    fun expandUntilTermination(onNewPathStarted: (path: Path) -> Unit) {
        // while this path hasn't yet terminated
        var terminated = false
        while (!terminated) {
            // if this path has no terminus, create one
            if (terminus == null) terminus = dungeon.createStartingTerminus()

            // determine which ways we can go from this path's terminus
            val directions =
                dungeon.findLikelyDirectionsFromRectangle(terminus!!.rectangle)
                    .toMutableList()

            // determine if the next addition is going to be a hallway
            val additionIsHallway = randomFloat < 0.75

            // try to create the addition
            val additionTerminus =
                dungeon.createAddition(terminus!!, directions, additionIsHallway)

            // if an addition was not created
            if (additionTerminus == null) {
                // this path is terminated
                terminated = true
                continue
            }

            // if new paths haven't already been created at this path's pre-addition
            // terminus
            if (!terminus!!.checkedForNewPaths) {
                // for each likely-suitable direction leftover from above
                directions.forEach { _ ->
                    // start a new path at the pre-addition terminus
                    val newPath = Path(dungeon)
                    newPath.terminus = Terminus(terminus!!)
                    newPath.terminus!!.checkedForNewPaths = true
                    onNewPathStarted(newPath)
                }
            }

            // advance this path's terminus to that of the addition
            terminus = additionTerminus
        }
    }
}
