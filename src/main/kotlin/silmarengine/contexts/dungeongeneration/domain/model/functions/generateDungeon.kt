package silmarengine.contexts.dungeongeneration.domain.model.functions

import silmarengine.contexts.dungeongeneration.domain.model.*
import silmarengine.util.math.randomInt

fun generateDungeon(listener: DungeonListener? = null): Dungeon {
    val minLength = 45
    val maxLength = 65
    fun randomLength() = randomInt(minLength, maxLength)

    // create the dungeon object with a randomly chosen size
    val size = DungeonSize(randomLength(), randomLength())
    val dungeon = Dungeon1(size)
    if (listener != null) dungeon.addListener(listener)

    // start with one path that has no terminus
    val paths = ArrayList<Path>()
    val mainPath = Path(dungeon)
    paths.add(mainPath)

    // while there are still unterminated paths
    while (paths.isNotEmpty()) {
        // remove a path from the list and expand it until it terminates
        val path = paths.removeAt(0)
        path.expandUntilTermination { paths.add(it) }

        // if the path was the main path
        if (path === mainPath) {
            // add an exit tile randomly within the path's ending terminus
            dungeon.addTileToRectangle(Tiles.exit, path.terminus!!.rectangle)
        }
    }

    return dungeon
}
