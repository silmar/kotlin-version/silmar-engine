package silmarengine.contexts.dungeongeneration.domain.model.functions

import silmarengine.contexts.dungeongeneration.domain.model.Direction
import silmarengine.contexts.dungeongeneration.domain.model.Dungeon
import silmarengine.contexts.dungeongeneration.domain.model.TilePoint
import silmarengine.contexts.dungeongeneration.domain.model.TileRectangle
import silmarengine.util.math.randomBoolean
import silmarengine.util.math.randomInt

/**
 * Randomly generates (and returns) a rectangle representing a potential new
 * addition to a dungeon being generated.
 *
 * @param offOf     The rectangle off of which the addition will be added.
 *                  If none is provided, the location is determined randomly.
 * @param direction The direction from the offOf rectangle to add the addition.
 * @param isHallway    Whether to generate a hallway (vs. a room).
 * @param doorway      The connecting (for movement purposes) location between
 * the offOf rectangle and the addition.
 */
fun generateRectangleForNewSpace(
    dungeon: Dungeon, offOf: TileRectangle?, direction: Direction?, isHallway: Boolean,
    doorway: TilePoint?
): TileRectangle {
    // generate the rectangle's size
    var x = 0
    var y = 0
    var width = randomSpaceSideLength()
    var height = randomSpaceSideLength()

    // if the addition is going to be a hallway
    if (isHallway) {
        // either the width or the height will be 2
        if (direction == null && randomBoolean || direction != null && direction.x != 0 && direction.y == 0) height =
            2
        else width = 2
    }

    // if this addition isn't to be attached to an existing terminus
    if (offOf == null) {
        // determine the addition's location randomly
        val location = dungeon.randomRectangleLocation(width, height)
        x = location.x
        y = location.y
    }
    else {
        // if the addition is going to be a hallway
        if (isHallway) {
            // there is no offset from the doorway
            if (direction!!.x != 0 && direction.y == 0) {
                y = doorway!!.y
            }
            else {
                x = doorway!!.x
            }
        }
        else {
            // determine the addition's offset from the doorway
            if (direction!!.x != 0 && direction.y == 0) {
                y = doorway!!.y - randomInt(0, height - 2 - 1)
            }
            else {
                x = doorway!!.x - randomInt(0, width - 2 - 1)
            }
        }

        // determine the addition's other ordinate
        if (direction.x == -1 && direction.y == 0) {
            x = offOf.x - 1 - width
        }
        else if (direction.x == 1 && direction.y == 0) {
            x = offOf.x + offOf.width + 1
        }
        else if (direction.x == 0 && direction.y == -1) {
            y = offOf.y - 1 - height
        }
        else if (direction.x == 0 && direction.y == 1) {
            y = offOf.y + offOf.height + 1
        }
    }

    return TileRectangle(x, y, width, height)
}

