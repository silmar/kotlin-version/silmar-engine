package silmarengine.contexts.dungeongeneration.domain.model

typealias DGDungeonListener = DungeonListener

interface DungeonListener {
    /**
     * @param space     The tile boundaries of the space created.
     * @param fromSpace The tile boundaries of the space off of which the new space was created.
     * @param doorway   The location of the first of the two tiles that allow passage
     *                  between the above two spaces.
     * @param isDoorwayVertical     Whether the two doorway tiles between the above two
     *                              spaces run vertically (vs. horizontally).
     */
    fun onSpaceCreated(
        dungeon: Dungeon, space: TileRectangle, fromSpace: TileRectangle?,
        doorway: TilePoint?, isDoorwayVertical: Boolean
    )
}