package silmarengine.contexts.dungeongeneration.domain.model

data class DungeonSize(val width: Int, val height: Int)