package silmarengine.contexts.dungeongeneration.domain.model

typealias DGTile = Tile

class Tile(val id: Int) {
    val isPassible: Boolean
        get() = when (this) {
            Tiles.wall, Tiles.roomWall -> false
            else -> true
        }
    val isDoor: Boolean
        get() = when (this) {
            Tiles.door, Tiles.secretDoor, Tiles.roomSecretDoor -> true
            else -> false
        }
}

typealias DGTiles = Tiles

object Tiles {
    val floor = Tile(1)
    val wall = Tile(2)
    val roomFloor = Tile(3)
    val roomWall = Tile(4)
    val door = Tile(5)
    val secretDoor = Tile(6)
    val roomSecretDoor = Tile(7)
    val entrance = Tile(8)
    val exit = Tile(9)
}