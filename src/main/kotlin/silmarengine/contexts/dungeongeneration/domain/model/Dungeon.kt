package silmarengine.contexts.dungeongeneration.domain.model

import silmarengine.events.ReporterMixin

interface Dungeon : ReporterMixin<DungeonListener> {
    val size: DungeonSize

    fun getTile(location: TilePoint): Tile?
    fun setTile(location: TilePoint, tile: Tile)

    /**
     * Fills the given rectangular bounds within this dungeon with the given tile.
     */
    fun fillRectangle(r: TileRectangle, tile: Tile)

    /**
     * Makes the wall tiles surrounding the given room-rectangle into room-wall tiles.
     */
    fun changeWallsToRoomWalls(room: TileRectangle)

    /**
     * Returns a random location within this dungeon for the upper-left corner
     * of a rectangle of the given dimensions such that the rectangle would fit
     * entirely within this dungeon, not including a one-tile boundary around this
     * dungeon's outside.
     */
    fun randomRectangleLocation(forRectWidth: Int, forRectHeight: Int): TilePoint

    /**
     * Finds (and returns) which directions from the given rectangle might allow additions.
     */
    fun findLikelyDirectionsFromRectangle(rectangle: TileRectangle): List<Direction>

    /**
     * Returns whether this dungeon (minus its outer one-tile boundary)
     * completely contains the given rectangular tile region.
     */
    fun containsRectangle(r: TileRectangle): Boolean

    /**
     * Returns whether the tiles at all tile-locations within the given
     * rectangle are impassible.
     */
    fun isRectanglePlusBoundaryAllImpassible(rect: TileRectangle): Boolean

    /**
     * Places a door, secret door, or open area at the given doorway location,
     * as well as a second same such tile right next to it, to the right or downward.
     *
     * @param additionIsHallway     Whether the addition into which the door leads is a
     * hallway, which helps detm the odds of what the door tile
     * will be.
     * @param terminusIsHallway     Whether the addition into which the door leads is a
     * terminus, which also helps detm the odds of what the door tile
     * will be.
     * @param down                  Whether the set of doors goes down (vs. across).
     */
    fun placeDoor(
        doorway: TilePoint, additionIsHallway: Boolean, terminusIsHallway: Boolean,
        down: Boolean
    )

    /**
     * Prints an text representation of this dungeon to the console,
     * for debugging purposes.
     */
    fun print()

    /**
     * Places the given tile at a random location within the given rectangular area
     * within this dungeon.
     */
    fun addTileToRectangle(tile: Tile, rectangle: TileRectangle)

    /**
     * Creates within this dungeon (and returns) a terminus area that can be used to start
     * a (presumably empty) path.
     */
    fun createStartingTerminus(): Terminus

    /**
     * Attempts to creates a new addition (i.e. hallway or room) within in this dungeon,
     * in one of the given directions from the given path terminus.
     *
     * @return  The terminus within the newly created addition, or null if an addition
     *          could not be created.
     */
    fun createAddition(
        atTerminus: Terminus, directionChoices: MutableList<Direction>,
        isForHallway: Boolean
    ): Terminus?
}
