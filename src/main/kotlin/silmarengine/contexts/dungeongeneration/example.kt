package silmarengine.contexts.dungeongeneration

import silmarengine.contexts.dungeongeneration.domain.model.functions.generateDungeon

fun main() {
    val dungeon = generateDungeon()
    dungeon.print()
}