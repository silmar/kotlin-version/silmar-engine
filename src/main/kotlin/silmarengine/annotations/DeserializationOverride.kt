package silmarengine.annotations

/**
 * Documents that the annotated method overrides Java's readObject deserialization method.
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class DeserializationOverride