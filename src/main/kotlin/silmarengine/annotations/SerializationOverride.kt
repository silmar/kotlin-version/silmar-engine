package silmarengine.annotations

/**
 * Documents that the annotated method overrides Java's writeObject serialization method.
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class SerializationOverride