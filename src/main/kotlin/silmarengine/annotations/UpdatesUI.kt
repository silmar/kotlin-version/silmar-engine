package silmarengine.annotations

/**
 * Indicates a function which updates the user interface, implying it should only be
 * run on the Swing thread.
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class UpdatesUI