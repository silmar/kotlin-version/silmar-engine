package silmarengine.annotations

/**
 * Denotes an engine "hook" which may be set or overridden in order to specify
 * game-specific behavior.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.LOCAL_VARIABLE)
@Retention(AnnotationRetention.SOURCE)
annotation class ExtensionPoint