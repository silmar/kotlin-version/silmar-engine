package silmarengine

import silmarengine.util.image.loadImage
import java.awt.Image
import java.util.*

/**
 * Returns the image of the given file-name.
 */
fun getImage(name: String): Image {
    // if the image isn't already in this class's image-map
    var image: Image? = imageMap[name]
    if (image == null) {
        // load the image into the image-map
        image = loadImage(name)
        imageMap[name] = image
    }

    return image
}

/**
 * A mapping of all images by their names.
 */
private val imageMap = HashMap<String, Image>()
