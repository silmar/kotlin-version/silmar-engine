package silmar

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.usePower
import silmar.contexts.gameplay.entities.beings.talkerBeings.types.SilmarTalkerBeingTypes
import silmar.contexts.gameplay.entities.items.types.SilmarItemTypes
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.WorkThread
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.getExperienceNeededForLevel
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.createTalkerBeing
import silmarengine.contexts.gameplay.entities.items.*
import silmarengine.contexts.gameplay.entities.items.types.ItemTypes
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth

fun performDebugActionOne(player: Player) = performDebugAction(player, 2)
fun performDebugActionTwo(player: Player) {
    performDebugAction(player, 0, 9, 1)
//    performDebugAction(player, 0, 1, 1)
//    performDebugAction(player, 0, 0)
//    performDebugAction(player, 0, 1, 4)
}

fun performDebugAction(player: Player, choice: Int, subChoice: Int = 0, distance: Int = 1) {
    player as SilmarPlayer
    WorkThread.queueTask {
        val area = player.area
        when (choice) {
            1 -> player.takeHealing(player.maxHitPoints)
            2 -> player.levelValues.onExperienceGained(getExperienceNeededForLevel(
                player.levelValues.level + 1) - player.levelValues.experience)
            3 -> player.usePower(
                PlayerPowers.emitLightning,
                MutableMapPixelLocation(player.location).translate(-5 * tileWidth,
                    0))
            4 -> (player.items.inventory.getFirstItemOfType(
                ItemTypes.gold) as Item).quantity -= 2
            5 -> player.items.getEquippedItem(EquipLocation.LEFT_HAND)?.identify()
            6 -> area.moveEntity(player,
                area.terrains.filter { it !is Chest && it !is Trap }.random().location)
            7 -> area.monsters./*filter {
                        it.isOfType(MonsterTypes.bat)
                    }.*/map { it }.forEach { area.removeEntity(it) }
            8 -> area.moveEntity(player, PixelPoint(2 * tileWidth,
                area.size.height * tileHeight - 2 * tileHeight))
            9 -> area.moveEntity(player,
                area.getTerrainOfType(SilmarTerrainTypes.downExit)!!.location)
            10 -> area.moveEntity(player,
                MutablePixelPoint(area.items.random().location).translate(32, 0))
            else -> {
                val location = MutablePixelPoint(player.location)
                location.translate(distance * tileWidth, 0)
                val entity = when (subChoice) {
                    1 -> createMonster(SilmarMonsterTypes.assassin)
                    2 -> createTerrain(SilmarTerrainTypes.moatLever)
                    3 -> createTalkerBeing(SilmarTalkerBeingTypes.medicalRobot)
                    4 -> createTerrain(SilmarTerrainTypes.fountain)
                    5 -> createTerrain(TerrainTypes.damageTrap)
                    6 -> createItemEntity(
                        createItem(SilmarItemTypes.necklaceOfChoking))
                    7 -> createItemEntity(createItem(WeaponTypes.shortSword))
                    8 -> createItemEntity(createItem(WeaponTypes.longSword))
                    9 -> createItemEntity(createItem(SilmarWeaponTypes.laserPistol))
                    10 -> createItemEntity(createItem(SilmarWeaponTypes.clubOfImpact))
                    else -> createItemEntity(createGroupableItem(ItemTypes.gold, 500))
                }
                player.area.addEntity(entity, location)
            }
        }
    }
}
