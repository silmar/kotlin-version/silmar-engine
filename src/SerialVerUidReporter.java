import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectStreamClass;
import java.util.ArrayList;
import java.util.List;

/**
 * Writes to an output file "serial_vers.txt" the serialVerUID's 
 * of all serializable classes in the subtree of folders rooted 
 * at the one in which the application is run.
 * 
 * Note that there are sets of two or more classes that have
 * a circular dependency on each other during static initialization
 * of instances.  For such sets, the dependency cycle must be 
 * temporarily broken by commenting out code representing at 
 * least one link in the cycle.
 */
public class SerialVerUidReporter 
{
    /**
     * Runs this application.  No arguments are used.
     */
    public static void main(String[] args)
    {
        new SerialVerUidReporter();
        
        System.exit(0);
    }
    
    /**
     * Constructor.
     */
    private SerialVerUidReporter()
    {
        FileWriter writer;
        try {
            // create a writer on the output file
            writer = new FileWriter("serial_vers.txt");

            // start recursing from the current folder
            processFolder(new File("."), "", writer);

            writer.close();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Writes to the given writer the serialVerUID's of all serializable 
     * java classes whose class files are located in the given folder.
     * The given package-prefix is prepended to the names of the class files
     * in the given folder to retrieve the java classes from the class-loader.   
     */
    private void processFolder(File folder, String packagePrefix, FileWriter writer) 
    	throws ClassNotFoundException, IOException
    {
        // for each file that is in given folder
        File[] files = folder.listFiles();
        List folders = new ArrayList();
        String classFileExtension = ".class";
        Class serializable = Class.forName("java.io.Serializable");
        int classFileExtensionLength = classFileExtension.length();
        for (int i = 0; i < files.length; i++) {
            // if this file is actually a folder
            if (files[i].isDirectory()) {
                // we want to process all files in the given folder
                // before moving on to its sub-folders, so save
                // this folder for processing, below
                folders.add(files[i]);
                continue;
            }
        
            // if this file is a java class file
            String name = files[i].getName();
            if (name.endsWith(".class")) {
	        	// get the java class whose file this is
                String qualifiedName = packagePrefix 
                	+ name.substring(0, name.length() - classFileExtensionLength);
                Class clazz = Class.forName(qualifiedName);
                
                // if the class is serializable
                if (serializable.isAssignableFrom(clazz)) {
	                // calculate the serial-ver-UID of the java class
	                long uid = ObjectStreamClass.lookup(clazz).getSerialVersionUID();
	                
		        	// write out the class name and the UID as a new file in 
		        	// our output file
	                writer.write(qualifiedName + "\n\t" + uid + "\n");
                }
            }
        }        
        
        // for each folder found above
        for (int i = 0; i < folders.size(); i++) {
            // call this method recursively on this folder
            File childFolder = (File)folders.get(i);
            processFolder(childFolder, packagePrefix + childFolder.getName() + ".", writer);
        }
    }
}
